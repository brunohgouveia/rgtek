CGVERTEX
#version 400

#include <res/shaders/Util/TransformationMatrices.shader>

layout(location = 0) in vec4 Vertex;

void main()
{
    gl_Position = ViewProjectionMatrix *  Vertex;
}

ENDCG
CGFRAGMENT
#version 400

uniform vec4 LineColor;

out vec4 FragColor;

void main()
{
	FragColor = LineColor;
}

ENDCG