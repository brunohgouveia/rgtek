CGVERTEX
#version 400

layout(location = 0) in vec4 Vertex;
layout(location = 1) in vec3 Normal;

uniform mat4 ModelMatrix;
uniform mat4 ViewProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 NormalMatrix;

out vec3 IPosition;
out vec3 INormal;

void main()
{
	IPosition = vec3(ViewMatrix * ModelMatrix * Vertex);
	INormal   = (mat3(NormalMatrix) * normalize(mat3(ModelMatrix) * Normal));

    gl_Position = ViewProjectionMatrix * ModelMatrix * Vertex;
}

 
ENDCG
CGFRAGMENT

#version 400

#include <res/shaders/Util/TransformationMatrices.shader>

uniform vec3 CameraPosition;

const vec4 LightAmbient  = vec4(0.5, 0.5, 0.5, 1.0);
const vec4 LightColor    = vec4(1.0, 1.0, 1.0, 1.0);

uniform vec4 PrimitiveColor;

in vec3 IPosition;
in vec3 INormal;

out vec4 FragColor;

vec4 phong()
{

	vec3 pos = IPosition;
	vec3 N = normalize(INormal);
	// vec3 L = normalize(vec3(ViewMatrix * vec4(CameraPosition, 1.0f)) - pos);
	vec3 L = normalize(-pos);
	vec3 R = reflect(-L,N);
	vec3 V = normalize(-pos);

	float Id = max(0.0, dot(N,L));

	return (PrimitiveColor * LightColor*Id);
}

void main()
{
	FragColor = (PrimitiveColor * LightAmbient) + 0.5 * phong();
}

 
ENDCG