uniform mat4 ModelMatrix;
uniform mat4 ViewProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 NormalMatrix;