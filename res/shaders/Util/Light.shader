uniform mat4 LightVPMatrix;
uniform mat4 LightBiasVPMatrix;

uniform vec4 LightAmbient;

uniform vec4 LightPosition;
uniform vec4 LightColor;
uniform float LightBias;

uniform float LightRange;
uniform float LightSpotAngle;
uniform float LightIntensity;

uniform sampler2DShadow depthText;


float ComputeLightAttenuation(vec4 wFragmentPosition)
{
    float dist = length(LightPosition.xyz / LightPosition.w - wFragmentPosition.xyz / wFragmentPosition.w);
    float r = dist / LightRange;
    float attenuation = 1.0 / (1.0 + 25.0*pow(r*r, LightIntensity));
    
    return attenuation;
}

float ComputeShadowFactor(vec4 wFragmentPosition)
{
    vec4 ShadowCoord = LightBiasVPMatrix * wFragmentPosition;
    vec4 ShadowCoordNoBias = LightVPMatrix * wFragmentPosition;
    
    vec4 wLightDirectionVector = inverse(LightVPMatrix) * vec4(0, 0, 1, 1);
    wLightDirectionVector /= wLightDirectionVector.w;
    
    // Directions in world coordinates
    vec3 wLightDir = normalize(wLightDirectionVector.xyz - LightPosition.xyz);
    vec3 wPointDir = normalize(wFragmentPosition.xyz - LightPosition.xyz);
     
    float halfRadians = radians(LightSpotAngle / 2.0);
    bool insideCone = dot(wLightDir, wPointDir) > cos(halfRadians);
    
    float shadowFactor = 0.0;
    
    if (insideCone)
    {
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-2,-2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-1,-2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 1,-2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 2,-2));

        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-2,-1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-1,-1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 1,-1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 2,-1));

        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-2, 1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-1, 1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 1, 1));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 2, 1));

        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-2, 2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2(-1, 2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 1, 2));
        shadowFactor += textureProjOffset(depthText, ShadowCoord, ivec2( 2, 2));

        shadowFactor = shadowFactor/16.0;
        
        // Smoothness
        // float range = 1.0f - cos(halfRadians);
        // float valueInRange = (dot(wLightDir, wPointDir) - cos(halfRadians)) / range;
        // shadowFactor = shadowFactor * sqrt(sqrt(valueInRange));
    }
    
    return shadowFactor;
}