CGVERTEX
#version 400

#include <res/shaders/Util/TransformationMatrices.shader>
#include <res/shaders/Util/Light.shader>

layout(location = 0) in vec4 Vertex;

void main()
{
    vec4 position = LightVPMatrix * ModelMatrix * (Vertex);
    position.z += (LightBias * 0.0001) * position.w;
    gl_Position = position;
}

ENDCG
CGFRAGMENT
#version 400

out float FragmentDepth;

void main()
{
    FragmentDepth = gl_FragCoord.z;
}

ENDCG