CGVERTEX
#version 400

#include <res/shaders/Util/TransformationMatrices.shader>

layout(location = 0) in vec4 Vertex;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;

out vec3 IPosition;
out vec3 INormal;
out vec2 ITexCoord;

void main()
{
	IPosition = vec3(ViewMatrix * ModelMatrix * Vertex);
	INormal   = (mat3(NormalMatrix) * normalize(mat3(ModelMatrix) * Normal));
	ITexCoord = TexCoord;

    gl_Position = ViewProjectionMatrix * ModelMatrix * Vertex;
}

 
ENDCG
CGFRAGMENT

#version 400

#include <res/shaders/Util/TransformationMatrices.shader>
#include <res/shaders/Util/Light.shader>

uniform vec4 MaterialMainColor;

uniform sampler2D diffuseTexture;

in vec3 IPosition;
in vec3 INormal;
in vec2 ITexCoord;

out vec4 FragColor;

void main()
{
	FragColor = texture(diffuseTexture, ITexCoord) * LightAmbient;
}

 
ENDCG