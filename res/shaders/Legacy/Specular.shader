CGVERTEX
#version 400

#include <res/shaders/Util/TransformationMatrices.shader>

layout(location = 0) in vec4 Vertex;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;

out vec3 IPosition;
out vec3 INormal;
out vec2 ITexCoord;

out vec4 PosModelCoord;

void main()
{
    IPosition = vec3(ViewMatrix * ModelMatrix * Vertex);
    
    // This should not be computed here
    INormal   = mat3(inverse(transpose(ViewMatrix * ModelMatrix))) * Normal.xyz;
    ITexCoord = TexCoord;

    PosModelCoord = ModelMatrix * Vertex;

    gl_Position = ViewProjectionMatrix * ModelMatrix * Vertex;
}

ENDCG
CGFRAGMENT

#version 400

#include <res/shaders/Util/TransformationMatrices.shader>
#include <res/shaders/Util/Light.shader>
#include <res/shaders/Util/Material.shader>

in vec3 IPosition;
in vec3 INormal;
in vec2 ITexCoord;

in vec4 PosModelCoord;

out vec4 FragColor;

vec4 phong()
{
    vec3 pos = IPosition;
    vec3 N = normalize(INormal);
    vec3 L = normalize(vec3(ViewMatrix * LightPosition) - pos);
    vec3 R = reflect(-L,N);
    vec3 V = normalize(-pos);
    
    float shadowFactor = ComputeShadowFactor(PosModelCoord);
    float attenuation = ComputeLightAttenuation(PosModelCoord);

    float Id = max(0.0, dot(N,L));
    float nh = clamp(dot(normalize(L + V), N), 0.0, 1.0);
    float Is = (Id > 0.0) ? pow(max(0.0, nh), 128.0) : 0.0;

    return (MaterialSpecularColor * LightColor*Id + MaterialSpecularColor * LightColor*Is) * attenuation * shadowFactor;
}

void main()
{
    FragColor = phong() * texture(diffuseTexture, ITexCoord);
}

ENDCG