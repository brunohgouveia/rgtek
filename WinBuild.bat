@echo off

rem Make Sure to create the build directory first.
IF NOT EXIST msvs mkdir msvs
cd msvs

rem Generate solution with cmake.
echo Generating Visual Studio 12 2013 solution
cmake -G "Visual Studio 12 2013" ..

MSBuild.exe RGTekProject.sln /property:Configuration=Debug
MSBuild.exe RGTekProject.sln /property:Configuration=Release

rem Go back to project's folder
cd ..

rem Copy necessary files to build.
if exist dependencies (
    echo Coping the dll's files ...
    XCOPY "dependencies\bin" "msvs\bin" /D /E /C /R /I /K /Y
    echo ... done!
)
