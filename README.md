---

# Getting dependencies
In order to build RGTek, a few dependencies are required (depending on which modules one wants to build):

- assimp 3.1.1
- glew 1.7.0
- OpenGL
- SDL2 2.0.4
- CEGUI 0.8.7 (For GUI purposes, required by the Editor Module)
- Bullet 2.83.7 (Required by the Physics System.)

Such dependencies can be found at [rgtek-dependencies](https://bitbucket.org/brunohgouveia/rgtek-dependency) repository.

---

# Building RGTek
After the dependencies have finished compiling, copy the "dependencies" folder to RGTek project's root folder. Now, run the following script:

##### Windows
```bash
# Go to RGTek root folder
cd $RGTEK_ROOT_FOLDER

# Build
.\WinBuild.bat
```

Such script will generate a folder called `msvs` which will have inside it a Visual Studio 2013 solution and a folder called `dependencies` with RGTek's libraries and headers.

##### Unix
```bash
# Go to RGTek root folder
cd $RGTEK_ROOT_FOLDER

# Build
./UnixBuild.sh
```

Such script will generate a folder called `buid` which will have a folder inside it called `dependencies` with RGTek's libraries and headers.

---

# Choosing which modules to build
Within the RGTek's root folder, there is a file called `CMakeSettings.txt` which contains options to enable/disable modules. It's important to know that there is a hierarchical dependency between modules, and disabling a module `A` will consequently disable all modules depending on `A`.