# Copyright (C) 2014 Barry Deeney
# Copyright (C) 2014 Benny Bobaganoosh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#############################################
# Try to find ASSMIP and set the following: #
#                                           #
# ZLIB_FOUND                              #
# ZLIB_INCLUDE_DIRS                       #
# ZLIB_LIBRARIES                          #
#############################################

SET( ZLIB_SEARCH_PATHS
	./dependencies
	$ENV{PROGRAMFILES}/ZLIB			# WINDOWS
	~/Library/Frameworks				# MAC
	/Library/Frameworks					# MAC
	/usr/local							# LINUX/MAC/UNIX
	/usr								# LINUX/MAC/UNIX
	/opt								# LINUX/MAC/UNIX
	/sw									# Fink
	/opt/local							# DarwinPorts
	/opt/csw							# Blastwave
)

FIND_PATH( ZLIB_INCLUDE_DIRS
	NAMES
		zlib.h
	PATHS
		${ZLIB_SEARCH_PATHS}
	PATH_SUFFIXES
		include
	DOC
		"The directory where zlib.h resides"
)

FIND_LIBRARY( ZLIB_LIBRARIES
	NAMES
		zlib
	PATHS
		${ZLIB_SEARCH_PATHS}
	PATH_SUFFIXES
		lib
        lib/static
		lib64
		lib/x86
		lib/x64
	DOC
		"The ZLIB library"
)

# Check if we found it!
IF ( ZLIB_LIBRARIES )
	SET( ZLIB_FOUND TRUE )
	MESSAGE(STATUS "Looking for ZLIB - found")
ELSE ( ZLIB_LIBRARIES )
	SET( ZLIB_FOUND FALSE )
	MESSAGE(STATUS "Looking for ZLIB - not found")
ENDIF ( ZLIB_LIBRARIES )
