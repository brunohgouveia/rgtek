################################################################################
# Macro definitions used in the RGTEK's cmake project
# ===================================================
include(FindPackageHandleStandardArgs)

#
# add dynamic dependency libraries to a target
#
macro (rgtek_add_dependency_dynamic_libs _TARGET_NAME _DEP_NAME)
    get_target_property(_TARGET_EXISTS ${_TARGET_NAME} TYPE)

    if (_TARGET_EXISTS)
        if (${_DEP_NAME}_LIBRARIES)
            if (${_DEP_NAME}_LIBRARIES_DBG)
                foreach(_LIB ${${_DEP_NAME}_LIBRARIES})
                    target_link_libraries(${_TARGET_NAME} optimized ${_LIB})
                endforeach()
                foreach(_LIB ${${_DEP_NAME}_LIBRARIES_DBG})
                    target_link_libraries(${_TARGET_NAME} debug ${_LIB})
                endforeach()
            else()
                target_link_libraries(${_TARGET_NAME} ${${_DEP_NAME}_LIBRARIES})
            endif()
        endif()
    endif()
endmacro()

#
# add static dependency libraries to a target, falling  back to dynamic libraries
# if static do not exist)
#
macro (rgtek_add_dependency_static_libs _TARGET_NAME _DEP_NAME)
    get_target_property(_TARGET_EXISTS ${_TARGET_NAME} TYPE)

    if (_TARGET_EXISTS)
        if (${_DEP_NAME}_LIBRARIES_STATIC)
            if (${_DEP_NAME}_LIBRARIES_STATIC_DBG)
                foreach(_LIB ${${_DEP_NAME}_LIBRARIES_STATIC})
                    target_link_libraries(${_TARGET_NAME} optimized ${_LIB})
                endforeach()
                foreach(_LIB ${${_DEP_NAME}_LIBRARIES_STATIC_DBG})
                    target_link_libraries(${_TARGET_NAME} debug ${_LIB})
                endforeach()
            else()
                target_link_libraries(${_TARGET_NAME} ${${_DEP_NAME}_LIBRARIES_STATIC})
            endif()
        else()
            rgtek_add_dependency_dynamic_libs(${_TARGET_NAME} ${_DEP_NAME})            
        endif()
    endif()
endmacro()

#
# add a dependency to a target (and it's static equivalent, if it exists)
#
macro (rgtek_add_dependency _TARGET_NAME _DEP_NAME)
    include_directories(${${_DEP_NAME}_INCLUDE_DIR})

    ###########################################################################
    #                    NON-STATIC VERSION OF TARGET
    ###########################################################################
    if (${_DEP_NAME}_DEFINITIONS)
        set_property( TARGET ${_TARGET_NAME} APPEND PROPERTY COMPILE_DEFINITIONS ${${_DEP_NAME}_DEFINITIONS} )
    endif()

    if (${_DEP_NAME}_DEFINITIONS_DYNAMIC)
        set_property( TARGET ${_TARGET_NAME} APPEND PROPERTY COMPILE_DEFINITIONS ${${_DEP_NAME}_DEFINITIONS_DYNAMIC} )
    endif()

    rgtek_add_dependency_dynamic_libs(${_TARGET_NAME} ${_DEP_NAME})
    
    ###########################################################################
    #    ADD DEPENDENCY DEFS TO STATIC VERSION OF TARGET (if it exists)
    ###########################################################################
    get_target_property(_STATIC_EXISTS ${_TARGET_NAME} TYPE)

    if (_STATIC_EXISTS)
        if (${_DEP_NAME}_DEFINITIONS)
            set_property( TARGET ${_TARGET_NAME} APPEND PROPERTY COMPILE_DEFINITIONS ${${_DEP_NAME}_DEFINITIONS} )
        endif()

        if (${_DEP_NAME}_DEFINITIONS_STATIC)
            set_property( TARGET ${_TARGET_NAME} APPEND PROPERTY COMPILE_DEFINITIONS ${${_DEP_NAME}_DEFINITIONS_STATIC} )
        endif()

        rgtek_add_dependency_static_libs(${_TARGET_NAME} ${_DEP_NAME})
    endif()

endmacro()

#
# add a module dependency to a target
#
macro (rgtek_add_module_dependency _TARGET_NAME _MODULE_NAME)
    target_link_libraries(${_TARGET_NAME} ${_MODULE_NAME})
endmacro()

#
# Define a regular library - this is usually linked to directly.
#
macro (rgtek_add_library _LIB_NAME _SOURCE_FILES_VAR _HEADER_FILES_VAR)
    ###########################################################################
    ## STATIC LIBRARY SET UP
    ###########################################################################
    add_library(${_LIB_NAME} STATIC ${${_SOURCE_FILES_VAR}} ${${_HEADER_FILES_VAR}})

    if (APPLE)
        set_target_properties(${_LIB_NAME} PROPERTIES
            INSTALL_NAME_DIR ${RGTEK_APPLE_DYLIB_INSTALL_PATH}
            BUILD_WITH_INSTALL_RPATH TRUE
        )
    endif()
    
    set_target_properties(${_LIB_NAME} PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        ARCHIVE_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        ARCHIVE_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_BINARY_DIR}/dependencies/lib/static"

        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_BINARY_DIR}/dependencies/lib/static"
        
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies/bin"
        RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/dependencies/bin"
        RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/dependencies/bin"
        RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_BINARY_DIR}/dependencies/bin"
        RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_BINARY_DIR}/dependencies/bin")
        
    # Copy headers
    message("ADDING")
    foreach(_H_FILE ${${_HEADER_FILES_VAR}})
        message("HEADER: " ${_H_FILE})
        add_custom_command(TARGET ${_LIB_NAME} POST_BUILD 
                        COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${_H_FILE}"
                                                            "${CMAKE_BINARY_DIR}/dependencies/${_H_FILE}")
    endforeach()    

endmacro()

#
# Define an executable
#
macro (rgtek_add_executable _NAME _SOURCE_FILES_VAR _HEADER_FILES_VAR)
    add_executable(${_NAME} ${${_SOURCE_FILES_VAR}} ${${_HEADER_FILES_VAR}})
    
    set_target_properties(${_NAME} PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
        RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin"
        RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin"
        RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_BINARY_DIR}/bin"
        RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_BINARY_DIR}/bin")
endmacro()

################################################################################
# Checks a set of variables and determines whether a package was 'found' or not.
#
# This is used to try and handle the plethora of dependency options that are
# created when using the CEGUI dependencies package in combination with cmake
# settings and options used here in the main CEGUI build.
#
# NOTE: On platforms where the dependency pack is not intended to be used, the
# extra library configurations are NOT checked, on those platforms we will use
# the base library only.
#
# _PKGNAME: The name of package we're checking for.
# _LIBBASENAMEVAR: name of the library base name variable.  This name will be
#                  used in the construction of other variable names which should
#                  be set for various configurations (eg. BASENAMEVAR_DBG for 
#                  dynamic debug config, BASENAMEVAR_STATIC for non debug static)
# Optional other args: all other args will be tested verbatim (they're passed
#                      along to find_package_handle_standard_args).
#
# NOTICE: COPIED FROM CEGUI'S CMAKE PROJECT
################################################################################
macro (rgtek_find_package_handle_standard_args _PKGNAME _LIBBASENAMEVAR)
    unset(_FPHSA_LIBS)

    if (WIN32 OR APPLE)
        if (CMAKE_CONFIGURATION_TYPES)
            if (CMAKE_CONFIGURATION_TYPES MATCHES "Debug")
                set (_WANT_DBG_LIBS TRUE)
            else()
                unset (_WANT_DBG_LIBS)
            endif()
            if (CMAKE_CONFIGURATION_TYPES MATCHES "Release" OR CMAKE_CONFIGURATION_TYPES MATCHES "Rel[A-Z]")
                set (_WANT_REL_LIBS TRUE)
            else()
                unset (_WANT_REL_LIBS)
            endif()
        elseif (NOT CMAKE_BUILD_TYPE OR NOT CMAKE_BUILD_TYPE STREQUAL Debug)
            set (_WANT_REL_LIBS TRUE)
            unset (_WANT_DBG_LIBS)
        else()
            set (_WANT_DBG_LIBS TRUE)
            unset (_WANT_REL_LIBS)
        endif()
    else()
        set(_FPHSA_LIBS ${_LIBBASENAMEVAR})
    endif()

    find_package_handle_standard_args(${_PKGNAME} DEFAULT_MSG ${_FPHSA_LIBS} ${ARGN})
endmacro()
