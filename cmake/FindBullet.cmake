################################################################################
# Custom cmake module for RGTek to find Bullet Collision
################################################################################
find_path(BULLET_COLLISION_H_PATH NAMES btBulletCollisionCommon.h)
find_library(BULLET_COLLISION_LIB NAMES BulletCollision PATH_SUFFIXES static)
find_library(BULLET_COLLISION_LIB_DBG NAMES BulletCollision_Debug PATH_SUFFIXES static)
mark_as_advanced(BULLET_COLLISION_H_PATH BULLET_COLLISION_LIB BULLET_COLLISION_LIB_DBG)

if (WIN32 OR APPLE)
    find_library(BULLET_COLLISION_LIB_STATIC NAMES 0 0 PATH_SUFFIXES static)
    find_library(BULLET_COLLISION_LIB_STATIC_DBG NAMES 0_d 0_d PATH_SUFFIXES static)
    mark_as_advanced(BULLET_COLLISION_LIB_STATIC BULLET_COLLISION_LIB_STATIC_DBG)
endif()

rgtek_find_package_handle_standard_args(BULLET_COLLISION BULLET_COLLISION_LIB BULLET_COLLISION_H_PATH)


# set up output vars
if (BULLET_COLLISION_FOUND)
    set (BULLET_COLLISION_INCLUDE_DIR ${BULLET_COLLISION_H_PATH})
    set (BULLET_COLLISION_LIBRARIES ${BULLET_COLLISION_LIB})
    if (BULLET_COLLISION_LIB_DBG)
        set (BULLET_COLLISION_LIBRARIES_DBG ${BULLET_COLLISION_LIB_DBG})
    endif()
    if (BULLET_COLLISION_LIB_STATIC)
        set (BULLET_COLLISION_LIBRARIES_STATIC ${BULLET_COLLISION_LIB_STATIC})
    endif()
    if (BULLET_COLLISION_LIB_STATIC_DBG)
        set (BULLET_COLLISION_LIBRARIES_STATIC_DBG ${BULLET_COLLISION_LIB_STATIC_DBG})
    endif()
else()
    message("BULLET COLLISION NOT FOUND")
    set (BULLET_COLLISION_INCLUDE_DIR)
    set (BULLET_COLLISION_LIBRARIES)
    set (BULLET_COLLISION_LIBRARIES_DBG)
    set (BULLET_COLLISION_LIBRARIES_STATIC)
    set (BULLET_COLLISION_LIBRARIES_STATIC_DBG)
endif()

################################################################################
# Custom cmake module for RGTek to find Bullet Dynamics
################################################################################
find_path(BULLET_DYNAMICS_H_PATH NAMES btBulletDynamicsCommon.h)
find_library(BULLET_DYNAMICS_LIB NAMES BulletDynamics PATH_SUFFIXES static)
find_library(BULLET_DYNAMICS_LIB_DBG NAMES BulletDynamics_Debug PATH_SUFFIXES static)
mark_as_advanced(BULLET_DYNAMICS_H_PATH BULLET_DYNAMICS_LIB BULLET_DYNAMICS_LIB_DBG)

if (WIN32 OR APPLE)
    find_library(BULLET_DYNAMICS_LIB_STATIC NAMES 0 0 PATH_SUFFIXES static)
    find_library(BULLET_DYNAMICS_LIB_STATIC_DBG NAMES 0_d 0_d PATH_SUFFIXES static)
    mark_as_advanced(BULLET_DYNAMICS_LIB_STATIC BULLET_DYNAMICS_LIB_STATIC_DBG)
endif()

rgtek_find_package_handle_standard_args(BULLET_DYNAMICS BULLET_DYNAMICS_LIB BULLET_DYNAMICS_H_PATH)


# set up output vars
if (BULLET_DYNAMICS_FOUND)
    set (BULLET_DYNAMICS_INCLUDE_DIR ${BULLET_DYNAMICS_H_PATH})
    set (BULLET_DYNAMICS_LIBRARIES ${BULLET_DYNAMICS_LIB})
    if (BULLET_DYNAMICS_LIB_DBG)
        set (BULLET_DYNAMICS_LIBRARIES_DBG ${BULLET_DYNAMICS_LIB_DBG})
    endif()
    if (BULLET_DYNAMICS_LIB_STATIC)
        set (BULLET_DYNAMICS_LIBRARIES_STATIC ${BULLET_DYNAMICS_LIB_STATIC})
    endif()
    if (BULLET_DYNAMICS_LIB_STATIC_DBG)
        set (BULLET_DYNAMICS_LIBRARIES_STATIC_DBG ${BULLET_DYNAMICS_LIB_STATIC_DBG})
    endif()
else()
    message("BULLET DYNAMICS NOT FOUND")
    set (BULLET_DYNAMICS_INCLUDE_DIR)
    set (BULLET_DYNAMICS_LIBRARIES)
    set (BULLET_DYNAMICS_LIBRARIES_DBG)
    set (BULLET_DYNAMICS_LIBRARIES_STATIC)
    set (BULLET_DYNAMICS_LIBRARIES_STATIC_DBG)
endif()

################################################################################
# Custom cmake module for RGTek to find Bullet LinearMath
################################################################################
find_path(BULLET_LINMATH_H_PATH NAMES LinearMath/btVector3.h)
find_library(BULLET_LINMATH_LIB NAMES LinearMath PATH_SUFFIXES static)
find_library(BULLET_LINMATH_LIB_DBG NAMES LinearMath_Debug PATH_SUFFIXES static)
mark_as_advanced(BULLET_LINMATH_H_PATH BULLET_LINMATH_LIB BULLET_LINMATH_LIB_DBG)

if (WIN32 OR APPLE)
    find_library(BULLET_LINMATH_LIB_STATIC NAMES 0 0 PATH_SUFFIXES static)
    find_library(BULLET_LINMATH_LIB_STATIC_DBG NAMES 0_d 0_d PATH_SUFFIXES static)
    mark_as_advanced(BULLET_LINMATH_LIB_STATIC BULLET_LINMATH_LIB_STATIC_DBG)
endif()

rgtek_find_package_handle_standard_args(BULLET_LINMATH BULLET_LINMATH_LIB BULLET_LINMATH_H_PATH)


# set up output vars
if (BULLET_LINMATH_FOUND)
    set (BULLET_LINMATH_INCLUDE_DIR ${BULLET_LINMATH_H_PATH})
    set (BULLET_LINMATH_LIBRARIES ${BULLET_LINMATH_LIB})
    if (BULLET_LINMATH_LIB_DBG)
        set (BULLET_LINMATH_LIBRARIES_DBG ${BULLET_LINMATH_LIB_DBG})
    endif()
    if (BULLET_LINMATH_LIB_STATIC)
        set (BULLET_LINMATH_LIBRARIES_STATIC ${BULLET_LINMATH_LIB_STATIC})
    endif()
    if (BULLET_LINMATH_LIB_STATIC_DBG)
        set (BULLET_LINMATH_LIBRARIES_STATIC_DBG ${BULLET_LINMATH_LIB_STATIC_DBG})
    endif()
else()
    message("BULLET LINEAR MATH NOT FOUND")
    set (BULLET_LINMATH_INCLUDE_DIR)
    set (BULLET_LINMATH_LIBRARIES)
    set (BULLET_LINMATH_LIBRARIES_DBG)
    set (BULLET_LINMATH_LIBRARIES_STATIC)
    set (BULLET_LINMATH_LIBRARIES_STATIC_DBG)
endif()
