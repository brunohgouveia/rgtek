################################################################################
# Find ASSIMP
# ============================
include(FindPackageHandleStandardArgs)

find_path(ASSIMP_H_PATH NAMES assimp/mesh.h)
find_library(ASSIMP_LIB NAMES assimp ASSIMP PATH_SUFFIXES static)
find_library(ASSIMP_LIB_DBG NAMES assimpd ASSIMPd PATH_SUFFIXES static)
mark_as_advanced(ASSIMP_H_PATH ASSIMP_LIB ASSIMP_LIB_DBG)

if (WIN32 OR APPLE)
    find_library(ASSIMP_LIB_STATIC NAMES assimp ASSIMP PATH_SUFFIXES static)
    find_library(ASSIMP_LIB_STATIC_DBG NAMES assimpd ASSIMPd PATH_SUFFIXES static)
    mark_as_advanced(ASSIMP_LIB_STATIC ASSIMP_LIB_STATIC_DBG)
endif()

rgtek_find_package_handle_standard_args(ASSIMP ASSIMP_LIB ASSIMP_H_PATH)


# set up output vars
if (ASSIMP_FOUND)
    set (ASSIMP_INCLUDE_DIR ${ASSIMP_H_PATH})
    set (ASSIMP_LIBRARIES ${ASSIMP_LIB})
    if (ASSIMP_LIB_DBG)
        set (ASSIMP_LIBRARIES_DBG ${ASSIMP_LIB_DBG})
    endif()
    if (ASSIMP_LIB_STATIC)
        set (ASSIMP_LIBRARIES_STATIC ${ASSIMP_LIB_STATIC})
    endif()
    if (ASSIMP_LIB_STATIC_DBG)
        set (ASSIMP_LIBRARIES_STATIC_DBG ${ASSIMP_LIB_STATIC_DBG})
    endif()
else()
    message("ASSIMP NOT FOUND")
    set (ASSIMP_INCLUDE_DIR)
    set (ASSIMP_LIBRARIES)
    set (ASSIMP_LIBRARIES_DBG)
    set (ASSIMP_LIBRARIES_STATIC)
    set (ASSIMP_LIBRARIES_STATIC_DBG)
endif()

