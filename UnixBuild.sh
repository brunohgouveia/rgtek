mkdir -p build
cd build

mkdir -p Frameworks
cp -rf ../dependencies/lib/dynamic/* Frameworks


cmake -DCMAKE_BUILD_TYPE=Release ..
make -j4
cd ..
