//
// Created by Bruno Gouveia on 12/28/15.
//

#include <Util/Parser/Parser.h>
#include <ctype.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Parser::Parser()
        : mFileBuffer(nullptr)
        , mProcessedTokens(nullptr)
    {
        // Do Nothing
    }

	Parser::~Parser()
	{
		if (mProcessedTokens)
		{
			delete mProcessedTokens;
		}
	}

	DataBin* Parser::Parse(const String &fileName)
    {
        Debugger::AssertTrue(File::Exists(fileName), "Impossible to parse file [%s], because it does not exist.");

        File inputFile(fileName);
        Debugger::AssertTrue(inputFile.isOpen(), "Failed to open file [%s].", fileName.c_str());
        SetBuffer(new FileBuffer(inputFile));

        DataBin* shader = ParseObject();

        SetBuffer(nullptr);
        inputFile.close();

        return shader;
    }

    void Parser::SaveToFile(const String& fileName, DataBin* dataBin)
    {
        File inputFile(fileName, File::Mode::ReadWrite | File::Mode::Create);
        Debugger::AssertTrue(inputFile.isOpen(), "Failed to open file [%s].", fileName.c_str());

        SaveDataBin(dataBin, inputFile);
    }

    void Parser::SaveDataBin(DataBin* dataBin, File& file, int identation)
    {
        if (!dataBin) return;

        String identationString;
        for (int i = 0; i < identation; ++i)
        {
            identationString += '\t';
        }

        switch (dataBin->GetType())
        {
            case DataBin::Object:
            {
                file.Write(identationString + dataBin->GetName());

                if (dataBin->HasLabel()) file.Write(" \"" + dataBin->GetLabel() + "\"");

                file.Write(" {\n");

                for (auto child : dataBin->GetChildren())
                {
                    SaveDataBin(child, file, identation + 1);
                }

                file.Write(identationString + "}");
                break;
            }
            case DataBin::Array:break;
            case DataBin::GLSL:
            {
                file.Write(identationString + dataBin->GetName());
                file.Write(" = GLSL(");
                file.Write(dataBin->GetDataString());
                file.Write(")");
                break;
            }
            case DataBin::Value:
            {
                file.Write(identationString + dataBin->GetName());
                file.Write(" = ");
                if (dataBin->GetDataType() == DataBin::DataType::Literal) file.Write('\"');
                file.Write(dataBin->GetDataString());
                if (dataBin->GetDataType() == DataBin::DataType::Literal) file.Write('\"');
                break;
            }
        }

        file.Write('\n');
    }

    DataBin* Parser::ParseObject()
    {
        static Token token = Token(Token::Type::None, "");
        token = GetNextToken();

        if (token.mType == Token::Type::EndObject) return nullptr;
        if (token.mType == Token::Type::EndArray)  return nullptr;
        if (token.mType == Token::Type::EndOfFile) return nullptr;

        if (token.mType == Token::Type::Name)
        {
            DataBin* dataBin = new DataBin(token.mLexeme);

            token = GetNextToken();

            // Check if element has a label
            if (token.mType == Token::Type::Literal)
            {
                Token nextToken = LookAhead();
                if (nextToken.mType == Token::Type::BeginArray || nextToken.mType == Token::Type::BeginObject)
                {
                    dataBin->SetLabel(token.mLexeme);
                    token = GetNextToken();
                }
                else
                {
					Debugger::Error("Labels are not allowed in elements that are not an object or array definition.");
                }
            }

            // Check if it's an object definition.
            if (token.mType == Token::Type::BeginObject)
            {
                while (DataBin *child = ParseObject())
                {
                    Debugger::AssertNotNull(child, "Child is null");
                    dataBin->AddChild(child);
                    dataBin->SetType(DataBin::Type::Object);
                }

                if (token.mType != Token::Type::EndObject)
                {
					Debugger::Error("End of object's definition not found, perhaps you forgot '}'?");
                }
            }
            else
            {
                // Otherwise it should be just a field with a primitive value. Therefore, we need to check for assignment token
                if (token.mType != Token::Type::Assignment)
                {
					Debugger::Error("Unexpected token [%s], perhaps your forgot '='?", token.mLexeme.c_str());
                }
                token = GetNextToken();

                dataBin->SetType(DataBin::Type::Value);

                // Read the field's value.
                switch (token.mType)
                {
                    case Token::Type::Integer:
                    {
                        dataBin->SetDataType(DataBin::DataType::Int);
                        dataBin->SetInt(Math::ParseToInteger(token.mLexeme));
                        break;
                    }
                    case Token::Type::Float:
                    {
                        dataBin->SetDataType(DataBin::DataType::Float);
                        dataBin->SetFloat(Math::ParseToFloat(token.mLexeme));
                        break;
                    }
                    case Token::Type::Literal:
                    {
                        dataBin->SetDataType(DataBin::DataType::Literal);;
                        dataBin->SetString(token.mLexeme);
                        break;
                    }
                    case Token::Type::GLSL:
                    {
                        dataBin->SetType(DataBin::Type::GLSL);
                        token = GetNextToken();

                        if (token.mType == Token::Type::GLSLBody)
                        {
                            dataBin->SetDataType(DataBin::DataType::Literal);
                            dataBin->SetString(token.mLexeme.substr(1, token.mLexeme.size() - 2));
                        }

                        break;
                    }
                    case Token::Type::Name:break;
                    case Token::Type::BeginArray:break;
                    case Token::Type::EndArray:break;
                    case Token::Type::BeginObject:break;
                    case Token::Type::EndObject:break;
                    case Token::Type::EndOfFile:break;
                    case Token::Type::Assignment:break;
                    case Token::Type::None:break;
                }
            }

            return dataBin;
        }

        Debugger::Fatal("TODO - Handle this error properly");
        // Suppress Warning
        return nullptr;
    }

    Token Parser::GetNextToken()
    {
        if (mProcessedTokens)
        {
            Token token = *mProcessedTokens;
            delete mProcessedTokens;
            mProcessedTokens = nullptr;
            return token;
        }
        else
        {
            return GetNextTokenFromBuffer();
        }
    }

    Token Parser::GetNextTokenFromBuffer()
    {
        if (*GetBuffer() == '\0')
        {
            return Token(Token::Type::EndOfFile, "");
        }

        // Consume white spaces
        while (isspace(*GetBuffer())) GetBuffer()++;

        // Start lexeme
        GetBuffer().StartLexeme();

        // Single character tokens
        if (*GetBuffer() == '(')
        {
            int openParentheses = 1;

            while (openParentheses > 0)
            {
                GetBuffer()++;
                if (*GetBuffer() == '(') openParentheses++;
                else if (*GetBuffer() == ')') openParentheses--;
            }

            GetBuffer()++;
            return Token(Token::Type::GLSLBody, GetBuffer().GetLexeme());
        }
        else if (*GetBuffer() == '{')
        {
            GetBuffer()++;
            return Token(Token::Type::BeginObject, "{");
        }
        else if (*GetBuffer() == '}')
        {
            GetBuffer()++;
            return Token(Token::Type::EndObject, "}");
        }
        else if (*GetBuffer() == '[')
        {
            GetBuffer()++;
            return Token(Token::Type::BeginArray, "[");
        }
        else if (*GetBuffer() == ']')
        {
            GetBuffer()++;
            return Token(Token::Type::EndArray, "]");
        }
        else if (*GetBuffer() == '=')
        {
            GetBuffer()++;
            return Token(Token::Type::Assignment, "=");
        }

        // Numbers
        if (isdigit(*GetBuffer()) || *GetBuffer() == '+' || *GetBuffer() == '-')
        {
            GetBuffer()++;
            // Integer part
			while (isdigit(*GetBuffer()))
            {
                GetBuffer()++;
            }

            // Check if it's float
            if (*GetBuffer() == '.')
            {
                GetBuffer()++;

				while (isdigit(*GetBuffer()))
                {
                    GetBuffer()++;
                }
                return Token(Token::Type::Float, GetBuffer().GetLexeme());
            }
            else
            {
                return Token(Token::Type::Integer, GetBuffer().GetLexeme());
            }
        }

        // Name or CGPROGRAM
        if (isalpha(*GetBuffer()))
        {
            while (isalpha(*GetBuffer()))
            {
                GetBuffer()++;
            }

            // Check if it's CGPROGRAM
            if (GetBuffer().GetLexeme() == "GLSL")
            {
                return Token(Token::Type::GLSL, GetBuffer().GetLexeme());
            }
            else
            {
                return Token(Token::Type::Name, GetBuffer().GetLexeme());
            }

        }

        // Literal
        if (*GetBuffer() == '\"')
        {
            // Consume the character \"
            GetBuffer()++;

            while (*GetBuffer() != '\"')
            {
                GetBuffer()++;
            }

            // Consume the character \"
            GetBuffer()++;

            String lexeme = GetBuffer().GetLexeme();
            return Token(Token::Type::Literal, lexeme.substr(1, lexeme.size() - 2));
        }

        // Invalid token
        while (!isspace(*GetBuffer()))
        {
            GetBuffer()++;
        }

		Debugger::Error("Unexpected token [%s].", GetBuffer().GetLexeme().c_str());

        // Suppress warning
        return Token(Token::Type::None, GetBuffer().GetLexeme());

    }

    Token Parser::LookAhead()
    {

        mProcessedTokens = new Token(GetNextTokenFromBuffer());
        return *mProcessedTokens;
    }

}