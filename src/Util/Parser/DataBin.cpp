//
// Created by Bruno Gouveia on 1/14/16.
//

#include <Util/Parser/DataBin.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    DataBin::DataBin(const String& name, DataBin* parent, Type type)
            : mName(name)
            , mType(type)
            , mDataType(DataType::None)
            , mData()
    {
        // Do nothing
        if (parent) parent->AddChild(this);
    }

    DataBin::DataBin(const String& name, DataType dataType)
        : mName(name)
        , mType(Type::Object)
        , mDataType(dataType)
        , mData()
    {
        // Do nothing
    }

    DataBin::DataBin(const String& name, int value, DataBin* parent)
        : mName(name)
        , mType(Type::Value)
        , mDataType(DataType::Int)
        , mData()
    {
        if (parent) parent->AddChild(this);
        SetInt(value);
    }

    DataBin::DataBin(const String& name, float value, DataBin* parent)
        : mName(name)
        , mType(Type::Value)
        , mDataType(DataType::Float)
        , mData()
    {
        if (parent) parent->AddChild(this);
        SetFloat(value);
    }

    DataBin::DataBin(const String& name, const String& value, DataBin* parent)
        : mName(name)
        , mType(Type::Value)
        , mDataType(DataType::Literal)
        , mData()
    {
        if (parent) parent->AddChild(this);
        SetString(value);
    }

    DataBin::~DataBin()
    {
        // Do nothing
    }

    void DataBin::AddChild(DataBin *dataBin)
    {
        mChildren.push_back(dataBin);
    }

    DataBin* DataBin::GetChildByName(const String& name) const
    {
        for (auto& child : GetChildren())
        {
            if (child->GetName() == name) return child;
        }
        return nullptr;
    }

	void DataBin::DeleteChildren()
	{
		for (auto child : mChildren)
		{
			if (child)
			{
				child->DeleteChildren();
				delete child;
			}
		}
	}

	void DataBin::Print()
    {
        String value = (mDataType == DataType::Int ? std::to_string(mData.integerData) : (mDataType == DataType::Float ? std::to_string(mData.floatData) : (mDataType == DataType::Literal ? mData.stringData : "(None)")));
        Debugger::Debug("DataBin - name %s, label %s, children count %d, value %s", mName.c_str(), (mLabel.size() ? mLabel.c_str() : "(None)"), mChildren.size(), value.c_str());

        for (auto& child: mChildren)
        {
            if (child) child->Print();
        }
    }


    String DataBin::GetDataString() const
    {
        switch (GetDataType())
        {
            case None: return "None";
            case Int: return std::to_string(GetData().integerData);
            case Float: return std::to_string(GetData().floatData);
            case Literal: return GetData().stringData;
        }
    }
}