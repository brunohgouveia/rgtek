//
// Created by Bruno Gouveia on 3/12/16.
//

#include <Util/Parser/DataTranslator.h>

namespace RGTek
{
    Color DataTranslator::TranslateColorFrom(DataBin* dataBin)
    {
        Color color;
        DataBin* r = dataBin->GetChildByName("R");
        DataBin* g = dataBin->GetChildByName("G");
        DataBin* b = dataBin->GetChildByName("B");
        DataBin* a = dataBin->GetChildByName("A");

        if (r) color.r = r->GetData().floatData;
        if (g) color.g = g->GetData().floatData;
        if (b) color.b = b->GetData().floatData;
        if (a) color.a = a->GetData().floatData;

        return color;
    }

    Vector3 DataTranslator::TranslateVector3From(DataBin* dataBin)
    {
        Vector3 vec;
        DataBin* x = dataBin->GetChildByName("X");
        DataBin* y = dataBin->GetChildByName("Y");
        DataBin* z = dataBin->GetChildByName("Z");

        if (x) vec.x = x->GetData().floatData;
        if (y) vec.y = y->GetData().floatData;
        if (z) vec.z = z->GetData().floatData;

        return vec;
    }

    Quaternion DataTranslator::TranslateQuaternionFrom(DataBin* dataBin)
    {
        Quaternion quat;
        DataBin* x = dataBin->GetChildByName("X");
        DataBin* y = dataBin->GetChildByName("Y");
        DataBin* z = dataBin->GetChildByName("Z");
        DataBin* w = dataBin->GetChildByName("W");

        if (x) quat.x = x->GetData().floatData;
        if (y) quat.y = y->GetData().floatData;
        if (z) quat.z = z->GetData().floatData;
        if (w) quat.w = w->GetData().floatData;

        return quat;
    }

    String DataTranslator::TranslateStringFrom(DataBin* bin)
    {
        return bin->GetDataString();
    }

    float DataTranslator::TranslateFloatFrom(DataBin* bin)
    {
        return bin->GetData().floatData;
    }
}