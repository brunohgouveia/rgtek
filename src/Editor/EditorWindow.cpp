//
// Created by Bruno Gouveia on 11/22/15.
//
#ifdef RGTEK_CLING_ENABLED
#include "cling/Interpreter/Interpreter.h"
#include "cling/MetaProcessor/MetaProcessor.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <memory>
#include <future>
#include <thread>
#include <chrono>
#include "clang/Basic/LangOptions.h"
#include "clang/Frontend/CompilerInstance.h"
#endif

#include <Application/Application.h>
#include <Core/Camera.h>
#include <System/Input.h>
#include <Editor/EditorWindow.h>

#ifdef RGTEK_CEGUI_ENABLED
    #include <Rendering/CEGUI.h>
#endif

#include <System/InputMap.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdio.h>
#include <Tools/Debugger.h>

namespace RGTek
{
#ifdef RGTEK_CLING_ENABLED
    static cling::Interpreter*                    m_interp;
    static std::unique_ptr<cling::MetaProcessor>  m_MetaProcessor;
    static std::string llvmString;
    static std::string tokens [] = {"\x1B[0m", "\x1B[1m", "\x1B[0;1;30m", "\x1B[0;1;31m", "\x1B[0;1;32m"};
    static auto result = std::async(std::launch::async,[] {
        return;
    });
    static std::atomic<bool> done(true);
#endif

    EditorWindow::EditorWindow(const String& title, int width, int height, int argc, char** argv)
        : Window(title, width, height)
        , mSceneViewCallback(nullptr)
        , mEditorSystem(nullptr)

    {
        Debugger::Debug("EditorWindow starting...");

        InitCEGUI();
        CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Sizef(GetFramebufferSize().x, GetFramebufferSize().y));
        CEGUI::Window* root = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("mainLayout.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(root);

        /*** Toolbar */
        CEGUI::Window* toolbar = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("toolbar.layout");
        root->getChild("Toolbar")->addChild(toolbar);

        CEGUI::PushButton* viewButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("View"));
        viewButton->subscribeEvent(CEGUI::PushButton::EventClicked, [&](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->SetTool(EditorSystem::Tool::View);
            return true;
        });

        CEGUI::PushButton* moveButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("Move"));
        moveButton->subscribeEvent(CEGUI::PushButton::EventClicked, [&](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->SetTool(EditorSystem::Tool::Move);
            return true;
        });

        CEGUI::PushButton* rotateButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("Rotate"));
        rotateButton->subscribeEvent(CEGUI::PushButton::EventClicked, [&](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->SetTool(EditorSystem::Tool::Rotate);
            return true;
        });

        CEGUI::PushButton* scaleButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("Scale"));
        scaleButton->subscribeEvent(CEGUI::PushButton::EventClicked, [&](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->SetTool(EditorSystem::Tool::Scale);
            return true;
        });

        static CEGUI::PushButton* playEditButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("ToogleMode"));
        playEditButton->subscribeEvent(CEGUI::PushButton::EventClicked, [&](const CEGUI::EventArgs&) -> bool
        {
            if (mEditorSystem->GetMode() == EditorSystem::Mode::Playing)
            {
                mEditorSystem->SetMode(EditorSystem::Mode::Editing);
                playEditButton->setText("Play");
            }
            else
            {
                mEditorSystem->SetMode(EditorSystem::Mode::Playing);
                playEditButton->setText("Edit");
            }
            return true;
        });

        CEGUI::PushButton* saveSceneButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("SaveScene"));
        saveSceneButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->SaveCurrentScene();
            FillGraphSceneWindow();
            return true;
        });

        CEGUI::PushButton* loadSceneButton = static_cast<CEGUI::PushButton*>(toolbar->getChild("LoadScene"));
        loadSceneButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            mEditorSystem->LoadCurrentScene();
            FillGraphSceneWindow();
            return true;
        });


        /*** Toolbar */
        CEGUI::Window* graphSceneWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("GraphScene/GraphSceneTab");
        CEGUI::Editbox* entityNameWindow = static_cast<CEGUI::Editbox*>(graphSceneWindow->getChild("EntityName"));

        CEGUI::PushButton* addEntityButton = static_cast<CEGUI::PushButton*>(graphSceneWindow->getChild("AddEntity"));
        addEntityButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            EntityPtr newEntity = new Entity(entityNameWindow->getText().c_str());
            mEditorSystem->GetCurrentScene()->AddEntity(newEntity);
            FillGraphSceneWindow();
            entityNameWindow->setText("");
            return true;
        });

        CEGUI::PushButton* deleteEntityButton = static_cast<CEGUI::PushButton*>(graphSceneWindow->getChild("DeleteEntity"));
        deleteEntityButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            Entity* entity = mEditorSystem->GetCurrentScene()->FindEntityByName(entityNameWindow->getText().c_str());

            mEditorSystem->GetCurrentScene()->DeleteEntity(entity);
            FillGraphSceneWindow();
            entityNameWindow->setText("");
            return true;
        });



        CEGUI::Window* interpreterPanel = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("interpreterPanel.layout");
        root->getChild("InterpreterPanel")->addChild(interpreterPanel);

        static CEGUI::Window* interpreterOutput = interpreterPanel->getChild("Interpreter Output");
        static CEGUI::MultiLineEditbox* interpreterConsole = static_cast<CEGUI::MultiLineEditbox*>(interpreterPanel->getChild("Interpreter Console"));
        interpreterConsole->subscribeEvent(CEGUI::MultiLineEditbox::EventKeyDown, [=](const CEGUI::EventArgs& event) -> bool
        {
            const CEGUI::KeyEventArgs& keyEvent = static_cast<const CEGUI::KeyEventArgs&>(event);

            bool control = CEGUI::System::getSingleton().getDefaultGUIContext().getSystemKeys().isPressed(CEGUI::SystemKeys::SystemKey::Control);

            if (keyEvent.scancode == CEGUI::Key::Scan::Return && control)
            {
                // Acquire lockers in the same order

#ifdef RGTEK_CLING_ENABLED
                std::streambuf* oldCoutStreamBuf = std::cout.rdbuf();
                std::streambuf* oldCerrStreamBuf = std::cerr.rdbuf();

                // Change rdbuff of cout and ceer
                std::ostringstream strCout;
                std::cout.rdbuf( strCout.rdbuf() );
                std::cerr.rdbuf( strCout.rdbuf() );

                // Process each line
                std::istringstream iss(keyEvent.window->getText().c_str());
                std::string line;
                while (std::getline(iss,line))
                {
                m_MetaProcessor->getOuts().flush();

                cling::Interpreter::CompilationResult compRes;
                //cling::MetaProcessor::MaybeRedirectOutputRAII RAII(m_MetaProcessor.get());

                m_MetaProcessor->process(line.c_str(), compRes, 0/*result*/);

                m_MetaProcessor->getOuts().flush();

                if (!llvmString.empty())
                    {
                std::cout << llvmString;
            }
                llvmString.clear();
            }

                // Restore rdbuff
                std::cout.rdbuf(oldCoutStreamBuf);
                std::cerr.rdbuf(oldCerrStreamBuf);
                interpreterOutput->setText(strCout.str());
#else
                interpreterOutput->setText("Interpreter CLING not supported.");
#endif
                interpreterConsole->setText("");
                return true;
            }

            return false;
        });

        mCanvasWindow = root->getChild("Canvas");

#ifdef RGTEK_CLING_ENABLED
        m_interp = new cling::Interpreter(argc, argv);
        static llvm::raw_string_ostream m_MPOuts (llvmString);
        m_MetaProcessor.reset(new cling::MetaProcessor(*m_interp, m_MPOuts));
#endif
    }

    EditorWindow::~EditorWindow()
    {
        Debugger::Debug("EditorWindow shutting down...");
		auto& renderer = static_cast<CEGUI::OpenGL3Renderer&>(*CEGUI::System::getSingleton().getRenderer());
		CEGUI::System::destroy();
		CEGUI::OpenGL3Renderer::destroy(renderer);
		
		
    }

    void EditorWindow::InitCEGUI()
    {
        // create renderer and enable extra states
        CEGUI::OpenGL3Renderer& cegui_renderer = CEGUI::OpenGL3Renderer::create(CEGUI::Sizef(800.f, 600.f));
        cegui_renderer.enableExtraStateSettings(true);

        // create CEGUI system object
        CEGUI::System::create(cegui_renderer);

        // setup resource directories
        CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
        rp->setResourceGroupDirectory("schemes", "../datafiles/schemes/");
        rp->setResourceGroupDirectory("imagesets", "../datafiles/imagesets/");
        rp->setResourceGroupDirectory("fonts", "../datafiles/fonts/");
        rp->setResourceGroupDirectory("layouts", "../datafiles/layouts/");
        rp->setResourceGroupDirectory("looknfeels", "../datafiles/looknfeel/");
        rp->setResourceGroupDirectory("lua_scripts", "../datafiles/lua_scripts/");
        rp->setResourceGroupDirectory("schemas", "../datafiles/xml_schemas/");

        // set default resource groups
        CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
        CEGUI::Font::setDefaultResourceGroup("fonts");
        CEGUI::Scheme::setDefaultResourceGroup("schemes");
        CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
        CEGUI::WindowManager::setDefaultResourceGroup("layouts");
        CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

        CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
        if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
            parser->setProperty("SchemaDefaultResourceGroup", "schemas");

        // load TaharezLook scheme and DejaVuSans-10 font
        CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme", "schemes");
        CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme", "schemes");
		CEGUI::FontManager::getSingleton().createFromString("<?xml version=\"1.0\" ?> <Font version=\"3\" name=\"DejaVuSans-10\" filename=\"DejaVuSans.ttf\" type=\"FreeType\" size=\"10\" nativeHorzRes=\"1280\" nativeVertRes=\"720\" autoScaled=\"false\"/>");

        // set default font and cursor image and tooltip type
        CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
        CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("WindowsLook/Tooltip");
    }

    CEGUI::Window* EditorWindow::GetInspectorWindow()
    {
        return CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("Inspector/InspectorTab");
    }

    void EditorWindow::Render()
    {
        auto& pixelPosition = mCanvasWindow->getPixelPosition();
		auto& pixelSize = mCanvasWindow->getPixelSize();
		Vector2 position = Vector2(pixelPosition.d_x, GetFramebufferSize().y - (pixelPosition.d_y + pixelSize.d_height));

		BindAsRenderTarget();
		glClearColor(0.445f, 0.445f, 0.445f, 1.0f);
		Clear();

		mEditorSystem->RecomputedSelectedEntityReference();
		if (mEditorSystem->GetMode() == EditorSystem::Mode::Playing)
		{
			mCanvasCallback(position, Vector2(pixelSize.d_width, pixelSize.d_height));
		}
		else
		{
			mSceneViewCallback(position, Vector2(pixelSize.d_width, pixelSize.d_height));
		}

		static float time = SDL_GetTicks() / 1000.f;
		const float newtime = SDL_GetTicks() / 1000.f;
		const float time_elapsed = newtime - time;
		CEGUI::System::getSingleton().injectTimePulse(time_elapsed);
		CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(time_elapsed);
		time = newtime;

		CEGUI::System::getSingleton().getRenderer()->beginRendering();
		CEGUI::System::getSingleton().getDefaultGUIContext().draw();
		CEGUI::System::getSingleton().getRenderer()->endRendering();

		mEditorSystem->GetInspector().UpdateItems();        

        Input::ResetMap();
        Input::EndMouseInjection();

    }

    void EditorWindow::FillGraphSceneWindow()
    {
        CEGUI::Window* graphSceneWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("GraphScene/GraphSceneTab");
        Debugger::AssertNotNull(graphSceneWindow, "GraphScene Window not found.");

        CEGUI::Editbox* entityNameWindow = static_cast<CEGUI::Editbox*>(graphSceneWindow->getChild("EntityName"));
        CEGUI::Tree* tree = static_cast<CEGUI::Tree*>(graphSceneWindow->getChild("Layout"));
        ScenePtr scene = Application::GetSharedApplication()->GetScene();

        tree->resetList();
        for (Entity* entity : scene->GetRoot()->GetChildren())
        {
            CEGUI::TreeItem *item = new CEGUI::TreeItem(entity->GetName());
            tree->addItem(item);
            item->setUserData(static_cast<void*>(new std::string(entity->GetName())));
        }

        tree->removeAllEvents();
        tree->subscribeEvent(CEGUI::Tree::EventSelectionChanged, [=] (const CEGUI::EventArgs& event) -> bool
        {
            const CEGUI::TreeEventArgs& treeEvent = static_cast<const CEGUI::TreeEventArgs&>(event);

            if (treeEvent.treeItem)
            {
                std::string* entityName = static_cast<std::string*>(treeEvent.treeItem->getUserData());
                Entity* entity = mEditorSystem->GetCurrentScene()->FindEntityByName(*entityName);
                if (entity)
                {
                    mEditorSystem->SetSelectedEntity(entity);
                    entityNameWindow->setText(entity->GetName());
                }

                return true;
            }
            return false;
        });
    }

    void EditorWindow::HandleEvents()
    {
        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                {
                    mIsCloseRequested = true;
                    break;
                }
                case SDL_MOUSEMOTION:
                {
                    Vector2 windowPD = GetWindowPixelDensity();
                    Input::mousePosition = Vector2(windowPD.x * event.motion.x, GetFramebufferSize().y - windowPD.y * event.motion.y);
                    Input::sInputActive = (mEditorSystem->GetMode() == EditorSystem::Mode::Playing ? mCanvasWindow->isActive() : mCanvasWindow->isMouseContainedInArea());

                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(windowPD.x * event.motion.x,windowPD.y * event.motion.y);

                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                {
                    Input::InjectMouseDown(event.button.button - 1u);
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(
                            static_cast<CEGUI::MouseButton>(InputMap::SDLtoCEGUIMouseButton(event.button.button))
                    );

                    break;
                }
                case SDL_MOUSEBUTTONUP:
                {
                    Input::InjectMouseUp(event.button.button - 1u);
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(
                            static_cast<CEGUI::MouseButton>(InputMap::SDLtoCEGUIMouseButton(event.button.button))
                    );

                    break;
                }
                case SDL_MOUSEWHEEL:
                {
                    Input::mouseWheel = (Input::sInputActive ? event.wheel.y : 0.0f);

                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseWheelChange(
                            static_cast<float>(event.wheel.y));

                    break;
                }
                case SDL_KEYDOWN:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode));

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt);
                    }

                    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(
                            static_cast<CEGUI::Key::Scan>(InputMap::toCEGUIKey(event.key.keysym.scancode))
                    );

                    break;
                }
                case SDL_KEYUP:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode), false);

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt, false);
                    }

                    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(
                            static_cast<CEGUI::Key::Scan>(InputMap::toCEGUIKey(event.key.keysym.scancode))
                    );

                    break;
                }
                case SDL_TEXTINPUT:
                {
                    InputMap::InjectUTF8Text(event.text.text);
                    break;
                }
                case SDL_WINDOWEVENT:
                {
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                    {
                        Vector2 framebufferSize = GetFramebufferSize();
                        CEGUI::System::getSingleton().notifyDisplaySizeChanged(
                                CEGUI::Sizef(framebufferSize.x, framebufferSize.y));

                        glViewport(0, 0, static_cast<int>(framebufferSize.x), static_cast<int>(framebufferSize.y));
                    }
                    else if (event.window.event == SDL_WINDOWEVENT_LEAVE)
                    {
                        CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseLeaves();
                    }

                    break;
                }
                default:
                    break;

            }
        }
    }

}
