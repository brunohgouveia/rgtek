//
// Created by Bruno Gouveia on 12/4/15.
//

#include <Editor/Handle3D.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Handle3D::Handle3D()
        : mSelectedAxis(Axis::None)
    {
        // Do nothing
    }

    Handle3D::~Handle3D()
    {
        // Do nothing
    }

    float Handle3D::GetAxisLength(Axis axis) const
    {
        return mSize;
    }

    Vector3 Handle3D::GetRayIntersectionPoint(const Ray& ray, const Vector3& viewDirection)
    {
        switch (mSelectedAxis)
        {
            case Axis::X:
            {
                Vector3 normal = mTransform.GetRotation().GetRight().Cross(viewDirection).Cross(mTransform.GetRotation().GetRight());
                Vector3 intersection;
                ray.IntersectPlane(Plane(mTransform.GetPosition(), normal), intersection);

                return intersection;
            }
            case Axis::Y:
            {
                Vector3 normal = mTransform.GetRotation().GetUp().Cross(viewDirection).Cross(mTransform.GetRotation().GetUp());
                Vector3 intersection;
                ray.IntersectPlane(Plane(mTransform.GetPosition(), normal), intersection);

                return intersection;
            }
            case Axis::Z:
            {
                Vector3 normal = mTransform.GetRotation().GetForward().Cross(viewDirection).Cross(mTransform.GetRotation().GetForward());
                Vector3 intersection;
                ray.IntersectPlane(Plane(mTransform.GetPosition(), normal), intersection);

                return intersection;
            }
            case Axis::None:
            {
                Debugger::Fatal("Calling this API with no axis selected makes no sense.");

            }
            case Axis::All:
            {
                // Suppress warning
                return Vector3::zero;
            }
        }
    }

    Handle3D::Axis Handle3D::GetSelectedAxis() const
    {
        return mSelectedAxis;
    }

    float Handle3D::GetSize() const
    {
        return mSize;
    }

    Transform Handle3D::GetTransform() const
    {
        return mTransform;
    }

    TransformWeakPtr Handle3D::GetCameraTransform() const
    {
        return mCameraTransform;
    }

    void Handle3D::SetSelectedAxis(Axis axis)
    {
        mSelectedAxis = axis;
    }

    void Handle3D::SetSize(float size)
    {
        if (mSize == size) return;

        mSize = size;

        OnSetSize();

        SetTransform(mTransform);

    }

    void Handle3D::SetTransform(const Transform& transform)
    {
        mTransform = transform;

        OnSetTransform();
    }

    void Handle3D::SetCameraTransform(TransformWeakPtr transform)
    {
        mCameraTransform = transform;
    }

	Handle3D::Axis AxisBaseHandle3D::GetAxisIntersectedByRay(const Ray& mouseClickRay)
	{
		Vector3 intersectionPoint;
		Vector3 aabbMin = 0.5f * Vector3(-0.05f * mSize, -mSize, -0.05f * mSize);
		Vector3 aabbMax = 0.5f * Vector3(0.05f * mSize, mSize, 0.05f * mSize);;
		Axis axis = Axis::None;
		float closestSqDistance = std::numeric_limits<float>::max();

		// Test x axis
		if (mouseClickRay.IntersectOBB(xAxisTransform, aabbMin, aabbMax, intersectionPoint))
		{
			axis = Axis::X;
			float intersectionPointSqDistance = (intersectionPoint - mouseClickRay.origin).MagnitudeSq();
			closestSqDistance = intersectionPointSqDistance;
		}

		// Test y axis
		if (mouseClickRay.IntersectOBB(yAxisTransform, aabbMin, aabbMax, intersectionPoint))
		{
			float intersectionPointSqDistance = (intersectionPoint - mouseClickRay.origin).MagnitudeSq();
			if (intersectionPointSqDistance < closestSqDistance)
			{
				closestSqDistance = intersectionPointSqDistance;
				axis = Axis::Y;
			}
		}

		// Test z axis
		if (mouseClickRay.IntersectOBB(zAxisTransform, aabbMin, aabbMax, intersectionPoint))
		{
			float intersectionPointSqDistance = (intersectionPoint - mouseClickRay.origin).MagnitudeSq();
			if (intersectionPointSqDistance < closestSqDistance)
			{
				closestSqDistance = intersectionPointSqDistance;
				axis = Axis::Z;
			}
		}

		// Test all axis
		if (HasAllAxis() && mouseClickRay.IntersectOBB(mTransform, Vector3(-0.1f * mSize), Vector3(0.1f * mSize), intersectionPoint))
		{
			float intersectionPointSqDistance = (intersectionPoint - mouseClickRay.origin).MagnitudeSq();
			if (intersectionPointSqDistance < closestSqDistance)
			{
				closestSqDistance = intersectionPointSqDistance;
				axis = Axis::All;
			}
		}

		return axis;
	}

	void AxisBaseHandle3D::OnSetTransform()
	{
		xAxisTransform = mTransform;
		xAxisTransform.MoveRight(mSize * 0.6f);
		xAxisTransform.SetRotation(Quaternion(Vector3(0.0f, 0.0f, 1.0f), ToRadians(-90.0f)));

		yAxisTransform = mTransform;
		yAxisTransform.MoveUp(mSize * 0.6f);

		zAxisTransform = mTransform;
		zAxisTransform.MoveForward(mSize * 0.6f);
		zAxisTransform.SetRotation(Quaternion(Vector3(1.0f, 0.0f, 0.0f), ToRadians(90.0f)));
	}
}