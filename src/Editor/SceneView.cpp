//
// Created by Bruno Gouveia on 12/3/15.
//

#include <Editor/SceneView.h>
#include <Editor/EditorSystem.h>
#include <Core/Camera.h>
#include <Core/Transform.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    SceneView::SceneView()
    {
        Debugger::Debug("SceneView starting...");

        CameraPtr camera = new Camera();
        camera->SetFarClip(100.0f);
        SetCamera(camera);
    }

    SceneView::~SceneView()
    {
        Debugger::Debug("SceneView shutting down...");
    }

    void SceneView::SetCamera(const CameraPtr& camera)
    {
        TransformPtr transform = new Transform();
        transform->SetPosition(Vector3(0.0f, 3.0f, 10.0f));
        transform->SetRotation(Quaternion(Vector3(0.0f, 1.0f, 0.0f), ToRadians(180.0f)));

        camera->SetTransform(transform);
        mCamera = camera;

    }

}