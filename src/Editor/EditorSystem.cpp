//
// Created by Bruno Gouveia on 12/2/15.
//

#include <Editor/EditorSystem.h>
#include <Editor/EditorWindow.h>
#include <Application/Application.h>
#include <System/Input.h>

#include <Rendering/CEGUI.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    EditorSystem::EditorSystem(EditorWindowWeakPtr window, RenderingSystemWeakPtr renderingSystem, PhysicsSystemWeakPtr physicsSystem, ScriptSystemWeakPtr scriptSystem)
        : mRenderingSystem(renderingSystem)
        , mPhysicsSystem(physicsSystem)
        , mScriptSystem(scriptSystem)
        , mCurrentScene(nullptr)
        , mInspector(this, window->GetInspectorWindow())
        , mMode(Mode::Editing)
        , mCurrentTool(Tool::View)
        , mCurrentHandle3D(nullptr)
        , mSelectedEntity(nullptr)
    {
        // Do nothing
		mDrawPrimitive = mRenderingSystem->GetResourceManager().CreateResource<Shader>("shaders/Util/DrawPrimitive.shader");

        mConeMeshHandle = mRenderingSystem->GetResourceManager().CreateResource<Mesh>("models/Primitives/cone.obj");
        mCubeMeshHandle = mRenderingSystem->GetResourceManager().CreateResource<Mesh>("models/Primitives/cube.obj");

        SceneViewPtr mainSceneView = new SceneView();
        AddSceneView(mainSceneView);

        window->SetEditorSystem(this);
    }

    void EditorSystem::AddSceneView(const SceneViewPtr& sceneView)
    {
        mSceneViews.push_back(sceneView);
    }

    void EditorSystem::DrawSceneView(unsigned int sceneViewIndex, Viewport viewport)
    {

        mRenderingSystem->Render(mSceneViews[sceneViewIndex]->GetCamera(), viewport);
        mSceneViews[sceneViewIndex]->GetCamera()->SetViewport(viewport);

        Application::GetSharedApplication()->GetWindow()->BindCanvasAsRenderTarget(viewport);

        mMoveHandle3D.SetCameraTransform(GetSceneView(sceneViewIndex)->GetCamera()->GetTransform());
        mScaleHandle3D.SetCameraTransform(GetSceneView(sceneViewIndex)->GetCamera()->GetTransform());

        SetSelectedEntity(GetCurrentScene()->FindEntityByName(mSelectedEntityName));
        if (mSelectedEntity)
        {

            glEnable(GL_SCISSOR_TEST);
            glEnable(GL_DEPTH_TEST);
            glClear(GL_DEPTH_BUFFER_BIT);
            glDepthFunc(GL_LEQUAL);

            if (mCurrentTool == Tool::Move)
            {
                DrawMoveTool();
            }
            else if (mCurrentTool == Tool::Scale)
            {
                DrawScaleTool();
            }
            else if (mCurrentTool == Tool::Rotate)
            {
                DrawRotateTool();
            }

            mSelectedEntity->RenderGizmos();
            mRenderingSystem->GetBackend().Dump();

            Debugger::CheckOpenGLError();
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_SCISSOR_TEST);
        }

        ProcessInput();
    }

    void EditorSystem::DrawAxis(const Vector3 &position, const Quaternion &rotation, float size)
    {
        DrawAxis(position, rotation, Vector3(size));
    }

    void EditorSystem::DrawAxis(const Vector3& position, const Quaternion& rotation, const Vector3& size)
    {
        // Y axis
        mRenderingSystem->GetBackend().DrawLine(Line(position, position + rotation.GetUp() * size.y), (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Y) ? Color::yellow : Color::green);
        // X axis
        mRenderingSystem->GetBackend().DrawLine(Line(position, position + rotation.GetRight() * size.x), (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::X) ? Color::yellow : Color::red);
        // Z axis
        mRenderingSystem->GetBackend().DrawLine(Line(position, position + rotation.GetForward() * size.z), (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Z) ? Color::yellow : Color::blue);
    }

    void EditorSystem::DrawWireArc(const Vector3 &center, const Vector3 &normal, const Vector3 &direction, float angle,
                                   float radius)
    {
        mRenderingSystem->GetBackend().DrawWireArc(center, normal, direction, mHandleColor, angle, radius);
    }

    void EditorSystem::DrawWireDisc(const Vector3& center, const Vector3& normal, float radius, bool culling)
    {
        mRenderingSystem->GetBackend().DrawWireDisc(center, normal, mHandleColor, radius, culling);
    }

    Vector3 EditorSystem::PositionHandle(const Vector3& position, const Quaternion& rotation, float size)
    {
        SetCurrentHandle3D(&mMoveHandle3D);
        mCurrentHandle3D->SetSize(size);

        Transform transform;
        transform.SetPosition(position);
        transform.SetRotation(rotation);

        mCurrentHandle3D->SetTransform(transform);

        CameraWeakPtr sceneViewCamera = GetSceneView(0)->GetCamera();
        Vector3 viewDirection = mCurrentHandle3D->GetTransform().GetPosition() - sceneViewCamera->GetTransform()->GetPosition();
        if (Input::GetMouseButtonDown(0) && !Input::GetKey(KeyCode::Alt))
        {
			Ray mouseClickRay = GetSceneView(0)->GetCamera()->ScreenPointToRay(Input::mousePosition);

			Handle3D::Axis axis = mCurrentHandle3D->GetAxisIntersectedByRay(mouseClickRay);
			mCurrentHandle3D->SetSelectedAxis(axis);
			
			if (axis != Handle3D::Axis::None)
			{
				mCurrentHandle3D->OnMouseDown(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
			}
        }

        if (mCurrentHandle3D->GetSelectedAxis() != Handle3D::Axis::None && !Input::GetKey(KeyCode::Alt))
        {
            if (Input::GetMouseButton(0))
            {
                mCurrentHandle3D->OnMouseDrag(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }

            if (Input::GetMouseButtonUp(0))
            {
                mCurrentHandle3D->OnMouseUp(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }
        }

        Vector3 axisSize = Vector3(
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::X),
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::Y),
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::Z)
        );

        // Draw axis first
        DrawAxis(position, rotation, axisSize);

		Shader* drawPrimitive = mRenderingSystem->GetResourceManager().GetResource<Shader>(mDrawPrimitive);
		drawPrimitive->UseShader();
        mRenderingSystem->UpdateUniformsToShader(drawPrimitive);

		drawPrimitive->SetUniform("CameraPosition", GetSceneView(0)->GetCamera()->GetTransform()->GetPosition());

        MeshWeakPtr mesh = mRenderingSystem->GetResourceManager().GetResource<Mesh>(mConeMeshHandle);

        transform.SetScale(transform.GetScale() * Vector3(0.03f, 0.05f, 0.03f) * size);

        // Y axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Y) ? Color::yellow : Color::green);
        transform.SetPosition(position + Vector3(0.0f, axisSize.y, 0.0f));

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        // X axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::X) ? Color::yellow : Color::red);
        transform.SetPosition(position + Vector3(axisSize.x, 0.0f, 0.0f));
        transform.SetRotation(Quaternion(Vector3(0.0f, 0.0f, 1.0f), ToRadians(-90.0f)));

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        // Z axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Z) ? Color::yellow : Color::blue);
        transform.SetPosition(position + Vector3(0.0f, 0.0f, axisSize.z));
        transform.SetRotation(Quaternion(Vector3(1.0f, 0.0f, 0.0f), ToRadians(90.0f)));

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        return mCurrentHandle3D->GetTransform().GetPosition();
    }

    Vector3 EditorSystem::ScaleHandle(const Vector3& scale, const Vector3& position, const Quaternion& rotation, float size)
    {
        SetCurrentHandle3D(&mScaleHandle3D);
        mCurrentHandle3D->SetSize(size);

        Transform transform;
        transform.SetPosition(position);
        transform.SetRotation(rotation);
        transform.SetScale(scale);

        mCurrentHandle3D->SetTransform(transform);

        CameraWeakPtr sceneViewCamera = GetSceneView(0)->GetCamera();
        Vector3 viewDirection = mCurrentHandle3D->GetTransform().GetPosition() - sceneViewCamera->GetTransform()->GetPosition();
        if (Input::GetMouseButtonDown(0) && !Input::GetKey(KeyCode::Alt))
        {
			Ray mouseClickRay = GetSceneView(0)->GetCamera()->ScreenPointToRay(Input::mousePosition);

			Handle3D::Axis axis = mCurrentHandle3D->GetAxisIntersectedByRay(mouseClickRay);
			mCurrentHandle3D->SetSelectedAxis(axis);

			if (axis != Handle3D::Axis::None)
			{
				mCurrentHandle3D->OnMouseDown(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
			}
        }

        if (mCurrentHandle3D->GetSelectedAxis() != Handle3D::Axis::None && !Input::GetKey(KeyCode::Alt))
        {
            if (Input::GetMouseButton(0))
            {
                mCurrentHandle3D->OnMouseDrag(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }

            if (Input::GetMouseButtonUp(0))
            {
                mCurrentHandle3D->OnMouseUp(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }
        }

        Vector3 axisSize = Vector3(
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::X),
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::Y),
                mCurrentHandle3D->GetAxisLength(Handle3D::Axis::Z)
        );
        // Draw axis first
        DrawAxis(position, rotation, axisSize);

		Shader* drawPrimitive = mRenderingSystem->GetResourceManager().GetResource<Shader>(mDrawPrimitive);
		drawPrimitive->UseShader();
        mRenderingSystem->UpdateUniformsToShader(drawPrimitive);

		drawPrimitive->SetUniform("CameraPosition", GetSceneView(0)->GetCamera()->GetTransform()->GetPosition());

        MeshWeakPtr mesh = mRenderingSystem->GetResourceManager().GetResource<Mesh>(mCubeMeshHandle);

        // Center
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::All) ? Color::yellow : Color::gray);
        transform.SetPosition(position);
        transform.SetScale(Vector3(0.1) * size);

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        transform.SetScale(Vector3(0.05f) * size);

        // Y axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Y) ? Color::yellow : Color::green);
        transform.SetPosition(position + rotation.GetUp() * axisSize.y);

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        // X axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::X) ? Color::yellow : Color::red);
        transform.SetPosition(position + rotation.GetRight() * axisSize.x);

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        // Z axis
		drawPrimitive->SetUniform("PrimitiveColor", (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Z) ? Color::yellow : Color::blue);
        transform.SetPosition(position + rotation.GetForward() * axisSize.z);

		drawPrimitive->SetUniform("ModelMatrix", transform.GetTransformation());
        for (const MeshDataPtr& subMesh : mesh->GetSubMeshes())
        {
            subMesh->Draw();
        }

        return mCurrentHandle3D->GetTransform().GetScale();
    }

    Quaternion EditorSystem::RotationHandle(const Quaternion &rotation, const Vector3 &position, float size)
    {
        SetCurrentHandle3D(&mRotateHandle3D);
        mCurrentHandle3D->SetSize(size * 0.6f);

        Transform transform;
        transform.SetPosition(position);
        transform.SetRotation(rotation);

        mCurrentHandle3D->SetTransform(transform);

        CameraWeakPtr camera = GetSceneView(0)->GetCamera();
        float discRadius = size * 0.6f;

        // Draw handle Discs
        mHandleColor = (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::X ? Color::yellow : Color::red);
        DrawWireDisc(position, rotation.GetRight(), discRadius, true);
        mHandleColor = (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Z ? Color::yellow : Color::blue);
        DrawWireDisc(position, rotation.GetForward(), discRadius, true);
        mHandleColor = (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::Y ? Color::yellow : Color::green);
        DrawWireDisc(position, rotation.GetUp(), discRadius, true);

        mHandleColor = Color::gray;
        DrawWireDisc(position, camera->GetTransform()->GetPosition() - position, discRadius);
        mHandleColor = (mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::All ? Color::yellow : Color::gray);
        DrawWireDisc(position, camera->GetTransform()->GetPosition() - position, 1.1f * discRadius);

        CameraWeakPtr sceneViewCamera = GetSceneView(0)->GetCamera();
        Vector3 viewDirection = mCurrentHandle3D->GetTransform().GetPosition() - sceneViewCamera->GetTransform()->GetPosition();

        if (Input::GetMouseButtonDown(0) && !Input::GetKey(KeyCode::Alt))
        {
            mCurrentHandle3D->OnMouseDown(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
        }

        if (mCurrentHandle3D->GetSelectedAxis() != Handle3D::Axis::None && !Input::GetKey(KeyCode::Alt))
        {
            if (Input::GetMouseButton(0))
            {
                mCurrentHandle3D->OnMouseDrag(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }

            if (Input::GetMouseButtonUp(0))
            {
                mCurrentHandle3D->OnMouseUp(sceneViewCamera->ScreenPointToRay(Input::mousePosition), viewDirection);
            }
        }

        return mCurrentHandle3D->GetTransform().GetRotation();
    }

    EditorSystem::Mode EditorSystem::GetMode()
    {
        return mMode;
    }

    Entity* EditorSystem::GetSelectedEntity() const
    {
        return mSelectedEntity;
    }

    void EditorSystem::ProcessInput()
    {
        TransformPtr sceneViewCameraTransform = GetSceneView(0)->GetCamera()->GetTransform();
        const float scrollVelocity         = 0.20f;
        const float dragVerticalVelocity   = 0.02f;
        const float dragHorizontalVelocity = 0.02f;

        if (Input::GetMouseButtonDown(Input::MouseButton::LeftButton) && !Input::GetKey(KeyCode::Alt))
        {
            if (mCurrentHandle3D == nullptr || mCurrentHandle3D->GetSelectedAxis() == Handle3D::Axis::None)
            {
                RaycastHit hitInfo;
                if (mPhysicsSystem->Raycast(GetSceneView(0)->GetCamera()->ScreenPointToRay(Input::mousePosition), hitInfo))
                {
                    SetSelectedEntity(hitInfo.mEntity);
                }
                else
                {
                    SetSelectedEntity(nullptr);
                }
            }
        }
        else if (Input::GetMouseDragging(Input::MouseButton::LeftButton) && Input::GetKey(KeyCode::Alt))
        {
            sceneViewCameraTransform->Rotate(Quaternion(Vector3(0.1f, 0.0f, 0.0f), -ToRadians(Input::GetMouseDelta(Input::MouseButton::LeftButton).y)), Space::Self);
            sceneViewCameraTransform->Rotate(Quaternion(Vector3(0.0f, 0.1f, 0.0f), +ToRadians(Input::GetMouseDelta(Input::MouseButton::LeftButton).x)), Space::World);

            Input::ResetDelta(Input::MouseButton::LeftButton);
        }

        // Middle button drag
        if (Input::GetMouseDragging(Input::MouseButton::MiddleButton))
        {
            sceneViewCameraTransform->MoveLeft(Input::GetMouseDelta(Input::MouseButton::MiddleButton).x * dragHorizontalVelocity);
            sceneViewCameraTransform->MoveDown(Input::GetMouseDelta(Input::MouseButton::MiddleButton).y * dragVerticalVelocity);

            Input::ResetDelta(Input::MouseButton::MiddleButton);
        }

        // Middle button scroll
        if (!Math::IsZero(Input::mouseWheel))
        {
            sceneViewCameraTransform->MoveForward(Input::mouseWheel * scrollVelocity);
        }
    }

    void EditorSystem::DrawMoveTool()
    {
        Debugger::AssertNotNull(mSelectedEntity, "No game object selected.");

        Vector3 position = mSelectedEntity->GetTransform()->GetPosition();
        float size = (GetSceneView(0)->GetCamera()->GetTransform()->GetPosition() - position).Magnitude() * 0.2f;

        mSelectedEntity->GetTransform()->SetPosition(PositionHandle(position, Quaternion(Vector3(0.0f), ToRadians(0.0f)), size));
    }

    void EditorSystem::DrawRotateTool()
    {
        Debugger::AssertNotNull(mSelectedEntity, "No game object selected.");

        Vector3 position    = mSelectedEntity->GetTransform()->GetPosition();
        Quaternion rotation = mSelectedEntity->GetTransform()->GetRotation();
        float size          = (GetSceneView(0)->GetCamera()->GetTransform()->GetPosition() - position).Magnitude() * 0.2f;

        mSelectedEntity->GetTransform()->SetRotation(RotationHandle(rotation, position, size));
    }

    void EditorSystem::DrawScaleTool()
    {
        Debugger::AssertNotNull(mSelectedEntity, "No game object selected.");

        Vector3 position    = mSelectedEntity->GetTransform()->GetPosition();
        Vector3 scale       = mSelectedEntity->GetTransform()->GetScale();
        Quaternion rotation = mSelectedEntity->GetTransform()->GetRotation();
        float size          = (GetSceneView(0)->GetCamera()->GetTransform()->GetPosition() - position).Magnitude() * 0.2f;

        mSelectedEntity->GetTransform()->SetScale(ScaleHandle(scale, position, rotation, size));
    }

    void EditorSystem::SetTool(Tool tool)
    {
        mCurrentTool = tool;

        switch (tool)
        {
            case View:
            {
                SetCurrentHandle3D(nullptr);
                break;
            }
            case Tool::Move:
            {
                SetCurrentHandle3D(&mMoveHandle3D);
                break;
            }
            case Tool::Scale:
            {
                SetCurrentHandle3D(&mScaleHandle3D);
                break;
            }

            case Rotate:break;
        }
    }

    void EditorSystem::SetMode(Mode mode)
    {
        if (mode == Mode::Editing) {
            if (mPhysicsSystem)
            {
                mPhysicsSystem->Pause();
            }
            if (mScriptSystem)
            {
                mScriptSystem->Pause();
            }
        }
        else if (mode == Mode::Playing)
        {
            if (mPhysicsSystem)
            {
                mPhysicsSystem->Resume();
            }
            if (mScriptSystem)
            {
                mScriptSystem->Resume();
            }
        }

        mMode = mode;
    }

    void EditorSystem::SetCurrentHandle3D(Handle3D *handle3D)
    {
        if (mCurrentHandle3D == handle3D) return;
		mCurrentHandle3D = handle3D;
		if (mCurrentHandle3D) mCurrentHandle3D->SetSelectedAxis(Handle3D::Axis::None);
    }

    void EditorSystem::SetSelectedEntity(Entity* entity)
    {
        if (mSelectedEntity == entity) return;

        mSelectedEntity = entity;
        if (mSelectedEntity)
        {
            mInspector.CleanUp();
            mInspector.Populate(entity);

            mSelectedEntityName = mSelectedEntity->GetName();
        }
        else
        {
            mInspector.CleanUp();
        }
    }

    void EditorSystem::SaveCurrentScene() const
    {
        if (mCurrentScene)
        {
            mCurrentScene->Save();
        }
    }

    void EditorSystem::LoadCurrentScene() const
    {
        if (mCurrentScene)
        {
            mCurrentScene->Load();
        }
    }

    void EditorSystem::RecomputedSelectedEntityReference()
    {
        SetSelectedEntity(GetCurrentScene()->FindEntityByName(mSelectedEntityName));
    }

}