//
// Created by Bruno Gouveia on 12/8/15.
//

#include <Editor/ScaleHandle3D.h>
#include <Physics/PhysicsSystem.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    ScaleHandle3D::ScaleHandle3D()
    {
        SetSize(1.0f);
    }

    ScaleHandle3D::~ScaleHandle3D()
    {
		// Do nothing
    }

    void ScaleHandle3D::OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        mFirstIntersectionPoint = mCurrentIntersectionPoint = GetRayIntersectionPoint(mouseClickDirection, viewDirection);

        mouseClickDirection.IntersectPlane(Plane(mTransform.GetPosition(), viewDirection), mCurrentCenterIntersectionPoint);
        mFirstCenterIntersectionPoint = mCurrentCenterIntersectionPoint;
    }

    void ScaleHandle3D::OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        Vector3 newIntersectionPoint = GetRayIntersectionPoint(mouseClickDirection, viewDirection);

        if (mSelectedAxis != Axis::All && newIntersectionPoint == mCurrentIntersectionPoint) return;

        switch (mSelectedAxis)
        {
            case Axis::X:
            {
                float currentSize = (mTransform.GetRotation().GetRight().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
                float newSize = (mTransform.GetRotation().GetRight().Dot(newIntersectionPoint - mTransform.GetPosition()));

                Vector3 newScale = mTransform.GetScale();
                newScale.x *= newSize / currentSize;
                mTransform.SetScale(newScale);
                break;
            }
            case Axis::Y:
            {
                float currentSize = (mTransform.GetRotation().GetUp().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
                float newSize = (mTransform.GetRotation().GetUp().Dot(newIntersectionPoint - mTransform.GetPosition()));

                Vector3 newScale = mTransform.GetScale();
                newScale.y *= newSize / currentSize;
                mTransform.SetScale(newScale);
                break;
            }
            case Axis::Z:
            {
                float currentSize = (mTransform.GetRotation().GetForward().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
                float newSize = (mTransform.GetRotation().GetForward().Dot(newIntersectionPoint - mTransform.GetPosition()));

                Vector3 newScale = mTransform.GetScale();
                newScale.z *= newSize / currentSize;
                mTransform.SetScale(newScale);
                break;
            }
            case Axis::All:
            {
                Vector3 newCenterIntersectionPoint;
                mouseClickDirection.IntersectPlane(Plane(mTransform.GetPosition(), mCameraTransform->GetRotation().GetForward()), newCenterIntersectionPoint);

                Vector3 centerPosition = mTransform.GetPosition() + mSize * mCameraTransform->GetRotation().GetDown() + mSize * mCameraTransform->GetRotation().GetLeft();
                Vector3 raisingDirection = mSize * mCameraTransform->GetRotation().GetUp() + mSize * mCameraTransform->GetRotation().GetRight();

                float currentSize = (mCurrentCenterIntersectionPoint - centerPosition).Dot(raisingDirection);
                float newSize = (newCenterIntersectionPoint - centerPosition).Dot(raisingDirection);

                mTransform.SetScale(mTransform.GetScale() * newSize/currentSize);

                mCurrentCenterIntersectionPoint = newCenterIntersectionPoint;
            }
            case None:
            {
                // Do nothing
            }
        }
        mCurrentIntersectionPoint = newIntersectionPoint;
    }

    void ScaleHandle3D::OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        mFirstIntersectionPoint = mCurrentIntersectionPoint;
        mFirstCenterIntersectionPoint = mCurrentCenterIntersectionPoint;
    }

    float ScaleHandle3D::GetAxisLength(Axis axis) const
    {
        if (axis == mSelectedAxis && axis == Axis::X)
        {
            float initialSize = (mTransform.GetRotation().GetRight().Dot(mFirstIntersectionPoint - mTransform.GetPosition()));
            float currentSize = (mTransform.GetRotation().GetRight().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
            return mSize * currentSize/initialSize;
        }
        else if (axis == mSelectedAxis && axis == Axis::Y)
        {
            float initialSize = (mTransform.GetRotation().GetUp().Dot(mFirstIntersectionPoint - mTransform.GetPosition()));
            float currentSize = (mTransform.GetRotation().GetUp().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
            return mSize * currentSize/initialSize;
        }
        else if (axis == mSelectedAxis && axis == Axis::Z)
        {
            float initialSize = (mTransform.GetRotation().GetForward().Dot(mFirstIntersectionPoint - mTransform.GetPosition()));
            float currentSize = (mTransform.GetRotation().GetForward().Dot(mCurrentIntersectionPoint - mTransform.GetPosition()));
            return mSize * currentSize/initialSize;
        }
        else if (mSelectedAxis == Axis::All)
        {
            Vector3 centerPosition = mTransform.GetPosition() + mSize * mCameraTransform->GetRotation().GetDown() + mSize * mCameraTransform->GetRotation().GetLeft();
            Vector3 raisingDirection = mSize * mCameraTransform->GetRotation().GetUp() + mSize * mCameraTransform->GetRotation().GetRight();

            float initialSize = (mFirstCenterIntersectionPoint - centerPosition).Dot(raisingDirection);
            float currentSize = (mCurrentCenterIntersectionPoint - centerPosition).Dot(raisingDirection);
            return mSize * currentSize/initialSize;
        }
        else
        {
            return mSize;
        }
    }

}