//
// Created by Bruno Gouveia on 12/11/15.
//

#include <Editor/RotateHandle3D.h>
#include <Math/Plane.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    RotateHandle3D::RotateHandle3D()
    {

    }

    RotateHandle3D::~RotateHandle3D()
    {

    }

    void RotateHandle3D::OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        Vector3 intersectedPoint;

        if (mouseClickDirection.IntersectSphere(mTransform.GetPosition(), mSize, intersectedPoint))
        {
            Vector3 intersectionDirection = intersectedPoint - mTransform.GetPosition();
            mIntersectionPlane = Plane(intersectedPoint, intersectionDirection);

            // Test x disc
            Plane plane = Plane(mTransform.GetPosition(), mTransform.GetRotation().GetRight());
            Vector3 intersectionDirectionProjected = intersectionDirection.ProjectOnto(plane);

            if (Math::Abs(intersectionDirectionProjected.Magnitude() - mSize) < 0.001f)
            {
                SetSelectedAxis(Axis::X);

                mIntersectionTangent = mTransform.GetRotation().GetRight().Cross(intersectionDirection);
                mCurrentIntersectionDirectionProjected = Vector3::zero;
                return;
            }

            // Test y disc
            plane.normal = mTransform.GetRotation().GetUp();
            intersectionDirectionProjected = intersectionDirection.ProjectOnto(plane);

            if (Math::Abs(intersectionDirectionProjected.Magnitude() - mSize) < 0.001f)
            {
                SetSelectedAxis(Axis::Y);

                mIntersectionTangent = mTransform.GetRotation().GetUp().Cross(intersectionDirection);
                mCurrentIntersectionDirectionProjected = Vector3::zero;
                return;
            }

            // Test z disc
            plane.normal = mTransform.GetRotation().GetForward();
            intersectionDirectionProjected = intersectionDirection.ProjectOnto(plane);

            if (Math::Abs(intersectionDirectionProjected.Magnitude() - mSize) < 0.001f)
            {
                SetSelectedAxis(Axis::Z);

                mIntersectionTangent = mTransform.GetRotation().GetForward().Cross(intersectionDirection);
                mCurrentIntersectionDirectionProjected = Vector3::zero;
                return;
            }

            SetSelectedAxis(Axis::None);
        }
        else if (mouseClickDirection.IntersectSphere(mTransform.GetPosition(), 1.1f * mSize, intersectedPoint))
        {
            Vector3 intersectionDirection = intersectedPoint - mTransform.GetPosition();
            mIntersectionPlane = Plane(intersectedPoint, viewDirection);

            SetSelectedAxis(Axis::All);

            mIntersectionTangent = viewDirection.Cross(intersectionDirection);
            mCurrentIntersectionDirectionProjected = Vector3::zero;
        }
        else
        {
            SetSelectedAxis(Axis::None);
        }
    }

    void RotateHandle3D::OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        Vector3 newPlaneIntersectionPoint;
        mouseClickDirection.IntersectPlane(mIntersectionPlane, newPlaneIntersectionPoint);
        Vector3 newIntersectionDirection = newPlaneIntersectionPoint - mTransform.GetPosition();
        Vector3 newIntersectionDirectionProjected = newIntersectionDirection.ProjectOnto(mIntersectionTangent);

        Vector3 projectedIntersectionDirectionDiff = newIntersectionDirectionProjected - mCurrentIntersectionDirectionProjected;
        float orientation = (mIntersectionTangent.Dot(projectedIntersectionDirectionDiff) >= 0.0f ? 1.0f : -1.0f);

        switch (mSelectedAxis)
        {
            case Axis::X:
            {
                mTransform.Rotate(Quaternion(orientation, 0.0f, 0.0f), projectedIntersectionDirectionDiff.Magnitude());
                break;
            }
            case Y:
            {
                mTransform.Rotate(Quaternion(0.0f, orientation, 0.0f), projectedIntersectionDirectionDiff.Magnitude());
                break;
            }
            case Z:
            {
                mTransform.Rotate(Quaternion(0.0f, 0.0f, orientation), projectedIntersectionDirectionDiff.Magnitude());
                break;
            }
            case All:
            {
                mTransform.Rotate(viewDirection, 0.1f * orientation * projectedIntersectionDirectionDiff.Magnitude(), Space::World);
                break;
            }
            case None:break;
        }

        mCurrentIntersectionDirectionProjected = newIntersectionDirectionProjected;


    }

    void RotateHandle3D::OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {

    }

	Handle3D::Axis RotateHandle3D::GetAxisIntersectedByRay(const Ray&)
	{
		return Handle3D::None;
	}

    float RotateHandle3D::GetAxisLength(Axis axis) const
    {
        return 1.0f;
    }

    void RotateHandle3D::OnSetTransform()
    {

    }

    void RotateHandle3D::OnSetSize()
    {

    }

}