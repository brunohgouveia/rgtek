//
// Created by Bruno Gouveia on 1/7/16.
//

#include <Editor/EditorItem.h>
#include <Editor/Inspector.h>
#include <Core/Component.h>
#include <Core/Transform.h>
#include <Tools/Debugger.h>

namespace RGTek
{
	EditorItem::EditorItem(Transform* transform, Inspector* inspector)
		: mInspector(inspector)
		, mTransform(transform)
		, mGameComponent(nullptr)
	{
		Debugger::AssertNotNull(mTransform, "Transform class can't be null");
		mItemName = mTransform->GetType().GetName() + std::to_string(inspector->GetEditorItemsLayout()->getChildCount());

		// Do Nothing
		CEGUI::Window* editorItemWindow = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("editorItem.layout");
		editorItemWindow->setName(mItemName);

		mInspector->GetEditorItemsLayout()->addChild(editorItemWindow);

		GetEditorItemLabel()->setText(mTransform->GetType().GetName());

		// Transforms are not deletable
		GetEditorItemTitle()->removeChild(GetEditorItemDeleteButton());

	}

	///////////////////////////////////////////////////////////////////////

    EditorItem::EditorItem(Component* component, Inspector* inspector)
        : mInspector(inspector)
		, mTransform(nullptr)
        , mGameComponent(component)
    {
		Debugger::AssertNotNull(mGameComponent, "Component class can't be null");
		mItemName = mGameComponent->GetType().GetName() + std::to_string(inspector->GetEditorItemsLayout()->getChildCount());
        
		// Do Nothing
        CEGUI::Window* editorItemWindow = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("editorItem.layout");
        editorItemWindow->setName(mItemName);

        mInspector->GetEditorItemsLayout()->addChild(editorItemWindow);

		GetEditorItemLabel()->setText(mGameComponent->GetType().GetName());

		// Configure delete button
	    CEGUI::PushButton* deleteButton = static_cast<CEGUI::PushButton*>(GetEditorItemDeleteButton());
        deleteButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=](const CEGUI::EventArgs& args) -> bool
        {
            mInspector->DeleteEditorItem(this);
            return true;
        });
        
    }
	
	///////////////////////////////////////////////////////////////////////

    EditorItem::~EditorItem()
    {
        while (GetEditorItemLayout()->getChildCount())
        {
            CEGUI::Window* child = GetEditorItemLayout()->getChildAtIdx(0);
            GetEditorItemLayout()->removeChild(child);
        }
        CEGUI::Window* window = mInspector->GetEditorItemsLayout()->getChild(mItemName);
        mInspector->GetEditorItemsLayout()->removeChild(window);
    }

	///////////////////////////////////////////////////////////////////////

	void EditorItem::OnInspectorGUI()
	{
		if (mGameComponent)
		{
			for (auto& property : mGameComponent->GetType().GetProperties())
			{
				auto& field = property.second;
				if (field.GetValueType() == typeof(float))
				{
					FloatField(field.GetName(), field.GetCallableGetter<float>(mGameComponent), field.GetCallableSetter<float>(mGameComponent));
				}
				if (field.GetValueType() == typeof(int))
				{
					IntField(field.GetName(), field.GetCallableGetter<int>(mGameComponent), field.GetCallableSetter<int>(mGameComponent));
				}
				if (field.GetValueType() == typeof(String))
				{
					StringField(field.GetName(), field.GetCallableGetter<String>(mGameComponent), field.GetCallableSetter<String>(mGameComponent));
				}
				if (field.GetValueType() == typeof(Vector3))
				{
					Vector3Field(field.GetName(), field.GetCallableGetter<Vector3>(mGameComponent), field.GetCallableSetter<Vector3>(mGameComponent));
				}
				if (field.GetValueType() == typeof(Quaternion))
				{
					QuaternionField(field.GetName(), field.GetCallableGetter<Quaternion>(mGameComponent), field.GetCallableSetter<Quaternion>(mGameComponent));
				}
				if (field.GetValueType() == typeof(Color))
				{
					ColorField(field.GetName(), field.GetCallableGetter<Color>(mGameComponent), field.GetCallableSetter<Color>(mGameComponent));
				}
			}
		}
		else if (mTransform)
		{
			for (auto& propertyPair : mTransform->GetType().GetProperties())
			{
				auto& property = propertyPair.second;
				if (property.GetValueType() == typeof(float))
				{
					FloatField(property.GetName(), property.GetCallableGetter<float>(mTransform), property.GetCallableSetter<float>(mTransform));
				}
				if (property.GetValueType() == typeof(int))
				{
					IntField(property.GetName(), property.GetCallableGetter<int>(mTransform), property.GetCallableSetter<int>(mTransform));
				}
				if (property.GetValueType() == typeof(String))
				{
					StringField(property.GetName(), property.GetCallableGetter<String>(mTransform), property.GetCallableSetter<String>(mTransform));
				}
				if (property.GetValueType() == typeof(Vector3))
				{
					Vector3Field(property.GetName(), property.GetCallableGetter<Vector3>(mTransform), property.GetCallableSetter<Vector3>(mTransform));
				}
				if (property.GetValueType() == typeof(Quaternion))
				{
					QuaternionField(property.GetName(), property.GetCallableGetter<Quaternion>(mTransform), property.GetCallableSetter<Quaternion>(mTransform));
				}
				if (property.GetValueType() == typeof(Color))
				{
					ColorField(property.GetName(), property.GetCallableGetter<Color>(mTransform), property.GetCallableSetter<Color>(mTransform));
				}
			}
		}

	}

	///////////////////////////////////////////////////////////////////////

	void EditorItem::BuildLayout()
	{
		for (auto* propertyDrawer : mPropertyDrawers)
		{
			propertyDrawer->BuildLayout();
		}
	}

	///////////////////////////////////////////////////////////////////////

    void EditorItem::Synchronize()
    {
		for (auto* propertyDrawer : mPropertyDrawers)
		{
			propertyDrawer->Synchronize();
		}
    }

	///////////////////////////////////////////////////////////////////////

	void EditorItem::FloatField(const String& label, FloatPropertyDrawer::Getter getter, FloatPropertyDrawer::Setter setter)
	{
		mPropertyDrawers.push_back(new FloatPropertyDrawer(this, label.c_str(), getter, setter));
	}

	void EditorItem::IntField(const String& label, IntPropertyDrawer::Getter getter, IntPropertyDrawer::Setter setter)
	{
		mPropertyDrawers.push_back(new IntPropertyDrawer(this, label.c_str(), getter, setter));
	}

	void EditorItem::StringField(const String& label, StringFieldLayout::Getter getter, StringFieldLayout::Setter setter)
	{
		mPropertyDrawers.push_back(new StringFieldLayout(this, label.c_str(), getter, setter));
	}

	void EditorItem::Vector3Field(const String& label, Vector3PropertyDrawer::Getter getter, Vector3PropertyDrawer::Setter setter)
	{
		mPropertyDrawers.push_back(new Vector3PropertyDrawer(this, label.c_str(), getter, setter));
	}

	void EditorItem::QuaternionField(const String& label, QuaternionPropertyDrawer::Getter getter, QuaternionPropertyDrawer::Setter setter)
	{
		mPropertyDrawers.push_back(new QuaternionPropertyDrawer(this, label.c_str(), getter, setter));
	}

	void EditorItem::ColorField(const String& label, ColorPropertyDrawer::Getter getter, ColorPropertyDrawer::Setter setter)
	{
		mPropertyDrawers.push_back(new ColorPropertyDrawer(this, label.c_str(), getter, setter));
	}

	///////////////////////////////////////////////////////////////////////

    CEGUI::Window* EditorItem::GetEditorItemLabel()  const { return mInspector->GetEditorItemsLayout()->getChild(mItemName + "/Title/Label"); }
    CEGUI::Window* EditorItem::GetEditorItemLayout() const { return mInspector->GetEditorItemsLayout()->getChild(mItemName + "/Layout"); }
    CEGUI::Window* EditorItem::GetEditorItemTitle() const { return mInspector->GetEditorItemsLayout()->getChild(mItemName + "/Title"); }
    CEGUI::Window* EditorItem::GetEditorItemDeleteButton() const { return mInspector->GetEditorItemsLayout()->getChild(mItemName + "/Title/Delete"); }

}
