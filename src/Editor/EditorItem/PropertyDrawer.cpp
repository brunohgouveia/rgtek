#include <Editor/EditorItem/PropertyDrawer.h>
#include <Editor/EditorItem.h>
#include <Rendering/CEGUI.h>

namespace RGTek
{
    PropertyDrawer::PropertyDrawer(EditorItem* editorItem, const char* label)
        : mEditorItem(editorItem)
		, mWindowLayout(nullptr)
		, mLabel(label)
    {
        // Do nothing
    }

    PropertyDrawer::~PropertyDrawer()
    {
        if (mWindowLayout) mEditorItem->GetEditorItemLayout()->removeChild(mWindowLayout);
    }

	void PropertyDrawer::BuildLayout()
	{
		mWindowLayout = CEGUI::WindowManager::getSingleton().loadLayoutFromFile(GetLayoutFileName());

		mWindowLayout->setName(mLabel.c_str());
		mWindowLayout->setTooltipText(mLabel.c_str());

		mWindowLayout->getChild("Label")->setText(mLabel.c_str());

		mEditorItem->GetEditorItemLayout()->addChild(mWindowLayout);
	}

	void IntPropertyDrawer::Synchronize()
	{
		CEGUI::Window* value = mWindowLayout->getChild("Value");

		int currentValue = mGetter();
		bool tryToUpdate = false;

		if (!value->isActive()) value->setText(std::to_string(currentValue)); else tryToUpdate = true;

		if (tryToUpdate)
		{
			int newValue = Math::ParseToInteger(value->getText().c_str());

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

	void FloatPropertyDrawer::Synchronize()
	{
		CEGUI::Window* value = mWindowLayout->getChild("Value");

		float currentValue = mGetter();
		bool tryToUpdate = false;

		if (!value->isActive()) value->setText(std::to_string(currentValue)); else tryToUpdate = true;

		if (tryToUpdate)
		{
			float newValue = Math::ParseToFloat(value->getText().c_str());

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

	void StringFieldLayout::Synchronize()
	{
		CEGUI::Window* value = mWindowLayout->getChild("Content");

		String currentValue = mGetter();
		bool tryToUpdate = false;

		if (!value->isActive()) value->setText(currentValue); else tryToUpdate = true;

		if (tryToUpdate)
		{
			String newValue = String(value->getText().c_str());

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

	void Vector3PropertyDrawer::Synchronize()
	{
		CEGUI::Window* x = mWindowLayout->getChild("X");
		CEGUI::Window* y = mWindowLayout->getChild("Y");
		CEGUI::Window* z = mWindowLayout->getChild("Z");

		Vector3 currentValue = mGetter();
		bool tryToUpdate = false;


		if (!x->isActive()) x->setText(std::to_string(currentValue.x)); else tryToUpdate = true;
		if (!y->isActive()) y->setText(std::to_string(currentValue.y)); else tryToUpdate = true;
		if (!z->isActive()) z->setText(std::to_string(currentValue.z)); else tryToUpdate = true;

		if (tryToUpdate)
		{
			Vector3 newValue = Vector3(Math::ParseToFloat(x->getText().c_str()), Math::ParseToFloat(y->getText().c_str()), Math::ParseToFloat(z->getText().c_str()));

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

	void QuaternionPropertyDrawer::Synchronize()
	{
		CEGUI::Window* x = mWindowLayout->getChild("X");
		CEGUI::Window* y = mWindowLayout->getChild("Y");
		CEGUI::Window* z = mWindowLayout->getChild("Z");
		
		Quaternion currentValue = mGetter();
		bool tryToUpdate = false;


		if (!x->isActive()) x->setText(std::to_string(currentValue.x)); else tryToUpdate = true;
		if (!y->isActive()) y->setText(std::to_string(currentValue.y)); else tryToUpdate = true;
		if (!z->isActive()) z->setText(std::to_string(currentValue.z)); else tryToUpdate = true;

		if (tryToUpdate)
		{
			Quaternion newValue = Quaternion(Math::ParseToFloat(x->getText().c_str()), Math::ParseToFloat(y->getText().c_str()), Math::ParseToFloat(z->getText().c_str()));

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

	void ColorPropertyDrawer::Synchronize()
	{
		CEGUI::Window* r = mWindowLayout->getChild("X");
		CEGUI::Window* g = mWindowLayout->getChild("Y");
		CEGUI::Window* b = mWindowLayout->getChild("Z");

		Color currentValue = mGetter();
		bool tryToUpdate = false;

		if (!r->isActive()) r->setText(std::to_string(currentValue.r)); else tryToUpdate = true;
		if (!g->isActive()) g->setText(std::to_string(currentValue.g)); else tryToUpdate = true;
		if (!b->isActive()) b->setText(std::to_string(currentValue.b)); else tryToUpdate = true;

		if (tryToUpdate)
		{
			Color newValue = Color(Math::ParseToFloat(r->getText().c_str()), Math::ParseToFloat(g->getText().c_str()), Math::ParseToFloat(b->getText().c_str()));

			if (currentValue != newValue)
			{
				mSetter(newValue);
			}
		}
	}

}
