//
// Created by Bruno Gouveia on 12/8/15.
//

#include <Editor/MoveHandle3D.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    MoveHandle3D::MoveHandle3D()
    {
		SetSize(1.0f);
    }

    MoveHandle3D::~MoveHandle3D()
    {
		// Do nothing
    }

    void MoveHandle3D::OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        mLastIntersectionPoint = GetRayIntersectionPoint(mouseClickDirection, viewDirection);
    }

    void MoveHandle3D::OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {
        Vector3 intersectionPoint = GetRayIntersectionPoint(mouseClickDirection, viewDirection);

        if (intersectionPoint == mLastIntersectionPoint) return;

        Vector3 mouseDelta = intersectionPoint - mLastIntersectionPoint;
        mLastIntersectionPoint = intersectionPoint;
        switch (mSelectedAxis)
        {
            case Axis::X:
            {
                mTransform.MoveRight(mTransform.GetRotation().GetRight().Dot(mouseDelta));
                break;
            }
            case Axis::Y:
            {
                mTransform.MoveUp(mTransform.GetRotation().GetUp().Dot(mouseDelta));
                break;
            }
            case Axis::Z:
            {
                mTransform.MoveForward(mTransform.GetRotation().GetForward().Dot(mouseDelta));
                break;
            }
            case None:
            {
                // Do nothing
            }
        }
    }

    void MoveHandle3D::OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection)
    {

    }
}