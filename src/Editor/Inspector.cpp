//
// Created by Bruno Gouveia on 1/14/16.
//

#include <Editor/Inspector.h>
#include <Rendering/CEGUI.h>
#include <Application/Application.h>
#include <Editor/EditorSystem.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Inspector::Inspector(EditorSystem* editorSystem, CEGUI::Window* window)
        : mEditorSystem(editorSystem)
        , mEntity(nullptr)
        , mInspectorWindow(window)
        , mInspectorWindowContent(nullptr)
    {
        // Do nothing
    }

    Inspector::~Inspector()
    {
		// Delete items
		for (auto& item : mItems)
		{
			delete item;
		}
		mItems.clear();
    }

    void Inspector::UpdateItems()
    {
        for (auto& item : mItems)
        {
            item->Synchronize();
        }
    }

    void Inspector::Populate(Entity* entity)
    {
        Debugger::AssertNotNull(mInspectorWindow, "Inspector window is null.");
        Debugger::AssertNotNull(entity, "No entity selected.");
        mEntity = entity;

        mInspectorWindowContent = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("inspector.layout");
        mInspectorWindow->addChild(mInspectorWindowContent);

		if (EditorItem* editorItem = new EditorItem(entity->GetTransform(), this))
		{
			mItems.push_back(editorItem);
			editorItem->OnInspectorGUI();
			editorItem->BuildLayout();
		}

		for (Component* component : entity->GetGameComponents())
		{
			String componentClassName = component->GetType().GetName();

			auto editorItemInstantiator = mEditorSystem->GetEditorItemManager().GetEditorItemInstantiator(componentClassName);
			auto* editorItem = (editorItemInstantiator) ? editorItemInstantiator(component, this) : new EditorItem(component, this);
			mItems.push_back(editorItem);
			editorItem->OnInspectorGUI();
			editorItem->BuildLayout();			
		}

        // Add 'Add component' button
        GetMainLayout()->addChild(CreateAddComponentContextMenu(entity));

    }

    void Inspector::CleanUp()
    {
        mEntity = nullptr;
        if (!mInspectorWindowContent)
            return;

        mInspectorWindow->removeChild(mInspectorWindowContent);

        // Delete items
        for (auto& item : mItems)
        {
            delete item;
        }
        mItems.clear();
    }

	void Inspector::AddEditorItem(Component* component)
    {
		if (GetEditorItemsLayout())
		{

			String componentClassName = component->GetType().GetName();

			auto editorItemInstantiator = mEditorSystem->GetEditorItemManager().GetEditorItemInstantiator(componentClassName);
			auto* editorItem = (editorItemInstantiator) ? editorItemInstantiator(component, this) : new EditorItem(component, this);
			editorItem->OnInspectorGUI();
			editorItem->BuildLayout();
			mItems.push_back(editorItem);

		}
    }

    void Inspector::DeleteEditorItem(EditorItem* editorItem)
    {
        if (mEntity)
        {
            mEntity->DeleteComponent(editorItem->GetGameComponent());
        }
        mItems.erase(std::remove(mItems.begin(), mItems.end(), editorItem), mItems.end());
        delete editorItem;

    }

    CEGUI::Window* Inspector::CreateAddComponentContextMenu(Entity* entity)
    {
        CEGUI::Window* addComponent = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("addComponent.layout");
        CEGUI::PushButton* addComponentButton = static_cast<CEGUI::PushButton*>(addComponent->getChild("AddComponent"));

        // Create context menu
        CEGUI::PopupMenu* contextMenu = static_cast<CEGUI::PopupMenu*>(CEGUI::WindowManager::getSingleton().loadLayoutFromFile("contextMenu.layout"));
        addComponent->addChild(contextMenu);

        // Populate items
        CEGUI::MenuItem* menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-MeshRenderer");
        menuItem->setText("MeshRenderer");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            MeshHandle meshHandle = Application::GetSharedApplication()->GetRenderingSystem()->GetResourceManager().CreateResource<Mesh>("models/cube.obj");
			Component* component = Application::GetSharedApplication()->GetRenderingSystem()->CreateMeshRenderer(meshHandle);
            AddEditorItem(component);
            entity->AddComponent(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-LightComponent");
        menuItem->setText("LightComponent");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
			Component* component = Application::GetSharedApplication()->GetRenderingSystem()->CreateLightComponent(Color::white);
            AddEditorItem(component);
            entity->AddComponent(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-CameraComponent");
        menuItem->setText("CameraComponent");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            Component* component = Application::GetSharedApplication()->GetRenderingSystem()->CreateCameraComponent();
            AddEditorItem(component);
            entity->AddComponent(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-Rigidbody");
        menuItem->setText("Rigidbody");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
			Component* component = Application::GetSharedApplication()->GetPhysicsSystem()->CreateRigidbody(1.0);
            AddEditorItem(component);
            entity->AddComponent(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-BoxCollider");
        menuItem->setText("Box Collider");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
			Component* component = Application::GetSharedApplication()->GetPhysicsSystem()->CreateComponentByName(BoxCollider::COMPONENT_NAME);
            entity->AddComponent(component);
            AddEditorItem(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-SphereCollider");
        menuItem->setText("Sphere Collider");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
			Component* component = Application::GetSharedApplication()->GetPhysicsSystem()->CreateSphereCollider(Vector4(0, 0, 0, 1), 1.0);
            entity->AddComponent(component);
            AddEditorItem(component);
            return true;
        });
        contextMenu->addChild(menuItem);

        menuItem = static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MenuItem"));
        menuItem->setName("MenuItem-Cancel");
        menuItem->setText("Cancel");
        menuItem->subscribeEvent(CEGUI::MenuItem::EventClicked, [=](const CEGUI::EventArgs&) -> bool
        {
            contextMenu->hide();
            return true;
        });
        contextMenu->addChild(menuItem);

        addComponentButton->subscribeEvent(CEGUI::PushButton::EventClicked, [=] (const CEGUI::EventArgs&) -> bool
        {
            using namespace CEGUI;

            UVector2 popup_pos;
            popup_pos.d_x = addComponentButton->getPosition().d_x;
            popup_pos.d_y = addComponentButton->getPosition().d_y + addComponentButton->getSize().d_height;

            USize size = contextMenu->getSize();
            size.d_width = addComponentButton->getSize().d_width;

            // set position of menu so it's around where the mouse was clicked.
            contextMenu->setPosition(popup_pos );
            contextMenu->setSize(size);

            // show the menu.
            contextMenu->show();

            // event was handled.
            return true;
        });

        return addComponent;
    }

    CEGUI::Window* Inspector::GetEditorItemsLayout()
    {
        if (mInspectorWindowContent)
        {
            return mInspectorWindowContent->getChild("ScrollablePane/MainLayout/EditorItemsLayout");
        }
        else
        {
            return nullptr;
        }
    }

    CEGUI::Window* Inspector::GetMainLayout()
    {
        if (mInspectorWindowContent)
        {
            return mInspectorWindowContent->getChild("ScrollablePane/MainLayout");
        }
        else
        {
            return nullptr;
        }
    }

}