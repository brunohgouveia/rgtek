//
// Created by Bruno Gouveia on 12/11/15.
//

#include <Rendering/RenderingBackend.h>
#include <Rendering/RenderingSystem.h>
#include <GL/glew.h>

namespace RGTek
{

    RenderingBackend::RenderingBackend(RenderingSystemWeakPtr renderingSystem)
        : mRenderingSystem(renderingSystem)
    {
        mWireFrameShader = mRenderingSystem->GetResourceManager().CreateResource<Shader>("shaders/Util/DrawLine.shader");

        glGenVertexArrays(1, &mLineVertexArray);
        glBindVertexArray(mLineVertexArray);
        float initialData2[8] = {0};
        mLineVertexBuffer.Init(initialData2, 8, GL_STATIC_DRAW);
        mLineVertexBuffer.VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    }

    void RenderingBackend::DrawLine(const Line &line, const Color &color)
    {
        auto lineMapIterator = mLinesMap.find(color);
        if (lineMapIterator != mLinesMap.end())
        {
            Vector<Line>& lineSet = lineMapIterator->second;
            lineSet.push_back(line);
        }
        else
        {
            Vector<Line> newLineSet;
            newLineSet.push_back(line);
            mLinesMap[color] = newLineSet;
        }
    }

    void RenderingBackend::DrawRay(const Ray &ray, const Color &color)
    {
        DrawLine(Line(ray.origin, ray.origin + ray.direction), color);
    }

    void RenderingBackend::DrawWireArc(const Vector3 &center, const Vector3 &normal, const Vector3 &direction, const Color& color, float angle,
                                   float radius)
    {
        const float radians = ToRadians(Math::Abs(angle / 2.0f));

        unsigned int numberOfSegments = static_cast<unsigned int>(Math::Abs(angle)) / 10;

        Vector3 orthogonalVector = normal.Cross(direction).Normalized();

        Vector3 currentPoint = center + Math::Cos(-radians) * direction + Math::Sin(-radians) * orthogonalVector;;
        for (unsigned int i = 0; i < numberOfSegments; i++)
        {
            float newRadian = -radians + ((2 * (i + 1)) * radians / numberOfSegments);

            Vector3 newPoint = center + Math::Cos(newRadian) * direction + Math::Sin(newRadian) * orthogonalVector;

            mRenderingSystem->GetBackend().DrawLine(Line(currentPoint, newPoint));
            currentPoint = newPoint;

        }
    }

    void RenderingBackend::DrawWireDisc(const Vector3& center, const Vector3& normal, const Color& color, float radius, bool culling)
    {
        bool isNormalUpVector = Math::IsEqual(Math::Abs(normal.Normalized().Dot(Vector3::up)), 1.0f);
        Vector3 uVector = (isNormalUpVector) ? normal.Cross(Vector3::forward).Normalized() : normal.Cross(Vector3::up).Normalized();
        Vector3 vVector = normal.Cross(uVector).Normalized();

        Vector3 currentPoint = center + vVector * radius;
        bool validLine = false;
        for (unsigned int i = 0; i <= 128; i++)
        {
            Vector3 pointDisplacement = radius * (Math::Sin((2.0f * Math::pi / 128.0f) * i) * uVector + Math::Cos((2.0f * Math::pi / 128.0f) * i) * vVector);
            Vector3 newPoint = center + pointDisplacement;

            Vector3 viewDirection = mRenderingSystem->GetCurrentCamera()->GetTransform()->GetPosition() - newPoint;
            if (!culling || (pointDisplacement.Dot(viewDirection) >= 0.01f))
            {
                // Check if previous point was valid.
                if (validLine)
                {
                    mRenderingSystem->GetBackend().DrawLine(Line(Vector4(currentPoint, 1.0f), Vector4(newPoint, 1.0f)),
                                                            color);
                }

                // This point is valid.
                validLine = true;
            }
            else
            {
                validLine = false;
            }

            currentPoint = newPoint;
        }
    }

    void RenderingBackend::DrawClampedWireDisc(const Circle& circle, const Vector3& center, const Vector3& normal,
                                               const Color& color, float radius, bool culling)
    {
        Matrix4 viewProjectionMatrix = mRenderingSystem->GetCurrentCamera()->GetViewProjection();
        Vector3 viewDirection = (mRenderingSystem->GetCurrentCamera()->GetTransform()->GetPosition() - center).Normalized();

        bool isNormalUpVector = Math::IsEqual(Math::Abs(normal.Normalized().Dot(Vector3::up)), 1.0f);
        Vector3 uVector = (isNormalUpVector) ? normal.Cross(Vector3::forward).Normalized() : normal.Cross(Vector3::up).Normalized();
        Vector3 vVector = normal.Cross(uVector).Normalized();

        Vector3 centerProjected = viewProjectionMatrix.Transform(center);

        Vector3 currentPoint;
        bool validLine = false;
        for (unsigned int i = 0; i <= 128; i++)
        {
            Vector3 pointDisplacement = radius * (Math::Sin((2.0f * Math::pi / 128.0f) * i) * uVector + Math::Cos((2.0f * Math::pi / 128.0f) * i) * vVector);
            Vector3 newPoint = center + pointDisplacement;
            Vector3 newPointOnCircle = center + pointDisplacement.ProjectOnto(Plane(circle.center, circle.normal)).Normalized() * circle.radius;

            Vector3 newPointProjected = viewProjectionMatrix.Transform(newPoint);
            Vector3 newPointOnCircleProjected = viewProjectionMatrix.Transform(newPointOnCircle);

            if (!culling || (pointDisplacement.Dot(viewDirection) >= 0.0f))
            {
                if ((newPointOnCircleProjected - centerProjected).Magnitude() <= (newPointProjected - centerProjected).Magnitude())
                {
                    newPoint = newPointOnCircle;
                }

                if (validLine)
                {
                    mRenderingSystem->GetBackend().DrawLine(Line(currentPoint, newPoint), color);
                }
                validLine = true;
            }
            else
            {
                validLine = false;
            }
            currentPoint = newPoint;
        }
    }

    void RenderingBackend::Dump()
    {
        ShaderWeakPtr wireFrameShader = mRenderingSystem->GetResourceManager().GetResource<Shader>(mWireFrameShader);

        wireFrameShader->UseShader();
        mRenderingSystem->UpdateUniformsToShader(wireFrameShader);
        glBindVertexArray(mLineVertexArray);

        for (auto& lineMapPair : mLinesMap)
        {
            Vector<Line>& lineSet = lineMapPair.second;

			// Test if lineSet is empty
			if (lineSet.empty())
				continue;

            wireFrameShader->SetUniform("LineColor", lineMapPair.first);

            mLineVertexBuffer.Data(static_cast<float*>(lineSet[0]), lineSet.size() * 8);
            mLineVertexBuffer.Bind();

            glDrawArrays(GL_LINES, 0, lineSet.size() * 2);

            lineSet.clear();
        }
    }

}