#include <Rendering/RenderingSystem.h>

#include <Core/Camera.h>
#include <Util/Parser/DataTranslator.h>
#include <Tools/Debugger.h>
#include <GL/glew.h>

namespace RGTek
{
	RSYSTEM_IMPLEMENTATION(RenderingSystem);

    RenderingSystem::RenderingSystem(Window* window)
        : mWindow(window)
        , mResourceManager("../res/")
        , mRenderingBackend(this)
        , mActiveLight(nullptr)
    {
        Debugger::Debug("RenderingSystem start");

        mBasicShader = mResourceManager.CreateResource<Shader>("shaders/BasicShader.shader");
        mShadowMapShader = mResourceManager.CreateResource<Shader>("shaders/Commons/ShadowMap.shader");

        mShadowTexture = mResourceManager.CreateResource<Texture>("ShadowTexture", 1, 2048, 2048, true);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        Debugger::CheckOpenGLError();

        //Debugger::InitDrawAssets();
	}

    RenderingSystem::~RenderingSystem()
	{
        Debugger::Debug("RenderingSystem shutdown");

        mResourceManager.DestroyAllResources();

        /*ApplicationWeakPtr app = Application::GetSharedApplication();
        Debugger::AssertNotNull(app, "Application should not be null");
        Debugger::AssertFalse(app->IsRunning(), "Application should not be deleted while is running");*/
	}

	void RenderingSystem::UpdateUniformsToShader(ShaderWeakPtr shader)
	{
        Debugger::AssertNotNull(mCurrentCamera, "Camera null");

        mCurrentCamera->UpdateUniformsToShader(shader);

        shader->SetUniform("NormalMatrix", mCurrentCamera->GetView().Inverse().Transpose());

        shader->SetUniform("Background", Vector4(0.0f, 1.0f, 0.0f, 1.0f));
        shader->SetUniform("LightAmbient", Vector4(0.3f, 0.3f, 0.3f, 1.0f));
        if (mActiveLight)
        {
            mActiveLight->UpdateUniformsToShader(shader);
        }
    }

    void RenderingSystem::Render(const CameraPtr &camera, const Viewport &viewport)
    {
        SetCurrentCamera(camera);
        Render(viewport);
    }

    void RenderingSystem::Render(const Viewport& viewport)
    {
        GetWindow()->BindCanvasAsRenderTarget(viewport);
        Camera::GetMainCamera()->SetViewport(viewport);

        glEnable(GL_SCISSOR_TEST);
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDepthFunc(GL_LEQUAL);

        // Front face culling
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);

//        PhysicsSystemPtr pe = Application::GetSharedApplication()->GetPhysicsSystem();
////        pe->RenderBVH();
////        pe->RenderRay();
//        pe->RenderDebugInfo();

        mResourceManager.GetResource<Shader>(mBasicShader)->UseShader();
        UpdateUniformsToShader(mResourceManager.GetResource<Shader>(mBasicShader));

        RenderComponents(mBasicShader);

        //Debugger::FlushDrawRays();
        mRenderingBackend.Dump();

        for (LightComponentPtr lightComponent : mLightCompeonts)
        {
            // Disable blend
            glDisable(GL_BLEND);
            // Set depth func (because at this points, it's probably GL_EQUAL)
            glDepthFunc(GL_LEQUAL);

            mActiveLight = lightComponent->GetLight();

            TextureWeakPtr shadowTexture = mResourceManager.GetResource<Texture>(mShadowTexture);
            shadowTexture->BindAsRenderTarget();
            glClear(GL_DEPTH_BUFFER_BIT);

            mResourceManager.GetResource<Shader>(mShadowMapShader)->UseShader();
            UpdateUniformsToShader(mResourceManager.GetResource<Shader>(mShadowMapShader));

            glEnable(GL_DEPTH_CLAMP);
            RenderComponents(mShadowMapShader);
            glDisable(GL_DEPTH_CLAMP);


            // Render to window
            GetWindow()->BindCanvasAsRenderTarget(viewport);

            glEnable(GL_BLEND);
            glBlendFunc(GL_ONE, GL_ONE);
            glDepthFunc(GL_EQUAL);

            RenderComponents();
            glDisable(GL_BLEND);
        }
        mActiveLight = NULL;

        Debugger::CheckOpenGLError();

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_SCISSOR_TEST);
        GetWindow()->BindAsRenderTarget();
    }

    void RenderingSystem::RenderComponents()
    {
        TextureWeakPtr shadowTexture = mResourceManager.GetResource<Texture>(mShadowTexture);
        shadowTexture->Active();
        shadowTexture->Bind();
        for (auto& meshRendererComponent : mMeshRendererComponents)
        {
            const MeshWeakPtr mesh = mResourceManager.GetResource<Mesh>(meshRendererComponent->GetMesh());

            for (unsigned int i = 0; i < mesh->GetSubMeshes().size(); ++i)
            {
                MeshDataWeakPtr subMesh = mesh->GetSubMeshes()[i];
                MaterialWeakPtr material = mResourceManager.GetResource<Material>(mesh->GetMaterials()[i]);

                ShaderWeakPtr shader = mResourceManager.GetResource<Shader>(material->GetProgram());
                shader->UseShader();
                UpdateUniformsToShader(shader);

                shader->SetUniform("ModelMatrix", meshRendererComponent->GetEntity()->GetTransform()->GetTransformation());
                material->UpdateUniformsToShader(shader, &mResourceManager);

                subMesh->Draw();
            }
        }
    }

    void RenderingSystem::RenderComponents(ShaderHandle shaderHandle)
    {
        for (auto& meshRendererComponent : mMeshRendererComponents)
        {
            const MeshWeakPtr mesh = mResourceManager.GetResource<Mesh>(meshRendererComponent->GetMesh());

            for (unsigned int i = 0; i < mesh->GetSubMeshes().size(); ++i)
            {
                MeshDataWeakPtr subMesh = mesh->GetSubMeshes()[i];
                MaterialWeakPtr material = mResourceManager.GetResource<Material>(mesh->GetMaterials()[i]);

                ShaderWeakPtr shader = mResourceManager.GetResource<Shader>(shaderHandle);
                shader->SetUniform("ModelMatrix", meshRendererComponent->GetEntity()->GetTransform()->GetTransformation());
                material->UpdateUniformsToShader(shader, &mResourceManager);

                subMesh->Draw();
            }
        }
    }

    MeshRendererWeakPtr RenderingSystem::CreateMeshRenderer(MeshHandle mesh)
    {
        MeshRendererPtr newComponent = new MeshRenderer(mesh);
        newComponent->SetSystem(this);
        mMeshRendererComponents.push_back(newComponent);

        return static_cast<MeshRendererWeakPtr>(newComponent);
    }

    CameraComponentWeakPtr RenderingSystem::CreateCameraComponent()
    {
        CameraComponentPtr newComponent = new CameraComponent();
        newComponent->SetSystem(this);
        mCameraComponents.push_back(newComponent);

        return static_cast<CameraComponentWeakPtr>(newComponent);
    }

    LightComponentWeakPtr RenderingSystem::CreateLightComponent(const Color& color, float intensity)
    {
        LightComponentPtr newComponent = new LightComponent(color, intensity);
        newComponent->SetSystem(this);
        mLightCompeonts.push_back(newComponent);

        return static_cast<LightComponentWeakPtr>(newComponent);
    }

    CameraWeakPtr RenderingSystem::GetCurrentCamera() const
    {
        return mCurrentCamera;
    }

    ResourceManager& RenderingSystem::GetResourceManager()
    {
        return mResourceManager;
    }

    RenderingBackend& RenderingSystem::GetBackend()
    {
        return mRenderingBackend;
    }

    void RenderingSystem::RegisterToWindow(WindowPtr window)
    {
        window->SetDrawCallback([&] (Vector2 position, Vector2 size) {
            Vector2 canvasPos(position.x, position.y);
            Vector2 canvasSize(size.x, size.y);

            Viewport viewport(canvasPos, canvasSize);
            Render(Camera::GetMainCamera(), viewport);
        });
    }

	Component* RenderingSystem::CreateComponentByName(const String& componentClass)
    {
        if (componentClass == MeshRenderer::COMPONENT_NAME)
        {
            MeshRendererWeakPtr meshRenderer = new MeshRenderer();
            meshRenderer->SetSystem(this);
            mMeshRendererComponents.push_back(meshRenderer);
            return  meshRenderer;
        }
        else if (componentClass == LightComponent::COMPONENT_NAME)
        {
            LightComponentWeakPtr lightComponent = new LightComponent(Color::white);
            lightComponent->SetSystem(this);
            mLightCompeonts.push_back(lightComponent);
            return lightComponent;
        }
        else if (componentClass == CameraComponent::COMPONENT_NAME)
        {
            CameraComponentWeakPtr cameraComponent = new CameraComponent();
            cameraComponent->SetSystem(this);
            mCameraComponents.push_back(cameraComponent);
            return cameraComponent;
        }

		Debugger::Error("Component [%s] doesn't belong to RenderingSystem.", componentClass.c_str());
        return nullptr;
    }

    void RenderingSystem::DeleteComponent(Component *component)
    {
        if (MeshRenderer* meshRenderer = dynamic_cast<MeshRenderer*>(component))
        {
            mMeshRendererComponents.erase(
                    std::remove(
                            mMeshRendererComponents.begin(),
                            mMeshRendererComponents.end(),
                            meshRenderer
                    ),
                    mMeshRendererComponents.end()
            );
        }
        else if (LightComponent* lightComponent = dynamic_cast<LightComponent*>(component))
        {
            mLightCompeonts.erase(
                    std::remove(
                            mLightCompeonts.begin(),
                            mLightCompeonts.end(),
                            lightComponent
                    ),
                    mLightCompeonts.end()
            );
        }
        else if (CameraComponent* cameraComponent = dynamic_cast<CameraComponent*>(component))
        {
            mCameraComponents.erase(
                    std::remove(
                            mCameraComponents.begin(),
                            mCameraComponents.end(),
                            cameraComponent
                    ),
                    mCameraComponents.end()
            );
        }
    }

    void RenderingSystem::DeleteComponentsOfEntity(Entity* entity)
    {
        mMeshRendererComponents.erase(
                std::remove_if(
                        mMeshRendererComponents.begin(),
                        mMeshRendererComponents.end(),
                        [&] (const MeshRendererPtr& meshRendererPtr) -> bool
                        {
                            return meshRendererPtr->GetEntity() == entity;
                        }
                ),
                mMeshRendererComponents.end()
        );

        mLightCompeonts.erase(
                std::remove_if(
                        mLightCompeonts.begin(),
                        mLightCompeonts.end(),
                        [&] (const LightComponentPtr& lightComponentPtr) -> bool
                        {
                            return lightComponentPtr->GetEntity() == entity;
                        }
                ),
                mLightCompeonts.end()
        );

        mCameraComponents.erase(
                std::remove_if(
                        mCameraComponents.begin(),
                        mCameraComponents.end(),
                        [&] (const CameraComponentPtr& cameraComponentPtr) -> bool
                        {
                            return cameraComponentPtr->GetEntity() == entity;
                        }
                ),
                mCameraComponents.end()
        );
    }

}
