//
// Created by Bruno Gouveia on 11/22/15.
//
#include <Core/Camera.h>
#include <Rendering/GameWindow.h>
#include <System/Input.h>
#include <System/InputMap.h>
#include <Tools/Debugger.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#undef main

#ifdef RGTEK_CEGUI_ENABLED
#include <Rendering/CEGUI.h>
#endif

namespace RGTek
{

    GameWindow::GameWindow(const string& title, int width, int height)
        : Window(title, width, height)
    {
        Debugger::Debug("GameWindow starting...");

#ifdef RGTEK_CEGUI_ENABLED
        InitCEGUI();
        CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Sizef(GetFramebufferSize().x, GetFramebufferSize().y));
        CEGUI::Window* root = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("gameLayout.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(root);
#endif
    }

    GameWindow::~GameWindow()
    {
        Debugger::Debug("GameWindow shutting down...");
    }

    void GameWindow::InitCEGUI()
    {
#ifdef RGTEK_CEGUI_ENABLED
        // create renderer and enable extra states
        CEGUI::OpenGL3Renderer& cegui_renderer = CEGUI::OpenGL3Renderer::create(CEGUI::Sizef(800.f, 600.f));
        cegui_renderer.enableExtraStateSettings(true);

        // create CEGUI system object
        CEGUI::System::create(cegui_renderer);

        // setup resource directories
        CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
        rp->setResourceGroupDirectory("schemes", "../datafiles/schemes/");
        rp->setResourceGroupDirectory("imagesets", "../datafiles/imagesets/");
        rp->setResourceGroupDirectory("fonts", "../datafiles/fonts/");
        rp->setResourceGroupDirectory("layouts", "../datafiles/layouts/");
        rp->setResourceGroupDirectory("looknfeels", "../datafiles/looknfeel/");
        rp->setResourceGroupDirectory("lua_scripts", "../datafiles/lua_scripts/");
        rp->setResourceGroupDirectory("schemas", "../datafiles/xml_schemas/");

        // set default resource groups
        CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
        CEGUI::Font::setDefaultResourceGroup("fonts");
        CEGUI::Scheme::setDefaultResourceGroup("schemes");
        CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
        CEGUI::WindowManager::setDefaultResourceGroup("layouts");
        CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

        CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
        if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
            parser->setProperty("SchemaDefaultResourceGroup", "schemas");

        // load TaharezLook scheme and DejaVuSans-10 font
        CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme", "schemes");
        CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme", "schemes");
        CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-10.font");

        // set default font and cursor image and tooltip type
        CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
        CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("WindowsLook/Tooltip");
#endif
    }

    void GameWindow::Render()
    {
        mCanvasCallback(Vector2(0.0f, 0.0f), GetFramebufferSize());

#ifdef RGTEK_CEGUI_ENABLED
        static float time = SDL_GetTicks() / 1000.f;
        const float newtime = SDL_GetTicks() / 1000.f;
        const float time_elapsed = newtime - time;
        CEGUI::System::getSingleton().injectTimePulse(time_elapsed);
        CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(time_elapsed);
        time = newtime;

        CEGUI::System::getSingleton().getRenderer()->beginRendering();
        CEGUI::System::getSingleton().getDefaultGUIContext().draw();
        CEGUI::System::getSingleton().getRenderer()->endRendering();
#endif

        Input::ResetMap();
        Input::EndMouseInjection();
    }

    void GameWindow::HandleEvents()
    {
        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    mIsCloseRequested = true;
                    break;

                case SDL_MOUSEMOTION:
                {
                    Vector2 windowPD = GetWindowPixelDensity();
                    Input::mousePosition = Vector2(windowPD.x * event.motion.x, GetFramebufferSize().y - windowPD.y * event.motion.y);
                    Input::sInputActive = true;

#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(windowPD.x * event.motion.x,windowPD.y * event.motion.y);
#endif
                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                {
                    Input::InjectMouseDown(event.button.button - 1u);

#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(
                            static_cast<CEGUI::MouseButton>(InputMap::SDLtoCEGUIMouseButton(event.button.button))
                    );
#endif
                    break;
                }
                case SDL_MOUSEBUTTONUP:
                {
                    Input::InjectMouseUp(event.button.button - 1u);

#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(
                            static_cast<CEGUI::MouseButton>(InputMap::SDLtoCEGUIMouseButton(event.button.button))
                    );
#endif
                    break;
                }
                case SDL_MOUSEWHEEL:
                {
                    Input::mouseWheel = event.wheel.y;

#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseWheelChange(
                            static_cast<float>(event.wheel.y));
#endif
                    break;
                }
                case SDL_KEYDOWN:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode));

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt);
                    }
#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(
                            static_cast<CEGUI::Key::Scan>(InputMap::toCEGUIKey(event.key.keysym.scancode))
                    );
#endif
                    break;
                }
                case SDL_KEYUP:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode), false);

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt, false);
                    }
#ifdef RGTEK_CEGUI_ENABLED
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(
                            static_cast<CEGUI::Key::Scan>(InputMap::toCEGUIKey(event.key.keysym.scancode))
                    );
#endif
                    break;
                }
                case SDL_TEXTINPUT:
                    InputMap::InjectUTF8Text(event.text.text);
                    break;

                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                    {
                        Vector2 framebufferSize = GetFramebufferSize();
#ifdef RGTEK_CEGUI_ENABLED
                        CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Sizef(framebufferSize.x, framebufferSize.y));
#endif
                        glViewport(0, 0, static_cast<int>(framebufferSize.x), static_cast<int>(framebufferSize.y));
                    }
#ifdef RGTEK_CEGUI_ENABLED
                    else if (event.window.event == SDL_WINDOWEVENT_LEAVE)
                    {
                        CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseLeaves();
                    }
#endif
                    break;

                default:
                    break;

            }
        }
    }

}
