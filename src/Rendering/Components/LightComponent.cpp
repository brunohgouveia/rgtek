#include <Rendering/Components/LightComponent.h>
#include <Rendering/RenderingSystem.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
	RCOMPONENT_IMPLEMENTATION(LightComponent);

	///////////////////////////////////////////////////////////////////////
    
    LightComponent::LightComponent(const Color& color, float intensity) :
        mLight(new SpotLight(color, intensity))
    {
        Debugger::Debug("LightComponent start");
    }

	///////////////////////////////////////////////////////////////////////
    
    LightComponent::~LightComponent()
    {
        Debugger::Debug("LightComponent shutdown");
    }

	///////////////////////////////////////////////////////////////////////

    void LightComponent::RenderGizmos()
    {
//        ApplicationWeakPtr app = Application::GetSharedApplication();
//        Debugger::AssertNotNull(app, "Application should not be null.");
//        RenderingSystemWeakPtr renderingSystem = app->GetRenderingSystem();
//        Debugger::AssertNotNull(renderingSystem, "RenderingSystem should not be null.");
//
//        TransformWeakPtr transform = GetEntity()->GetTransform();
//        Vector3 viewDirection = (renderingSystem->GetCurrentCamera()->GetTransform()->GetPosition() - transform->GetPosition()).Normalized();
//
//        Vector3 position    = transform->GetPosition();
//        Quaternion rotation = transform->GetRotation();
//
//        renderingSystem->GetBackend().DrawClampedWireDisc(Circle(position, viewDirection, 4.0f), position, rotation.GetUp(), Color::yellow, 4.0f, true);
//        renderingSystem->GetBackend().DrawClampedWireDisc(Circle(position, viewDirection, 4.0f), position, rotation.GetRight(), Color::yellow, 4.0f, true);
//        renderingSystem->GetBackend().DrawClampedWireDisc(Circle(position, viewDirection, 4.0f), position, rotation.GetForward(), Color::yellow, 4.0f, true);
//        renderingSystem->GetBackend().DrawWireDisc(position, viewDirection, Color::yellow, 4.0f);
        if (SpotLight* spotLight = dynamic_cast<SpotLight*>(GetLight()))
        {
            Matrix4 viewProjectionInverse = spotLight->GetViewProjection().Inverse();
            Vector4 leftDownBack = viewProjectionInverse * Vector4(-1.0f, -1.0f, -1.0f);
            Vector4 leftUpBack = viewProjectionInverse * Vector4(-1.0f, +1.0f, -1.0f);
            Vector4 rightUpBack = viewProjectionInverse * Vector4(+1.0f, +1.0f, -1.0f);
            Vector4 rightDownBack = viewProjectionInverse * Vector4(+1.0f, -1.0f, -1.0f);
            Vector4 leftDownFwd = viewProjectionInverse * Vector4(-1.0f, -1.0f, +1.0f);
            Vector4 leftUpFwd = viewProjectionInverse * Vector4(-1.0f, +1.0f, +1.0f);
            Vector4 rightUpFwd = viewProjectionInverse * Vector4(+1.0f, +1.0f, +1.0f);
            Vector4 rightDownFwd = viewProjectionInverse * Vector4(+1.0f, -1.0f, +1.0f);

            RenderingSystemWeakPtr renderingSystem = static_cast<RenderingSystem*>(GetSystem());
            Debugger::AssertNotNull(renderingSystem, "RenderingSystem should not be null.");

            Color color = Color::yellow;
            renderingSystem->GetBackend().DrawLine(Line(leftDownBack, leftUpBack), color);
            renderingSystem->GetBackend().DrawLine(Line(leftUpBack, rightUpBack), color);
            renderingSystem->GetBackend().DrawLine(Line(rightUpBack, rightDownBack), color);
            renderingSystem->GetBackend().DrawLine(Line(rightDownBack, leftDownBack), color);

            renderingSystem->GetBackend().DrawLine(Line(leftDownFwd, leftUpFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(leftUpFwd, rightUpFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(rightUpFwd, rightDownFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(rightDownFwd, leftDownFwd), color);

            renderingSystem->GetBackend().DrawLine(Line(leftDownBack, leftDownFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(leftUpBack, leftUpFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(rightDownBack, rightDownFwd), color);
            renderingSystem->GetBackend().DrawLine(Line(rightUpBack, rightUpFwd), color);
        }
    }

	///////////////////////////////////////////////////////////////////////

    void LightComponent::RegisterToSystem()
    {
        RenderingSystemWeakPtr renderingSystem = static_cast<RenderingSystem*>(GetSystem());
        Debugger::AssertNotNull(renderingSystem, "RenderingSystem should not be null.");

        renderingSystem->AddLight(mLight);
    }

	///////////////////////////////////////////////////////////////////////

    void LightComponent::SetEntity(Entity* entity)
    {
        // Call base method
        Debugger::AssertNotNull(entity, "Setting a null entity.");
        Component::SetEntity(entity);

        // Set transform
        TransformPtr entityTransform = entity->GetTransform();
        Debugger::AssertNotNull(entityTransform, "Transform should not be null.");
        mLight->SetTransform(entityTransform);
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* LightComponent::GetDataTranslator()
    {
        auto* dataTranslator = new CustomObjectDataTranslator<SpotLight>(COMPONENT_NAME, dynamic_cast<SpotLight*>(GetLight()));

        dataTranslator->Add("Color", DataTranslatorProperty<SpotLight, Color>(dynamic_cast<SpotLight*>(GetLight()), &SpotLight::GetColorRef, &SpotLight::SetColorRef));
        dataTranslator->Add("Intensity", DataTranslatorProperty<SpotLight, float>(dynamic_cast<SpotLight*>(GetLight()), &SpotLight::GetIntensityRef, &SpotLight::SetIntensityRef));
        dataTranslator->Add("LightBias", DataTranslatorProperty<SpotLight, float>(dynamic_cast<SpotLight*>(GetLight()), &SpotLight::GetLightBiasRef, &SpotLight::SetLightBiasRef));

        dataTranslator->Add("Range", DataTranslatorProperty<SpotLight, float>(dynamic_cast<SpotLight*>(GetLight()), &SpotLight::GetRangeRef, &SpotLight::SetRangeRef));
        dataTranslator->Add("SpotAngle", DataTranslatorProperty<SpotLight, float>(dynamic_cast<SpotLight*>(GetLight()), &SpotLight::GetSpotAngleRef, &SpotLight::SetSpotAngleRef));

        return dataTranslator;
    }

}