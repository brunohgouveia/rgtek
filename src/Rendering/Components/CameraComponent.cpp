#include <Rendering/Components/CameraComponent.h>
#include <Rendering/RenderingSystem.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
	RCOMPONENT_IMPLEMENTATION(CameraComponent);

	///////////////////////////////////////////////////////////////////////

    CameraComponent::CameraComponent() :
        mCamera(new Camera())
    {
        Debugger::Debug("CameraComponent start");
    }

	///////////////////////////////////////////////////////////////////////

    CameraComponent::~CameraComponent()
    {
		if (Camera::GetMainCamera() == mCamera)
		{
			Camera::SetMainCamera(nullptr);
		}
        Debugger::Debug("CameraComponent shutdown");
    }

	///////////////////////////////////////////////////////////////////////
    
    void CameraComponent::RegisterToSystem()
    {
        Camera::SetMainCamera(mCamera);
    }

	///////////////////////////////////////////////////////////////////////

    void CameraComponent::RenderGizmos()
    {
        Matrix4 viewProjectionInverse = mCamera->GetViewProjection().Inverse();
        Vector4 leftDownBack  = viewProjectionInverse * Vector4(-1.0f, -1.0f, -1.0f);
        Vector4 leftUpBack    = viewProjectionInverse * Vector4(-1.0f, +1.0f, -1.0f);
        Vector4 rightUpBack   = viewProjectionInverse * Vector4(+1.0f, +1.0f, -1.0f);
        Vector4 rightDownBack = viewProjectionInverse * Vector4(+1.0f, -1.0f, -1.0f);
        Vector4 leftDownFwd   = viewProjectionInverse * Vector4(-1.0f, -1.0f, +1.0f);
        Vector4 leftUpFwd     = viewProjectionInverse * Vector4(-1.0f, +1.0f, +1.0f);
        Vector4 rightUpFwd    = viewProjectionInverse * Vector4(+1.0f, +1.0f, +1.0f);
        Vector4 rightDownFwd  = viewProjectionInverse * Vector4(+1.0f, -1.0f, +1.0f);

        RenderingSystemWeakPtr renderingSystem = static_cast<RenderingSystem*>(GetSystem());
        Debugger::AssertNotNull(renderingSystem, "RenderingSystem should not be null.");

        Color color = Color::white;
        renderingSystem->GetBackend().DrawLine(Line(leftDownBack, leftUpBack), color);
        renderingSystem->GetBackend().DrawLine(Line(leftUpBack, rightUpBack), color);
        renderingSystem->GetBackend().DrawLine(Line(rightUpBack, rightDownBack), color);
        renderingSystem->GetBackend().DrawLine(Line(rightDownBack, leftDownBack), color);

        renderingSystem->GetBackend().DrawLine(Line(leftDownFwd, leftUpFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(leftUpFwd, rightUpFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(rightUpFwd, rightDownFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(rightDownFwd, leftDownFwd), color);

        renderingSystem->GetBackend().DrawLine(Line(leftDownBack, leftDownFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(leftUpBack, leftUpFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(rightDownBack, rightDownFwd), color);
        renderingSystem->GetBackend().DrawLine(Line(rightUpBack, rightUpFwd), color);
    }

	///////////////////////////////////////////////////////////////////////

    void CameraComponent::SetEntity(Entity* entity)
    {
        // Call base method
        Debugger::AssertNotNull(entity, "Setting a null entity.");
        Component::SetEntity(entity);

        // Set transform
        TransformPtr entityTransform = entity->GetTransform();
        Debugger::AssertNotNull(entityTransform, "Transform should not be null.");
        mCamera->SetTransform(entityTransform);
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* CameraComponent::GetDataTranslator()
    {
        Camera* camera = GetCamera();
        auto* dataTranslator = new CustomObjectDataTranslator<Camera>(COMPONENT_NAME, camera);

        dataTranslator->Add("FieldOfView", DataTranslatorProperty<Camera, float>(camera, &Camera::GetFieldOfViewRef, &Camera::SetFieldOfViewRef));
        dataTranslator->Add("NearClip", DataTranslatorProperty<Camera, float>(camera, &Camera::GetNearClipRef, &Camera::SetNearClipRef));
        dataTranslator->Add("FarClip", DataTranslatorProperty<Camera, float>(camera, &Camera::GetFarClipRef, &Camera::SetFarClipRef));

        return dataTranslator;
    }

}