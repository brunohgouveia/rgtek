#include <Rendering/Components/MeshRenderer.h>
#include <Rendering/RenderingSystem.h>
#include <Math/Vector4.h>
#include <Gl/Texture.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
    using namespace Math;

	RCOMPONENT_IMPLEMENTATION(MeshRenderer);

	///////////////////////////////////////////////////////////////////////

	MeshRenderer::MeshRenderer()
	{
		// Do nothing
	}

	///////////////////////////////////////////////////////////////////////

	MeshRenderer::MeshRenderer(MeshHandle mesh)
        : mMesh(mesh)
    {
        Debugger::Debug("MeshRenderer starting...");
    }

	///////////////////////////////////////////////////////////////////////

    MeshRenderer::~MeshRenderer()
    {
        Debugger::Debug("MeshRenderer shutting down...");
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* MeshRenderer::GetDataTranslator()
    {
        auto dataTranslator = new CustomObjectDataTranslator<MeshRenderer>("MeshRenderer", this);
        dataTranslator->Add("MeshName", DataTranslatorProperty<MeshRenderer, String>(this, &MeshRenderer::GetMeshName, &MeshRenderer::SetMeshByName));
        return dataTranslator;
    }

	///////////////////////////////////////////////////////////////////////

    void MeshRenderer::SetMeshByName(const String &meshName)
    {
        RenderingSystem* renderingSystem = static_cast<RenderingSystem*>(GetSystem());
        Debugger::AssertNotNull(renderingSystem, "System should not be null");

        SetMesh(renderingSystem->GetResourceManager().CreateResource<Mesh>(meshName));
    }

	///////////////////////////////////////////////////////////////////////

    const String& MeshRenderer::GetMeshName() const
    {
        static String meshName;
        RenderingSystem* renderingSystem = static_cast<RenderingSystem*>(GetSystem());
        Debugger::AssertNotNull(renderingSystem, "System should not be null");

        meshName = renderingSystem->GetResourceManager().GetResourceName<Mesh>(mMesh);
        return meshName;
    }
}
