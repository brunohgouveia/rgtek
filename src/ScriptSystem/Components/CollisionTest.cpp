#include <ScriptSystem/Components/CollisionTest.h>
#include <ScriptSystem/ScriptSystem.h>
#include <Tools/Debugger.h>
#include <Util/Serializer.h>

namespace RGTek
{
    RSCRIPT_IMPLEMENTATION(CollisionTest);

    CollisionTest::CollisionTest()
    {
        Debugger::Debug("CollisionTest start");
    }

    CollisionTest::~CollisionTest()
    {
        Debugger::Debug("CollisionTest shutdown");
    }

    void CollisionTest::OnCollisionEnter(const CollisionPtr& collision)
    {
        Debugger::Info("%s collided to %s", GetEntity()->GetName().c_str(), collision->entity()->GetName().c_str());
    }

    void CollisionTest::OnCollisionStay(const CollisionPtr&)
    {
//        Debugger::Info("%s is colliding", GetEntity()->GetName().c_str());
    }

    void CollisionTest::OnCollisionExit(const CollisionPtr& collision)
    {
        Debugger::Info("%s stop colliding to %s", GetEntity()->GetName().c_str(), collision->entity()->GetName().c_str());
    }

} /* RGTek */
