#include <ScriptSystem/Components/CameraController.h>
#include <Application/Application.h>
#include <ScriptSystem/ScriptSystem.h>
#include <Physics/PhysicsSystem.h>
#include <Tools/Debugger.h>
#include <Util/Serializer.h>

namespace RGTek
{
    RSCRIPT_IMPLEMENTATION(CameraController);

    CameraController::CameraController()
        : mNumHits(0)
        , mFlags(false)
    {
        Debugger::Debug("CameraController starting...");
    }

    CameraController::~CameraController()
    {
        Debugger::Debug("CameraController shutting down...");
    }

    void CameraController::Update()
    {
        // Should be empty, it's just a test
        Debugger::AssertNotNull(GetEntity(), "Script's entity should not be null.");

        if (Input::GetKey(KeyCode::UpArrow))
        {
            GetEntity()->GetTransform()->Rotate(Quaternion(-0.01, 0.0, 0.0));
        }
        else if (Input::GetKey(KeyCode::DownArrow))
        {
            GetEntity()->GetTransform()->Rotate(Quaternion(0.01, 0.0, 0.0));
        }
        else if (Input::GetKey(KeyCode::RightArrow))
        {
            GetEntity()->GetTransform()->Rotate(Quaternion(0.0, 0.01, 0.0), Space::World);

        }
        else if (Input::GetKey(KeyCode::LeftArrow))
        {
            GetEntity()->GetTransform()->Rotate(Quaternion(0.0, -0.01, 0.0), Space::World);
        }

        float velocity = 0.1f;
        // Move camera
        if (Input::GetKey('q') || Input::GetKey('Q'))
        {
            GetEntity()->GetTransform()->MoveUp(velocity);
        }
        if (Input::GetKey('z') || Input::GetKey('Z'))
        {
            GetEntity()->GetTransform()->MoveDown(velocity);
        }
        if (Input::GetKey('w') || Input::GetKey('W'))
        {
            GetEntity()->GetTransform()->MoveForward(velocity);
        }
        if (Input::GetKey('s') || Input::GetKey('S'))
        {
            GetEntity()->GetTransform()->MoveBackward(velocity);
        }
        if (Input::GetKey('a') || Input::GetKey('A'))
        {
            GetEntity()->GetTransform()->MoveLeft(velocity);
        }
        if (Input::GetKey('d') || Input::GetKey('D'))
        {
            GetEntity()->GetTransform()->MoveRight(velocity);
        }

        // Mouse
        if (Input::GetMouseButtonDown(0))
        {
            PhysicsSystemPtr physics = Application::GetSharedApplication()->GetPhysicsSystem();

            RaycastHit hit;
            Ray ray = Camera::GetMainCamera()->ScreenPointToRay(Input::mousePosition);
            if (physics->Raycast(ray, hit))
            {
                EntityPtr entity = hit.mEntity;
                Debugger::Info(entity->GetName().c_str());
                if (!entity->IsStatic())
                {
                    //Debugger::DrawRay(ray, Color::green, Time::Duration(1000));
                    entity->GetGameComponent<Rigidbody>()->AddForce(ray.direction * 2);
                }
                Debugger::Info("Number of hits %d", mNumHits++);
                Debugger::Info("Flags %d", int(mFlags));
                mFlags = !mFlags;
            }
            else
            {
                Debugger::Info("Nothing hit.");
            }

            //Debugger::DrawRay(ray, Color::green, Time::Duration(1000));
        }
    }

    void CameraController::Serialize(Serializer *serializer)
    {
        Script::Serialize(serializer);

        serializer->Serialize<int>(mNumHits);
        serializer->Serialize<bool>(mFlags);
    }
} /* RGTek */
