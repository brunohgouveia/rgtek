//
// Created by Bruno Gouveia on 11/19/15.
//

#include <ScriptSystem/Components/Script.h>
#include <Util/Serializer.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
    Script::Script()
    {
        Debugger::Debug("Script start");
    }

	///////////////////////////////////////////////////////////////////////

    Script::~Script()
    {
        Debugger::Debug("Scripts shutdown");
    }

	///////////////////////////////////////////////////////////////////////

    void Script::Serialize(Serializer *serializer)
    {
        Debugger::AssertNotNull(serializer, "Serializer is null");
        serializer->Serialize(mEntity);
    }
	
}