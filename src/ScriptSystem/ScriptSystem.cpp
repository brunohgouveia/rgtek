//
// Created by Bruno Gouveia on 12/1/15.
//

#include <ScriptSystem/ScriptSystem.h>
#include <ScriptSystem/Components/Script.h>
#include <Tools/Debugger.h>

namespace RGTek
{
	RSYSTEM_IMPLEMENTATION(ScriptSystem);
    std::map<String, ScriptSystem::ScriptInstantiatorFunction> ScriptSystem::sScriptInstantiatorMap;

    ScriptSystem::ScriptSystem()
        : mDynamicLibHandle(nullptr)
        , mFileWatcherListener(this)

    {
        Debugger::Debug("ScriptSystem starting...");

        mWatchId = mFileWatcher.addWatch("../res/scripts", &mFileWatcherListener);
    }

    ScriptSystem::~ScriptSystem()
    {
		sScriptInstantiatorMap.clear();
        Debugger::Debug("ScriptSystem shutting down...");
    }

    void ScriptSystem::Update()
    {
        if (!IsRunning()) return;

        for (auto& scriptMetaData : mScripts)
        {
            Debugger::AssertNotNull(scriptMetaData.mScriptPtr, "Script is null");
            scriptMetaData.mScriptPtr->Update();
        }
    }

    void ScriptSystem::CheckChangeOnScripts()
    {
        mFileWatcher.update();

        if (mFilesOutdated.size() > 0)
        {
            ReloadScripts();
        }
        mFilesOutdated.clear();
    }

    Component* ScriptSystem::CreateComponentByName(const String& scriptName)
    {
        //if (mDynamicLibHandle == nullptr)
        //{
        //    BuildDynamicLibHandle();

        //    LoadDynamicLib();
        //}

        //Debugger::AssertNotNull(mDynamicLibHandle, "Script Dynamic library not found");

        ScriptWeakPtr scriptComponent = InstantiateScript(scriptName);
        scriptComponent->SetScriptName(scriptName);
        scriptComponent->SetSystem(this);
        mScripts.push_back(ScriptMetaData{scriptComponent, scriptName, Serializer()});
        return scriptComponent;
    }

    void ScriptSystem::DeleteComponent(Component* component)
    {
        mScripts.erase(
                std::remove_if(
                        mScripts.begin(),
                        mScripts.end(),
                        [=](const ScriptMetaData& scriptMetaData) -> bool
                        {
                            return scriptMetaData.mScriptPtr == component;
                        }
                )
        );

		delete component;
    }

    void ScriptSystem::RegisterScriptInsantiator(const String& scriptName, ScriptInstantiatorFunction instantiator)
    {
        Debugger::Warning("Registering script: %s", scriptName.c_str());
        ScriptSystem::sScriptInstantiatorMap[scriptName] = instantiator;
        Debugger::Warning("Registering script: %s", scriptName.c_str());
    }

    ScriptWeakPtr ScriptSystem::InstantiateScript(const String &scriptName, ScriptWeakPtr oldAddress)
    {
        ScriptInstantiatorFunction instantiator = sScriptInstantiatorMap[scriptName];

        if (instantiator != nullptr)
        {
            return instantiator(oldAddress);
        }
        else
        {
			Debugger::Error("Script instantiator not found for Script [%s]. Perhaps you didn't include the REGISTER_SCRIPT macro on your Script source file", scriptName.c_str());
            return nullptr;
        }
    }

    void ScriptSystem::LoadDynamicLib()
    {
//#ifdef EDITOR_RGTEK
//        mDynamicLibHandle = dlopen("res/scripts/build/libScriptsEditor.dylib", RTLD_LAZY);
//#else
//        mDynamicLibHandle = dlopen("res/scripts/build/libScriptsEditor.dylib", RTLD_LAZY);
//#endif
//        Debugger::AssertNotNull(mDynamicLibHandle, "Unable to load Script dynamic library.");
    }

    void ScriptSystem::UnloadDynamicLib()
    {
        //dlclose(mDynamicLibHandle);
    }

    void ScriptSystem::BuildDynamicLibHandle()
    {
        Debugger::Debug("Building scripts");
        Time::Duration buildDuration;
        buildDuration.Start();
        system("cd res/scripts/build/ && make -j4 && cd ../../../");
        buildDuration.End();

        Debugger::Debug("Time to build scripts: %lld", buildDuration.Count());
    }

    void ScriptSystem::ReloadScripts()
    {
        //Debugger::Debug("ResourceManager - reloading scripts");

        //// Serialize scripts
        //for (ScriptMetaData& scriptMetaData : mScripts)
        //{
        //    Script* script = scriptMetaData.mScriptPtr;
        //    scriptMetaData.mSerializer.SetMode(Serializer::SavingMode);
        //    script->Serialize(&scriptMetaData.mSerializer);
        //}

        //// Reload dynamic library
        //UnloadDynamicLib();
        //BuildDynamicLibHandle();
        //LoadDynamicLib();

        //// Rebuild all the scripts
        //for (ScriptMetaData& scriptMetaData : mScripts)
        //{
        //    scriptMetaData.mScriptPtr = InstantiateScript(scriptMetaData.mScriptName, scriptMetaData.mScriptPtr);
        //    scriptMetaData.mSerializer.SetMode(Serializer::LoadingMode);
        //    scriptMetaData.mScriptPtr->Serialize(&scriptMetaData.mSerializer);

        //    scriptMetaData.mSerializer.ClearBuffer();
        //}
    }

    ScriptSystem::FileWatcherListener::FileWatcherListener(ScriptSystem *scriptSystem)
        : mScriptSystem(scriptSystem)
    {

    }

    void ScriptSystem::FileWatcherListener::handleFileAction(FW::WatchID, const FW::String&,
                                                             const FW::String& filename, FW::Action)
    {
        Debugger::AssertNotNull(mScriptSystem, "ScritSystem is null");

        Debugger::Debug("ScriptSystem - resource file modified: %s", filename.c_str());
        mScriptSystem->mFilesOutdated.insert(filename);

    }

    ScriptRegister::ScriptRegister(const String& scriptName, ScriptWeakPtr (*createScript)(ScriptWeakPtr))
    {
        Debugger::Warning("Registering script: %s", scriptName.c_str());
        //ScriptSystem::sScriptInstantiatorMap[scriptName] = createScript;
        Debugger::Warning("Registering script: %s", scriptName.c_str());
    }

}