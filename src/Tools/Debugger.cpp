#include <Tools/Time.h>
#include <Application/Application.h>
#include <Tools/Debugger.h>
#include <GL/glew.h>

namespace RGTek
{
	char Debugger::sBuffer[16384];
#ifdef NOTHING
	std::vector<Debugger::RayDrawerInfo> Debugger::sRayDrawerInfoSet;
    char                                 Debugger::sBuffer[16384];
    
    void Debugger::DrawRay(const RGTek::Ray &ray, const Color &color, const Time::Duration& duration)
    {
		sRayDrawerInfoSet.push_back(RayDrawerInfo(ray, color, duration));
    }
    
	void Debugger::InitDrawAssets()
    {

    }
    
	void Debugger::FlushDrawRays()
    {
		if (sRayDrawerInfoSet.size() == 0)
        {
            return;
        }

        ApplicationWeakPtr app = Application::GetSharedApplication();
        Debugger::AssertNotNull(app, "SharedCoreSystem should not be null.");

        RenderingSystemWeakPtr renderingSystem = app->GetRenderingSystem();
        Debugger::AssertNotNull(renderingSystem, "RenderingSystem should not be null");

		for (auto& rayDrawerInfo : sRayDrawerInfoSet)
        {
            renderingSystem->GetBackend().DrawRay(rayDrawerInfo.ray, rayDrawerInfo.color);
        }

		// Delete rays that should be no longer drawn
		sRayDrawerInfoSet.erase(
			std::remove_if(
				sRayDrawerInfoSet.begin(),
				sRayDrawerInfoSet.end(),
				[](const RayDrawerInfo& info) -> bool
				{
					return Time::Duration(info.start, Time::Now()) > info.duration;
				}
			),
			sRayDrawerInfoSet.end()
		);
    }
#endif

	void Debugger::__CheckOpenGLError(const char* file, int line)
	{
		int err = glGetError();
		if (err)
		{
			Console::Err::redPrint("-OpenGL error in method: ");
			Console::Err::println(" File: %s, line: %d", file, line);
			Console::Err::println(" Error code: %d\n", err);
		}
	}

	void Debugger::__Fatal(const char* file, int line, const char* format, ...)
	{
		Console::Err::redPrint("-Fatal error in method: ");
		Console::Err::println(" File: %s, line: %d", file, line);
		Console::Err::print(" Description: ");

		va_list args;
		va_start(args, format);
		vsprintf(sBuffer, format, args);
		va_end(args);
		Console::Err::println(sBuffer);

		exit(EXIT_FAILURE);
	}

	void Debugger::__Error(const char* file, int line, const char* format, ...)
	{
		Console::Err::redPrint("-Error in method: ");
		Console::Err::println(" File: %s, line: %d", file, line);
		Console::Err::print(" Description: ");

		va_list args;
		va_start(args, format);
		vsprintf(sBuffer, format, args);
		va_end(args);
		Console::Err::println(sBuffer);
	}

	void Debugger::__Warning(const char* /*file*/, int /*line*/, const char* format, ...)
	{
		Console::Out::yellowPrint("-Warning: ");
		va_list args;
		va_start(args, format);
		vsprintf(sBuffer, format, args);
		va_end(args);
		Console::Out::println(sBuffer);
	}

	void Debugger::__Info(const char* file, int line, const char* format, ...)
	{
		Console::Out::greenPrint("Info: ");
		va_list args;
		va_start(args, format);
		vsprintf(sBuffer, format, args);
		va_end(args);
		Console::Out::println(sBuffer);
	}
    
	void Debugger::__Debug(const char* file, int line, const char* format, ...)
	{
		Console::Out::bluePrint("Debug: ");
		va_list args;
		va_start(args, format);
		vsprintf(sBuffer, format, args);
		va_end(args);
		Console::Out::println(sBuffer);
	}

	void Debugger::__AssertNotNull(const char* file, int line, AnyPtr value, const char* format, ...)
	{
		if (value == NULL)
		{
			Console::Err::redPrint("AssertNotNul failed in ");
			Console::Err::println("%s:%d", file, line);
			Console::Err::print("Description: ");
			va_list args;
			va_start(args, format);
			vsprintf(sBuffer, format, args);
			va_end(args);
			Console::Err::println(sBuffer);

			throw "Assert failed";
		}
	}

	void Debugger::__AssertNull(const char* file, int line, AnyPtr value, const char* format, ...)
	{
		if (value != NULL)
		{
			Console::Err::redPrint("AssertNul failed in ");
			Console::Err::println("%s:%d", file, line);
			Console::Err::print("Description: ");
			va_list args;
			va_start(args, format);
			vsprintf(sBuffer, format, args);
			va_end(args);
			Console::Err::println(sBuffer);

			throw "Assert failed";
		}
	}

	void Debugger::__AssertTrue(const char* file, int line, bool value, const char* format, ...)
	{
		if (!value)
		{
			Console::Err::redPrint("AssertTrue failed in ");
			Console::Err::println("%s:%d", file, line);
			Console::Err::print("Description: ");
			va_list args;
			va_start(args, format);
			vsprintf(sBuffer, format, args);
			va_end(args);
			Console::Err::println(sBuffer);

			throw "Assert failed";
		}
	}

	void Debugger::__AssertFalse(const char* file, int line, bool value, const char* format, ...)
	{
		if (value)
		{
			Console::Err::redPrint("AssertFalse failed in ");
			Console::Err::println("%s:%d", file, line);
			Console::Err::print("Description: ");
			va_list args;
			va_start(args, format);
			vsprintf(sBuffer, format, args);
			va_end(args);
			Console::Err::println(sBuffer);

			throw "Assert failed";
		}
	}

}
