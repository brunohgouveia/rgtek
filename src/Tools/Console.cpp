#include <Tools/Console.h>

// The following codes must be used in Unix platform
//const char* ColorTrait<red>::prefix = "\x1B[0;31m";
//const char* ColorTrait<RED>::prefix = "\x1B[1;31m";
//const char* ColorTrait<blue>::prefix = "\x1B[0;34m";
//const char* ColorTrait<BLUE>::prefix = "\x1B[1;34m";
//const char* ColorTrait<cyan>::prefix = "\x1B[0;36m";
//const char* ColorTrait<CYAN>::prefix = "\x1B[1;36m";
//const char* ColorTrait<green>::prefix = "\x1B[0;32m";
//const char* ColorTrait<GREEN>::prefix = "\x1B[1;32m";
//const char* ColorTrait<yellow>::prefix = "\x1B[0;33m";
//const char* ColorTrait<YELLOW>::prefix = "\x1B[1;33m";
//const char* ColorTrait<NC>::prefix = "\x1B[0m";

#ifdef _WIN32
#include <Windows.h>
#else
static const char* sMacColorCode[] = {
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "\x1B[0m",      // NC
        "",
        "\x1B[1;34m",
        "\x1B[1;32m",
        "\x1B[1;36m",
        "\x1B[1;31m",   // RED
        "\x1B[1;35m",   // PINK
        "\x1B[1;33m",   // YELLOW
};
#endif


namespace RGTek
{
	void Console::SetColor(ColorCode colorCode, std::ostream& streamBuffer)
	{
#ifdef _WIN32
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), static_cast<WORD>(colorCode));
#else
        streamBuffer << sMacColorCode[static_cast<unsigned int>(colorCode)];
#endif
	}

    void Console::Out::TestColors()
    {
        for (int i = 0; i < 15; ++i)
        {
#ifndef _WIN32
            std::cout << sMacColorCode[i] << "Test" << std::endl;
#endif
        }
    }

    char Console::sBuffer[16384];
}
