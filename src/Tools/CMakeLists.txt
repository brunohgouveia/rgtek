if (RGTEK_UNIFIED_LIBRARY)
    file (GLOB_RECURSE TOOLS_SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/../.." *.cpp)
    set (RGTEK_SOURCE_FILES ${RGTEK_SOURCE_FILES} ${TOOLS_SOURCE_FILES} PARENT_SCOPE)

    file (GLOB_RECURSE TOOLS_HEADER_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/../.." ../../include/Tools/*.h)
    set (RGTEK_HEADER_FILES ${RGTEK_HEADER_FILES} ${TOOLS_HEADER_FILES} PARENT_SCOPE)
else()
    set(RGTEK_MODULE_NAME ${RGTEK_TOOLS_LIBNAME} )

    # we do not use cegui_gather_files() here since our needs are more complex.
    file (GLOB_RECURSE TOOLS_SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" *.cpp)

    file (GLOB_RECURSE TOOLS_HEADER_FILES ../include/Tools/*.h)

    rgtek_add_library(${RGTEK_MODULE_NAME} TOOLS_SOURCE_FILES TOOLS_HEADER_FILES)

    rgtek_add_module_dependency(${RGTEK_MODULE_NAME} ${RGTEK_MATH_LIBNAME})

    rgtek_add_dependency(${RGTEK_MODULE_NAME} OPENGL)
    rgtek_add_dependency(${RGTEK_MODULE_NAME} GLEW)
endif()