#include <Tools/Time.h>

namespace RGTek
{
    
    Time::TimePoint Time::Now()
    {
        return TimePoint(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count());
    }
    
    Time::Duration::Duration(long long  milliseconds)
    {
        mStart = mEnd = Time::Now();
        
        mEnd.SetTimeSinceEpoch(mEnd.GetTimeSinceEpoch() + milliseconds);
    }

    void Time::Duration::Start()
    {
        mStart = Time::Now();
    }

    void Time::Duration::End()
    {
        mEnd = Time::Now();
    }

    long long Time::Duration::Count() const
    {
        return mEnd.GetTimeSinceEpoch() - mStart.GetTimeSinceEpoch();
    }
    
    bool Time::Duration::operator>(const Time::Duration& duration)
    {
        long long myDuration = mEnd.GetTimeSinceEpoch() - mStart.GetTimeSinceEpoch();
        long long otherDuration = duration.mEnd.GetTimeSinceEpoch() - duration.mStart.GetTimeSinceEpoch();
        return myDuration > otherDuration;
    }
    
    bool Time::Duration::operator<(const Time::Duration& duration)
    {
        long long myDuration = mEnd.GetTimeSinceEpoch() - mStart.GetTimeSinceEpoch();
        long long otherDuration = duration.mEnd.GetTimeSinceEpoch() - duration.mStart.GetTimeSinceEpoch();
        return myDuration < otherDuration;
    }
    
    bool Time::Duration::operator>=(const Time::Duration& duration)
    {
        long long myDuration = mEnd.GetTimeSinceEpoch() - mStart.GetTimeSinceEpoch();
        long long otherDuration = duration.mEnd.GetTimeSinceEpoch() - duration.mStart.GetTimeSinceEpoch();
        return myDuration >= otherDuration;
    }
    
    bool Time::Duration::operator<=(const Time::Duration& duration)
    {
        long long myDuration = mEnd.GetTimeSinceEpoch() - mStart.GetTimeSinceEpoch();
        long long otherDuration = duration.mEnd.GetTimeSinceEpoch() - duration.mStart.GetTimeSinceEpoch();
        return myDuration <= otherDuration;
    }
    
}