//
// Created by Bruno Gouveia on 12/15/15.
//

#include <Tools/ProfileTimer.h>
#include <Tools/Debugger.h>

namespace RGTek
{
    ProfileTimer::ProfileTimer()
        : mStartTimePoint(0)
        , mTotalTime(0)
        , mNumInvocations(0)
    {

    }

    void ProfileTimer::StartInvocation()
    {
        mStartTimePoint = Time::Now();
    }

    void ProfileTimer::StopInvocation()
    {
        if (mStartTimePoint.GetTimeSinceEpoch() == 0)
        {
			Debugger::Error("StopInvocation being called without previously calling StartInvocation");
        }
        mTotalTime += Time::Duration(mStartTimePoint, Time::Now()).Count();
        mNumInvocations++;
        mStartTimePoint.SetTimeSinceEpoch(0);
    }

    long long ProfileTimer::GetDuration() const
    {
        return mTotalTime;
    }

    unsigned int ProfileTimer::GetNumInvocations() const
    {
        return mNumInvocations;
    }

    void ProfileTimer::Reset()
    {
        mTotalTime = 0;
        mNumInvocations = 0;
    }

}