#include <Resources/Mesh.h>
#include <Resources/ResourceManager.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Mesh::Mesh(const String& fileName, ResourceManagerWeakPtr resourceManager)
    {
        Debugger::Debug("Mesh starting... %s", fileName.c_str());

        Assimp::Importer importer;
        const aiScene *scene = importer.ReadFile(fileName.c_str(),
                                                 aiProcess_Triangulate |
                                                 aiProcess_GenNormals);

        Debugger::AssertNotNull(scene, "Scene should not be null, the file %s probably doesn't exist",
                                fileName.c_str());

        // Load mesh in a per-material fashion
        for (unsigned int m = 0; m < scene->mNumMaterials; m++)
        {
            std::vector<Vector4> vertices;
            std::vector<Vector4> normals;
            std::vector<Vector4> textCoords;

            // For each mesh with the current material
            for (unsigned int i = 0; i < scene->mNumMeshes; i++)
            {
                if (scene->mMeshes[i]->mMaterialIndex != m)
                {
                    continue;
                }

                // For each face
                for (unsigned int j = 0; j < scene->mMeshes[i]->mNumFaces; j++)
                {
                    Debugger::AssertTrue(scene->mMeshes[i]->mFaces[j].mNumIndices == 3, "Face should be a triangle");
                    for (unsigned int k = 0; k < 3; k++)
                    {
                        int index = scene->mMeshes[i]->mFaces[j].mIndices[k];
                        // Vertice
                        float x = scene->mMeshes[i]->mVertices[index].x;
                        float y = scene->mMeshes[i]->mVertices[index].y;
                        float z = scene->mMeshes[i]->mVertices[index].z;
                        float w = 1.0;

                        vertices.push_back(Vector4(x, y, z, w));

                        // Normal
                        x = scene->mMeshes[i]->mNormals[index].x;
                        y = scene->mMeshes[i]->mNormals[index].y;
                        z = scene->mMeshes[i]->mNormals[index].z;

                        normals.push_back(Vector4(x, y, z, w));

                        // Text coord
                        x = scene->mMeshes[i]->mTextureCoords[0][index].x;
                        y = scene->mMeshes[i]->mTextureCoords[0][index].y;

                        textCoords.push_back(Vector4(x, y, 1.0f, 1.0f));
                    }
                }
            }

			// There is no reason to create empty meshData objects
			if (vertices.size() == 0)
				continue;

            // Add sub mesh
            MeshDataPtr meshData = new MeshData(std::move(vertices), std::move(normals), std::move(textCoords));
            mSubMeshes.push_back(meshData);

            // Add Material
            aiString aiMaterialName;
            scene->mMaterials[m]->Get(AI_MATKEY_NAME, aiMaterialName);
            String materialName(aiMaterialName.C_Str());

            // If no resource manager is provided, than do not load the material information
            if (resourceManager == nullptr) continue;

            MaterialHandle materialHandle = resourceManager->CreateResource<Material>(materialName);
            MaterialWeakPtr material = resourceManager->GetResource<Material>(materialHandle);
            {
                // Load Shader
                ShaderHandle shader = resourceManager->CreateResource<Shader>("shaders/Legacy/Specular.shader");

                material->SetProgram(shader);

                /**
                 * Load textures
                 * TODO - USER RESOURCE MANAGER
                 */
                if (scene->mMaterials[m]->GetTextureCount(aiTextureType_DIFFUSE) > 0)
                {
                    aiString path;
                    scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, 0, &path);

                    material->SetTexture("diffuseTexture", resourceManager->CreateResource<Texture>("textures/" + String(path.C_Str()), 0));
                }
                else
                {
                    material->SetTexture("diffuseTexture", resourceManager->CreateResource<Texture>("textures/blankTexture.bmp", 0));
                }
            }
            mMaterials.push_back(materialHandle);
        }
    }

    
    Mesh::~Mesh()
    {
        Debugger::Debug("Mesh shutting down...");

    }

    void Mesh::Draw(ResourceManagerWeakPtr resourceManager) const
    {
        for (unsigned int i = 0; i < GetSubMeshes().size(); ++i)
        {
            MeshDataWeakPtr subMesh = GetSubMeshes()[i];

            if (GetMaterials().size() > 0)
            {
                MaterialWeakPtr material = resourceManager->GetResource<Material>(GetMaterials()[i]);

                ShaderWeakPtr shader = resourceManager->GetResource<Shader>(material->GetProgram());
                shader->UseShader();

                material->UpdateUniformsToShader(shader, resourceManager);
            }

            subMesh->Draw();
        }
    }

}