#include <Resources/MeshData.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Tools/Debugger.h>
#include <GL/glew.h>

namespace RGTek
{

    MeshData::MeshData() :
        mVertexArray(-1),
        mNumVertices(0)
    {
        Debugger::Debug("MeshData start");

        glGenVertexArrays(1, &mVertexArray);
    }

    MeshData::MeshData(Vector<Vector4>&& vertices, Vector<Vector4>&& normals) :
        mVertices(std::move(vertices)),
        mNormals(std::move(normals)),
        mVertexArray(-1),
        mNumVertices(mVertices.size())
    {
        Debugger::Debug("MeshData start");

        glGenVertexArrays(1, &mVertexArray);
        glBindVertexArray(mVertexArray);

        mVertexBuffer.Init(&mVertices[0][0], mVertices.size()*4, GL_STATIC_DRAW);
        mVertexBuffer.VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4, 0);

        mNormalBuffer.Init(&mNormals[0][0], mNormals.size()*4, GL_STATIC_DRAW);
        mNormalBuffer.VertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 4, 0);
    }

    MeshData::MeshData(Vector<Vector4>&& vertices, Vector<Vector4>&& normals, Vector<Vector4>&& textCoords) :
        mVertices(std::move(vertices)),
        mNormals(std::move(normals)),
        mTextCoords(std::move(textCoords)),
        mVertexArray(-1),
        mNumVertices(mVertices.size())
    {
        Debugger::Debug("MeshData start");

        glGenVertexArrays(1, &mVertexArray);
        glBindVertexArray(mVertexArray);

        mVertexBuffer.Init(&mVertices[0][0], mVertices.size()*4, GL_STATIC_DRAW);
        mVertexBuffer.VertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4, 0);

        mNormalBuffer.Init(&mNormals[0][0], mNormals.size()*4, GL_STATIC_DRAW);
        mNormalBuffer.VertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 4, 0);

        mTextCoordBuffer.Init(&mTextCoords[0][0], mTextCoords.size()*4, GL_STATIC_DRAW);
        mTextCoordBuffer.VertexAttribPointer(2,2, GL_FLOAT, GL_FALSE, 4, 0);
    }

    MeshData::~MeshData()
    {
        Debugger::Debug("MeshData shutdown");
        glDeleteVertexArrays(1, &mVertexArray);
    }

    void MeshData::Draw() const
    {
        glBindVertexArray(mVertexArray);
        glDrawArrays(GL_TRIANGLES, 0, mNumVertices);
    }

}
