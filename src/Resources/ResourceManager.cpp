#include <Resources/ResourceManager.h>

#include <Tools/Time.h>
#include <Util/Serializer.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    ResourceManager::ResourceManager(const String& resourcePath)
        : mResourcePath(resourcePath)
        , mResourceWatchListener(this)
    {
        mShaderFileWatcher.addWatch(mResourcePath + "shaders", &mResourceWatchListener);
        mTextureFileWatcher.addWatch(mResourcePath + "textures", &mResourceWatchListener);
    }

    ResourceManager::ResourceManager(const String&& resourcePath)
        : mResourcePath(std::move(resourcePath))
        , mResourceWatchListener(this)
    {
        mShaderFileWatcher.addWatch(mResourcePath + "shaders", &mResourceWatchListener);
        mTextureFileWatcher.addWatch(mResourcePath + "textures", &mResourceWatchListener);
    }

    ResourceManager::~ResourceManager()
    {
        DestroyAllResources();
    }

    void ResourceManager::CheckResourcesChanges()
    {
        mShaderFileWatcher.update();
        mTextureFileWatcher.update();

        for (const String& shaderFile : mShadersOutdated)
        {
            if (mShaderMap.find(shaderFile) != mShaderMap.end())
            {
                ReloadResource<Shader>(shaderFile);
            }
        }
        mShadersOutdated.clear();

        for (const String& textureFile : mTexturesOutdated)
        {
            if (mTextureMap.find(textureFile) != mTextureMap.end())
            {
                ReloadResource<Texture>(textureFile);
            }
        }
        mTexturesOutdated.clear();
    }

    void ResourceManager::DestroyAllResources()
    {
        DestroyResources<Shader>();
        DestroyResources<Mesh>();
        DestroyResources<Material>();
        DestroyResources<Texture>();
    }

    void ResourceManager::FileWatchListener::handleFileAction(FW::WatchID watchid, const FW::String &dir,
                                                              const FW::String &filename, FW::Action action)
    {
        Debugger::AssertNotNull(mResourceManager, "ResourceManager is null");

        if (dir == mResourceManager->mResourcePath + "shaders")
        {
            Debugger::Debug("Shader %s changed", filename.c_str());
            mResourceManager->mShadersOutdated.insert(filename);
        }
        else if (dir == mResourceManager->mResourcePath + "textures")
        {
            Debugger::Debug("Texture %s changed", filename.c_str());
            mResourceManager->mTexturesOutdated.insert(filename);
        }
    }

    template <> std::map<String, ShaderHandle>& ResourceManager::GetResourceMap<Shader>()
    {
        return mShaderMap;
    }

    template <> std::map<String, MeshHandle>& ResourceManager::GetResourceMap<Mesh>()
    {
        return mMeshMap;
    }

    template <> std::map<String, MaterialHandle>& ResourceManager::GetResourceMap<Material>()
    {
        return mMaterialMap;
    }

    template <> std::map<String, TextureHandle>& ResourceManager::GetResourceMap<Texture>()
    {
        return mTextureMap;
    }

    template <> HandleManager<Shader>& ResourceManager::GetResourceHandleManager<Shader>()
    {
        return mShaderHandleManager;
    }

    template <> HandleManager<Mesh>& ResourceManager::GetResourceHandleManager<Mesh>()
    {
        return mMeshHandleManager;
    }

    template <> HandleManager<Material>& ResourceManager::GetResourceHandleManager<Material>()
    {
        return mMaterialHandleManager;
    }

    template <> HandleManager<Texture>& ResourceManager::GetResourceHandleManager<Texture>()
    {
        return mTextureHandleManager;
    }

} /* RGTek */
