#include <Resources/Material.h>
#include <Resources/ResourceManager.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Material::Material(const String& materialName, ResourceManagerWeakPtr resourceManager)
    {
        Debugger::Debug("Material starting... [%s]", materialName.c_str());
        mColors["MaterialMainColor"]     = Color(0.1f);
        mColors["MaterialSpecularColor"] = Color(1.0f);
    }

    Material::~Material()
    {
        Debugger::Debug("Material shutting down...");
    }

    void Material::UpdateUniformsToShader(ShaderWeakPtr shader, ResourceManagerWeakPtr resourceManager)
    {
        for (auto& colorPair : mColors)
        {
            shader->SetUniform<Color>(colorPair.first, colorPair.second);
        }

        if (resourceManager)
        {
            for (auto& texturePair : mTextures)
            {
                TextureWeakPtr texture = resourceManager->GetResource<Texture>(texturePair.second);
                texture->Active();
                texture->Bind();
                shader->SetUniform(texturePair.first, texture->GetUnit());
            }
        }
    }

}
