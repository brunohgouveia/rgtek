/*
 * Shader.cpp
 *
 *  Created on: May 21, 2015
 *      Author: Bruno Gouveia
 */

#include <Resources/Shader.h>
#include <System/File.h>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <stdlib.h>
#include <regex>
#include <Tools/Debugger.h>
#include <GL/glew.h>

namespace RGTek
{

	Shader::Shader(const String& fileName, ResourceManagerWeakPtr resourceManager) 
	{
        RGTek::Debugger::Debug("Shader starting...");

		mProgramName = glCreateProgram();

        Debugger::AssertTrue(File::Exists(fileName), "Shader file [%s] not found.", fileName.c_str());

        Vector<String> shadersCode = LoadShadersCode(fileName);
        Debugger::AssertTrue(shadersCode.size() == 2, "Only vertex/fragment shader supported.");
        RGTek::Debugger::AssertFalse(shadersCode[0].empty(), "Vertex Shader's code should not be empty");
        RGTek::Debugger::AssertFalse(shadersCode[1].empty(), "Fragment Shader's code should not be empty");

        if (!CompileShader(shadersCode, fileName))
        {
            LoadErrorShader();
        }
	}

	Shader::~Shader() {
        Debugger::Debug("Shader shutting down...");
		glDeleteProgram(mProgramName);
	}

	void Shader::UseShader() const { glUseProgram(mProgramName); }

	Uniform Shader::GetUniform(const std::string name) const { return glGetUniformLocation(mProgramName, name.c_str()); }

	void Shader::SetUniform(const Uniform uniform, GLuint value)         const { glUniform1i(uniform, value); }
	void Shader::SetUniform(const Uniform uniform, int value)            const { glUniform1i(uniform, value); }
	void Shader::SetUniform(const Uniform uniform, float value)          const { glUniform1f(uniform, value); }
	void Shader::SetUniform(const Uniform uniform, const Matrix4& value) const { glUniformMatrix4fv(uniform, 1, GL_FALSE, static_cast<const float*>(value)); }
	void Shader::SetUniform(const Uniform uniform, const Vector3& value) const { glUniform3f(uniform, value[0], value[1], value[2]); }
	void Shader::SetUniform(const Uniform uniform, const Vector4& value) const { glUniform4f(uniform, value[0], value[1], value[2], value[3]); }
	void Shader::SetUniform(const Uniform uniform, const Color& value)   const { glUniform4f(uniform, value.r, value.g, value.b, value.a); }

    bool Shader::CompileShader(const Vector<String> &shadersCode, const String &fileName)
    {
        // Compile vertex shader
        GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
        const char * source = shadersCode[0].c_str();
        glShaderSource(vertShader, 1, &source, NULL);
        glCompileShader(vertShader);
        if (!CheckGLShaderStatus(vertShader, fileName))
        {
            glDeleteShader(vertShader);
            return false;
        }

        // Compile fragment shader
        GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
        source = shadersCode[1].c_str();
        glShaderSource(fragShader, 1, &source, NULL);
        glCompileShader(fragShader);
        if (!CheckGLShaderStatus(fragShader, fileName))
        {
            glDeleteShader(vertShader);
            glDeleteShader(fragShader);
            return false;
        }

        // Link program
        glAttachShader(mProgramName, vertShader);
        glAttachShader(mProgramName, fragShader);

        glLinkProgram(mProgramName);

        glDetachShader(mProgramName, vertShader);
        glDetachShader(mProgramName, fragShader);

        // Delete shaders
        glDeleteShader(vertShader);
        glDeleteShader(fragShader);

        GLint isLinked = 0;
        glGetProgramiv(mProgramName, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE)
        {
			Debugger::Error("Failed to link program.");
            return false;
        }

        Debugger::CheckOpenGLError();

        return true;
    }


	Vector<String> Shader::LoadShadersCode(const String& fileName)
	{
		// Input stream
		std::ifstream inputFile(fileName.c_str());

        String vertShader;
        String fragShader;

        String line;

        // Read Vertex shader
        std::getline(inputFile, line);
        if (line != "CGVERTEX") Debugger::Fatal("CGVERTEX missing");

        while (std::getline(inputFile, line) && line != "ENDCG")
        {
            if (line.compare(0, 8, "#include") == 0)
            {
                std::smatch matchResult;
                std::regex includedFileRegex("#include <([\\w/]+[.]\\w+)>");
                String includedFile;

                if (std::regex_search(line, matchResult, includedFileRegex))
                {
                    includedFile = "../" + matchResult.str(1);

                    Debugger::AssertTrue(File::Exists(includedFile), "Included file [%s] not found.", includedFile.c_str());

                    std::ifstream includedFileStream(includedFile);
                    String includedFileLine;
                    while (std::getline(includedFileStream, includedFileLine))
                    {
                        vertShader += includedFileLine + "\n";
                    }

					includedFileStream.close();
                }

                continue;
            }
            vertShader += line + "\n";
        }

        if (line != "ENDCG") Debugger::Fatal("CGVERTEX not finished");

        // Read Fragment Shader
        std::getline(inputFile, line);
        if (line != "CGFRAGMENT") Debugger::Fatal("CGFRAGMENT missing");

        while (std::getline(inputFile, line) && line != "ENDCG")
        {
            if (line.compare(0, 8, "#include") == 0)
            {
                std::smatch matchResult;
                std::regex includedFileRegex("#include <([\\w/]+[.]\\w+)>");
                String includedFile;

                if (std::regex_search(line, matchResult, includedFileRegex))
                {
                    includedFile = "../" + matchResult.str(1);

                    Debugger::AssertTrue(File::Exists(includedFile), "Included file [%s] not found.", includedFile.c_str());

                    std::ifstream includedFileStream(includedFile);
                    String includedFileLine;
                    while (std::getline(includedFileStream, includedFileLine))
                    {
                        fragShader += includedFileLine + "\n";
                    }

					includedFileStream.close();
                }

                continue;
            }
            fragShader += line + "\n";
        }

		inputFile.close();

        if (line != "ENDCG") Debugger::Fatal("CGFRAGMENT not finished");

		Vector<String> shadersCode;
		shadersCode.push_back(vertShader);
        shadersCode.push_back(fragShader);

		return shadersCode;
	}

    bool Shader::CheckGLShaderStatus(GLuint shaderName, std::string fileName)
	{
		int len=0;
		glGetShaderiv(shaderName, GL_INFO_LOG_LENGTH, &len);
		if (len>1)
		{
			int n=0;
			char* buffer = (char *)malloc(len);
			if (!buffer)
			{
				std::cerr << "Cannot allocate " << len <<  "bytes of text for shader log" << std::endl;
				exit(1);
			}
			glGetShaderInfoLog(shaderName, len, &n, buffer);
			std::cerr << fileName << ": \n" << buffer << std::endl;
			// fprintf(stderr,"%s:\n%s\n",fileName.c_str(),buffer);
			free(buffer);
		}
		glGetShaderiv(shaderName, GL_COMPILE_STATUS, &len);
		if (!len)
		{
			Debugger::Error("Error compiling shader [%s].", fileName.c_str());
            return false;

		}
        return true;
	}

    void Shader::LoadErrorShader()
    {
        Vector<String> shadersCode;
        shadersCode.push_back(
                "#version 400\n"
                "uniform mat4 LightVPMatrix;\n"
                "uniform mat4 LightBiasVPMatrix;\n"
                "uniform vec4 LightAmbient;\n"
                "uniform vec4 LightPosition;\n"
                "uniform vec4 LightColor;\n"
                "uniform mat4 ModelMatrix;\n"
                "uniform mat4 ViewProjectionMatrix;\n"
                "uniform mat4 ViewMatrix;\n"
                "uniform mat4 ProjectionMatrix;\n"
                "uniform mat4 NormalMatrix;\n"
                "uniform vec4 MaterialMainColor;\n"
                "uniform vec4 MaterialSpecularColor;\n"
                "uniform sampler2D diffuseTexture;\n"
                "uniform sampler2DShadow depthText;\n"
                "layout(location = 0) in vec4 Vertex;\n"
                "layout(location = 1) in vec3 Normal;\n"
                "layout(location = 2) in vec2 TexCoord;\n"
                "\n"
                "void main()\n"
                "{\n"
                "\tgl_Position = ViewProjectionMatrix * ModelMatrix * Vertex;\n"
                "}"
        );

        shadersCode.push_back(
                "#version 400\n"
                "uniform mat4 LightVPMatrix;\n"
                "uniform mat4 LightBiasVPMatrix;\n"
                "uniform vec4 LightAmbient;\n"
                "uniform vec4 LightPosition;\n"
                "uniform vec4 LightColor;\n"
                "uniform mat4 ModelMatrix;\n"
                "uniform mat4 ViewProjectionMatrix;\n"
                "uniform mat4 ViewMatrix;\n"
                "uniform mat4 ProjectionMatrix;\n"
                "uniform mat4 NormalMatrix;\n"
                "uniform vec4 MaterialMainColor;\n"
                "uniform vec4 MaterialSpecularColor;\n"
                "uniform sampler2D diffuseTexture;\n"
                "uniform sampler2DShadow depthText;\n"
                "\n"
                "out vec4 FragColor;\n"
                "\n"
                "void main()\n"
                "{\n"
                "\tFragColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);\n"
                "}"
        );

        CompileShader(shadersCode, "ErrorShader");
    }

}
