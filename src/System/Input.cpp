/*
 * Inpuc.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: Bruno Gouveia
 */

#include <System/Input.h>

namespace RGTek
{

    KeyCode KeyCode::Esc(27);
    KeyCode KeyCode::UpArrow(256);
    KeyCode KeyCode::DownArrow(257);
    KeyCode KeyCode::LeftArrow(258);
    KeyCode KeyCode::RightArrow(259);
    KeyCode KeyCode::PageUp(260);
    KeyCode KeyCode::PageDown(261);
    KeyCode KeyCode::Return(262);
    KeyCode KeyCode::Alt(263);

    Vector2 Input::mousePosition;
    float   Input::mouseWheel;
    bool    Input::sInputActive;
    Vector2 Input::sMouseButtonsClickedPosition[5];
    bool    Input::sMouseButtonsPreviousFrame[5];
    bool    Input::sMouseButtons[5];
    bool    Input::sKeyMap[264];

    void Input::Init() {
	    for (int i = 0; i < 264; i++) {
		    sKeyMap[i] = false;
	    }

        for (unsigned int i = 0; i < MouseButton::NumberOfButtons; i++)
        {
            sMouseButtonsPreviousFrame[i] = false;
            sMouseButtons[i] = false;
        }
    }

	void Input::Finish()
	{
		
	}


    void Input::ResetMap()
    {
//        for (std::vector<bool>::iterator key = sKeyMap.begin(); key != sKeyMap.end(); key++)
//        {
////            (*key) = false;
//        }

        mouseWheel = 0.0f;
    }

    void Input::ResetDelta(int button)
    {
        sMouseButtonsClickedPosition[button] = mousePosition;
    }

    void Input::EndMouseInjection()
    {
        for (unsigned int i = 0; i < MouseButton::NumberOfButtons; i++)
        {
            sMouseButtonsPreviousFrame[i] = sMouseButtons[i];
        }
    }

    bool Input::GetMouseButtonDown(int button)
    {
        return sInputActive && !sMouseButtonsPreviousFrame[button] && sMouseButtons[button];
    }

    bool Input::GetMouseButton(int button)
    {
        return sInputActive && sMouseButtonsPreviousFrame[button] && sMouseButtons[button];
    }

    bool Input::GetMouseButtonUp(int button)
    {
        return sInputActive && sMouseButtonsPreviousFrame[button] && !sMouseButtons[button];
    }

    bool Input::GetMouseDragging(int button)
    {
        return GetMouseButton(button) && GetMouseDelta(button) != Vector2(0.0f);
    }

    Vector2 Input::GetMouseDelta(int button)
    {
        return (GetMouseButton(button) ? mousePosition - sMouseButtonsClickedPosition[button] : Vector2(0.0f));
    }

}
