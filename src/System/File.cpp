//
// Created by Bruno Gouveia on 1/14/16.
//

#include <System/File.h>

typedef signed long int32;
typedef unsigned long uint32;

#ifndef WIN32
long _filelength(int filedes)
{
    struct stat statbuffer;

    fstat(filedes, &statbuffer);
    return statbuffer.st_size;
}
#endif

namespace RGTek
{

    //////////////////////////////////////////////////////////
    //
    // File implementation
    // ====
    bool
    File::open(const String& name, uint access, uint permission)
    {
        if (isOpen())
            return false;

        this->name = name;

        handle = ::_open(this->name.c_str(), access, permission);
        return isOpen();
    }

    bool
    File::close()
    {
        if (isOpen())
        {
            if (::_close(handle) != 0)
                return false;
            handle = fileNull;
        }
        return true;
    }

    int
    File::readArray(void*& buffer, int elementSize)
    {
        int32 length = 0;
        int size;

        size = read(length);
        if (length == 0)
            buffer = 0;
        else
        {
            buffer = new char[length *= elementSize];
            size += read(buffer, length);
        }
        return size;
    }

    int
    File::writeArray(const void* value, int length, int elementSize)
    {
        int size = write(length);

        if (length != 0)
            size += write(value, length * elementSize);
        return size;
    }


}