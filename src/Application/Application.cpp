#include <Application/Application.h>
#include <Resources/ResourceManager.h>
#include <Math/Vector2.h>
#include <Tools/ProfileTimer.h>
#include <Tools/Debugger.h>
#include <SDL2/SDL.h>
#undef main

namespace RGTek
{
    using namespace Math;

    ApplicationWeakPtr Application::sSharedApplication = NULL;

    Application::Application(WindowPtr window, ScenePtr scene)
        : mWindow(window)
        , mRenderingSystem(nullptr)
        , mPhysicsSystem(nullptr)
        , mScriptSystem(nullptr)
        , mEditorSystem(nullptr)
        , mScene(scene)
        , mRunning(false)
    {
        Debugger::Debug("Application start");
        Debugger::AssertNull(sSharedApplication, "More than one instance of Application.");

        sSharedApplication = this;
    }

    Application::~Application()
    {
        Debugger::Debug("Application shutdown");
        Debugger::AssertFalse(mRunning, "Application should not be deleted while is running");
        mScene->ClearScene();
    }

    void Application::Start()
    {
        if (mRunning)
        {
            return;
        }

#ifdef RGTEK_EDITOR_ENABLED
        if (EditorWindow* editorWindow = dynamic_cast<EditorWindow*>(GetWindow())) editorWindow->FillGraphSceneWindow();
#endif

        ProfileTimer mWindowProfileTimer;
        ProfileTimer mRenderingProfileTimer;
        ProfileTimer mPhysicsProfileTimer;
        ProfileTimer mScriptProfileTimer;
        ProfileTimer mEditorProfileTimer;

        Debugger::AssertNotNull(GetWindow(), "Window is null");
        Debugger::AssertNotNull(mRenderingSystem, "RenderingSystem is null.");
        Debugger::AssertNotNull(mPhysicsSystem, "PhysicsSystem is null.");
        Debugger::AssertNotNull(mScriptSystem, "ScriptSystem is null.");
#ifdef RGTEK_EDITOR_ENABLED
        Debugger::AssertNotNull(mEditorSystem, "EditorSystem is null.");
#endif

        mRunning = true;

        mPhysicsSystem->Start();
        mScriptSystem->Start();

        // Callback called to draw the OpenGLCanvas
        GetWindow()->SetDrawCallback([&] (Vector2 position, Vector2 size) {
            Vector2 canvasPos(position.x, position.y);
            Vector2 canvasSize(size.x, size.y);

            Viewport viewport(canvasPos, canvasSize);

            mRenderingProfileTimer.StartInvocation();
            mRenderingSystem->Render(Camera::GetMainCamera(), viewport);
            mRenderingProfileTimer.StopInvocation();
        });

#ifdef RGTEK_EDITOR_ENABLED
        if (mEditorSystem)
        {
            dynamic_cast<EditorWindow*>(GetWindow())->SetSceneViewCallback([&](Vector2 position, Vector2 size)
                                              {
                                                  Vector2 canvasPos(position.x, position.y);
                                                  Vector2 canvasSize(size.x, size.y);

                                                  Viewport viewport(canvasPos, canvasSize);

                                                  mEditorProfileTimer.StartInvocation();
                                                  mEditorSystem->DrawSceneView(0, viewport);
                                                  mEditorProfileTimer.StopInvocation();
                                              });

            mPhysicsSystem->Pause();
            mScriptSystem->Pause();
        }
#endif

        Time::Duration profileTimerDuration = Time::Duration(Time::Now());
        Time::TimePoint current = Time::Now();
        long int accumulator = 0;
        long int accumulator2 = 0;

        while (mRunning)
        {
            auto newTime = Time::Now();
            Time::Duration frameDuration(current, newTime);
            current = newTime;

            accumulator += frameDuration.Count();
            accumulator2 += frameDuration.Count();

            while (accumulator > 20)
            {
                mPhysicsProfileTimer.StartInvocation();
                mPhysicsSystem->StepSimulation();
                mPhysicsProfileTimer.StopInvocation();
                accumulator -= 20;
            }

            // Let window handle events and check if there is a close request
            GetWindow()->HandleEvents();

            if (GetWindow()->IsCloseRequested())
            {
                mRunning = false;
            }

            if (accumulator2 > 20)
            {
                // Update scene
                mScriptProfileTimer.StartInvocation();
                mScriptSystem->Update();
                mScriptProfileTimer.StopInvocation();

                mWindowProfileTimer.StartInvocation();
                GetWindow()->Render();
                mWindowProfileTimer.StopInvocation();

                GetWindow()->SwapBuffers();
                accumulator2 = 0;
            }
            else
            {
                SDL_Delay(1);
            }

            mScriptSystem->CheckChangeOnScripts();
            mRenderingSystem->GetResourceManager().CheckResourcesChanges();

            profileTimerDuration.End();
            if (profileTimerDuration.Count() > 5000)
            {
                Debugger::Debug("RenderingTime: %ld", mRenderingProfileTimer.GetDuration());
                Debugger::Debug("EditorTime   : %ld", mEditorProfileTimer.GetDuration());
                Debugger::Debug("PhysicsTime  : %ld", mPhysicsProfileTimer.GetDuration());
                Debugger::Debug("ScriptTime   : %ld", mScriptProfileTimer.GetDuration());
                Debugger::Debug("WindowTime   : %ld", mWindowProfileTimer.GetDuration());

                mRenderingProfileTimer.Reset();
                mEditorProfileTimer.Reset();
                mPhysicsProfileTimer.Reset();
                mScriptProfileTimer.Reset();
                mWindowProfileTimer.Reset();

                profileTimerDuration.Start();
                profileTimerDuration.End();
            }

        }
    }

    void Application::Stop()
    {
        mScriptSystem->Stop();
        mPhysicsSystem->Stop();

        mRunning = false;
    }

    void Application::AddCustomSystem(SystemPtr system, int frequency)
    {
        mScriptSystem = dynamic_cast<ScriptSystem*>(static_cast<System*>(system));

        if (mScene)
        {
            mScene->AddSystem(system);
        }
    }

}