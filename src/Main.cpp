﻿#include <Core/Platform.h>
#include <Application/Application.h>

#include <Editor/EditorItem/EditorItemManager.h>
#include <Editor/Custom/LightComponentEditor.h>

#include <ScriptSystem/Components/CameraController.h>
#include <ScriptSystem/Components/CollisionTest.h>

#include <Core/Reflection/Type.h>
#include <Core/Reflection/TypeInfo.h>
#include <Core/Reflection/TypeData.h>
#include <Core/Reflection/TypeDatabase.h>

int main( int argc, char** argv )
{
    if (argc == 2)
    {
        switch (atoi(argv[1]))
        {
            case 1:
            {
                using namespace RGTek;
                WindowPtr window = new Window("RGTek", 720, 720);
                ShaderPtr shader = new Shader("../res/shaders/ShaderExample1.shader");
                MeshPtr mesh = new Mesh("../res/Primitives/models/cube.obj");
                shader->UseShader();
                while (!window->IsCloseRequested())
                {
                    window->HandleEvents();
                    window->Clear();
                    // Alternativa para esse ’for’ seria "mesh->Draw();"
                    for (unsigned int i = 0; i < mesh->GetSubMeshes().size(); ++i)
                    {
                        MeshDataWeakPtr subMesh = mesh->GetSubMeshes()[i];
                        subMesh->Draw();
                    }
                    window->SwapBuffers();
                }
                break;
            }
            case 2:
            {
                using namespace RGTek;
                WindowPtr window = new Window("RGTek", 720, 720);
                ResourceManagerPtr resourceManager = new ResourceManager("../res/");
                ShaderHandle shaderHandle = resourceManager->CreateResource <Shader>("shaders/ShaderExample2.shader");
                MeshHandle meshHandle = resourceManager->CreateResource<Mesh>("models/cube.obj");

                // Obter endereço atual do objeto
                Shader* shader = resourceManager->GetResource <Shader>(shaderHandle);
                shader->UseShader();

                while (!window->IsCloseRequested())
                {
                    window->HandleEvents();
                    window->Clear();

                    // Necessário obter referência em cada iteração.
                    Shader *shader = resourceManager->GetResource<Shader>(shaderHandle);
                    shader->UseShader();
                    Mesh *mesh = resourceManager->GetResource<Mesh>(meshHandle);

                    for (unsigned int i = 0; i < mesh->GetSubMeshes().size(); ++i)
                    {
                        MeshDataWeakPtr subMesh = mesh->GetSubMeshes()[i];

                        Material *material = resourceManager->GetResource<Material>(mesh->GetMaterials()[i]);
                        material->UpdateUniformsToShader(shader);

                        subMesh->Draw();
                    }
                    resourceManager->CheckResourcesChanges();

                    window->SwapBuffers();
                }
                break;
            }
            case 3:
            {
                using namespace RGTek;

                // Create window, scene and application
                WindowPtr window = new Window("RGTek", 1280, 720);
                ScenePtr scene = new Scene();
                ApplicationPtr application = new Application(window, scene);

                // Setup systems if available
#ifdef RGTEK_RENDERING_ENABLED
                RenderingSystemPtr renderingSystem = new RenderingSystem(window);
                application->SetRenderingSystem(renderingSystem);
                scene->AddSystem(renderingSystem);
#endif

#ifdef RGTEK_PHYSICS_ENABLED
                PhysicsSystemPtr physicsSystem = new PhysicsSystem();
                application->SetPhysicsSystem(physicsSystem);
                scene->AddSystem(physicsSystem);
#endif

#ifdef RGTEK_SCRIPT_ENABLED
                ScriptSystemPtr scriptSystem = new ScriptSystem();
                application->SetScriptSystem(scriptSystem);
                scene->AddSystem(scriptSystem);

                scriptSystem->RegisterScriptInsantiator(CameraController::COMPONENT_NAME, CameraController::CreateInstance);
                scriptSystem->RegisterScriptInsantiator(CollisionTest::COMPONENT_NAME, CollisionTest::CreateInstance);
#endif

                scene->LoadFromFile("../DefaultScene.scene");

                application->Run();

            }
            default:
            {
                break;
            }
        }
    }
    else
    {
        using namespace RGTek;
        // Create window
#ifdef RGTEK_EDITOR_ENABLED
        EditorWindowPtr window = new EditorWindow("RGTek", 1280, 720, argc, argv);
#else
        RGTek::WindowPtr window = new RGTek::Window("RGTek", 1280, 720);
#endif
        // Create scene and application
        ScenePtr scene = new Scene();
        ApplicationPtr application = new Application(window, scene);

        // Setup systems if available
#ifdef RGTEK_RENDERING_ENABLED
        RenderingSystemPtr renderingSystem = new RenderingSystem(window);
        application->SetRenderingSystem(renderingSystem);
        scene->AddSystem(renderingSystem);

		renderingSystem->GetResourceManager().SetReourcePath("../res/");
#endif

#ifdef RGTEK_PHYSICS_ENABLED
        PhysicsSystemPtr physicsSystem = new PhysicsSystem();
        application->SetPhysicsSystem(physicsSystem);
        scene->AddSystem(physicsSystem);
#endif

#ifdef RGTEK_SCRIPT_ENABLED
        ScriptSystemPtr scriptSystem = new ScriptSystem();
        application->SetScriptSystem(scriptSystem);
        scene->AddSystem(scriptSystem);

        scriptSystem->RegisterScriptInsantiator(CameraController::COMPONENT_NAME, CameraController::CreateInstance);
        scriptSystem->RegisterScriptInsantiator(CollisionTest::COMPONENT_NAME, CollisionTest::CreateInstance);
#endif

        // Setup editor if available
#ifdef RGTEK_EDITOR_ENABLED
        EditorSystemPtr editorSystem = new EditorSystem(window, renderingSystem, physicsSystem, scriptSystem);
        application->SetEditorSystem(editorSystem);
        editorSystem->SetCurrentScene(scene);
        
        //LightComponent* lc = new LightComponent(Color::white);
        //Debugger::Info("Num properties s:%d", lc->GetClass().GetProperties().size());
        //
        editorSystem->GetEditorItemManager().RegisterEditorItemInstantiator("LightComponent", LightComponentEditor::InstantiateEditorItem);
#endif

        scene->LoadFromFile("../DefaultScene.scene");

        application->Run();
    }
	RGTek::TypeDatabase::GetInstance().Destroy();
    RGTek::Debugger::Debug("RGTek - shutdown");
    return 0;
}
