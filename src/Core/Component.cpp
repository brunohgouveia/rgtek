#include <Core/Component.h>
#include <Core/System.h>

namespace RGTek {

	RCOMPONENT_IMPLEMENTATION(Component);

	Component::Component()
	{

	}

	Component::~Component()
	{

	}

	void Component::Update()
	{

	}

	void Component::RegisterToSystem()
	{

	}

	void Component::OnCollisionEnter(const CollisionPtr& collision)
	{

	}

	void Component::OnCollisionStay(const CollisionPtr& collision)
	{

	}

	void Component::OnCollisionExit(const CollisionPtr& collision)
	{

	}

	void Component::RenderGizmos()
    {

    }

	void Component::Destroy(Component* component)
	{
        System* system = component->GetSystem();
        system->DeleteComponent(component);
	}

}
