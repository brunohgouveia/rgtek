#include <Core/Reflection/Property.h>

namespace RGTek
{

	Property::Property()
		: mName("INVALID")
		, mClassType(Type::Invalid)
		, mValueType(Type::Invalid)
		, mFieldData(nullptr)
	{
		// Do nothing
	}

	Property::Property(const String& name, Type classType, Type valueType, IPropertyData* fieldData)
		: mName(name)
	    , mClassType(classType)
	    , mValueType(valueType)
	    , mFieldData(fieldData)
	{
		// Do nothing
	}

	Property::Property(Property&& prop)
		: mFieldData(nullptr)
	{
		std::swap(mName, prop.mName);
		std::swap(mClassType, prop.mClassType);
		std::swap(mValueType, prop.mValueType);
		std::swap(mFieldData, prop.mFieldData);
	}
	
	Property::Property(const Property& prop)
		: mName(prop.mName)
		, mClassType(prop.mClassType)
		, mValueType(prop.mValueType)
		, mFieldData(prop.mFieldData)
	{
		// Do nothing
	}


	bool Property::IsReadOnly() const
	{
		return !mFieldData->HasSetter();
	}

	Type Property::GetClassType() const
	{
		return mClassType;
	}

	Type Property::GetValueType() const
	{
		return mValueType;
	}

	const String& Property::GetName() const
	{
		return mName;
	}

	Property& Property::operator=(const Property& prop)
	{
		mName = prop.mName;
		mClassType = prop.mClassType;
		mValueType = prop.mValueType;
		mFieldData = mFieldData;

		return *this;
	}
}
