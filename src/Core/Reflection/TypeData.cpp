#include <Core/Reflection/TypeData.h>

namespace RGTek
{
	TypeData::TypeData()
		: isEnum(false)
		, isPrimitive(false)
		, isPointer(false)
		, isClass(false)
	{
		// Do nothing
	}

	TypeData::TypeData(const std::string& name)
		: isEnum(false)
		, isPrimitive(false)
		, isPointer(false)
		, isClass(false)
		, name(name)
	{
		// Do nothing
	}

	TypeData::~TypeData()
	{
		// Do nothing
	}
}
