#include <Core/Reflection/TypeDatabase.h>

#include <Core/Reflection/TypeInfo.h>
#include <Core/Reflection/TypeData.h>

#include <Math/Vector2.h>
#include <Math/Vector3.h>
#include <Math/Vector4.h>
#include <Math/Quaternion.h>

// Define macros internally, because it might be confusing for users
// to put them along other REGISTER* macros into 'TypeDefinitions.h'.
#define REGISTER_PRIMITIVE_TYPE(type)					\
	{													\
		auto typeID = AllocateType(#type);				\
		auto& handle = mTypes[typeID];					\
														\
		TypeInfo<type>::Register(typeID, handle, true);	\
	}													\

#define REGISTER_PRIMITIVE_TYPE_VARIANTS(type)	\
	REGISTER_PRIMITIVE_TYPE(type)				\
	REGISTER_PRIMITIVE_TYPE(type*)				\
	REGISTER_PRIMITIVE_TYPE(const type*)		\

namespace RGTek
{

	TypeDatabase::TypeDatabase()
		: mNextTypeID(1)
		, mTypes(1)
	{
		mTypes[Type::Invalid].name = "INVALID";

		mTypes.reserve(64);

		// Register all primitive types and its variations
		REGISTER_PRIMITIVE_TYPE_VARIANTS(void);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(int);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(bool);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(float);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(double);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(std::string);

		// Register Math classes automatically, because
		// they are kind of primitives
		REGISTER_PRIMITIVE_TYPE_VARIANTS(Math::Vector2);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(Math::Vector3);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(Math::Vector4);
		REGISTER_PRIMITIVE_TYPE_VARIANTS(Math::Quaternion);
	}

	///////////////////////////////////////////////////////////////////////

	TypeDatabase::~TypeDatabase()
	{
		mTypes.clear();
		mIDMap.clear();
	}

	///////////////////////////////////////////////////////////////////////

	TypeDatabase& TypeDatabase::GetInstance()
	{
		static TypeDatabase instance;
		return instance;
	}

	void TypeDatabase::Destroy()
	{
		mTypes.clear();
		mIDMap.clear();
	}

	///////////////////////////////////////////////////////////////////////

	TypeID TypeDatabase::AllocateType(const String& name)
	{
		// Check if type already exists
		if (mIDMap.count(name) > 0)
		{
			return Type::Invalid;
		}

		// Emplate new DataType
		mTypes.emplace_back(name);

		// Generate new typeID
		TypeID newTypeID = mNextTypeID++;
		mIDMap[name] = newTypeID;

		return newTypeID;
	}

	///////////////////////////////////////////////////////////////////////

	std::vector<TypeData>& TypeDatabase::GetTypes()
	{
		return mTypes;
	}

	TypeData& TypeDatabase::GetTypeData(TypeID typeID)
	{
		return mTypes[typeID];
	}

	const TypeData& TypeDatabase::GetTypeData(TypeID typeID) const
	{
		return mTypes[typeID];
	}
}
