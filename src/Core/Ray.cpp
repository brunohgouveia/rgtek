//
// Created by Bruno Gouveia on 12/7/15.
//

#include <Core/Ray.h>
#include <Core/Transform.h>
#include <Math/Math.h>

namespace RGTek
{

	Ray::Ray(const Vector3& origin, const Vector3& direction)
		: origin(origin)
		, direction(direction)
	{
		inverseDirection = Math::Vector3(1.0f / direction.x, 1.0f / direction.y, 1.0f / direction.z);
		negative[0] = static_cast<unsigned int>(direction.x < 0.0f);
		negative[1] = static_cast<unsigned int>(direction.y < 0.0f);
		negative[2] = static_cast<unsigned int>(direction.z < 0.0f);
	}

	bool Ray::IntersectPlane(const Plane& plane, Vector3& intersectionPoint) const
	{
		// Plane equation is all points p such that <(p - p0),normal> == 0
		float dn = direction.Normalized().Dot(plane.normal);

		// Check if plane and ray are parallel
		// If the inner product between the ray's direction and plane's normal is 0, they are parallel.
		if (dn == 0.0f) return false;

		// A point p in the ray's line is all p such that p = origin + t * direction
		// Now compute the t such that p lies on the plane and the ray.
		float t = (plane.point - origin).Dot(plane.normal) / dn;

		intersectionPoint = origin + t * direction.Normalized();

		return true;
	}

	bool Ray::IntersectSphere(const Vector3 &center, float radius, Vector3 &intersectionPoint) const
	{
		Vector3 rayDirection = direction.Normalized();
		Vector3 sphereDirection = (origin - center);

		float innerRaySphere = rayDirection.Dot(sphereDirection);

		float sqrPart = (innerRaySphere * innerRaySphere) - sphereDirection.MagnitudeSq() + (radius * radius);

		if (sqrPart < 0.0f) return false;

		else if (Math::IsEqual(sqrPart, 0.0f))
		{
			intersectionPoint = origin - innerRaySphere * rayDirection;
			return -innerRaySphere > 0.0f;
		}
		else
		{
			float innerMinusSqrtPart = -innerRaySphere - Math::Sqrt(sqrPart);
			float innerPlusSqrtPart = -innerRaySphere + Math::Sqrt(sqrPart);

			if (innerMinusSqrtPart >= 0.0f && innerMinusSqrtPart <= innerPlusSqrtPart)
			{
				intersectionPoint = origin + innerMinusSqrtPart * rayDirection;
				return true;
			}
			if (innerPlusSqrtPart >= 0.0f && innerPlusSqrtPart <= innerMinusSqrtPart)
			{
				intersectionPoint = origin + innerPlusSqrtPart * rayDirection;
				return true;
			}

			return false;
		}

	}

	bool Ray::IntersectOBB(const Transform& transform, const Vector3& min, const Vector3& max, Vector3& intersectionPoint) const
	{
		float tMin = 0.0f;
		float tMax = std::numeric_limits<float>::max();

		Vector3 delta = transform.GetPosition() - origin;

		// Test intersection with the 2 planes perpendicular to the OBB's X axis
		{
			Vector3 xAxis = transform.GetRotation().GetRight();
			float e = xAxis.Dot(delta);
			float f = direction.Dot(xAxis);

			if (Math::Abs(f) > 0.0001f)
			{
				float t1 = (e + min.x) / f; // Intersection with the "left" plane
				float t2 = (e + max.x) / f; // Intersection with the "right" plane
				// t1 and t2 now contain distances betwen ray origin and ray-plane intersections

				// We want t1 to represent the nearest intersection, 
				// so if it's not the case, invert t1 and t2
				if (t1 > t2){
					float w = t1; t1 = t2; t2 = w; // swap t1 and t2
				}

				// tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
				if (t2 < tMax)
					tMax = t2;
				// tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
				if (t1 > tMin)
					tMin = t1;

				// And here's the trick :
				// If "far" is closer than "near", then there is NO intersection.
				// See the images in the tutorials for the visual explanation.
				if (tMax < tMin)
					return false;
			}
			else
			{ // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
				if (-e + min.x > 0.0f || -e + max.x < 0.0f)
					return false;
			}
		}

		// Test intersection with the 2 planes perpendicular to the OBB's Y axis
		// Exactly the same thing than above.
		{
			Vector3 yAxis = transform.GetRotation().GetUp();
			float e = yAxis.Dot(delta);
			float f = direction.Dot(yAxis);

			if (fabs(f) > 0.001f){

				float t1 = (e + min.y) / f;
				float t2 = (e + max.y) / f;

				if (t1 > t2){ float w = t1; t1 = t2; t2 = w; }

				if (t2 < tMax)
					tMax = t2;
				if (t1 > tMin)
					tMin = t1;
				if (tMin > tMax)
					return false;

			}
			else{
				if (-e + min.y > 0.0f || -e + max.y < 0.0f)
					return false;
			}
		}

		// Test intersection with the 2 planes perpendicular to the OBB's Z axis
		// Exactly the same thing than above.
		{
			Vector3 zAxis = transform.GetRotation().GetForward();
			float e = zAxis.Dot(delta);
			float f = direction.Dot(zAxis);

			if (fabs(f) > 0.001f){

				float t1 = (e + min.z) / f;
				float t2 = (e + max.z) / f;

				if (t1 > t2){ float w = t1; t1 = t2; t2 = w; }

				if (t2 < tMax)
					tMax = t2;
				if (t1 > tMin)
					tMin = t1;
				if (tMin > tMax)
					return false;

			}
			else{
				if (-e + min.z > 0.0f || -e + max.z < 0.0f)
					return false;
			}
		}

		intersectionPoint = GetPoint(tMin);
		return true;
	}

}