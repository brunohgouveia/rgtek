#include <Core/Transform.h>
#include <Physics/Components/Rigidbody.h>

#ifdef RGTEK_PHYSICS_ENABLED
#include <LinearMath/btTransform.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#endif

#include <Tools/Debugger.h>

namespace RGTek
{

	Transform::Transform()
		: mPosition(0.0f)
		, mRotation(0.0f, 0.0f, 0.0f)
		, mScale(1.0f)
		, mParent(nullptr)
		, mParentMatrix(Matrix4().InitIdentity())
		, mInitializedOldStuff(false)
	{
		
	}


	Transform::Transform(const Vector3& pos, const Quaternion& rot, const Vector3& scale) 
		: mPosition(pos)
		, mRotation(rot)
		, mScale(scale)
		, mParent(nullptr)
		, mParentMatrix(Matrix4().InitIdentity())
		, mInitializedOldStuff(false)
    {
        // Do nothing
    }

#ifdef RGTEK_PHYSICS_ENABLED
    Transform::Transform(const btTransform& transform)
    {
        (*this) = transform;
    }
#endif

    Transform::~Transform()
    {
        // Do nothing
    }

    bool Transform::HasChanged()
    {
        if(mParent != 0 && mParent->HasChanged())
        {
            return true;
        }

        if(mPosition != mOldPosition)
        {
            return true;
        }

        if(mRotation != mOldRotation)
        {
            return true;
        }

        if(mScale != mScale)
        {
            return true;
        }

        return false;
    }

    void Transform::Update()
    {
        if(mInitializedOldStuff)
        {
            mOldPosition = mPosition;
            mOldRotation = mRotation;
            mOldScale = mScale;
        }
        else
        {
            mOldPosition = mPosition + Vector3(1,1,1);
            mOldRotation = mRotation * 0.5f;
            mOldScale = mScale + Vector3(1,1,1);
            mInitializedOldStuff = true;
        }
    }

    void Transform::Translate(const Vector3& translation)
    {
        SetPosition(mPosition + translation);
    }

    void Transform::Rotate(const Vector3& axis, float angle, const Space& relativeTo)
    {
        Rotate(Quaternion(axis, angle), relativeTo);
    }

    void Transform::Rotate(const Quaternion& rotation, const Space& relativeTo)
    {
        if (relativeTo == Space::Self)
        {
            SetRotation(Quaternion((mRotation * rotation).Normalized()));
        }
        else
        {
            SetRotation(Quaternion((rotation * mRotation).Normalized()));
        }
    }

    void Transform::LookAt(const Vector3& point, const Vector3& up)
    {
        SetRotation(GetLookAtRotation(point, up));
    }

    Matrix4 Transform::GetTransformation() const
    {
        Matrix4 translationMatrix;
        Matrix4 scaleMatrix;

        translationMatrix.InitTranslation(Vector3(mPosition.x, mPosition.y, mPosition.z));
        scaleMatrix.InitScale(mScale);

        Matrix4 result = translationMatrix * mRotation.ToRotationMatrix() * scaleMatrix;

        return GetParentMatrix() * result;
    }

    const Matrix4& Transform::GetParentMatrix() const
    {
        if(mParent != 0 && mParent->HasChanged())
        {
            mParentMatrix = mParent->GetTransformation();
        }

        return mParentMatrix;
    }

    Vector3 Transform::GetTransformedPosition() const
    {
        return Vector3(GetParentMatrix().Transform(mPosition));
    }

    Quaternion Transform::GetTransformedRotation() const
    {
        Quaternion parentRot = Quaternion(0,0,0,1);

        if(mParent)
        {
            parentRot = mParent->GetTransformedRotation();
        }

        return parentRot * mRotation;
    }

    void Transform::SetPosition(const Vector3& pos)
    {
        mPosition = pos;
		OnSetTransform();
    }

    void Transform::SetRotation(const Quaternion& rot)
    {
        mRotation = rot;
		OnSetTransform();
    }

    void Transform::SetScale(const Vector3& scale)
    {
        mScale = scale;

        // No need to synchronize rigidbody's transform, because there is no scale factor on it.
    }

    void Transform::SetParent(Transform* parent)
    {
        mParent = parent;
    }

    DataTranslator* Transform::GetDataTranslator()
    {
        auto* dataTranslator = new CustomObjectDataTranslator<Transform>("Transform", this);
        dataTranslator->Add("Position", DataTranslatorProperty<Transform, Vector3>(this, &Transform::GetPosition, &Transform::SetPosition));
        dataTranslator->Add("Rotation", DataTranslatorProperty<Transform, Quaternion>(this, &Transform::GetRotation, &Transform::SetRotation));
        dataTranslator->Add("Scale", DataTranslatorProperty<Transform, Vector3>(this, &Transform::GetScale, &Transform::SetScale));

        return dataTranslator;
    }

#ifdef RGTEK_PHYSICS_ENABLED
	Transform& Transform::operator=(const btTransform& transform)
    {
        mPosition = transform.getOrigin();
        mRotation = transform.getRotation();

        return *this;
    }

    Transform::operator btTransform() const
    {
        return btTransform(static_cast<btQuaternion>(mRotation), static_cast<btVector3>(mPosition));
    }
#endif
}
