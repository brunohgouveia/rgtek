//
// Created by Bruno Gouveia on 2/3/16.
//

#include <Core/Window.h>
#include <System/Input.h>
#include <System/InputMap.h>
#include <Tools/Debugger.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#undef main

namespace RGTek
{

    Window::Window(const string& title, int width, int height)
        : mTitle(title)
        , mWidth(width)
        , mHeight(height)
        , mIsCloseRequested(false)
        , mCanvasCallback(nullptr)
    {
        Debugger::Debug("Window starting...");

        // init everything from SDL
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        {
            Debugger::Fatal("SDL could not be initialized! Error message: %s", SDL_GetError());
        }
		SDL_GL_LoadLibrary(nullptr);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

        //	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        //	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

        //SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );

        mWindow = SDL_CreateWindow(mTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                   mWidth, mHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        Debugger::AssertNotNull(mWindow, "Could not create SDL window: %s", SDL_GetError());

        mContext = SDL_GL_CreateContext(mWindow);
//        SDL_SetWindowMinimumSize(mWindow, 800, 600);

        //SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
        SDL_GL_SetSwapInterval(1);

        //Apparently this is necessary to build with Xcode
        glewExperimental = GL_TRUE;

        GLenum res = glewInit();
        if(res != GLEW_OK)
        {
            Debugger::Fatal("GlewError: '%s'\n", glewGetErrorString(res));
        }
        else
        {
            // Consumer error caused due glewExperimental
            glGetError();
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	        Window::ClearColor();
            glEnable(GL_DEPTH_TEST);
            glDisable(GL_BLEND);
        }

        Input::Init();
    }

    Window::~Window()
    {
        Debugger::Debug("Window shutting down...");
		Input::Finish();
        SDL_GL_DeleteContext(mContext);
        SDL_DestroyWindow(mWindow);
		SDL_Quit();
    }

    void Window::Render()
    {
        if (mCanvasCallback)
        {
            mCanvasCallback(Vector2(0.0f, 0.0f), GetFramebufferSize());
        }

        Input::ResetMap();
        Input::EndMouseInjection();
    }

    void Window::HandleEvents()
    {
        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    mIsCloseRequested = true;
                    break;

                case SDL_MOUSEMOTION:
                {
                    Vector2 windowPD = GetWindowPixelDensity();
                    Input::mousePosition = Vector2(windowPD.x * event.motion.x, GetFramebufferSize().y - windowPD.y * event.motion.y);
                    Input::sInputActive = true;
                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                {
                    Input::InjectMouseDown(event.button.button - 1u);
                    break;
                }
                case SDL_MOUSEBUTTONUP:
                {
                    Input::InjectMouseUp(event.button.button - 1u);
                    break;
                }
                case SDL_MOUSEWHEEL:
                {
                    Input::mouseWheel = event.wheel.y;
                    break;
                }
                case SDL_KEYDOWN:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode));

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt);
                    }
                    break;
                }
                case SDL_KEYUP:
                {
                    Input::SetKey(InputMap::GetKeyFromSDL(event.key.keysym.scancode), false);

                    if (event.key.keysym.scancode == SDL_SCANCODE_LALT)
                    {
                        Input::SetKey(KeyCode::Alt, false);
                    }
                    break;
                }
                case SDL_TEXTINPUT:
                {
                    // Do nothing
                    break;
                }
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                    {
                        Vector2 framebufferSize = GetFramebufferSize();
                        glViewport(0, 0, static_cast<int>(framebufferSize.x), static_cast<int>(framebufferSize.y));
                    }
                    break;

                default:
                    break;

            }
        }
    }

    void Window::SwapBuffers()
    {
        SDL_GL_SwapWindow(mWindow);
    }

    void Window::BindAsRenderTarget()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glScissor(0, 0, static_cast<int>(GetFramebufferSize().x), static_cast<int>(GetFramebufferSize().y));
        glViewport(0, 0, static_cast<int>(GetFramebufferSize().x), static_cast<int>(GetFramebufferSize().y));
    }

    void Window::ClearColor()
    {
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void Window::ClearDepth()
    {
        glClear(GL_DEPTH_BUFFER_BIT);
    }

    void Window::Clear()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void Window::BindCanvasAsRenderTarget(const Viewport& viewport)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glScissor(
                static_cast<int>(viewport.position.x),
                static_cast<int>(viewport.position.y),
                static_cast<int>(viewport.size.x),
                static_cast<int>(viewport.size.y)
        );

        glViewport(
                static_cast<int>(viewport.position.x),
                static_cast<int>(viewport.position.y),
                static_cast<int>(viewport.size.x),
                static_cast<int>(viewport.size.y)
        );
    }

    Vector2 Window::GetWindowPixelDensity() const
    {
        return GetFramebufferSize() / GetWindowSize();
    }

    Vector2 Window::GetWindowSize() const
    {
        int width, height;
        SDL_GetWindowSize(mWindow, &width, &height);
        return Vector2(width, height);
    }

    Vector2 Window::GetFramebufferSize() const
    {
        int width, height;
        SDL_GL_GetDrawableSize(mWindow, &width, &height);
        return Vector2(width, height);
    }

}