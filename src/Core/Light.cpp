#include <Core/Light.h>
#include <Tools/Debugger.h>

namespace RGTek
{
    void BaseLight::UpdateUniformsToShader(ShaderWeakPtr shader)
    {
        shader->SetUniform("LightPosition", Vector4(GetTransform()->GetPosition(), 1.0f));
        shader->SetUniform("LightColor", GetColor());
        shader->SetUniform("LightBias", GetLightBias());
        shader->SetUniform("LightVPMatrix", GetViewProjection());

        const Matrix4 BIAS_MATRIX = Matrix4().InitScale(Vector3(0.5, 0.5, 0.5)) * Matrix4().InitTranslation(Vector3(1.0, 1.0, 1.0));
        shader->SetUniform("LightBiasVPMatrix", BIAS_MATRIX * GetViewProjection());
        shader->SetUniform("depthText", 1);
    }

    SpotLight::SpotLight(const Color &color, float intensity)
        : SpotLight(new Transform(), color, intensity)
    {

    }

    SpotLight::SpotLight(TransformWeakPtr transform, const Color &color, float intensity)
        : BaseLight(transform, color, intensity)
        , mSpotAngle(55.0f)
        , mRange(10.0f)
    {
        // Do nothing
    }

    void SpotLight::UpdateUniformsToShader(ShaderWeakPtr shader)
    {
        BaseLight::UpdateUniformsToShader(shader);
        shader->SetUniform("LightRange", GetRange());
        shader->SetUniform("LightSpotAngle", GetSpotAngle());
        shader->SetUniform("LightIntensity", GetIntensity());
    }

    Matrix4 SpotLight::GetViewProjection()
    {
        Debugger::AssertNotNull(mTransform, "Light's transform should not be null");

        Matrix4 cameraRotation = GetTransform()->GetTransformedRotation().Conjugate().ToRotationMatrix();
        Matrix4 cameraTranslation;
        cameraTranslation.InitTranslation(GetTransform()->GetTransformedPosition() * -1);

        Matrix4 viewMatrix = cameraRotation * cameraTranslation;

        Matrix4 projectionMatrix = Matrix4().InitPerspective(Math::ToRadians(mSpotAngle), 1.0f, 0.001f, mRange);

        return projectionMatrix * viewMatrix;
    }
}
