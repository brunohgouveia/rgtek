#include <Core/Entity.h>
#include <Core/Component.h>
#include <Physics/Collision.h>
#include <Tools/Debugger.h>
#include <algorithm>

namespace RGTek
{
    InstanceID                 Entity::sCurrentInstanceID = 0u;

    Entity::Entity(const String& name, Transform* transform)
        : mInstanceID(sCurrentInstanceID++)
        , mName(name)
        , mTransform(transform)
        , mStatic(false)
    {
        Debugger::Debug("Entity start");
    }

    Entity::~Entity()
    {
        Debugger::Debug("Entity shutting down... %s", mName.c_str());
        for (Component* component : mComponents)
        {
            Component::Destroy(component);
        }
        Debugger::Debug("Entity shutting down... done %s", mName.c_str());
    }

    void Entity::AddChild(EntityPtr entity)
    {
        Debugger::AssertNotNull(entity, "Trying to add null entity.");
        mChildren.push_back(entity);
        entity->RegisterComponents();
        entity->GetTransform()->SetParent(GetTransform());
    }

    void Entity::AddComponent(Component* component)
    {
        Debugger::AssertNotNull(component, "Trying to add null component.");
        mComponents.push_back(component);
        component->SetEntity(this);
    }

	void Entity::DeleteComponent(Component* component)
    {
        Debugger::AssertNotNull(component, "Trying to delete null component.");
        mComponents.erase(std::remove(mComponents.begin(), mComponents.end(), component), mComponents.end());
        Component::Destroy(component);
    }

    void Entity::RegisterComponents()
    {
        for (Component* component : mComponents)
        {
            Debugger::AssertNotNull(component, "Trying to register null component");
            component->RegisterToSystem();
        }
//        for (GameComponentPtrVectorItr component = mComponents.begin(); component != mComponents.end(); ++component)
//        {
//            Debugger::AssertNotNull((*component), "Trying to register null component");
//            (*component)->RegisterToSystem();
//        }
    }

    void Entity::OnCollisionEnter(const CollisionPtr& collision)
    {
        for (Component* component : mComponents)
        {
            Debugger::AssertNotNull(component, "Null component.");
            component->OnCollisionEnter(collision);
        }
//
//        for (const ObjectHandlePtr<Component>& component : mScripts)
//        {
//            Debugger::AssertNotNull(component, "Null component.");
//            component->OnCollisionEnter(collision);
//        }
    }

    void Entity::OnCollisionStay(const CollisionPtr& collision)
    {
        for (Component* component : mComponents)
        {
            Debugger::AssertNotNull(component, "Null component.");
            component->OnCollisionStay(collision);
        }
//
//        for (const ObjectHandlePtr<Component>& component : mScripts)
//        {
//            Debugger::AssertNotNull(component, "Null component.");
//            component->OnCollisionStay(collision);
//        }
    }

    void Entity::OnCollisionExit(const CollisionPtr& collision)
    {
        for (Component* component : mComponents)
        {
            Debugger::AssertNotNull(component, "Null component.");
            component->OnCollisionExit(collision);
        }
//
//        for (const ObjectHandlePtr<Component>& component : mScripts)
//        {
//            Debugger::AssertNotNull(component, "Null component.");
//            component->OnCollisionExit(collision);
//        }
    }

    void Entity::RenderGizmos()
    {
        for (const EntityPtr& child : mChildren)
        {
            child->RenderGizmos();
        }
        for (Component* component : mComponents)
        {
            Debugger::AssertNotNull(component, "Null component.");
            component->RenderGizmos();
        }
    }

}
