#include <Core/Scene.h>
#include <Util/Parser/Parser.h>

namespace RGTek
{

    Scene::~Scene()
    {
        Debugger::Debug("Shutting down Scene...");
		delete mRoot;
    }

    Entity* Scene::FindEntityByName(const String& entityName)
    {
        for (EntityPtr entity : GetRoot()->GetChildren())
        {
            if (entity->GetName() == entityName)
            {
                return entity;
            }
        }
        return nullptr;
    }

    void Scene::LoadFromFile(const String& fileName)
    {
        mSceneFile = fileName;

        ClearScene();

        Parser parser;

        DataBin* sceneData = parser.Parse(fileName);

        DataBin* entitiesData = sceneData->GetChildByName("Entities");
        if (entitiesData)
        {
            for (DataBin* entityData : entitiesData->GetChildren())
            {
                EntityPtr newEntity = new Entity(entityData->GetLabel());
//                DataBin* transform = entityData->GetChildByName("Transform");
//                if (transform)
//                {
//                    DataTranslator* dt = newEntity->GetTransform()->GetDataTranslator();
//                    dt->TranslateFrom(transform);
//                }
//
//                DataBin* renderingSystemDataBin = entityData->GetChildByName(mRenderingSystem->GetSystemName());
//                if (renderingSystemDataBin)
//                {
//                    for (auto* componentDataBin : renderingSystemDataBin->GetChildren())
//                    {
//                        GameComponentWeakPtr component = mRenderingSystem->CreateComponentByName(componentDataBin->GetName());
//
//                        DataTranslator* dataTranslator = component->GetDataTranslator();
//                        dataTranslator->TranslateFrom(componentDataBin);
//
//                        newEntity->AddComponent(component);
//
//                        delete dataTranslator;
//                    }
//                }

                for (DataBin* systemDataBin : entityData->GetChildren())
                {
                    if (systemDataBin->GetName() == "Transform")
                    {
                        DataTranslator* dt = newEntity->GetTransform()->GetDataTranslator();
                        dt->TranslateFrom(systemDataBin);
						delete dt;
                    }
                    else
                    {
                        System* system = GetSceneByName(systemDataBin->GetName());
                        // TODO -delete this
                        if (!system) continue;
                        for (auto* componentDataBin : systemDataBin->GetChildren())
                        {
                            Component* component = system->CreateComponentByName(componentDataBin->GetName());

                            // TODO -delete this
                            if (!component) continue;

                            newEntity->AddComponent(component);

                            DataTranslator* dataTranslator = component->GetDataTranslator();
                            dataTranslator->TranslateFrom(componentDataBin);
                            delete dataTranslator;
                        }
                    }
                }

                AddEntity(newEntity);
            }
        }

		sceneData->DeleteChildren();
        delete sceneData;
    }

    void Scene::SaveToFile(const String &fileName)
    {
        mSceneFile = fileName;

        DataBin* sceneData = new DataBin("Scene");
        sceneData->SetLabel("SceneName");

        DataBin* entitiesData = new DataBin("Entities", sceneData);
        for (auto entity : GetRoot()->GetChildren())
        {
            DataBin*entityData = new DataBin("Entity", entitiesData);
            entityData->SetLabel(entity->GetName());

            // Save Transform
            DataBin* transformDataBin = new DataBin("Transform", entityData);

            DataTranslator* transformDataTranslator = entity->GetTransform()->GetDataTranslator();
            transformDataTranslator->TranslateTo(transformDataBin);
            delete transformDataTranslator;

            for (auto& systemPair : mSystems)
            {
                System* system = systemPair.second;
                DataBin* systemData = new DataBin(system->GetSystemName(), entityData);

                for (auto component : entity->GetAllComponents())
                {
                    // Ignore if this component does not belong to the current system
                    if (component->GetSystem() != system) continue;

                    DataBin* componentData = new DataBin(component->GetComponentName(), systemData);

                    // Check if Component implements the GetDataTranslator method
                    if (DataTranslator* dataTranslator = component->GetDataTranslator())
                    {
                        if (!dataTranslator) continue;

                        dataTranslator->TranslateTo(componentData);
                        delete dataTranslator;
                    }
                }
            }

        }

        Parser parser;
        parser.SaveToFile(fileName, sceneData);

		sceneData->DeleteChildren();
        delete sceneData;
    }

    void Scene::ClearScene()
    {
        GetRoot()->GetChildren().clear();
    }

    void Scene::AddEntity(EntityPtr entity)
    {
        if (!FindEntityByName(entity->GetName()))
        {
            mRoot->AddChild(entity);
        }
    }

    void Scene::DeleteEntity(Entity* entity)
    {
        GetRoot()->GetChildren().erase(
                std::remove_if(
                        GetRoot()->GetChildren().begin(),
                        GetRoot()->GetChildren().end(),
                        [=] (const EntityPtr& entityPtr) -> bool
                        {
                            return entityPtr == entity;
                        }
                ),
                GetRoot()->GetChildren().end()
        );
    }

    System* Scene::GetSceneByName(const String& systemName)
    {
        auto iterator = mSystems.find(systemName);
        return (iterator != mSystems.end() ? iterator->second : nullptr);
    }

}
