//
// Created by Bruno Gouveia on 12/2/15.
//

#include <Core/System.h>

namespace RGTek
{
	RSYSTEM_IMPLEMENTATION(System);

    System::System()
        : mRunning(false)
    {

    }

    System::~System()
    {
        // Do nothing
    }

    void System::Start()
    {
        mRunning = true;
    }

    void System::Stop()
    {
        mRunning = false;
    }

    void System::Pause()
    {
        mRunning = false;
    }

    void System::Resume()
    {
        mRunning = true;
    }

    bool System::IsRunning()
    {
        return mRunning;
    }

}
