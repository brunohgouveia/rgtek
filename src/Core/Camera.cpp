#include <Core/Camera.h>
#include <Resources/Shader.h>
#include <Tools/Debugger.h>

namespace RGTek
{
    using namespace Math;

    CameraPtr Camera::sMainCamera = new Camera();

	Camera::Camera(TransformWeakPtr transform):
        mFieldOfView(55.0f),
        mNear(0.1),
        mFar(50.0),
        mViewport(Vector2(0.0f, 0.0f), Vector2(0.0f, 0.0f)),
        mProjection(Matrix4().InitPerspective(Math::ToRadians(mFieldOfView), 1, mNear, mFar)),
        mTransform(transform)
    {

    }

    Camera::~Camera()
    {

    }

    void Camera::UpdateUniformsToShader(ShaderWeakPtr shader)
    {
        shader->SetUniform("ViewProjectionMatrix", GetViewProjection());
        shader->SetUniform("ViewMatrix", GetView());
        shader->SetUniform("ProjectionMatrix", GetProjection());
        shader->SetUniform("NormalMatrix", GetView().Inverse().Transpose());
    }

    Vector3 Camera::ScreenPointToNDC(const Vector2 &position)
    {
        using namespace Math;

        // Convert to normalized device coordinate system
        Vector2 mouseScreenPos = position - mViewport.position;
        Vector2 mouseNDCPos = (2.0f * mouseScreenPos / mViewport.size) - 1.0f;

        return Vector3(mouseNDCPos.x, mouseNDCPos.y, 1.0);
    }

    Vector3 Camera::ScreenPointToWorld(const Vector2 &position)
    {
        using namespace Math;

        // Convert to normalized device coordinate system
        Vector2 mouseScreenPos = position - mViewport.position;
        Vector2 mouseNDCPos = (2.0f * mouseScreenPos / mViewport.size) - 1.0f;

        Vector4 mouseNDCPos4D = Vector4(mouseNDCPos.x, mouseNDCPos.y, 1.0, 1.0);
        Vector4 mouseWorldPos  = GetViewProjection().Inverse() * mouseNDCPos4D;
        mouseWorldPos = mouseWorldPos / mouseWorldPos.w;

        return Vector3(mouseWorldPos.x, mouseWorldPos.y, mouseWorldPos.z);
    }

    Ray Camera::ScreenPointToRay(const Vector2& position)
    {
        using namespace Math;

        // Convert to normalized device coordinate system
        Vector2 mouseScreenPos = position - mViewport.position;
        Vector2 mouseNDCPos = (2.0f * mouseScreenPos / mViewport.size) - 1.0f;

        Vector4 mouseNDCPos4D = Vector4(mouseNDCPos.x, mouseNDCPos.y, 1.0, 1.0);
        Vector4 mouseWorldPos  = GetViewProjection().Inverse() * mouseNDCPos4D;
        mouseWorldPos = mouseWorldPos / mouseWorldPos.w;

        Vector3 mouseWorldPos3D = Vector3(mouseWorldPos.x, mouseWorldPos.y, mouseWorldPos.z);

        Vector3 cameraPos = mTransform->GetPosition();
        return Ray(cameraPos, mouseWorldPos3D - cameraPos);
    }

    Ray Camera::ScreenPointToRay(const Vector3& position)
    {
        Vector2 pos2d(position.x, position.y);
        return ScreenPointToRay(pos2d);
    }

    Vector3 Camera::WorldToNDC(const Vector3& position)
    {
        Vector4 worldPos = GetViewProjection() * Vector4(position, 1.0f);
        return Vector3(worldPos.x, worldPos.y, worldPos.z) / worldPos.w;
    }

    Vector3 Camera::NDCToWorld(const Vector3& position)
    {
        Vector4 ndcPos = GetViewProjection().Inverse() * Vector4(position, 1.0f);
        return Vector3(ndcPos.x, ndcPos.y, ndcPos.z) / ndcPos.w;
    }

    Matrix4 Camera::GetView()
    {
        if (mTransform != NULL)
        {
            Matrix4 cameraRotation = mTransform->GetTransformedRotation().Conjugate().ToRotationMatrix();
            Matrix4 cameraTranslation;
            cameraTranslation.InitTranslation(mTransform->GetTransformedPosition() * -1);

            return cameraRotation * cameraTranslation;
        }
        else
        {
            Debugger::Warning("Camera transform null - should not use the default camera");
            return Matrix4();
        }
    }

    Matrix4 Camera::GetProjection()
    {
        return mProjection;
    }

    Matrix4 Camera::GetViewProjection()
	{
        return GetProjection() * GetView();
	}

    void Camera::SetViewport(const Viewport& viewport)
    {
        if (mViewport.GetAspectRatio() != viewport.GetAspectRatio())
        {
            mProjection = Matrix4().InitPerspective(Math::ToRadians(mFieldOfView), viewport.GetAspectRatio(), mNear, mFar);
        }
        mViewport = viewport;
    }

    void Camera::SetFieldOfView(float fov)
    {
        mFieldOfView = fov;
        ComputeProjectionMatrix();
    }

    void Camera::SetNearClip(float near)
    {
        mNear = near;
        ComputeProjectionMatrix();
    }

    void Camera::SetFarClip(float far)
    {
        mFar = far;
        ComputeProjectionMatrix();
    }

    void Camera::ComputeProjectionMatrix()
    {
        mProjection.InitPerspective(Math::ToRadians(mFieldOfView), mViewport.GetAspectRatio(), mNear, mFar);
    }

}
