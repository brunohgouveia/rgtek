#include <GL/Color.h>
#include <Math/Math.h>

namespace RGTek
{
    using namespace Math;
    
    Color Color::black (0.0f, 0.0f, 0.0f);
    Color Color::white (1.0f, 1.0f, 1.0f);
    Color Color::blue  (0.0f, 0.0f, 1.0f);
    Color Color::red   (1.0f, 0.0f, 0.0f);
    Color Color::green (0.0f, 1.0f, 0.0f);
    Color Color::gray  (0.5f, 0.5f, 0.5f);
    Color Color::yellow(1.0f, 1.0f, 0.0f);
    
    Color::Color(float gray) :
        r(gray),
        g(gray),
        b(gray),
        a(1.0f)
    {

    }
    
    Color::Color(float red, float green, float blue) :
        r(red),
        g(green),
        b(blue),
        a(1.0f)
    {
        
    }

    Color::Color(float red, float green, float blue, float alpha) :
        r(red),
        g(green),
        b(blue),
        a(alpha)
    {
        
    }
    
    Color::Color(const Color& color) :
        r(color.r),
        g(color.g),
        b(color.b),
        a(color.a)
    {
        
    }
    
    Color::~Color()
    {
        
    }

    bool Color::operator==(const Color& other) const
    {
        return Math::IsEqual(r, other.r) && Math::IsEqual(g, other.g) && Math::IsEqual(b, other.b) && Math::IsEqual(a, other.a);
    }

    bool Color::operator!=(const Color& other) const
    {
        return !(*this == other);
    }

    bool Color::operator<(const Color& other) const
    {
        if (r < other.r) return true;
        else if (r > other.r) return false;
        else
        {
            if (g < other.g) return true;
            else if (g > other.g) return false;
            else
            {
                if (b < other.b) return true;
                else if (b > other.b) return false;
                else
                {
                    return a < other.a;
                }
            }
        }
    }

    bool Color::operator>(const Color& other) const
    {
        return !((*this) < other);
    }

    bool Color::operator<=(const Color& other) const
    {
        if (r < other.r) return true;
        else if (r > other.r) return false;
        else
        {
            if (g < other.g) return true;
            else if (g > other.g) return false;
            else
            {
                if (b < other.b) return true;
                else if (b > other.b) return false;
                else
                {
                    return a <= other.a;
                }
            }
        }
    }

    bool Color::operator>=(const Color& other) const
    {
        return !((*this)<=other);
    }

}