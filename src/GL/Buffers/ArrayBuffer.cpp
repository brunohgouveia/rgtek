#include <GL/Buffers/ArrayBuffer.h>
#include <GL/glew.h>
#include <vector>

namespace GL {

	ArrayBufferBase::ArrayBufferBase(size_t sizeT)
		: mSizeT(sizeT)
	{
		//  Do nothing
	}

	void ArrayBufferBase::InitPrivate(void* data, GLsizeiptr size, GLenum usage) 
	{
		// Set attributes
		this->mUsage = usage;

		// Generate and bind buffer
		glGenBuffers(1, &mBufferName);
		glBindBuffer(GL_ARRAY_BUFFER, mBufferName);

		// Initialize buffer
		if (data) {
			glBufferData(GL_ARRAY_BUFFER, size * mSizeT, data, usage);
		}

		// Unbind buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ArrayBufferBase::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, mBufferName);
	}

	void ArrayBufferBase::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ArrayBufferBase::EnableVertexAttribArray(GLuint index) const
	{
		glEnableVertexAttribArray(index);
	}

	void ArrayBufferBase::DisableVertexAttribArray(GLuint index) const
	{
		glDisableVertexAttribArray(index);
	}

	void ArrayBufferBase::VertexAttribPointer(GLuint index, GLuint size, GLenum type, GLboolean normalized, GLsizei stride, GLuint pointer) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, mBufferName);
		glEnableVertexAttribArray(index);
		glVertexAttribPointer(index, size, type, normalized, stride * mSizeT, reinterpret_cast<GLvoid*>(pointer * mSizeT));
	}

	void ArrayBufferBase::DataPrivate(void* data, GLsizeiptr size) 
	{
		// Bind buffer
		glBindBuffer(GL_ARRAY_BUFFER, mBufferName);

		// Copy data
		glBufferData(GL_ARRAY_BUFFER, size * mSizeT, data, mUsage);

		// Unbind buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ArrayBufferBase::SubDataPrivate(void* data, GLintptr offset, GLsizeiptr size) 
	{
		// Bind buffer
		glBindBuffer(GL_ARRAY_BUFFER, mBufferName);

		// Copy data
		glBufferSubData(GL_ARRAY_BUFFER, offset * mSizeT, size * mSizeT, data);

		// Unbind buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ArrayBufferBase::Draw(GLint first, GLsizei count) 
	{
		glDrawArrays(GL_TRIANGLES, first, count);
	}

}
