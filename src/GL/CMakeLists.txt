if (RGTEK_UNIFIED_LIBRARY)
    file (GLOB_RECURSE GL_SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/../.." *.cpp *.c)
    set (RGTEK_SOURCE_FILES ${RGTEK_SOURCE_FILES} ${GL_SOURCE_FILES} PARENT_SCOPE)

    file (GLOB_RECURSE GL_HEADER_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/../.." ../../include/GL/*.h)
    set (RGTEK_HEADER_FILES ${RGTEK_HEADER_FILES} ${GL_HEADER_FILES} PARENT_SCOPE)
else()
    set(RGTEK_MODULE_NAME ${RGTEK_GL_LIBNAME} )

    # we do not use cegui_gather_files() here since our needs are more complex.
    file (GLOB_RECURSE GL_SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" *.cpp *.c)

    file (GLOB_RECURSE GL_HEADER_FILES ../include/GL/*.h)

    rgtek_add_library(${RGTEK_MODULE_NAME} GL_SOURCE_FILES GL_HEADER_FILES)

    rgtek_add_module_dependency(${RGTEK_MODULE_NAME} ${RGTEK_TOOLS_LIBNAME})
    rgtek_add_module_dependency(${RGTEK_MODULE_NAME} ${RGTEK_MATH_LIBNAME})

    rgtek_add_dependency(${RGTEK_MODULE_NAME} OPENGL)
    rgtek_add_dependency(${RGTEK_MODULE_NAME} GLEW)
    rgtek_add_dependency(${RGTEK_MODULE_NAME} SDL2)
endif()