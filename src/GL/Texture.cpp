#include <GL/Texture.h>
#include <GL/ThirdPart/Image.h>
#include <iostream>
#include <Tools/Debugger.h>
#include <GL/glew.h>

namespace RGTek
{

    Texture::Texture(const String& textureName, GLuint textureUnit, int width, int height, bool canBeRenderTarget, ResourceManagerWeakPtr resourceManager)
        : mFramebuffer(0)
        , mTextureUnit(textureUnit)
        , mWidth(width)
        , mHeight(height)
    {
        Debugger::Debug("Texture [%s] starting...", textureName.c_str());
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        glGenTextures(1, &mTextureName);
        glBindTexture(GL_TEXTURE_2D, mTextureName);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

        //  Map single depth value to RGBA (this is called intensity)
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_COMPARE_MODE,GL_COMPARE_R_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        // glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);

        //  Set texture mapping to clamp and linear interpolation
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

        if (canBeRenderTarget) {
            glGenFramebuffers(1, &mFramebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mTextureName, 0);

            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);

            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                Debugger::Fatal("Error setting up frame buffer\n");
            }
            glBindFramebuffer(GL_FRAMEBUFFER,0);
            Debugger::CheckOpenGLError();
        }
    }

    Texture::Texture(const String& fileName, GLuint textureUnit, ResourceManagerWeakPtr resourceManager)
        : mFramebuffer(0)
        , mTextureUnit(textureUnit)
    {
        Debugger::Debug("Texture [%s] starting...", fileName.c_str());
        glGenTextures(1, &mTextureName);

        LoadFile(fileName);
    }

    Texture::~Texture()
    {
        glDeleteTextures(1, &mTextureName);
    }

	void Texture::Bind()   { glBindTexture(GL_TEXTURE_2D, mTextureName); }
	void Texture::Active() { glActiveTexture(GL_TEXTURE0 + mTextureUnit); }

    void Texture::LoadFile(const String& textureFile, bool requireMipMap)
    {
        Debugger::Debug("Loading texture: %s", textureFile.c_str());
        //  Sanity check
        GL::Image im;
        im.Load(textureFile);
        //  Generate 2D texture
        glBindTexture(GL_TEXTURE_2D, mTextureName);

        //  Copy image
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,im.GetWidth(),im.GetHeight(),0,GL_RGBA,GL_UNSIGNED_BYTE,im.GetPixels());
        if (glGetError()) RGTek::Debugger::Fatal("Error in glTexImage2D %s\n", textureFile.c_str());

        //  Scale linearly when image size doesn't match
        if (requireMipMap)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
        }
        else
        {
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        }

        mWidth  = im.GetWidth();
        mHeight = im.GetHeight();

    }

    void Texture::BindAsRenderTarget()
    {
        if (mFramebuffer == 0) {
            Debugger::Fatal("Trying to bind an invalid framebuffer");
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
        glScissor(0, 0, mWidth, mHeight);
        glViewport(0, 0, mWidth, mHeight);
    }

}
