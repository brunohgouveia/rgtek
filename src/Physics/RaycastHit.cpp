#include <Physics/RaycastHit.h>
#include <Core/Entity.h>
#include <limits>

namespace RGTek
{
	
	RaycastHit::RaycastHit():
		mDistance(std::numeric_limits<float>::max())
	{
		
	}
	
	RaycastHit::~RaycastHit()
	{
		
	}
	
}