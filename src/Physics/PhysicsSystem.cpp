#include <Physics/PhysicsSystem.h>
#include <Rendering/RenderingSystem.h>
#include <Physics/Collision.h>
#include <Physics/Bullet/btSimpleDebugDraw.h>
#include <Tools/Debugger.h>

namespace RGTek
{
	RSYSTEM_IMPLEMENTATION(PhysicsSystem);

    PhysicsSystem::PhysicsSystem()
        : ray(Vector3(0,0,-10) , Vector3(0,0,1))
    {
        Debugger::Debug("PhysicsSystem start");

		///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
        mCollisionConfiguration = new btDefaultCollisionConfiguration();

        ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
        mDispatcher = new	btCollisionDispatcher(mCollisionConfiguration);

        ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
        mOverlappingPairCache = new btDbvtBroadphase();

        ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
        mSolver = new btSequentialImpulseConstraintSolver;

        mDynamicsWorld = new btDiscreteDynamicsWorld(mDispatcher, mOverlappingPairCache, mSolver, mCollisionConfiguration);

        mDynamicsWorld->setGravity(btVector3(0,-10,0));

        mDebugDraw = new btSimpleDebugDraw();
        mDynamicsWorld->setDebugDrawer(mDebugDraw);

        mGravity = Vector3(0.0f, -10.0f, 0.0f);
    }

    PhysicsSystem::~PhysicsSystem()
    {
        Debugger::Debug("PhysicsSystem shutting down...");

		Debugger::Debug("Num collision objects: %d", mDynamicsWorld->getNumCollisionObjects());
		//delete dynamics world
        delete mDynamicsWorld;
        //delete solver
        delete mSolver;
        //delete broadphase
        delete mOverlappingPairCache;
        //delete dispatcher
        delete mDispatcher;
        delete mCollisionConfiguration;
        delete mDebugDraw;

		Debugger::Debug("PhysicsSystem shutting down... done");
    }

    void PhysicsSystem::Pause()
    {
        System::Pause();
        mDynamicsWorld->setGravity(btVector3(0.0f, 0.0f, 0.0f));

        for (auto rigidbodyComponent : mRigidbodyComponents)
        {
            btRigidBody* rigidBody = static_cast<btRigidBody*>(*rigidbodyComponent);


            rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
//            rigidBody->setActivationState(DISABLE_SIMULATION);
            rigidBody->setMassProps(0.0f, btVector3(0.0f, 0.0f, 0.0f));

        }
    }

    void PhysicsSystem::Resume()
    {
        System::Resume();
        mDynamicsWorld->setGravity(static_cast<btVector3>(mGravity));

        for (auto rigidbodyComponent : mRigidbodyComponents)
        {
            btRigidBody* rigidBody = static_cast<btRigidBody*>(*rigidbodyComponent);

            rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() & ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
//            rigidBody->setActivationState(ACTIVE_TAG);
            rigidbodyComponent->SetMass(rigidbodyComponent->GetMass());

        }
    }

    void PhysicsSystem::RenderDebugInfo()
    {
        Debugger::AssertNotNull(mDynamicsWorld, "DynamicWorld null");
        mDynamicsWorld->debugDrawWorld();
    }

    void PhysicsSystem::StepSimulation()
    {
//        if (!IsRunning()) return;

        mDynamicsWorld->stepSimulation(1.f/60.f,10);

        static std::vector<Vector3> contactPointsA;
        static std::vector<Vector3> contactPointsB;

        // Reset the keep state
        for (auto& pair : pairsColliding)
        {
            pair.second = false;
        }

        int numManifolds = mDispatcher->getNumManifolds();
        for (int i=0;i<numManifolds;i++)
        {
            btPersistentManifold* contactManifold =  mDispatcher->getManifoldByIndexInternal(i);
            const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
            const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());

//#ifdef EDITOR_RGTEK
            if (obA->getUserIndex() != Layer::kRigidbody || obB->getUserIndex() != Layer::kRigidbody) continue;
//#endif
            Debugger::AssertTrue(obA->getUserIndex() == Layer::kRigidbody, "Not a valid rigidbody");
            Debugger::AssertTrue(obB->getUserIndex() == Layer::kRigidbody, "Not a valid rigidbody");

            const RigidbodyWeakPtr rigidbody1 = static_cast<RigidbodyWeakPtr>(obA->getUserPointer());
            const RigidbodyWeakPtr rigidbody2 = static_cast<RigidbodyWeakPtr>(obB->getUserPointer());


            const int numContacts = contactManifold->getNumContacts();
            contactPointsA.clear();
            contactPointsB.clear();
            contactPointsA.reserve(numContacts);
            contactPointsB.reserve(numContacts);
            for (int j=0;j<numContacts;j++)
            {
                if (contactManifold->getContactPoint(j).getDistance() < 0.0f)
                {
                    contactPointsA.push_back(Vector3(contactManifold->getContactPoint(j).getPositionWorldOnA()));
                    contactPointsB.push_back(Vector3(contactManifold->getContactPoint(j).getPositionWorldOnB()));
                }
            }

            const CollisionPtr collision1 = new Collision(rigidbody1->GetEntity(), rigidbody1, &contactPointsA);
            const CollisionPtr collision2 = new Collision(rigidbody2->GetEntity(), rigidbody2, &contactPointsB);

            auto pair = std::make_pair(obA, obB);
            if (pairsColliding.count(pair) > 0)
            {
                // This pair was colliding in the previous step, thus we should
                // call the OnCollisionStay api again.
                rigidbody1->GetEntity()->OnCollisionStay(collision2);
                rigidbody2->GetEntity()->OnCollisionStay(collision1);
            }
            else
            {
                // This pair was not colliding in the previous step, thus we should
                // call the OnCollisionEnter api again.
                rigidbody1->GetEntity()->OnCollisionEnter(collision2);
                rigidbody2->GetEntity()->OnCollisionEnter(collision1);
            }

            // Set the keep state, in order to not delete this pair from the map
            pairsColliding[pair] = true;
        }

        contactPointsA.clear();
        contactPointsB.clear();

        // Pairs that are no longer colliding
        // This code ain't pretty, but this was the most efficient performance-wise
        // that I found.
        for (auto it = pairsColliding.begin(); it != pairsColliding.end();)
        {
            if ((*it).second == false)
            {
                const btCollisionObject* obA = static_cast<const btCollisionObject*>((*it).first.first);
                const btCollisionObject* obB = static_cast<const btCollisionObject*>((*it).first.second);

                const RigidbodyWeakPtr rigidbody1 = static_cast<RigidbodyWeakPtr>(obA->getUserPointer());
                const RigidbodyWeakPtr rigidbody2 = static_cast<RigidbodyWeakPtr>(obB->getUserPointer());

                const CollisionPtr collision1 = new Collision(rigidbody1->GetEntity(), rigidbody1, &contactPointsA);
                const CollisionPtr collision2 = new Collision(rigidbody2->GetEntity(), rigidbody2, &contactPointsB);

                rigidbody1->GetEntity()->OnCollisionExit(collision2);
                rigidbody2->GetEntity()->OnCollisionExit(collision1);

                // Remove from map
                pairsColliding.erase(it++);
            }
            else
            {
                ++it;
            }
        }
    }

    void PhysicsSystem::AddRigidbody(const RigidbodyWeakPtr& rigidbody)
    {
		Debugger::AssertNotNull(mDynamicsWorld, "DynamicWorld null");
        mDynamicsWorld->addRigidBody(static_cast<btRigidBody*>(*rigidbody));
        mRigidbodyComponents.push_back(rigidbody);
    }

    void PhysicsSystem::AddCollider(const ColliderPtr &collider)
    {
        mBvh.Insert(collider);
    }

    void PhysicsSystem::DeleteRigidbody(Rigidbody* rigidbody)
    {
        Debugger::AssertNotNull(mDynamicsWorld, "DynamicWorld null");
        mDynamicsWorld->removeRigidBody(static_cast<btRigidBody*>(*rigidbody));

        // Clean up tracks of collisions related to this rigidbody
        for (auto it = pairsColliding.begin(), ite = pairsColliding.end(); it != ite;)
        {
            btCollisionObject* collisionObject = static_cast<btCollisionObject*>(*rigidbody);
            if ((collisionObject == it->first.first) || (collisionObject == it->first.second))
            {
                it = pairsColliding.erase(it);
            }
            else
            {
                it++;
            }
        }
    }

    RigidbodyWeakPtr PhysicsSystem::CreateRigidbody(float mass)
    {
        RigidbodyPtr newComponent = new Rigidbody(mass);
        newComponent->SetSystem(this);
        mRigidbodyComponents.push_back(newComponent);

        return static_cast<RigidbodyWeakPtr>(newComponent);
    }

    BoxColliderWeakPtr PhysicsSystem::CreateBoxCollider(const Vector4& position, const Vector4& size)
    {
        BoxColliderPtr newComponent = new BoxCollider(position, size);
        newComponent->SetSystem(this);
        mBoxColliderComponents.push_back(newComponent);

        return static_cast<BoxColliderWeakPtr>(newComponent);
    }

    SphereColliderWeakPtr PhysicsSystem::CreateSphereCollider(const Vector4& position, const float radius)
    {
        SphereColliderPtr newComponent = new SphereCollider(position, radius);
        newComponent->SetSystem(this);
        mSphereColliderComponents.push_back(newComponent);

        return static_cast<SphereColliderWeakPtr>(newComponent);
    }

    Component* PhysicsSystem::CreateComponentByName(const String& className)
    {
        if (className == Rigidbody::COMPONENT_NAME)
        {
            return CreateRigidbody();
        }
        else if (className == BoxCollider::COMPONENT_NAME)
        {
            return CreateBoxCollider(Vector4(0.0f), Vector4(1.0f, 1.0f, 1.0f, 1.0f));
        }
        else if (className == SphereCollider::COMPONENT_NAME)
        {
            return CreateSphereCollider(Vector4(0.0f), 1.0f);
        }

		Debugger::Error("Component [%s] doesn't belong to PhysicsSystem.", className.c_str());
        return nullptr;
    }

    void PhysicsSystem::DeleteComponent(Component* component)
    {
        if (Rigidbody* rigidBodyComponent = dynamic_cast<Rigidbody*>(component))
        {
            mRigidbodyComponents.erase(
                std::remove(
                    mRigidbodyComponents.begin(),
                    mRigidbodyComponents.end(),
                    rigidBodyComponent
                ),
                mRigidbodyComponents.end()
            );
        }
        else if (BoxCollider* boxColliderComponent = dynamic_cast<BoxCollider*>(component))
        {
            mBoxColliderComponents.erase(
                std::remove(
                    mBoxColliderComponents.begin(),
                    mBoxColliderComponents.end(),
                    boxColliderComponent
                ),
                mBoxColliderComponents.end()
            );
        }
        else if (SphereCollider* sphereColliderComponent = dynamic_cast<SphereCollider*>(component))
        {
            mSphereColliderComponents.erase(
                std::remove(
                    mSphereColliderComponents.begin(),
                    mSphereColliderComponents.end(),
                    sphereColliderComponent
                ),
                mSphereColliderComponents.end()
            );
        }
    }

    void PhysicsSystem::DeleteComponentsOfEntity(Entity* entity)
    {
        mRigidbodyComponents.erase(
            std::remove_if(
                mRigidbodyComponents.begin(),
                mRigidbodyComponents.end(),
                [=] (const RigidbodyPtr& rigidbody) -> bool
                {
                    return rigidbody->GetEntity() == entity;
                }
            ),
            mRigidbodyComponents.end()
        );

        mBoxColliderComponents.erase(
            std::remove_if(
                mBoxColliderComponents.begin(),
                mBoxColliderComponents.end(),
                [=] (const BoxColliderPtr& boxCollider) -> bool
                {
                    return boxCollider->GetEntity() == entity;
                }
            ),
            mBoxColliderComponents.end()
        );

        mSphereColliderComponents.erase(
            std::remove_if(
                mSphereColliderComponents.begin(),
                mSphereColliderComponents.end(),
                [=] (const SphereColliderPtr& sphereCollider) -> bool
                {
                    return sphereCollider->GetEntity() == entity;
                }
            ),
            mSphereColliderComponents.end()
        );

    }

    bool PhysicsSystem::Raycast(const Ray& ray, RaycastHit& rayInfo, Layer layer)
    {
        btVector3 fromWorld = static_cast<btVector3>(ray.origin);
        btVector3 toWorld = static_cast<btVector3>(Vector3(ray.origin + ray.direction));

        LayeredClosestRayResultCallback resultCallback(fromWorld, toWorld, layer);
        mDynamicsWorld->rayTest(fromWorld, toWorld, resultCallback);

        if (resultCallback.hasHit())
        {
            Debugger::AssertTrue(resultCallback.m_collisionObject->getUserIndex() == layer, "Layers are not compatible");

            rayInfo.mHitPoint = resultCallback.m_hitPointWorld;
            rayInfo.mDistance = (rayInfo.mHitPoint - ray.origin).Magnitude();

            if (layer == Layer::kRigidbody)
            {
                RigidbodyWeakPtr rb = static_cast<RigidbodyWeakPtr>(resultCallback.m_collisionObject->getUserPointer());
                rayInfo.mEntity = rb->GetEntity();
                rayInfo.mSelectedAxis = Handle3D::Axis::None;
            }
            else if (layer == Layer::kGizmo)
            {
                rayInfo.mSelectedAxis = *static_cast<Handle3D::Axis*>(resultCallback.m_collisionObject->getUserPointer());
                rayInfo.mEntity = nullptr;
            }

            return true;
        }
        return false;
    }

//    bool PhysicsSystem::RaycastGizmos(const Ray &ray, RaycastHit &rayInfo)
//    {
//        btVector3 fromWorld = static_cast<btVector3>(ray.origin);
//        btVector3 toWorld = static_cast<btVector3>(Vector3(ray.origin + ray.direction));
//
//        LayeredClosestRayResultCallback resultCallback(fromWorld, toWorld, Layer::Gizmo);
//        mDynamicsWorld->rayTest(fromWorld, toWorld, resultCallback);
//
//        if (resultCallback.hasHit())
//        {
//            Debugger::AssertTrue(resultCallback.m_collisionObject->getUserIndex() != Layer::Gizmo , "Not a valid 3D handle");
//
//            Debugger::Debug("Hitted a gizmo");
//
//            return true;
//        }
//
//        return false;
//    }

//    void PhysicsSystem::RenderBVH(int level)
//    {
//        mWireFrameProgram.UseShader();
//        mWireFrameProgram.SetUniform("LineColor", Vector4(1.0f, 0.0f, 0.0f, 1.0f));
//
//        CoreSystemWeakPtr sharedCoreSystem = Application::GetSharedApplication();
//        Debugger::AssertNotNull(sharedCoreSystem, "SharedCoreSystem should not be null.");
//
//        RenderingSystemPtr re = sharedCoreSystem->GetRenderingSystem();
//        Debugger::AssertNotNull(re, "RenderingSystem should not be null");
//        re->UpdateUniformsToProgram(mWireFrameProgram);
//
//        glBindVertexArray(mBvhVertexArray);
//        mBvhVertexBuffer.Bind();
//
//        RenderBVH(mBvh.GetRoot(), m_level, 1);
//    }
//
//    void PhysicsSystem::RenderRay()
//    {
//        mWireFrameProgram.UseShader();
//        mWireFrameProgram.SetUniform("LineColor", Vector4(0.0f, 0.0f, 1.0f, 1.0f));
//
//        CoreSystemWeakPtr sharedCoreSystem = Application::GetSharedApplication();
//        Debugger::AssertNotNull(sharedCoreSystem, "SharedCoreSystem should not be null.");
//
//        RenderingSystemPtr re = sharedCoreSystem->GetRenderingSystem();
//        Debugger::AssertNotNull(re, "RenderingSystem should not be null");
//        re->UpdateUniformsToProgram(mWireFrameProgram);
//
//        glBindVertexArray(mRayVertexArray);
//
//        float rayVertices[] = {
//            ray.origin.x, ray.origin.y, ray.origin.z, 1.0f,
//            ray.origin.x + 100*ray.direction.x, ray.origin.y + 100*ray.direction.y, ray.origin.z + 100*ray.direction.z, 1.0f
//        };
//        mRayVertexBuffer.SubData(rayVertices, 0, 8);
//        mRayVertexBuffer.Bind();
//
//        glDrawArrays(GL_LINES, 0, 2);
//
//    }

    void PhysicsSystem::RenderBVH(BVHNode* node, int level, int currentLevel)
    {
        if (!node)
        {
            return;
        }

        // RaycastHit info;
        // bool intersect = node->mBoundingBox.Intersect(ray, info);
        // if ((&node->mBoundingBox.mMin)[1].x != node->mBoundingBox.mMax.x) {
        //     Debugger::Fatal("Intersect wrogn");
        // }
        // if ((&node->mBoundingBox.mMin)[1].y != node->mBoundingBox.mMax.y) {
        //     Debugger::Fatal("Intersect wrogn");
        // }
        // if ((&node->mBoundingBox.mMin)[1].z != node->mBoundingBox.mMax.z) {
        //     Debugger::Fatal("Intersect wrogn");
        // }

        if (level == 0 || level == currentLevel)
        {
            // Draw node's aabb
            AABB bb = node->mBoundingBox;

            // if (intersect)
            // {
            //     mWireFrameProgram.SetUniform("LineColor", Vector4(0.0f, 1.0f, 0.0f, 1.0));
            // }

//            float lines [] = {
//                bb.mMin.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMax.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMin.y, bb.mMax.z, 1,
//
//                bb.mMax.x, bb.mMax.y, bb.mMax.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMax.z, 1,
//                bb.mMax.x, bb.mMax.y, bb.mMax.z, 1,
//                bb.mMax.x, bb.mMin.y, bb.mMax.z, 1,
//                bb.mMax.x, bb.mMax.y, bb.mMax.z, 1,
//                bb.mMax.x, bb.mMax.y, bb.mMin.z, 1,
//
//                bb.mMin.x, bb.mMin.y, bb.mMax.z, 1,
//                bb.mMax.x, bb.mMin.y, bb.mMax.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMin.z, 1,
//                bb.mMax.x, bb.mMax.y, bb.mMin.z, 1,
//
//                bb.mMax.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMax.x, bb.mMin.y, bb.mMax.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMax.z, 1,
//
//                bb.mMax.x, bb.mMin.y, bb.mMin.z, 1,
//                bb.mMax.x, bb.mMax.y, bb.mMin.z, 1,
//                bb.mMin.x, bb.mMin.y, bb.mMax.z, 1,
//                bb.mMin.x, bb.mMax.y, bb.mMax.z, 1,
//            };

//            mBvhVertexBuffer.SubData(lines, 0, 96);
//            mBvhVertexBuffer.Bind();
//
//            glDrawArrays(GL_LINES, 0, 24);

            // if (intersect)
            // {
            //     mWireFrameProgram.SetUniform("LineColor", Vector4(1.0f, 0.0f, 0.0f, 1.0));
            // }
        }

        // Draw children
        RenderBVH(node->m_leftChild, level, currentLevel + 1);
        RenderBVH(node->m_rightChild, level, currentLevel + 1);
    }

} /* RGTek */
