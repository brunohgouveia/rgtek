#include <Physics/BVH.h>
#include <functional>

namespace RGTek
{

    BVHNode::BVHNode():
        mParent(nullptr),
        m_leftChild(nullptr),
        m_rightChild(nullptr),
        m_valid(true),
        m_leaf(true)
    {

    }

    BVHNode::BVHNode(BVHNode* parent):
        mParent(parent),
        m_leftChild(nullptr),
        m_rightChild(nullptr),
        m_valid(true),
        m_leaf(true)
    {

    }

    BVHNode::~BVHNode()
    {
		if (m_leftChild) delete m_leftChild;
		if (m_rightChild) delete m_rightChild;
    }

    void BVHNode::InsertCollider(ColliderPtr collider)
    {
        m_colliders.push_back(collider);
        mBoundingBox.Inflate(collider->GetAABB());
    }

    void BVHNode::Split()
    {
        m_leftChild = new BVHNode(this);
        m_rightChild = new BVHNode(this);
        m_leaf = false;

        m_leftChild->InsertCollider(m_colliders[0]);
        m_rightChild->InsertCollider(m_colliders[1]);

        mBoundingBox.Inflate(m_leftChild->mBoundingBox);
        mBoundingBox.Inflate(m_rightChild->mBoundingBox);

        m_colliders.clear();
    }

    BVH::BVH():
        mRoot(nullptr),
        m_maxCollidersPerBox(1)
    {

    }

    BVH::~BVH()
    {
        if (mRoot)
        {
            delete mRoot;
        }
    }

    void BVH::Insert(ColliderPtr collider)
    {
        Insert(collider, mRoot);
    }

    void BVH::Insert(ColliderPtr collider, BVHNode*& node)
    {
        if (!node)
        {
            node = new BVHNode();
            node->mBoundingBox.Inflate(collider->GetAABB());
            node->m_colliders.push_back(collider);
            return;
        }

        BVHNode* current = node;
        while (!current->m_leaf)
        {
            // Original AABB info
            AABB colliderAABB   = collider->GetAABB();
            AABB leftChildAABB  = current->m_leftChild->mBoundingBox;
            AABB rightChildAABB = current->m_rightChild->mBoundingBox;

            float leftChildAABBVolume  = leftChildAABB.GetVolume();
            float rightChildAABBVolume = rightChildAABB.GetVolume();

            // New AABB info
            leftChildAABB.Inflate(colliderAABB);
            rightChildAABB.Inflate(colliderAABB);

            float newLeftChildAABBVolume  = leftChildAABB.GetVolume();
            float newRightChildAABBVolume = rightChildAABB.GetVolume();

            float leftEnlargement  = newLeftChildAABBVolume - leftChildAABBVolume;
            float rightEnlargement = newRightChildAABBVolume - rightChildAABBVolume;

            // Check which children would have the least enlargement of the volume
            if (leftEnlargement < rightEnlargement)
            {
                current = current->m_leftChild;
            }
            else if (rightEnlargement < leftEnlargement)
            {
                current = current->m_rightChild;
            }
            else
            {
                current = (leftChildAABBVolume < rightChildAABBVolume) ? current->m_leftChild : current->m_rightChild;
            }

        }

        // You can just insert in the leaf
        if (current->m_colliders.size() < m_maxCollidersPerBox)
        {
            current->InsertCollider(collider);
        }
        else
        {
            current->InsertCollider(collider);
            current->Split();
        }
        current = current->mParent;

        while (current) {
            current->mBoundingBox.Inflate(current->m_leftChild->mBoundingBox);
            current->mBoundingBox.Inflate(current->m_rightChild->mBoundingBox);
            current = current->mParent;
        }

    }

    void BVH::Remove(ColliderPtr collider)
    {

    }

    void BVH::Update(ColliderPtr collider)
    {

    }

} /* RGTek */
