#include <Physics/Collision.h>

namespace RGTek
{

    Collision::Collision(const Entity* entity, const Rigidbody* rigidbody, const Vector<Vector3>* contacts) :
        mEntity(entity),
        mRigidbody(rigidbody),
        mContacts(contacts)
    {
        // Do nothing
    }

} /* RGTek */
