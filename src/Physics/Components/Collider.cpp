#include <Physics/Components/Collider.h>
#include <Physics/Components/Rigidbody.h>
#include <Tools/Debugger.h>

namespace RGTek
{

    Collider::Collider() :
        mBtCollistionShape(nullptr)
    {
        // Do nothing
    }

	///////////////////////////////////////////////////////////////////////

    Collider::~Collider()
    {
        if (mBtCollistionShape)
        {
            delete mBtCollistionShape;
        }
        Debugger::Debug("Collider shuttingdown...");
    }

	///////////////////////////////////////////////////////////////////////

    void Collider::SetEntity(Entity* entity)
    {
        Component::SetEntity(entity);

        if (Rigidbody* rigidbody = entity->GetGameComponent<Rigidbody>())
        {
            rigidbody->InvalidateCollisionShape();
        }
    }

	///////////////////////////////////////////////////////////////////////

    const Vector3& Collider::GetRelativePosition() const
    {
        return GetRelativeTransform().GetPosition();
    }

	///////////////////////////////////////////////////////////////////////

    const Quaternion& Collider::GetRelativeRotation() const
    {
        return GetRelativeTransform().GetRotation();
    }

	///////////////////////////////////////////////////////////////////////

    const Transform& Collider::GetRelativeTransform() const
    {
        return mRelativeTransform;
    }

	///////////////////////////////////////////////////////////////////////

    btCollisionShape* Collider::GetBtCollisionShape() const
    {
        return mBtCollistionShape;
    }

	///////////////////////////////////////////////////////////////////////

    void Collider::SetRelativePosition(const Vector3& position)
    {
        mRelativeTransform.SetPosition(position);

        if (RigidbodyPtr rigidbody = GetEntity()->GetGameComponent<Rigidbody>())
        {
            rigidbody->SynchronizeColliderRelativeTransform(this);
        }
    }

	///////////////////////////////////////////////////////////////////////

    void Collider::SetRelativeRotation(const Quaternion& rotation)
    {
        mRelativeTransform.SetRotation(rotation);

        if (RigidbodyPtr rigidbody = GetEntity()->GetGameComponent<Rigidbody>())
        {
            rigidbody->SynchronizeColliderRelativeTransform(this);
        }

    }

	///////////////////////////////////////////////////////////////////////

    void Collider::SetRelativeTransform(const Transform& transform)
    {
        mRelativeTransform = transform;

        if (RigidbodyPtr rigidbody = GetEntity()->GetGameComponent<Rigidbody>())
        {
            rigidbody->SynchronizeColliderRelativeTransform(this);
        }
    }

}
