#include <Physics/Components/BoxCollider.h>
#include <Physics/PhysicsSystem.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
	RCOMPONENT_IMPLEMENTATION(BoxCollider);

	///////////////////////////////////////////////////////////////////////

    BoxCollider::BoxCollider()
    {
        Debugger::Debug("BoxCollider start");

        Vector3 boxHalfSize = mRelativeTransform.GetScale() * 0.5f;
        mBtCollistionShape = new btBoxShape(static_cast<btVector3>(boxHalfSize));
    }

	///////////////////////////////////////////////////////////////////////

    BoxCollider::BoxCollider(const Vector4& size)
    {
        Debugger::Debug("BoxCollider start");

        mRelativeTransform.SetScale(size);

		Vector4 minPoint(size * (-0.5f));
		Vector4 maxPoint(size * (+0.5f));
		mBoundingBox.Inflate(minPoint, maxPoint);

        Vector3 boxHalfSize = mRelativeTransform.GetScale() * 0.5f;
        mBtCollistionShape = new btBoxShape(static_cast<btVector3>(boxHalfSize));
    }

	///////////////////////////////////////////////////////////////////////

    BoxCollider::BoxCollider(const Vector4& position, const Vector4& size)
    {
        Debugger::Debug("BoxCollider start");

        mRelativeTransform.SetPosition(position);
        mRelativeTransform.SetScale(size);

		Vector4 minPoint(position + size * (-0.5f));
		Vector4 maxPoint(position + size * (+0.5f));
		mBoundingBox.Inflate(minPoint, maxPoint);

        Vector3 boxHalfSize = mRelativeTransform.GetScale() * 0.5f;
        mBtCollistionShape = new btBoxShape(static_cast<btVector3>(boxHalfSize));
    }

	///////////////////////////////////////////////////////////////////////

    BoxCollider::~BoxCollider()
    {
        Debugger::Debug("BoxCollider shutdown");
    }

	///////////////////////////////////////////////////////////////////////

    void BoxCollider::Update()
    {

    }

	///////////////////////////////////////////////////////////////////////

    void BoxCollider::RegisterToSystem()
    {
        PhysicsSystemPtr physicsSystem = static_cast<PhysicsSystem*>(GetSystem());
        physicsSystem->AddCollider(this);
    }

	///////////////////////////////////////////////////////////////////////

    bool BoxCollider::Intersect(const Ray& ray, RaycastHit& rayInfo) const{
        return mBoundingBox.Intersect(ray, rayInfo);
    }

	///////////////////////////////////////////////////////////////////////

    AABB BoxCollider::GetAABB() const
    {
        return mBoundingBox;
    }

	///////////////////////////////////////////////////////////////////////

    const Vector3& BoxCollider::GetSize() const
    {
        return GetRelativeTransform().GetScale();
    }

	///////////////////////////////////////////////////////////////////////

    void BoxCollider::SetSize(const Vector3& size)
    {
		Debugger::AssertNotNull(mBtCollistionShape, "btCollisionShape not created");
        (static_cast<btBoxShape*>(mBtCollistionShape))->setLocalScaling(static_cast<btVector3>(size));

        mRelativeTransform.SetScale(size);

        if (Entity* entity = GetEntity())
        {
            if (Rigidbody* rigidbody = entity->GetGameComponent<Rigidbody>())
            {
                rigidbody->SynchronizeColliderRelativeTransform(this);
            }
        }
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* BoxCollider::GetDataTranslator()
    {
        auto dataTranslator = new CustomObjectDataTranslator<BoxCollider>("BoxCollider", this);
        dataTranslator->Add("Position", DataTranslatorProperty<BoxCollider, Vector3>(this, &BoxCollider::GetRelativePosition, &BoxCollider::SetRelativePosition));
        dataTranslator->Add("Rotation", DataTranslatorProperty<BoxCollider, Quaternion>(this, &BoxCollider::GetRelativeRotation, &BoxCollider::SetRelativeRotation));
        dataTranslator->Add("Size", DataTranslatorProperty<BoxCollider, Vector3>(this, &BoxCollider::GetSize, &BoxCollider::SetSize));
        return dataTranslator;
    }

}
