#include <Physics/Components/SphereCollider.h>
#include <Physics/PhysicsSystem.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
	RCOMPONENT_IMPLEMENTATION(SphereCollider);

	///////////////////////////////////////////////////////////////////////

    SphereCollider::SphereCollider()
    {
        Debugger::Debug("SphereCollider start");

        mBtCollistionShape = new btSphereShape(mRelativeTransform.GetScale().x);
    }

	///////////////////////////////////////////////////////////////////////

    SphereCollider::SphereCollider(const float radius)
    {
        Debugger::Debug("SphereCollider start");

        Vector3 size(radius);
        mRelativeTransform.SetScale(size);

		Vector4 minPoint(size * (-0.5f));
		Vector4 maxPoint(size * (+0.5f));
		mBoundingBox.Inflate(minPoint, maxPoint);

        mBtCollistionShape = new btSphereShape(mRelativeTransform.GetScale().x);
    }

	///////////////////////////////////////////////////////////////////////

	SphereCollider::SphereCollider(const Vector3& position, const float radius)
    {
        Debugger::Debug("SphereCollider start");

        Vector3 size(radius);
        mRelativeTransform.SetPosition(position);
        mRelativeTransform.SetScale(size);

		Vector3 minPoint(position + size * (-0.5f));
		Vector3 maxPoint(position + size * (+0.5f));
		mBoundingBox.Inflate(minPoint, maxPoint);

        mBtCollistionShape = new btSphereShape(mRelativeTransform.GetScale().x);
    }

	///////////////////////////////////////////////////////////////////////

    SphereCollider::~SphereCollider()
    {
        Debugger::Debug("SphereCollider shutdown");
    }

	///////////////////////////////////////////////////////////////////////

    void SphereCollider::Update()
    {

    }

	///////////////////////////////////////////////////////////////////////

    void SphereCollider::RegisterToSystem()
    {
        PhysicsSystemPtr physicsSystem = static_cast<PhysicsSystem*>(GetSystem());
        physicsSystem->AddCollider(this);
    }

	///////////////////////////////////////////////////////////////////////

    bool SphereCollider::Intersect(const Ray& ray, RaycastHit& rayInfo) const{
        return mBoundingBox.Intersect(ray, rayInfo);
    }

	///////////////////////////////////////////////////////////////////////

    AABB SphereCollider::GetAABB() const
    {
        return mBoundingBox;
    }

	///////////////////////////////////////////////////////////////////////

    float SphereCollider::GetRadius() const
    {
        return GetRelativeTransform().GetScale().x;
    }

	///////////////////////////////////////////////////////////////////////

    void SphereCollider::SetRadius(float radius)
    {
        Debugger::AssertNotNull(mBtCollistionShape, "btCollisionShape not created");
        (static_cast<btSphereShape*>(mBtCollistionShape))->setUnscaledRadius(radius);

        mRelativeTransform.SetScale(Vector3(radius));

        if (Entity* entity = GetEntity())
        {
            RigidbodyPtr rigidbody = entity->GetGameComponent<Rigidbody>();
            if (rigidbody)
            {
                rigidbody->SynchronizeColliderRelativeTransform(this);
            }
        }
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* SphereCollider::GetDataTranslator()
    {
        auto dataTranslator = new CustomObjectDataTranslator<SphereCollider>("SphereCollider", this);
        dataTranslator->Add("Position", DataTranslatorProperty<SphereCollider, Vector3>(this, &SphereCollider::GetRelativePosition, &SphereCollider::SetRelativePosition));
        dataTranslator->Add("Rotation", DataTranslatorProperty<SphereCollider, Quaternion>(this, &SphereCollider::GetRelativeRotation, &SphereCollider::SetRelativeRotation));
        dataTranslator->Add("Radius", DataTranslatorProperty<SphereCollider, float>(this, &SphereCollider::GetRadiusRef, &SphereCollider::SetRadiusRef));
        return dataTranslator;
    }

}
