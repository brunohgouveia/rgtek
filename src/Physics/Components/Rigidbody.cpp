#include <Physics/Components/Rigidbody.h>
#include <Physics/Components/BoxCollider.h>
#include <Physics/PhysicsSystem.h>
#include <Tools/Debugger.h>

#ifdef RGTEK_EDITOR_ENABLED
#include <Editor/EditorItem.h>
#endif

namespace RGTek
{
	RCOMPONENT_IMPLEMENTATION(Rigidbody);

	///////////////////////////////////////////////////////////////////////

    Rigidbody::Rigidbody(float mass)
        : mBtRigidbody(nullptr)
        , mBtShape(nullptr)
        , mMass(mass)
    {
		// Do nothing
    }

	///////////////////////////////////////////////////////////////////////

    Rigidbody::~Rigidbody()
    {
		static_cast<PhysicsSystem*>(Component::GetSystem())->DeleteRigidbody(this);
		if (mBtRigidbody) delete mBtRigidbody;
        Debugger::Debug("Rigidbody shuttingdown...");
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::Update() {}

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::RegisterToSystem()
    {

    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SetEntity(Entity* entity)
    {
        // Call base method
        Debugger::AssertNotNull(entity, "Setting a null entity.");
        Component::SetEntity(entity);

        // Set transform
        const TransformPtr entityTransform = entity->GetTransform();
        Debugger::AssertNotNull(entityTransform, "Transform should not be null.");

		// Bind SynchronizeTransform to OnSetTransform Event
		entity->GetTransform()->OnSetTransform += std::bind(&Rigidbody::SynchronizeTransform, this);

        Debugger::AssertNotNull(GetEntity(), "Entity null");

        const std::vector<Collider*> colliders = GetEntity()->GetGameComponents<Collider>();

        mMotionState.SetTransform(GetEntity()->GetTransform());
        mMotionState.SetCenterOfMassTransformOffset(Transform());

        if (colliders.size() != 1)
        {
            mBtShape = new btCompoundShape(true, colliders.size());

            for (Collider* collider: colliders)
            {
                const btTransform transform = static_cast<btTransform>(collider->GetRelativeTransform());
                btCollisionShape* shape = collider->GetBtCollisionShape();
                (static_cast<btCompoundShape*>(mBtShape))->addChildShape(transform, shape);
            }
            mMotionState.SetCenterOfMassTransformOffset(Transform());
        }
        else
        {
            mBtShape = colliders[0]->GetBtCollisionShape();
            mMotionState.SetCenterOfMassTransformOffset(colliders[0]->GetRelativeTransform());
        }

        btVector3 localInertia(0,0,0);
        const float mass = (GetEntity()->IsStatic()) ? 0.0f : mMass;
        if (mass != 0.0f)
        {
            mBtShape->calculateLocalInertia(mass, localInertia);
        }

        const btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, &mMotionState, mBtShape, localInertia);
        mBtRigidbody = new btRigidBody(rbInfo);

        // This is completely necessary.
        // For ray tests, the btDynamicWorld returns an btCollisionObject. Thus,
        // we must bind the collision object to this Rigidibody object.
        mBtRigidbody->setUserPointer(this);

        static_cast<PhysicsSystem*>(GetSystem())->AddRigidbody(this);
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::Activate()
    {
        if (mBtRigidbody)
        {
            mBtRigidbody->activate(true);
        }
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SynchronizeTransform()
    {
		if (mBtRigidbody)
        {
            mBtRigidbody->setMotionState(mBtRigidbody->getMotionState());
            mBtRigidbody->activate(true);
        }

    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SynchronizeColliderRelativeTransform(const ColliderWeakPtr& collider)
    {
        Debugger::AssertNotNull(collider, "Collider null");

        if (btCompoundShape* compoundShape = dynamic_cast<btCompoundShape*>(mBtShape))
        {
            // Find the child holding the collider's btCollistionShape
            for (int i = 0; i < compoundShape->getNumChildShapes(); i++)
            {
                if (compoundShape->getChildShape(i) == collider->GetBtCollisionShape())
                {
                    compoundShape->updateChildTransform(i, static_cast<btTransform>(collider->GetRelativeTransform()));
                    mMotionState.SetCenterOfMassTransformOffset(Transform());
                    UpdateMassProps();
                    Activate();
                    return;
                }
            }
            Debugger::Fatal("Collider's shape was never added to the rigidbody");
        }
        else
        {
            mMotionState.SetCenterOfMassTransformOffset(collider->GetRelativeTransform());
            UpdateMassProps();
            Activate();
        }
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::AddForce(const Vector3& force)
    {
        mBtRigidbody->applyCentralForce(static_cast<btVector3>(force));
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SetMass(float mass)
    {
        mMass = mass;
        UpdateMassProps();
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SetPosition(const Vector3& position)
    {
        Debugger::AssertNotNull(GetEntity(), "Entity null");
        Debugger::AssertNotNull(GetEntity()->GetTransform(), "Transform null");

        GetEntity()->GetTransform()->SetPosition(position);
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SetRotation(const Quaternion& rotation)
    {
        Debugger::AssertNotNull(GetEntity(), "Entity null");
        Debugger::AssertNotNull(GetEntity()->GetTransform(), "Transform null");

        GetEntity()->GetTransform()->SetRotation(rotation);
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::SetScale(const Vector3& scale)
    {
        Debugger::AssertNotNull(GetEntity(), "Entity null");
        Debugger::AssertNotNull(GetEntity()->GetTransform(), "Transform null");

        GetEntity()->GetTransform()->SetScale(scale);
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::UpdateMassProps()
    {
        if (mBtRigidbody)
        {
            btVector3 localInertia(0,0,0);
            float mass = (GetEntity()->IsStatic()) ? 0.0f : mMass;
            if (mass != 0.0f)
            {
                mBtShape->calculateLocalInertia(mass, localInertia);
            }
            mBtRigidbody->setMassProps(mass, localInertia);
        }
    }

	///////////////////////////////////////////////////////////////////////

    void Rigidbody::InvalidateCollisionShape()
    {
        // Delete previous colision shape
        if (mBtShape)
        {
            delete mBtShape;
        }

        const std::vector<Collider*> colliders = GetEntity()->GetGameComponents<Collider>();
        if (colliders.size() != 1)
        {
            mBtShape = new btCompoundShape(true, colliders.size());

            for (Collider* collider: colliders)
            {
                const btTransform transform = static_cast<btTransform>(collider->GetRelativeTransform());
                btCollisionShape* shape = collider->GetBtCollisionShape();
                (static_cast<btCompoundShape*>(mBtShape))->addChildShape(transform, shape);
            }
            mMotionState.SetCenterOfMassTransformOffset(Transform());
        }
        else
        {
            mBtShape = colliders[0]->GetBtCollisionShape();
            mMotionState.SetCenterOfMassTransformOffset(colliders[0]->GetRelativeTransform());
        }
        mBtRigidbody->setCollisionShape(mBtShape);
    }

	///////////////////////////////////////////////////////////////////////

    DataTranslator* Rigidbody::GetDataTranslator()
    {
        auto dataTranslator = new CustomObjectDataTranslator<Rigidbody>("Rigidbody", this);
        dataTranslator->Add("Mass", DataTranslatorProperty<Rigidbody, float>(this, &Rigidbody::GetMassRef, &Rigidbody::SetMassRef));
        return dataTranslator;
    }

}
