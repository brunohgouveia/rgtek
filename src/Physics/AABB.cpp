#include <Physics/AABB.h>
#include <limits>
#include <Tools/Debugger.h>

namespace RGTek
{

    AABB::AABB():
        mMin(+std::numeric_limits<float>::max(),+std::numeric_limits<float>::max(),+std::numeric_limits<float>::max()),
        mMax(-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max())
    {

    }

	AABB::AABB(const Vector3& min, const Vector3& max) :
        mMin(+std::numeric_limits<float>::max(),+std::numeric_limits<float>::max(),+std::numeric_limits<float>::max()),
        mMax(-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max())
    {

    }

    void AABB::Inflate(const AABB& box)
    {
        Inflate(box.mMin, box.mMax);
    }

	void AABB::Inflate(const Vector3& min, const Vector3& max)
    {
        float xmin = (mMin.x < min.x) ? mMin.x : min.x;
        float ymin = (mMin.y < min.y) ? mMin.y : min.y;
        float zmin = (mMin.z < min.z) ? mMin.z : min.z;

        float xmax = (mMax.x > max.x) ? mMax.x : max.x;
        float ymax = (mMax.y > max.y) ? mMax.y : max.y;
        float zmax = (mMax.z > max.z) ? mMax.z : max.z;

		mMin = Vector3(xmin, ymin, zmin);
		mMax = Vector3(xmax, ymax, zmax);
    }

    void AABB::Fit(const AABB& box, float /*fatness*/)
    {
        Fit(box.mMin, box.mMax);
    }

	void AABB::Fit(const Vector3& min, const Vector3& max, float fatness)
    {
        Vector3 size = Vector3(max - min) * (1.0f + fatness);
        Vector3 center = Vector3((max + min) / 2.0f);
		mMin = Vector3(center - 0.5f * size);
		mMax = Vector3(center + 0.5f * size);
    }

    bool AABB::Intersect(const Ray& ray, RaycastHit& rayInfo) const
    {
        float tmin, tmax, amin, amax;

        // Important to cases where the ray is on the plane
        for (unsigned int i = 0; i < 3; i++)
        {
            if (ray.direction[i] == 0.0 && (ray.origin[i] < mMin[i] || ray.origin[i] > mMax[i]))
                return false;
        }

        tmin = ((&mMin)[ray.negative[0]].x - ray.origin.x) * ray.inverseDirection.x;
        tmax = ((&mMin)[1 - ray.negative[0]].x - ray.origin.x) * ray.inverseDirection.x;
        amin = ((&mMin)[ray.negative[1]].y - ray.origin.y) * ray.inverseDirection.y;
        amax = ((&mMin)[1 - ray.negative[1]].y - ray.origin.y) * ray.inverseDirection.y;

        if (tmin > amax || amin > tmax)
          return false;
        if (amin > tmin)
          tmin = amin;
        if (amax < tmax)
          tmax = amax;

        amin = ((&mMin)[ray.negative[2]].z - ray.origin.z) * ray.inverseDirection.z;
        amax = ((&mMin)[1 - ray.negative[2]].z - ray.origin.z) * ray.inverseDirection.z;

        if (tmin > amax || amin > tmax)
            return false;
        if (amin > tmin)
            tmin = amin;
        if (amax < tmax)
            tmax = amax;
        if (tmin > 0)
            rayInfo.mDistance = tmin;
        else if (tmax > 0)
            rayInfo.mDistance = tmax;
        else
            return false;

        return true;
    }

    float AABB::GetVolume() const
    {
        return (mMax.x - mMin.x) * (mMax.y - mMin.y) * (mMax.z - mMin.z);
    }

} /* RGTek */
