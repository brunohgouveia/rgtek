//
// Created by Bruno Gouveia on 12/11/15.
//

#include <Math/Plane.h>

namespace Math
{

    Plane::Plane()
    {

    }

    Plane::Plane(const Vector3& point, const Vector3& normal)
        : point(point)
        , normal(normal)
    {

    }

}