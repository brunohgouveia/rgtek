#include <Math/Vector4.h>
#include <Math/Vector3.h>
#include <Math/Math.h>

namespace Math
{
#define D 4

    Vector4::Vector4(float value) :
        x(value),
        y(value),
        z(value),
        w(value)
    {
        
    }
	
    Vector4::Vector4(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {
        
    }
    
    Vector4::Vector4(const float* data) :
        x(data[0]),
        y(data[1]),
        z(data[2]),
        w(data[3])
    {
        
    }
    
    Vector4::Vector4(const Vector4& vec) :
        x(vec.x),
        y(vec.y),
        z(vec.z),
        w(vec.w)
    {
        
    }
    
    Vector4::Vector4(const Vector3& vec, float w) :
        x(vec.x),
        y(vec.y),
        z(vec.z),
        w(w)
    {
        
    }
    
    float Vector4::Magnitude() const
    {
        return sqrtf(this->Dot(*this));
    }
    
    float Vector4::MagnitudeSq() const
    {
        return this->Dot(*this);
    }
    
    Vector4 Vector4::Normalized() const
    {
        return *this/Magnitude();
    }
    
    float Vector4::Dot(const Vector4& vec) const
    {
        float result = 0.0f;
        for (unsigned int i = 0; i < D; i++)
        {
            result += (*this)[i] * vec[i];
        }
        return result;
    }
    
    Vector4 Vector4::Lerp(const Vector4& vec, float factor) const
    {
        return *this + (vec - *this) * factor;
    }
    
    Vector4 Vector4::Reflect(const Vector4& normal) const
    {
        return *this - (normal * (this->Dot(normal) * 2.0f));
    }
    
    Vector4 Vector4::operator+(const Vector4& vec) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + vec[i];
        }
        return result;
    }
    
    Vector4 Vector4::operator-(const Vector4& vec) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + vec[i];
        }
        return result;
    }
    
    Vector4 Vector4::operator*(const Vector4& vec) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * vec[i];
        }
        return result;
    }
    
    Vector4 Vector4::operator/(const Vector4& vec) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / vec[i];
        }
        return result;
    }
    
    Vector4 Vector4::operator+(float value) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + value;
        }
        return result;
    }
    
    Vector4 Vector4::operator-(float value) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] - value;
        }
        return result;
    }
    
    Vector4 Vector4::operator*(float value) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * value;
        }
        return result;
    }
    
    Vector4 Vector4::operator/(float value) const
    {
        Vector4 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / value;
        }
        return result;
    }
    
    Vector4& Vector4::operator+=(const Vector4& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += vec[i];
        }
        return *this;
    }
    
    Vector4& Vector4::operator-=(const Vector4& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= vec[i];
        }
        return *this;
    }
    
    Vector4& Vector4::operator*=(const Vector4& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= vec[i];
        }
        return *this;
    }
    
    Vector4& Vector4::operator/=(const Vector4& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= vec[i];
        }
        return *this;
    }
    
    Vector4& Vector4::operator+=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += value;
        }
        return *this;
    }
    
    Vector4& Vector4::operator-=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= value;
        }
        return *this;
    }
    
    Vector4& Vector4::operator*=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= value;
        }
        return *this;
    }
    
    Vector4& Vector4::operator/=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= value;
        }
        return *this;
    }
    
    bool Vector4::operator==(const Vector4& vec) const
    {
        for (unsigned int i = 0; i < D; i++)
        {
            if ((*this)[i] != vec[i]) return false;
        }
        return true;
    }
    
    bool Vector4::operator!=(const Vector4& vec) const
    {
        return !(operator==(vec));
    }
    
    Vector4 operator*(float value, const Vector4& vec)
    {
        return vec * value;
    }
    
#undef D
} /* Math */
