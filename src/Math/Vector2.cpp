#include <Math/Vector2.h>
#include <Math/Math.h>

namespace Math
{
#define D 2

    Vector2::Vector2(float value) :
        x(value),
        y(value)
    {

    }

    Vector2::Vector2(float x, float y) :
        x(x),
        y(y)
    {

    }

    Vector2::Vector2(const float* data) :
        x(data[0]),
        y(data[1])
    {

    }

    Vector2::Vector2(const Vector2& vec) :
        x(vec.x),
        y(vec.y)
    {

    }

    float Vector2::Magnitude() const
    {
        return sqrtf(this->Dot(*this));
    }

    float Vector2::MagnitudeSq() const
    {
        return this->Dot(*this);
    }

    Vector2 Vector2::Normalized() const
    {
        return *this/Magnitude();
    }

    float Vector2::Dot(const Vector2& vec) const
    {
        float result = 0.0f;
        for (unsigned int i = 0; i < D; i++)
        {
            result += (*this)[i] * vec[i];
        }
        return result;
    }

    Vector2 Vector2::Lerp(const Vector2& vec, float factor) const
    {
        return *this + (vec - *this) * factor;
    }

    Vector2 Vector2::Reflect(const Vector2& normal) const
    {
        return *this - (normal * (this->Dot(normal) * 2.0f));
    }

    Vector2 Vector2::operator+(const Vector2& vec) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + vec[i];
        }
        return result;
    }

    Vector2 Vector2::operator-(const Vector2& vec) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] - vec[i];
        }
        return result;
    }

    Vector2 Vector2::operator*(const Vector2& vec) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * vec[i];
        }
        return result;
    }

    Vector2 Vector2::operator/(const Vector2& vec) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / vec[i];
        }
        return result;
    }

    Vector2 Vector2::operator+(float value) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + value;
        }
        return result;
    }

    Vector2 Vector2::operator-(float value) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] - value;
        }
        return result;
    }

    Vector2 Vector2::operator*(float value) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * value;
        }
        return result;
    }

    Vector2 Vector2::operator/(float value) const
    {
        Vector2 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / value;
        }
        return result;
    }

    Vector2& Vector2::operator+=(const Vector2& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += vec[i];
        }
        return *this;
    }

    Vector2& Vector2::operator-=(const Vector2& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= vec[i];
        }
        return *this;
    }

    Vector2& Vector2::operator*=(const Vector2& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= vec[i];
        }
        return *this;
    }

    Vector2& Vector2::operator/=(const Vector2& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= vec[i];
        }
        return *this;
    }

    Vector2& Vector2::operator+=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += value;
        }
        return *this;
    }

    Vector2& Vector2::operator-=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= value;
        }
        return *this;
    }

    Vector2& Vector2::operator*=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= value;
        }
        return *this;
    }

    Vector2& Vector2::operator/=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= value;
        }
        return *this;
    }

    bool Vector2::operator==(const Vector2& vec) const
    {
        for (unsigned int i = 0; i < D; i++)
        {
            if ((*this)[i] != vec[i]) return false;
        }
        return true;
    }

    bool Vector2::operator!=(const Vector2& vec) const
    {
        return !(operator==(vec));
    }

    Vector2 operator*(float value, const Vector2& vec)
    {
        return vec * value;
    }

#undef D
} /* Math */
