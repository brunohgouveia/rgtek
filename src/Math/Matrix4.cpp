#include <Math/Matrix4.h>

namespace Math
{
#define D 4
    
    Matrix4::Matrix4()
    {
        InitIdentity();
    }
    
    Matrix4::Matrix4(const float* data)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            for (unsigned int j = 0; j < D; j++)
            {
                m[i][j] = data[i * D + j];
            }
        }
    }
    
    Matrix4::Matrix4(const Matrix4& mat)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            for (unsigned int j = 0; j < D; j++)
            {
                m[i][j] = mat[i][j];
            }
        }
    }
    
    Matrix4 Matrix4::InitIdentity()
    {
        for(unsigned int i = 0; i < D; i++)
        {
            for(unsigned int j = 0; j < D; j++)
            {
                if(i == j)
                    m[i][j] = 1.0f;
                else
                    m[i][j] = 0.0f;
            }
        }
        return *this;
    }
    
    Matrix4 Matrix4::InitScale(const Vector3& r)
    {
        for(unsigned int i = 0; i < D; i++)
        {
            for(unsigned int j = 0; j < D; j++)
            {
                if(i == j && i != D - 1)
                    m[i][j] = r[i];
                else
                    m[i][j] = 0.0f;
            }
        }
        
        m[D - 1][D - 1] = 1.0f;
        
        return *this;
    }
    
    Matrix4 Matrix4::InitTranslation(const Vector3& r)
    {
        for(unsigned int i = 0; i < D; i++)
        {
            for(unsigned int j = 0; j < D; j++)
            {
                if(i == D - 1 && j != D - 1)
                    m[i][j] = r[j];
                else if(i == j)
                    m[i][j] = 1.0f;
                else
                    m[i][j] = 0.0f;
            }
        }
        
        m[D - 1][D - 1] = 1.0f;
        
        return *this;
    }
    
    Matrix4 Matrix4::InitRotationEuler(float rotateX, float rotateY, float rotateZ)
    {
        Matrix4 rx, ry, rz;
        
        const float x = rotateX;
        const float y = rotateY;
        const float z = rotateZ;
        
        rx[0][0] = 1.0f;   rx[1][0] = 0.0f  ;  rx[2][0] = 0.0f   ; rx[3][0] = 0.0f;
        rx[0][1] = 0.0f;   rx[1][1] = cos(x);  rx[2][1] = -sin(x); rx[3][1] = 0.0f;
        rx[0][2] = 0.0f;   rx[1][2] = sin(x);  rx[2][2] = cos(x) ; rx[3][2] = 0.0f;
        rx[0][3] = 0.0f;   rx[1][3] = 0.0f  ;  rx[2][3] = 0.0f   ; rx[3][3] = 1.0f;
        
        ry[0][0] = cos(y); ry[1][0] = 0.0f;    ry[2][0] = -sin(y); ry[3][0] = 0.0f;
        ry[0][1] = 0.0f  ; ry[1][1] = 1.0f;    ry[2][1] = 0.0f   ; ry[3][1] = 0.0f;
        ry[0][2] = sin(y); ry[1][2] = 0.0f;    ry[2][2] = cos(y) ; ry[3][2] = 0.0f;
        ry[0][3] = 0.0f  ; ry[1][3] = 0.0f;    ry[2][3] = 0.0f   ; ry[3][3] = 1.0f;
        
        rz[0][0] = cos(z); rz[1][0] = -sin(z); rz[2][0] = 0.0f;    rz[3][0] = 0.0f;
        rz[0][1] = sin(z); rz[1][1] = cos(z) ; rz[2][1] = 0.0f;    rz[3][1] = 0.0f;
        rz[0][2] = 0.0f  ; rz[1][2] = 0.0f   ; rz[2][2] = 1.0f;    rz[3][2] = 0.0f;
        rz[0][3] = 0.0f  ; rz[1][3] = 0.0f   ; rz[2][3] = 0.0f;    rz[3][3] = 1.0f;
        
        *this = rz * ry * rx;
        
        return *this;
    }
    
    Matrix4 Matrix4::InitRotationFromVectors(const Vector3& n, const Vector3& v, const Vector3& u)
    {
        (*this)[0][0] = u.x;   (*this)[1][0] = u.y;   (*this)[2][0] = u.z;   (*this)[3][0] = 0.0f;
        (*this)[0][1] = v.x;   (*this)[1][1] = v.y;   (*this)[2][1] = v.z;   (*this)[3][1] = 0.0f;
        (*this)[0][2] = n.x;   (*this)[1][2] = n.y;   (*this)[2][2] = n.z;   (*this)[3][2] = 0.0f;
        (*this)[0][3] = 0.0f;  (*this)[1][3] = 0.0f;  (*this)[2][3] = 0.0f;  (*this)[3][3] = 1.0f;
        
        return *this;
    }
    
    Matrix4 Matrix4::InitRotationFromDirection(const Vector3& forward, const Vector3& up)
    {
        Vector3 n = forward.Normalized();
        Vector3 u = Vector3(up.Normalized()).Cross(n);
        Vector3 v = n.Cross(u);
        
        return InitRotationFromVectors(n,v,u);
    }
    
    Matrix4 Matrix4::InitPerspective(float fov, float aspectRatio, float zNear, float zFar)
    {
        const float zRange     = zNear - zFar;
        const float tanHalfFOV = tanf(fov / 2.0f);
        
        (*this)[0][0] = 1.0f/(tanHalfFOV * aspectRatio); (*this)[1][0] = 0.0f;   (*this)[2][0] = 0.0f;            (*this)[3][0] = 0.0f;
        (*this)[0][1] = 0.0f;                   (*this)[1][1] = 1.0f/tanHalfFOV; (*this)[2][1] = 0.0f;            (*this)[3][1] = 0.0f;
        (*this)[0][2] = 0.0f;                   (*this)[1][2] = 0.0f;            (*this)[2][2] = (-zNear - zFar)/zRange ; (*this)[3][2] = 2.0f*zFar*zNear/zRange;
        (*this)[0][3] = 0.0f;                   (*this)[1][3] = 0.0f;            (*this)[2][3] = 1.0f;            (*this)[3][3] = 0.0f;
        
        return *this;
    }
    
    Matrix4 Matrix4::InitOrthographic(float left, float right, float bottom, float top, float near, float far)
    {
        const float width = (right - left);
        const float height = (top - bottom);
        const float depth = (far - near);
        
        (*this)[0][0] = 2.0f/width; (*this)[1][0] = 0.0f;        (*this)[2][0] = 0.0f;        (*this)[3][0] = -(right + left)/width;
        (*this)[0][1] = 0.0f;       (*this)[1][1] = 2.0f/height; (*this)[2][1] = 0.0f;        (*this)[3][1] = -(top + bottom)/height;
        (*this)[0][2] = 0.0f;       (*this)[1][2] = 0.0f;        (*this)[2][2] = -2.0f/depth; (*this)[3][2] = -(far + near)/depth;
        (*this)[0][3] = 0.0f;       (*this)[1][3] = 0.0f;        (*this)[2][3] = 0.0f;        (*this)[3][3] = 1.0f;
        
        return *this;
    }
    
    Matrix4 Matrix4::Inverse() const
    {
        Matrix4 ret;
        float* inv = (float *) ret.m, det;
        float* m = (float*) this->m;
        
        inv[0] = m[5]*m[10]*m[15] - m[5]*m[11]*m[14] - m[9]*m[6]*m[15] + m[9]*m[7]*m[14] + m[13]*m[6]*m[11] - m[13]*m[7]*m[10];
        inv[4] = -m[4]*m[10]*m[15] + m[4]*m[11]*m[14] + m[8]*m[6]*m[15] - m[8]*m[7]*m[14] - m[12]*m[6]*m[11] + m[12]*m[7]*m[10];
        inv[8] = m[4]*m[9]*m[15] - m[4]*m[11]*m[13] - m[8]*m[5]*m[15] + m[8]*m[7]*m[13] + m[12]*m[5]*m[11] - m[12]*m[7]*m[9];
        inv[12] = -m[4]*m[9]*m[14] + m[4]*m[10]*m[13] + m[8]*m[5]*m[14] - m[8]*m[6]*m[13] - m[12]*m[5]*m[10] + m[12]*m[6]*m[9];
        inv[1] = -m[1]*m[10]*m[15] + m[1]*m[11]*m[14] + m[9]*m[2]*m[15] - m[9]*m[3]*m[14] - m[13]*m[2]*m[11] + m[13]*m[3]*m[10];
        inv[5] = m[0]*m[10]*m[15] - m[0]*m[11]*m[14] - m[8]*m[2]*m[15] + m[8]*m[3]*m[14] + m[12]*m[2]*m[11] - m[12]*m[3]*m[10];
        inv[9] = -m[0]*m[9]*m[15] + m[0]*m[11]*m[13] + m[8]*m[1]*m[15] - m[8]*m[3]*m[13] - m[12]*m[1]*m[11] + m[12]*m[3]*m[9];
        inv[13] = m[0]*m[9]*m[14] - m[0]*m[10]*m[13] - m[8]*m[1]*m[14] + m[8]*m[2]*m[13] + m[12]*m[1]*m[10] - m[12]*m[2]*m[9];
        inv[2] = m[1]*m[6]*m[15] - m[1]*m[7]*m[14] - m[5]*m[2]*m[15] + m[5]*m[3]*m[14] + m[13]*m[2]*m[7] - m[13]*m[3]*m[6];
        inv[6] = -m[0]*m[6]*m[15] + m[0]*m[7]*m[14] + m[4]*m[2]*m[15] - m[4]*m[3]*m[14] - m[12]*m[2]*m[7] + m[12]*m[3]*m[6];
        inv[10] = m[0]*m[5]*m[15] - m[0]*m[7]*m[13] - m[4]*m[1]*m[15] + m[4]*m[3]*m[13] + m[12]*m[1]*m[7] - m[12]*m[3]*m[5];
        inv[14] = -m[0]*m[5]*m[14] + m[0]*m[6]*m[13] + m[4]*m[1]*m[14] - m[4]*m[2]*m[13] - m[12]*m[1]*m[6] + m[12]*m[2]*m[5];
        inv[3] = -m[1]*m[6]*m[11] + m[1]*m[7]*m[10] + m[5]*m[2]*m[11] - m[5]*m[3]*m[10] - m[9]*m[2]*m[7] + m[9]*m[3]*m[6];
        inv[7] = m[0]*m[6]*m[11] - m[0]*m[7]*m[10] - m[4]*m[2]*m[11] + m[4]*m[3]*m[10] + m[8]*m[2]*m[7] - m[8]*m[3]*m[6];
        inv[11] = -m[0]*m[5]*m[11] + m[0]*m[7]*m[9] + m[4]*m[1]*m[11] - m[4]*m[3]*m[9] - m[8]*m[1]*m[7] + m[8]*m[3]*m[5];
        inv[15] = m[0]*m[5]*m[10] - m[0]*m[6]*m[9] - m[4]*m[1]*m[10] + m[4]*m[2]*m[9] + m[8]*m[1]*m[6] - m[8]*m[2]*m[5];

        det = m[0]*inv[0] + m[1]*inv[4] + m[2]*inv[8] + m[3]*inv[12];
        
        if (det == 0.0f)
            return Matrix4();
        
        return ret;
    }
    
    Matrix4 Matrix4::Transpose() const
    {
        Matrix4 t;
        for (unsigned int j = 0; j < D; j++)
        {
            for (unsigned int i = 0; i < D; i++)
            {
                t[i][j] = m[j][i];
            }
        }
        return t;
    }
    
    Matrix4 Matrix4::operator*(const Matrix4& r) const
    {
        Matrix4 ret;
        for (unsigned int i = 0 ; i < D ; i++)
        {
            for (unsigned int j = 0 ; j < D; j++)
            {
                ret.m[i][j] = 0.0f;
                for(unsigned int k = 0; k < D; k++)
                {
                    ret.m[i][j] += m[k][j] * r.m[i][k];
                }
            }
        }
        return ret;
    }
    
    Vector4 Matrix4::operator*(const Vector4& r) const
    {
        Vector4 ret;
        for(unsigned int i = 0; i < D; i++)
        {
            ret[i] = 0;
            for(unsigned int j = 0; j < D; j++)
            {
                ret[i] += m[j][i] * r[j];
            }
        }
        return ret;
    }
    
    Vector4 Matrix4::Transform(const Vector4& r) const
    {
        Vector4 ret;
        for(unsigned int i = 0; i < D; i++)
        {
            ret[i] = 0;
            for(unsigned int j = 0; j < D; j++)
            {
                ret[i] += m[j][i] * r[j];
            }
        }
        return ret;
    }
    
    Vector3 Matrix4::Transform(const Vector3& r) const
    {
        Vector4 ret = Transform(Vector4(r));
        return Vector3(ret / ret.w);
    }
    
#undef D
}