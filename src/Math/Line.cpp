//
// Created by Bruno Gouveia on 12/11/15.
//

#include <Math/Line.h>

namespace Math
{
    Line::Line(const Vector3& startPoint, const Vector3& endPoint)
            : startPoint(startPoint)
            , endPoint(endPoint)
    {

    }

    Line::Line(const Vector4& startPoint, const Vector4& endPoint)
        : startPoint(startPoint)
        , endPoint(endPoint)
    {

    }
}