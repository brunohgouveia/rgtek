#include <Math/Quaternion.h>

#ifdef RGTEK_PHYSICS_ENABLED
#include <LinearMath/btQuaternion.h>
#endif

namespace Math
{
	
#ifdef RGTEK_PHYSICS_ENABLED
	Quaternion& Quaternion::operator=(const btQuaternion& quaternion)
	{
		x = quaternion.x();
		y = quaternion.y();
		z = quaternion.z();
		w = quaternion.w();

		return (*this);
	}
	
	Quaternion::operator btQuaternion() const
	{
		return btQuaternion(x, y, z, w);
	}
#endif
}