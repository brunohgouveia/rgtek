#include <Math/Vector3.h>
#include <Math/Vector4.h>
#include <Math/Quaternion.h>
#include <Math/Plane.h>
#include <Math/Circle.h>

#ifdef RGTEK_PHYSICS_ENABLED
#include <LinearMath/btVector3.h>
#endif

namespace Math
{
#define D 3

    Vector3 Vector3::zero    = Vector3( 0.0f,  0.0f,  0.0f);
    Vector3 Vector3::one     = Vector3( 1.0f,  1.0f,  1.0f);
    Vector3 Vector3::up      = Vector3( 0.0f,  1.0f,  0.0f);
    Vector3 Vector3::down    = Vector3( 0.0f, -1.0f,  0.0f);
    Vector3 Vector3::left    = Vector3(-1.0f,  0.0f,  0.0f);
    Vector3 Vector3::right   = Vector3( 1.0f,  0.0f,  0.0f);
    Vector3 Vector3::forward = Vector3( 0.0f,  0.0f,  1.0f);
    Vector3 Vector3::back    = Vector3( 0.0f,  0.0f, -1.0f);

    Vector3::Vector3(float value) :
        x(value),
        y(value),
        z(value)
    {

    }

    Vector3::Vector3(float x, float y, float z) :
        x(x),
        y(y),
        z(z)
    {

    }

    Vector3::Vector3(const float* data) :
        x(data[0]),
        y(data[1]),
        z(data[2])
    {

    }

    Vector3::Vector3(const Vector3& vec) :
        x(vec.x),
        y(vec.y),
        z(vec.z)
    {

    }

    Vector3::Vector3(const Vector4& vec) :
        x(vec.x),
        y(vec.y),
        z(vec.z)
    {

    }

    float Vector3::Magnitude() const
    {
        return sqrtf(this->Dot(*this));
    }

    float Vector3::MagnitudeSq() const
    {
        return this->Dot(*this);
    }

    Vector3 Vector3::Normalized() const
    {
        return *this/Magnitude();
    }

    float Vector3::Dot(const Vector3& vec) const
    {
        float result = 0.0f;
        for (unsigned int i = 0; i < D; i++)
        {
            result += (*this)[i] * vec[i];
        }
        return result;
    }

    Vector3 Vector3::Cross(const Vector3& vec) const
    {
        const float _x = this->y * vec.z - this->z * vec.y;
        const float _y = this->z * vec.x - this->x * vec.z;
        const float _z = this->x * vec.y - this->y * vec.x;

        return Vector3(_x, _y, _z);
    }

    Vector3 Vector3::Rotate(float angle, const Vector3& axis) const
    {
        const float sin = sinf(-angle);
        const float cos = cosf(-angle);

        return this->Cross(axis * sin) +        //Rotation on local X
        (*this * cos) +                     //Rotation on local Z
        axis * this->Dot(axis * (1 - cos)); //Rotation on local Y
    }

    Vector3 Vector3::Rotate(const Quaternion& rotation) const
    {
        Quaternion conjugateQ = rotation.Conjugate();
        Quaternion w = rotation * (*this) * conjugateQ;

        Vector3 ret(w.x, w.y, w.z);

        return ret;
    }

    Vector3 Vector3::Lerp(const Vector3& vec, float factor) const
    {
        return *this + (vec - *this) * factor;
    }

    Vector3 Vector3::Reflect(const Vector3& normal) const
    {
        return *this - (normal * (this->Dot(normal) * 2.0f));
    }

    Vector3 Vector3::ProjectOnto(const Vector3& vec) const
    {
        return (this->Dot(vec) / vec.MagnitudeSq()) * vec;
    }

    Vector3 Vector3::ProjectOnto(const Plane& plane) const
    {
        return (*this) - this->ProjectOnto(plane.normal);
    }

    Vector3 Vector3::ProjectOnto(const Circle& circle) const
    {
        Vector3 vectorProjected = this->ProjectOnto(Plane(circle.center, circle.normal));
        return vectorProjected.Magnitude() > circle.radius ? vectorProjected.Normalized() * circle.radius : vectorProjected;
    }

    Vector3 Vector3::operator-() const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = -(*this)[i];
        }
        return result;
    }

    Vector3 Vector3::operator+(const Vector3& vec) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + vec[i];
        }
        return result;
    }

    Vector3 Vector3::operator-(const Vector3& vec) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] - vec[i];
        }
        return result;
    }

    Vector3 Vector3::operator*(const Vector3& vec) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * vec[i];
        }
        return result;
    }

    Vector3 Vector3::operator/(const Vector3& vec) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / vec[i];
        }
        return result;
    }

    Vector3 Vector3::operator+(float value) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] + value;
        }
        return result;
    }

    Vector3 Vector3::operator-(float value) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] - value;
        }
        return result;
    }

    Vector3 Vector3::operator*(float value) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] * value;
        }
        return result;
    }

    Vector3 Vector3::operator/(float value) const
    {
        Vector3 result;
        for (unsigned int i = 0; i < D; i++)
        {
            result[i] = (*this)[i] / value;
        }
        return result;
    }

    Vector3& Vector3::operator+=(const Vector3& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += vec[i];
        }
        return *this;
    }

    Vector3& Vector3::operator-=(const Vector3& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= vec[i];
        }
        return *this;
    }

    Vector3& Vector3::operator*=(const Vector3& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= vec[i];
        }
        return *this;
    }

    Vector3& Vector3::operator/=(const Vector3& vec)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= vec[i];
        }
        return *this;
    }

    Vector3& Vector3::operator+=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] += value;
        }
        return *this;
    }

    Vector3& Vector3::operator-=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] -= value;
        }
        return *this;
    }

    Vector3& Vector3::operator*=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] *= value;
        }
        return *this;
    }

    Vector3& Vector3::operator/=(float value)
    {
        for (unsigned int i = 0; i < D; i++)
        {
            (*this)[i] /= value;
        }
        return *this;
    }

    bool Vector3::operator==(const Vector3& vec) const
    {
        for (unsigned int i = 0; i < D; i++)
        {
            if ((*this)[i] != vec[i]) return false;
        }
        return true;
    }

    bool Vector3::operator!=(const Vector3& vec) const
    {
        return !(operator==(vec));
    }

#ifdef RGTEK_PHYSICS_ENABLED
    Vector3& Vector3::operator=(const btVector3& vec)
    {
        x = vec.x();
        y = vec.y();
        z = vec.z();

        return (*this);
    }

    Vector3::operator btVector3() const
    {
        return btVector3(x, y, z);
    }
#endif

    Vector3 operator*(float value, const Vector3& vec)
    {
        return vec * value;
    }

#undef D
} /* Math */
