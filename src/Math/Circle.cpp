//
// Created by Bruno Gouveia on 12/14/15.
//

#include <Math/Circle.h>

namespace Math
{

    Circle::Circle(const Vector3 &center, const Vector3& normal, float radius)
        : center(center)
        , normal(normal)
        , radius(radius)
    {

    }

}