@echo off

if exist msvs\bin\Sample.exe (
    start /d ".\msvs" bin\Sample.exe
) else (
    echo Solution was not build in Release mode yet. You have to build it first!
)