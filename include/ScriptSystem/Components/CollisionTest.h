#ifndef RGTEK_COLLISIONTEST_H__
#define RGTEK_COLLISIONTEST_H__

#include <ScriptSystem/Components/Script.h>
#include <ScriptSystem/ScriptSystem.h>

namespace RGTek
{

    class CollisionTest: public Script
    {
    public:
        RSCRIPT(CollistionTest);

        CollisionTest();
        virtual ~CollisionTest();

        virtual void OnCollisionEnter(const CollisionPtr& collision) override;
        virtual void OnCollisionStay(const CollisionPtr& collision) override;
        virtual void OnCollisionExit(const CollisionPtr& collision) override;
    };

}

#endif
