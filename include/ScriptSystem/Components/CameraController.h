#ifndef RGTEK_CAMERACONTROLLER_H__
#define RGTEK_CAMERACONTROLLER_H__

#include <ScriptSystem/Components/Script.h>
#include <ScriptSystem/ScriptSystem.h>

namespace RGTek
{

    class CameraController: public Script
    {
    public:
        RSCRIPT(CameraController);

		RGTEK_REGISTER_TYPE(CameraController,
			RGTEK_FIELD("Num hits", &CameraController::GetNumHits, &CameraController::SetNumHits)	
		)

        CameraController();
        virtual ~CameraController();

        virtual void Update() override;

        virtual void Serialize(Serializer *serializer) override;

		int GetNumHits() const { return mNumHits; }
		void SetNumHits(int numHits) { mNumHits = numHits; }

    protected:
        int  mNumHits;
        bool mFlags;
    };

}

#endif
