//
// Created by Bruno Gouveia on 12/1/15.
//

#ifndef RGTEK_SCRIPTSystem_H
#define RGTEK_SCRIPTSystem_H

#include <Core/Platform.h>
#include <Core/System.h>
#include <Util/Serializer.h>
#include <Util/ThirdPart/FileWatcher/FileWatcher.h>

#include <set>
#include <ScriptSystem/Components/Script.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(GameComponent);
    FWD_CLASS_EXTEND_PTR_TYPES(Script);
    FWD_CLASS_EXTEND_PTR_TYPES(ScriptSystem)
    class ScriptSystem: public System
    {
    public:
        typedef Script* (*ScriptInstantiatorFunction)(Script* oldAddress);

        RSYSTEM(ScriptSystem)

        ScriptSystem();
        virtual ~ScriptSystem();

        void Update();

        void CheckChangeOnScripts();

        virtual Component* CreateComponentByName(const String& className) override;
        virtual void DeleteComponent(Component* component) override;
        virtual void DeleteComponentsOfEntity(Entity*) override {};

        void RegisterScriptInsantiator(const String& scriptName, ScriptInstantiatorFunction instantiator);

    private:
        ScriptWeakPtr InstantiateScript(const String& scriptName, ScriptWeakPtr oldAddress = nullptr);

        void LoadDynamicLib();
        void UnloadDynamicLib();
        void BuildDynamicLibHandle();
        void ReloadScripts();

        struct ScriptMetaData
        {
            ScriptWeakPtr mScriptPtr;
            String mScriptName;
            Serializer mSerializer;
        };

        struct FileWatcherListener: public FW::FileWatchListener
        {
            FileWatcherListener(ScriptSystem* scriptSystem);
            void handleFileAction(FW::WatchID watchid, const FW::String& dir, const FW::String& filename, FW::Action action);
            ScriptSystem* mScriptSystem;
        };

        void*                  mDynamicLibHandle;
        Vector<ScriptMetaData> mScripts;

        FW::FileWatcher        mFileWatcher;
        FW::WatchID            mWatchId;
        FileWatcherListener    mFileWatcherListener;

        std::set<String>       mFilesOutdated;

        friend class ScriptRegister;
        static std::map<String, ScriptInstantiatorFunction> sScriptInstantiatorMap;
    };

    class ScriptRegister
    {
    public:
        ScriptRegister(const String& scriptName, ScriptWeakPtr (*createScript)(ScriptWeakPtr));
    };

#define RSCRIPT(scriptName) \
    RCOMPONENT(scriptName) \
    static Script* CreateInstance(Script* oldAddress);

#define RSCRIPT_IMPLEMENTATION(scriptName) \
    RCOMPONENT_IMPLEMENTATION(scriptName) \
    Script* scriptName::CreateInstance(Script* oldAddress) { return (oldAddress != nullptr ? new (oldAddress) scriptName() : new scriptName()); }

#define REGISTER_SCRIPT(scriptName) \
    ScriptWeakPtr Create##scriptName(ScriptWeakPtr oldAddress) { return (oldAddress != nullptr ? new (oldAddress) scriptName() : new scriptName()); } \
    ScriptRegister scriptName##Register = ScriptRegister(#scriptName, Create##scriptName);

}

#endif //RGTEK_SCRIPTSystem_H
