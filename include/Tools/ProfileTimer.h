//
// Created by Bruno Gouveia on 12/15/15.
//

#pragma once

#include <Tools/Time.h>

namespace RGTek
{

    class ProfileTimer
    {
    public:
        ProfileTimer();

        void StartInvocation();
        void StopInvocation();

        long long GetDuration() const;
        unsigned int GetNumInvocations() const;

        void Reset();

    private:
        Time::TimePoint mStartTimePoint;
        long long  mTotalTime;
        unsigned int    mNumInvocations;
    };

}