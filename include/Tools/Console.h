#ifndef CONSOLE_H__
#define CONSOLE_H__

#include <string>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <iostream>

namespace RGTek
{

    class Console
    {
    public:
		enum ColorCode
		{
			BLUE = 9,
			GREEN = 10,
			CYAN = 11,
			RED = 12,
			PINK = 13,
			YELLOW = 14,
			NC = 7
		};

		static void SetColor(ColorCode colorCode, std::ostream& streamBuffer);

        class Out
        {
        private:
            Out();

            template<const Console::ColorCode colorCode = ColorCode::NC>
            static void colorPrint(const char* format, ...)
            {
                va_list args;
                va_start(args, format);
                vsprintf(sBuffer, format, args);
                va_end(args);

				Console::SetColor(colorCode, std::cout);
				std::cout << std::string(sBuffer);
				Console::SetColor(ColorCode::NC, std::cout);
            }

			template<const Console::ColorCode colorCode = ColorCode::NC>
            static void colorPrintln(const char* format, ...)
            {
                va_list args;
                va_start(args, format);
                vsprintf(sBuffer, format, args);
                va_end(args);

				Console::SetColor(colorCode, std::cout);
				std::cout << std::string(sBuffer) << std::endl;
				Console::SetColor(ColorCode::NC, std::cout);
            }

        public:

            template <typename... Args>
            static void print(const char* fmt, Args&& ... args)
            {
                return colorPrint<NC>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void println(const char* fmt, Args&& ... args)
            {
                return colorPrintln<NC>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void redPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<RED>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void redPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<RED>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void bluePrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<BLUE>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void bluePrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<BLUE>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void greenPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<GREEN>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void greenPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<GREEN>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void yellowPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<YELLOW>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void yellowPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<YELLOW>(fmt, std::forward<Args>(args)...);
            }

            static void TestColors();
        };

        class Err
        {
        private:
            Err();

			template<const Console::ColorCode colorCode = ColorCode::NC>
            static void colorPrint(const char* format, ...)
            {
                va_list args;
                va_start(args, format);
                vsprintf(sBuffer, format, args);
                va_end(args);

				Console::SetColor(colorCode, std::cerr);
				std::cerr << std::string(sBuffer);
				Console::SetColor(ColorCode::NC, std::cerr);
            }

			template<const Console::ColorCode colorCode = ColorCode::NC>
            static void colorPrintln(const char* format, ...)
            {
                va_list args;
                va_start(args, format);
                vsprintf(sBuffer, format, args);
                va_end(args);

				Console::SetColor(colorCode, std::cerr);
				std::cerr << std::string(sBuffer) << std::endl;
				Console::SetColor(ColorCode::NC, std::cerr);
            }
        public:
            template <typename... Args>
            static void print(const char* fmt, Args&& ... args)
            {
                return colorPrint<NC>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void println(const char* fmt, Args&& ... args)
            {
                return colorPrintln<NC>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void redPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<RED>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void redPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<RED>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void bluePrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<BLUE>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void bluePrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<BLUE>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void greenPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<GREEN>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void greenPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<GREEN>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void yellowPrint(const char* fmt, Args&& ... args)
            {
                return colorPrint<YELLOW>(fmt, std::forward<Args>(args)...);
            }

            template <typename... Args>
            static void yellowPrintln(const char* fmt, Args&& ... args)
            {
                return colorPrintln<YELLOW>(fmt, std::forward<Args>(args)...);
            }
        };

    private:
        Console();

        static char sBuffer[16384];

    };

}

#endif	// CONSOLE_H__
