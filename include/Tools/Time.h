#ifndef RGTEK_TIME_H__
#define RGTEK_TIME_H__

#include <chrono>
#include <ctime>

namespace RGTek
{

    class Time
    {
    public:

        class TimePoint;
        class Duration;

        static TimePoint Now();

        class TimePoint
        {
        public:
            TimePoint(long long timeSinceEpoch = 0) : mTimeSinceEpoch(timeSinceEpoch) {}
            TimePoint(const TimePoint& timePoint) : mTimeSinceEpoch(timePoint.mTimeSinceEpoch) {}

            long long GetTimeSinceEpoch() const { return mTimeSinceEpoch; }
            void SetTimeSinceEpoch(long long time) { mTimeSinceEpoch = time; }

        private:
            long long mTimeSinceEpoch;
        };

        class Duration
        {
        public:
            Duration() {}
            Duration(long long milliseconds);
            Duration(const TimePoint& start) : mStart(start), mEnd(start) {}
            Duration(const TimePoint& start, const TimePoint& end) : mStart(start), mEnd(end) {}

            void Start();
            void End();
            long long Count() const;
            void SetEndingPoint(const TimePoint& end) { mEnd = end; }

            bool operator>(const Duration& duration);
            bool operator<(const Duration& duration);
            bool operator>=(const Duration& duration);
            bool operator<=(const Duration& duration);

        private:
            TimePoint mStart;
            TimePoint mEnd;
        };
    };

}

#endif
