#pragma once

#include <Core/Platform.h>
//#include <Rendering/Shader.h>
//#include <GL/Buffers/ArrayBuffer.h>
//#include <GL/Color.h>
#include <Tools/Console.h>
#include <Tools/Time.h>
#include <Core/Ray.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#define PRINT_ASSERT_SUCCESS false

namespace RGTek
{
    using namespace Math;

	class Debugger
	{
	public:
		Debugger() = delete;
		~Debugger() {}

		static void __CheckOpenGLError(const char* file, int line);
		static void __Fatal(const char* file, int line, const char* format, ...);
		static void __Error(const char* file, int line, const char* format, ...);
		static void __Warning(const char* file, int line, const char* format, ...);
		static void __Info(const char* file, int line, const char* format, ...);
		static void __Debug(const char* file, int line, const char* format, ...);

		static void __AssertNotNull(const char* file, int line, AnyPtr value, const char* format, ...);
		static void __AssertNull(const char* file, int line, AnyPtr value, const char* format, ...);
		static void __AssertTrue(const char* file, int line, bool value, const char* format, ...);
		static void __AssertFalse(const char* file, int line, bool value, const char* format, ...);

#define CheckOpenGLError()  __CheckOpenGLError(__FILE__, __LINE__)
#define Fatal(fmt, ...)		__Fatal(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define Error(fmt, ...)		__Error(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define Warning(fmt, ...)	__Warning(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define Info(fmt, ...)		__Info(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define Debug(fmt, ...)		__Debug(__FILE__, __LINE__, fmt, ##__VA_ARGS__)

#define AssertNotNull(value, ...)	__AssertNotNull(__FILE__, __LINE__, value, ##__VA_ARGS__)
#define AssertNull(value, ...)		__AssertNull(__FILE__, __LINE__, value, ##__VA_ARGS__)
#define AssertTrue(value, ...)		__AssertTrue(__FILE__, __LINE__, value, ##__VA_ARGS__)
#define AssertFalse(value, ...)		__AssertFalse(__FILE__, __LINE__, value, ##__VA_ARGS__)

#ifdef NOTHING
		static void DrawRay(const Ray& ray, const Color& color = Color::green, const Time::Duration& duration = Time::Duration(0));
        
    private:
        friend class RenderingSystem;

		class RayDrawerInfo
		{
		public:
			RayDrawerInfo(const Ray& ray, const Color& color, const Time::Duration& duration)
				: ray(ray)
				, color(color)
				, start(Time::Now())
				, duration(duration)
			{
				// Do nothing
			}
			~RayDrawerInfo() {}

			Ray ray;
			Color color;
			Time::TimePoint start;
			Time::Duration duration;
		};

		static void InitDrawAssets();
		static void FlushDrawRays();

		static std::vector<RayDrawerInfo> sRayDrawerInfoSet;
#endif
		static char sBuffer[16384];
	};

}