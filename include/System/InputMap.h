//
// Created by Bruno Gouveia on 1/19/16.
//

#pragma once

#include <Core/Platform.h>
#include <System/Input.h>
#include <SDL2/SDL.h>
#undef main

namespace RGTek
{

    class InputMap
    {
    public:
        static KeyCode GetKeyFromSDL(SDL_Scancode scanCode);

        static void InjectUTF8Text(const char* utf8str);

        /** This method actually returns a CEGUI::MouseButton value */
        static unsigned int SDLtoCEGUIMouseButton(const Uint8& button);
        /** This method actually returns a CEGUI::Key::Scan value */
        static unsigned int toCEGUIKey(SDL_Scancode key);
    };

}
