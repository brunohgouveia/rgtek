#ifndef INPUT_H__
#define INPUT_H__

#include <Core/Platform.h>
#include <Math/Vector2.h>

namespace RGTek
{
    using namespace Math;

    class KeyCode
    {
    public:
        KeyCode(unsigned int key) :
            mValue(::toupper(static_cast<int>(key)))
        {}

        static KeyCode Esc;
        static KeyCode UpArrow;
        static KeyCode DownArrow;
        static KeyCode LeftArrow;
        static KeyCode RightArrow;
        static KeyCode PageUp;
        static KeyCode PageDown;
        static KeyCode Return;
        static KeyCode Alt;

        operator unsigned int() { return mValue; }

    private:
        unsigned int mValue;

    };

    class Input
    {
    public:
        enum MouseButton
        {
            LeftButton,
            MiddleButton,
            RightButton,
            NumberOfButtons
        };

        static void Init();
		static void Finish();
        static bool GetKey(KeyCode key) { return sInputActive && sKeyMap[key]; }
        static void SetKey(KeyCode key, bool state = true) { if (key < 264) sKeyMap[key] = state; }
        static void ResetMap();
        static void ResetDelta(int button);
        static void EndMouseInjection();

        static bool GetMouseButtonDown(int button);
        static bool GetMouseButton(int button);
        static bool GetMouseButtonUp(int button);
        static bool GetMouseDragging(int button);
        static Vector2 GetMouseDelta(int button);
        static Vector2 mousePosition;
        static float   mouseWheel;

    private:
        static void InjectMouseDown(int button) { sMouseButtons[button] = true; if (!sMouseButtonsPreviousFrame[button]) sMouseButtonsClickedPosition[button] = mousePosition; }
        static void InjectMouseUp(int button) { sMouseButtons[button] = false; }

        friend class Window;
        friend class GameWindow;
        friend class EditorWindow;

        static bool    sInputActive;
        static Vector2 sMouseButtonsClickedPosition[5];
        static bool    sMouseButtonsPreviousFrame[5];
        static bool    sMouseButtons[5];
        static bool    sKeyMap[264];
    };

}

#endif
