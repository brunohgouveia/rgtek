//
// Created by Bruno Gouveia on 1/14/16.
//

#pragma once

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#ifdef WIN32
#include <io.h>
#define ftruncate _chsize
#else
#include <sys/types.h>
#include <unistd.h>
#define O_BINARY 0
#define _access access
#define _open open
#define _lseek lseek
#define _read read
#define _write write
#define _close close
long _filelength(int);
#endif

#include <Core/Platform.h>

typedef signed long int32;
typedef unsigned long uint32;

namespace RGTek
{ // begin namespace System


//////////////////////////////////////////////////////////
//
// File: file class
// ====
    class File
    {
    public:
        enum
        {
            fileNull	= -1
        };

        // Open mode
        enum Mode
        {
            ReadOnly = O_RDONLY,
            ReadWrite = O_RDWR,
            WriteOnly = O_WRONLY,
            Create = O_CREAT | O_TRUNC,
            CreateExcl = O_CREAT | O_EXCL,
            Append = O_APPEND,
            Binary = O_BINARY
        };

        // Permission flags
        enum Permission
        {
            PermRead = S_IREAD, PermWrite = S_IWRITE,
            PermRdWr = S_IREAD | S_IWRITE
        };

        enum SeekDir
        {
            Beg = SEEK_SET,
            Cur = SEEK_CUR,
            End = SEEK_END
        };

        // Constructors
        File();
        File(const String&, uint = ReadWrite, uint = PermRdWr);

        // Destructor
        ~File();

        bool open(const String&, uint = ReadWrite, uint = PermRdWr);
        bool close();

        const String& getName() const;

        long position() const;
        long length() const;
        void setLength(long);
        long seek(long, int = Beg);
        long seekToBegin();
        long seekToEnd();
        bool isOpen() const;

        int read(void*, int);

        int read(uint32&);
        int read(int32&);
//        int read(uint16&);
        int read(int&);
        int read(char&);
        int read(float&);
        int read(double&);
        int read(char*&);
        int read(wchar_t*&);
        int read(bool&);

        int write(const void*, int);

        int write(uint32);
        int write(int32);
//        int write(uint16);
        int write(int);
        int Write(char);
        int write(float);
        int write(double);
        int Write(const char*);
//        int write(const wchar_t*);
        int write(bool);
        int Write(const String&);

        static bool Exists(const String&);
        static bool Exists(const char*);
        static void remove(const char*);
        static void rename(const char*, const char*);

    private:
        String name;
        int handle;

        int readArray(void*&, int);
        int writeArray(const void*, int, int);

    }; // File


//////////////////////////////////////////////////////////
//
// File inline implementation
// ====
    inline
    File::File()
        : handle(fileNull)
    {
        // do nothing
    }

    inline
    File::File(const String& name, uint access, uint permission)
        : handle(fileNull)
    {
        open(name, access, permission);
    }

    inline
    File::~File()
    {
        close();
    }

    inline const String& File::getName() const
    {
        return name;
    }

    inline long
    File::position() const
    {
        return ::_lseek(handle, 0, Cur);
    }

    inline bool
    File::isOpen() const
    {
        return handle > fileNull;
    }

    inline long
    File::length() const
    {
        return ::_filelength(handle);
    }

    inline void
    File::setLength(long length)
    {
        ::ftruncate(handle, length);
    }

    inline long
    File::seek(long offset, int origin)
    {
        return ::_lseek(handle, offset, origin);
    }

    inline long
    File::seekToBegin()
    {
        return ::_lseek(handle, 0, Beg);
    }

    inline long
    File::seekToEnd()
    {
        return ::_lseek(handle, 0, End);
    }

    inline int
    File::read(void* buffer, int size)
    {
        return ::_read(handle, buffer, size);
    }

    inline int
    File::read(uint32& value)
    {
        return read(&value, sizeof(uint32));
    }

//    inline int
//    File::read(uint16& value)
//    {
//        return read(&value, sizeof(uint16));
//    }

    inline int
    File::read(int32& value)
    {
        return read(&value, sizeof(int32));
    }

    inline int
    File::read(int& value)
    {
        return read(&value, sizeof(int));
    }

    inline int
    File::read(char& value)
    {
        return read(&value, sizeof(char));
    }

    inline int
    File::read(float& value)
    {
        return read(&value, sizeof(float));
    }

    inline int
    File::read(double& value)
    {
        return read(&value, sizeof(double));
    }

    inline int
    File::read(char*& value)
    {
        return readArray((void*&)value, sizeof(*value));
    }

    inline int
    File::read(wchar_t*& value)
    {
        return readArray((void*&)value, sizeof(*value));
    }

    inline int
    File::read(bool& value)
    {
        return read(&value, sizeof(bool));
    }

    inline int
    File::write(const void* buffer, int size)
    {
        return ::_write(handle, buffer, size);
    }

    inline int
    File::write(uint32 value)
    {
        return write(&value, sizeof(uint32));
    }

    inline int
    File::write(int32 value)
    {
        return write(&value, sizeof(int32));
    }

//    inline int
//    File::write(uint16 value)
//    {
//        return write(&value, sizeof(uint16));
//    }

    inline int
    File::write(int value)
    {
        return write(&value, sizeof(int));
    }

    inline int
    File::Write(char value)
    {
        return write(&value, sizeof(char));
    }

    inline int
    File::write(float value)
    {
        return write(&value, sizeof(float));
    }

    inline int
    File::write(double value)
    {
        return write(&value, sizeof(double));
    }

    inline int
    File::Write(const char* value)
    {
        int length = value != 0 ? (int)strlen(value) : 0;
        return write(value, length);
    }

//    inline int
//    File::write(const wchar_t* value)
//    {
//        int length = value != 0 ? (int)wcslen(value) + 1 : 0;
//        return writeArray(value, length, sizeof(*value));
//    }

    inline int
    File::write(bool value)
    {
        return write(&value, sizeof(bool));
    }

    inline int
    File::Write(const String& value)
    {
        return Write(value.c_str());
    }

    inline bool File::Exists(const String& fileName)
    {
        return Exists(fileName.c_str());
    }

    inline bool File::Exists(const char* fileName)
    {
        return ::_access(fileName, 0) == 0;
    }

    inline void
    File::remove(const char* fileName)
    {
        ::remove(fileName);
    }

    inline void
    File::rename(const char* fileName, const char* newFileName)
    {
        ::rename(fileName, newFileName);
    }

} // end namespace System
