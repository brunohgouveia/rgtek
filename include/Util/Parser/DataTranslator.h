//
// Created by Bruno Gouveia on 3/11/16.
//

#pragma once

#include <Util/Parser/Databin.h>
#include <Math/Vector3.h>
#include <Math/Quaternion.h>
#include <GL/Color.h>
#include <functional>

namespace RGTek
{
    using namespace Math;

    class DataTranslator
    {
    public:
        DataTranslator(){}
        virtual ~DataTranslator(){}

        virtual void TranslateFrom(DataBin*) = 0;
        virtual void TranslateTo(DataBin*) = 0;

        static Color TranslateColorFrom(DataBin*);
        static Vector3 TranslateVector3From(DataBin*);
        static Quaternion TranslateQuaternionFrom(DataBin*);

        static String TranslateStringFrom(DataBin* bin);
        static float TranslateFloatFrom(DataBin* bin);
    };

    template <typename T, typename U>
    class DataTranslatorProperty
    {
    public:
        typedef const U& (T::*Getter)(void) const;
        typedef void (T::*Setter)(const U&);

        DataTranslatorProperty(T* object, const Getter& getter, const Setter& setter)
                : mObject(object)
                , mGetter(getter)
                , mSetter(setter)
        {
            // Do nothing
        }

        const U& Get() const { return (mObject->*mGetter)(); }
        void Set(const U& value) { (mObject->*mSetter)(value); }
    private:
        T*     mObject;
        Getter mGetter;
        Setter mSetter;
    };

    template <typename T>
    class CustomObjectDataTranslator: public DataTranslator
    {
    public:

        CustomObjectDataTranslator(const String& objectType, T* object)
            : mObjectType(objectType)
            , mObject(object)
        {
            // Do Nothing
        }

        void Add(const String& name, DataTranslatorProperty<T, int> attribute) { mIntProperties.push_back(std::make_pair(name, attribute)); }
        void Add(const String& name, DataTranslatorProperty<T, float> attribute) { mFloatProperties.push_back(std::make_pair(name, attribute)); }
        void Add(const String& name, DataTranslatorProperty<T, Color> attribute) { mColorProperties.push_back(std::make_pair(name, attribute)); }
        void Add(const String& name, DataTranslatorProperty<T, Vector3> attribute) { mVec3Properties.push_back(std::make_pair(name, attribute)); }
        void Add(const String& name, DataTranslatorProperty<T, Quaternion> attribute) { mQuaternionProperties.push_back(std::make_pair(name, attribute)); }
        void Add(const String& name, DataTranslatorProperty<T, String> attribute) { mStringProperties.push_back(std::make_pair(name, attribute)); }

        void TranslateFrom(DataBin* dataBin)
        {
            // Return if dataBin is null
            if (!dataBin) return;

            for (auto& colorPropertyPair : mColorProperties)
            {
                DataBin* propertyDataBin = dataBin->GetChildByName(colorPropertyPair.first);
                if (propertyDataBin)
                {
                    colorPropertyPair.second.Set(DataTranslator::TranslateColorFrom(propertyDataBin));
                }
            }

            for (auto& vec3PropertyPair : mVec3Properties)
            {
                DataBin* propertyDataBin = dataBin->GetChildByName(vec3PropertyPair.first);
                if (propertyDataBin)
                {
                    vec3PropertyPair.second.Set(DataTranslator::TranslateVector3From(propertyDataBin));
                }
            }

            for (auto& quaternionPropertyPair : mQuaternionProperties)
            {
                DataBin* propertyDataBin = dataBin->GetChildByName(quaternionPropertyPair.first);
                if (propertyDataBin)
                {
                    quaternionPropertyPair.second.Set(DataTranslator::TranslateQuaternionFrom(propertyDataBin));
                }
            }

            for (auto& stringPropertyPair : mStringProperties)
            {
                DataBin* propertyDataBin = dataBin->GetChildByName(stringPropertyPair.first);
                if (propertyDataBin)
                {
                    stringPropertyPair.second.Set(DataTranslator::TranslateStringFrom(propertyDataBin));
                }
            }

            for (auto& floatPropertyPair : mFloatProperties)
            {
                DataBin* propertyDataBin = dataBin->GetChildByName(floatPropertyPair.first);
                if (propertyDataBin)
                {
                    floatPropertyPair.second.Set(DataTranslator::TranslateFloatFrom(propertyDataBin));
                }
            }
        }

        void TranslateTo(DataBin* dataBin)
        {
            // Return if dataBin is null
            if (!dataBin) return;

            for (auto& colorPropertyPair : mColorProperties)
            {
                DataBin* propertyDataBin = new DataBin(colorPropertyPair.first, dataBin);

                Color color = colorPropertyPair.second.Get();
                propertyDataBin->AddChild(new DataBin("R", color.r));
                propertyDataBin->AddChild(new DataBin("G", color.g));
                propertyDataBin->AddChild(new DataBin("B", color.b));
                propertyDataBin->AddChild(new DataBin("A", color.a));
            }

            for (auto& vec3PropertyPair : mVec3Properties)
            {
                DataBin* propertyDataBin = new DataBin(vec3PropertyPair.first, dataBin);

                Vector3 vec = vec3PropertyPair.second.Get();
                propertyDataBin->AddChild(new DataBin("X", vec.x));
                propertyDataBin->AddChild(new DataBin("Y", vec.y));
                propertyDataBin->AddChild(new DataBin("Z", vec.z));
            }

            for (auto& quaternionPropertyPair : mQuaternionProperties)
            {
                DataBin* propertyDataBin = new DataBin(quaternionPropertyPair.first, dataBin);

                Quaternion quat = quaternionPropertyPair.second.Get();
                propertyDataBin->AddChild(new DataBin("X", quat.x));
                propertyDataBin->AddChild(new DataBin("Y", quat.y));
                propertyDataBin->AddChild(new DataBin("Z", quat.z));
                propertyDataBin->AddChild(new DataBin("W", quat.w));
            }

            for (auto& stringPropertyPair : mStringProperties)
            {
                dataBin->AddChild(new DataBin(stringPropertyPair.first, stringPropertyPair.second.Get()));
            }

            for (auto& floatPropertyPair : mFloatProperties)
            {
                dataBin->AddChild(new DataBin(floatPropertyPair.first, floatPropertyPair.second.Get()));
            }
        }

    private:
        String mObjectType;
        T*     mObject;

        std::vector<std::pair<String, DataTranslatorProperty<T, Color> > >   mColorProperties;
        std::vector<std::pair<String, DataTranslatorProperty<T, Vector3> > > mVec3Properties;
        std::vector<std::pair<String, DataTranslatorProperty<T, float> > >   mFloatProperties;
        std::vector<std::pair<String, DataTranslatorProperty<T, int> > >     mIntProperties;
        std::vector<std::pair<String, DataTranslatorProperty<T, Quaternion> > > mQuaternionProperties;
        std::vector<std::pair<String, DataTranslatorProperty<T, String> > > mStringProperties;
    };

}
