//
// Created by Bruno Gouveia on 1/14/16.
//

#pragma once

#include <Core/Platform.h>

namespace RGTek
{

    class DataBin
    {
    public:
        enum Type
        {
            Object,
            Array,
            GLSL,
            Value
        };

        enum DataType
        {
            None,
            Int,
            Float,
            Literal
        };

        struct Data
        {
            Data() {}
            ~Data() {}
            union
            {
                int integerData;
                float floatData;
            };
            String stringData;
        };

        DataBin(const String& name, DataBin* parent, Type type = Type::Object);
        DataBin(const String& name, DataType dataType = DataType::None);

        DataBin(const String& name, int value, DataBin* parent = nullptr);
        DataBin(const String& name, float value, DataBin* parent = nullptr);
        DataBin(const String& name, const String& value, DataBin* parent = nullptr);

        ~DataBin();

        void AddChild(DataBin* dataBin);

        DataBin* GetChildByName(const String& name) const;

		void DeleteChildren();

        void Print();

        inline void SetName(const String& name)   { mName = name;   }
        inline void SetLabel(const String& label) { mLabel = label; }

        inline void SetType(Type binType)         { mType = binType;      }
        inline void SetDataType(DataType dataType){ mDataType = dataType; }

        inline void SetInt(int value)              { mData.integerData = value; }
        inline void SetFloat(float value)          { mData.floatData = value;   }
        inline void SetString(const String& value) { mData.stringData = value;  }

        inline const String& GetName() const  { return mName;  }
        inline const String& GetLabel() const { return mLabel; }

        inline Type GetType() const         { return mType;     }
        inline DataType GetDataType() const { return mDataType; }
        inline const Data& GetData() const  { return mData;     }

        inline bool HasLabel() const { return !mLabel.empty(); }

        inline const Vector<DataBin*> GetChildren() const { return mChildren; }

        String GetDataString() const;

    private:

        String   mName;
        String   mLabel;

        Type     mType;
        DataType mDataType;
        Data     mData;
        Vector<DataBin*> mChildren;
    };

}
