//
// Created by Bruno Gouveia on 1/14/16.
//

#pragma once

namespace RGTek
{

    class Token
    {
    public:
        enum Type
        {
            None,
            Integer,
            Float,
            Name,
            Literal,
            Assignment,
            GLSL,
            GLSLBody,
            BeginArray,
            EndArray,
            BeginObject,
            EndObject,
            EndOfFile
        };

        Token(Type type, const String& lexeme) : mType(type), mLexeme(lexeme) {}


        Type   mType;
        String mLexeme;
    };

}
