//
// Created by Bruno Gouveia on 1/14/16.
//

#pragma once

#include <stdio.h>

namespace RGTek
{

    class FileBuffer
    {
    public:
        FileBuffer(File& inputFile)
        {
            int size = inputFile.length();
            char* source = new char[size + 1];

            size = inputFile.read(source, size);
            source[size] = 0;
            SetSource(source);
        }
        ~FileBuffer()
        {
            delete []start;
        }

        void StartLexeme() { mark = current; }
        String GetLexeme() const { return String(mark, static_cast<int>(current - mark)); }

        char GetCurrent() const { return *current; }
        char Lookahead() const { return *current; }
        char Advance() { return static_cast<char>(*current ? *++current : '\0'); }

        bool LexemeEndsWith(const String& word) const
        {
            int wordSize = static_cast<int>(word.size());

            if (static_cast<int>(current - mark) < wordSize) return false;

            int currentIndex = wordSize;
            while (--currentIndex >= 0)
            {
                if (*(current - (wordSize - (currentIndex))) != word[currentIndex]) return false;
            }

            return true;
        }

        char operator *() const { return Lookahead(); }
        char operator ++() { return Advance(); }
        char operator ++(int) // postfix
        {
            char temp = Lookahead();

            Advance();
            return temp;
        }

    protected:
        void SetSource(char* source)
        {
            start = current = mark = source;
        }

        char* start;
        char* current;
        char* mark;
    };

}