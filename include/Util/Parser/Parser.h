//
// Created by Bruno Gouveia on 12/26/15.
//

#pragma once

#include <Core/Platform.h>
#include <System/File.h>
#include <Util/Parser/Token.h>
#include <Util/Parser/DataBin.h>
#include <Util/Parser/FileBuffer.h>

namespace RGTek
{

    class Parser
    {
    public:
        Parser();
		~Parser();

        DataBin* Parse(const String& fileName);
        void SaveToFile(const String& fileName, DataBin* dataBin);

    private:
        DataBin* ParseObject();
        Token GetNextToken();
        Token GetNextTokenFromBuffer();

        void SaveDataBin(DataBin* dataBin, File& file, int identation = 0);

        Token LookAhead();
        FileBuffer& GetBuffer() { return *mFileBuffer; }

        void SetBuffer(FileBuffer* buffer) { if (mFileBuffer) delete mFileBuffer; mFileBuffer = buffer; }

        FileBuffer* mFileBuffer;
        Token*      mProcessedTokens;
    };

}