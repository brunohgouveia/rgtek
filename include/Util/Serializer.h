//
// Created by Bruno Gouveia on 11/19/15.
//

#ifndef RGTEK_SERIALIZER_H
#define RGTEK_SERIALIZER_H

#include <queue>
#include <type_traits>

namespace RGTek
{
    template <typename T>
    class IsSerializable
    {
    private:
        using one = char[1];
        using two  = char[2];

        template <typename U> static one& test(decltype(&U::Serialize));
        template <typename U> static two& test(...);

    public:
        enum { value = (sizeof(test<T>(0)) == sizeof(one))};

    };

    class Serializer
    {
    public:
        enum Mode
        {
            SavingMode,
            LoadingMode
        };

        Serializer() {}
        ~Serializer() {}

        template <typename T>
        void Serialize(typename std::enable_if<IsSerializable<T>::value, T>::type& object)
        {
            object->Serialize(this);
        };

        template <typename T>
        void Serialize(typename std::enable_if<!IsSerializable<T>::value, T>::type& object)
        {
            union Data
            {
                T object;
                unsigned char bytes[sizeof(T)];
            } data;

            if (mMode == SavingMode)
            {
                data.object = object;
                for (size_t i = 0; i < sizeof(T); ++i)
                {
                    mBuffer.push(data.bytes[i]);
                }
            }
            else
            {
                for (size_t i = 0; i < sizeof(T); ++i)
                {
                    data.bytes[i] = mBuffer.front();
                    mBuffer.pop();
                }
                object = data.object;
            }
        };

        template <typename T>
        void Serialize(T*& object);

        inline void SetMode(Mode mode) { mMode = mode; }
        inline void ClearBuffer()      { std::queue<unsigned char>().swap(mBuffer); }

    protected:
        Mode mMode;
        std::queue<unsigned char> mBuffer;
    };


    template <typename T>
    void Serializer::Serialize(T *&object)
    {
        union Data
        {
            T* pointer;
            unsigned char bytes[sizeof(T*)];
        } data;

        if (mMode == SavingMode)
        {
            data.pointer = object;
            for (size_t i = 0; i < sizeof(T*); ++i)
            {
                mBuffer.push(data.bytes[i]);
            }
        }
        else
        {
            for (size_t i = 0; i < sizeof(T*); ++i)
            {
                data.bytes[i] = mBuffer.front();
                mBuffer.pop();
            }
            object = data.pointer;
        }
    }

}

#endif //RGTEK_SERIALIZER_H
