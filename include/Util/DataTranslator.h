//
// Created by Bruno Gouveia on 1/13/16.
//

#pragma once

#include <Util/Parser/DataBin.h>

namespace RGTek
{

    template <typename T>
    class DataTranslator
    {
        // single members
        typedef bool T::*BoolMember;
        typedef int T::*IntMember;
        typedef float T::*FloatMember;
        typedef String T::*StringMember;

        // array members
        typedef std::vector<bool> T::*BoolArrayMember;
        typedef std::vector<int> T::*IntArrayMember;
        typedef std::vector<float> T::*FloatArrayMember;
        typedef std::vector<std::string> T::*StringArrayMember;

    public:

        DataTranslator();
        ~DataTranslator();

        DataTranslator& Add(const String& name, BoolMember member);
        DataTranslator& Add(const String& name, IntMember member);
        DataTranslator& Add(const String& name, FloatMember member);
        DataTranslator& Add(const String& name, StringMember member);

        void TranslateObject(DataBin* dataBin, T& object);

    };

    template <typename T>
    DataTranslator<T>::DataTranslator()
    {

    }

    template <typename T>
    DataTranslator<T>::~DataTranslator()
    {

    }

    template <typename T>
    DataTranslator<T>& DataTranslator<T>::Add(const String& name, BoolMember member)
    {
        return *this;
    }

    template <typename T>
    DataTranslator<T>& DataTranslator<T>::Add(const String& name, IntMember member)
    {
        return *this;
    }

    template <typename T>
    DataTranslator<T>& DataTranslator<T>::Add(const String& name, FloatMember member)
    {
        return *this;
    }

    template <typename T>
    DataTranslator<T>& DataTranslator<T>::Add(const String& name, StringMember member)
    {
        return *this;
    }

    template <typename T>
    void DataTranslator<T>::TranslateObject(DataBin* dataBin, T& object)
    {

    }

}