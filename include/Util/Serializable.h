//
// Created by Bruno Gouveia on 11/18/15.
//

#ifndef RGTEK_SERIALIZABLE_H
#define RGTEK_SERIALIZABLE_H

namespace RGTek
{

    class Serializer;

    class Serializable
    {
    public:
        virtual ~Serializable() {}

        virtual void Serialize(Serializer* serializer) = 0;
    };

}

#endif //RGTEK_SERIALIZABLE_H
