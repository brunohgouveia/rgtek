//
// Created by Bruno Gouveia on -/-/15.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Transform.h>

namespace RGTek
{

    class Script;
    FWD_CLASS_EXTEND_PTR_TYPES(Component);
    FWD_CLASS_EXTEND_PTR_TYPES(Collision);
    FWD_CLASS_EXTEND_PTR_TYPES(Entity);

    class Entity : public Object
    {
    public:
		Entity(const String& name, Transform* transform = new Transform());
        virtual ~Entity();

        void AddChild(EntityPtr child);
		void AddComponent(Component* component);
		void DeleteComponent(Component* component);
        void RegisterComponents();

        void OnCollisionEnter(const CollisionPtr& collision);
        void OnCollisionStay(const CollisionPtr& collision);
        void OnCollisionExit(const CollisionPtr& collision);

        void RenderGizmos();

        template <class T> T* GetGameComponent();
        template <class T = Component> std::vector<T*> GetGameComponents();

        Vector<Component*>& GetAllComponents() { return mComponents; }

        /** Getters */
        inline const String& GetName() const { return mName; }
        inline TransformWeakPtr GetTransform() const { return mTransform; }
        inline EntityPtrVector& GetChildren() { return mChildren; }
        inline bool IsStatic() const { return mStatic; }

        /** Setters */
        inline void SetTransform(Transform transform) const { *mTransform = transform; }
        inline void SetStatic(bool isStatic) { mStatic = isStatic; }

    private:
        InstanceID         mInstanceID;
        String             mName;
        TransformPtr       mTransform;
        bool               mStatic;

        EntityPtrVector    mChildren;
		Vector<Component*> mComponents;

        static InstanceID             sCurrentInstanceID;
    };

    template <class T>
    T*Entity::GetGameComponent()
    {
        for (Component* component : mComponents)
        {
            T* weakPointer = dynamic_cast<T*>(component);
            if (weakPointer)
            {
                return weakPointer;
            }
        }
        return nullptr;
    }

    template <class T>
    std::vector<T*> Entity::GetGameComponents()
    {
        std::vector<T*> components;
        for (Component* component : mComponents)
        {
            T* weakPointer = dynamic_cast<T*>(component);
            if (weakPointer)
            {
                components.push_back(weakPointer);
            }
        }
        return components;
    }

}
