//
// Created by Bruno Gouveia on 12/1/15.
//

#ifndef RGTEK_TYPETRAITS_H
#define RGTEK_TYPETRAITS_H

namespace RGTek
{
    typedef unsigned int TypeId;

    class TypeIDGenerator
    {
    private:
        static TypeId sCounter;

    public:
        template<typename T>
        static TypeId GenerateID()
        {
			static TypeId id = sCounter++;
            return id;
        }
    };

    template<typename T>
    struct ComponentToTypeId
    {
        static TypeId ID;
    };

    template<typename T>
    TypeId ComponentToTypeId<T>::ID = TypeIDGenerator::GenerateID<T>();
}

#endif //RGTEK_TYPETRAITS_H
