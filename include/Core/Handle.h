//
// Created by Bruno Gouveia on 11/23/15.
//

#ifndef RGTEK_HANDLE_H
#define RGTEK_HANDLE_H

#include <utility>
#include <cstdint>

namespace RGTek
{
    template <typename T>
	class Handle
    {
    public:
        uint32_t mIndex     : 22;
        uint32_t mGeneration: 10;

        bool operator==(const Handle<T>& other) const
        {
            return (mIndex == other.mIndex) && (mGeneration == other.mGeneration);
        }

        bool operator!=(const Handle<T>& other) const
        {
            return !operator==(other);
        }
    };

    template <typename T, unsigned int SIZE = 16384>
	class HandleManager
    {
    public:
        HandleManager()
                : mCurrentSize(0)
        {
            for (unsigned int i = 0; i < SIZE; i++)
            {
                data[i].mObject = nullptr;
                data[i].mGeneration = 0;
            }
        }

        ~HandleManager()
        {
            for (size_t i = 0; i < SIZE; i++)
            {
                if (data[i].mObject) data[i].mObject->~T();
            }
        }

        template <typename ...Args>
        Handle<T> CreateHandle(Args... args)
        {
            uint32_t nextIndex = mCurrentSize++;

            data[nextIndex].mObject = new (reinterpret_cast<T*>(data[nextIndex].mBuffer)) T(std::forward<Args>(args)...);

            return Handle<T>{nextIndex, data[nextIndex].mGeneration};
        }

        template <typename ...Args>
        void ReplaceHandle(Handle<T> handle, Args... args)
        {
            if (handle.mGeneration != data[handle.mIndex].mGeneration) return;
            if (data[handle.mIndex].mObject == nullptr) return;

            // Call destructor
            data[handle.mIndex].mObject->~T();

            data[handle.mIndex].mObject = new (reinterpret_cast<T*>(data[handle.mIndex].mBuffer)) T(std::forward<Args>(args)...);
        }

        void DestroyHandle(Handle<T> handle)
        {
            if (handle.mGeneration != data[handle.mIndex].mGeneration) return;
            if (data[handle.mIndex].mObject == nullptr) return;

            // Call destructor
            data[handle.mIndex].mObject->~T();
            data[handle.mIndex].mObject = nullptr;

            data[handle.mIndex].mGeneration++;
        }

        inline T* GetObject(Handle<T> handle) const
        {
             return (handle.mGeneration == data[handle.mIndex].mGeneration) ? data[handle.mIndex].mObject : nullptr;
        }

    protected:
        struct Data
        {
            T* mObject;
            unsigned char mBuffer[sizeof(T)];
            uint32_t mGeneration;
        } data[SIZE];

        unsigned int mCurrentSize;
    };
}

#endif //RGTEK_HANDLE_H
