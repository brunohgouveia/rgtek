#ifndef SCENE_H__
#define SCENE_H__

#include <Core/Platform.h>
#include <Core/Entity.h>
#include <Rendering/RenderingSystem.h>
#include <Physics/PhysicsSystem.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(Scene);

    class Scene: public Object
    {
    public:
        Scene()
            : mSceneFile("../DefaultScene.scene")
            , mRoot(new Entity("RootObject"))
        {
        }
        virtual ~Scene();

        EntityPtr GetRoot() { return mRoot; };
        void AddEntity(EntityPtr entity);
        void DeleteEntity(Entity* entity);

        Entity* FindEntityByName(const String& entityName);

        void LoadFromFile(const String& fileName);
        void SaveToFile(const String& fileName);

        void Save() { SaveToFile(mSceneFile);   }
        void Load() { LoadFromFile(mSceneFile); }

        void ClearScene();

        void AddSystem(System* system) { mSystems[system->GetSystemName()] = system; }

    protected:
        System* GetSceneByName(const String& systemName);

        String        mSceneFile;
        EntityPtr     mRoot;
        std::map<String, System*> mSystems;

    private:
        Scene(Scene& scene) = delete;
        void operator=(Scene& scene) = delete;
    };

}

#endif // SCENE_H__
