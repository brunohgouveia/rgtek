#ifndef GAMECOMPONENT_H__
#define GAMECOMPONENT_H__

#include <Core/Platform.h>
#include <Core/Entity.h>
#include <Core/Transform.h>
#include <Core/Reflection/Type.h>
#include <Core/Reflection/TypeData.h>
#include <Core/Reflection/TypeInfo.h>
#include <Core/Reflection/TypeDatabase.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(Collision);
    FWD_CLASS_EXTEND_PTR_TYPES(System);
    FWD_CLASS_EXTEND_PTR_TYPES(GameComponent);
    class DataTranslator;
    
    class Component: public Object
    {
    public:
		static const String COMPONENT_NAME;
        
		RGTEK_REGISTER_TYPE(Component)

        Component();
        virtual ~Component();

        virtual void Update();

        virtual const String& GetComponentName() const { return COMPONENT_NAME; }

        virtual void OnCollisionEnter(const CollisionPtr& collision);
        virtual void OnCollisionStay(const CollisionPtr& collision);
        virtual void OnCollisionExit(const CollisionPtr& collision);

        virtual void RenderGizmos();

        virtual DataTranslator* GetDataTranslator() = 0;

        /**
         * Called only when the entity is attached to the scene (e.g. when it becomes child of an other entity).
         */
        virtual void RegisterToSystem();

        virtual void SetSystem(System* system) { mSystem = system; }
        virtual void SetEntity(Entity* entity) { mEntity = entity; }

        virtual System* GetSystem() const { return mSystem; }
        virtual Entity* GetEntity() const { return mEntity; }

        static void Destroy(Component* component);

    protected:
        System* mSystem;
        Entity* mEntity;

        Component(const Component& other) = delete;
        Component& operator=(const Component& other) = delete;
	};

#define RCOMPONENT(component) \
    static const String COMPONENT_NAME; \
    virtual const String& GetComponentName() const override { return COMPONENT_NAME; }

#define RCOMPONENT_IMPLEMENTATION(component) \
	const String component::COMPONENT_NAME = #component;

}

#endif // GAMECOMPONENT_H__
