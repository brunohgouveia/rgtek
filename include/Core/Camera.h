//
// Created by Bruno Gouveia on -/-/15.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Transform.h>
#include <Core/Viewport.h>
#include <Core/Ray.h>
#include <Math/Vector2.h>
#include <Math/Vector3.h>
#include <Math/Matrix4.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(Camera);
    FWD_CLASS_EXTEND_PTR_TYPES(Shader);

    class Camera: public Object
    {
    public:
        Camera(TransformWeakPtr transform = new Transform());
        ~Camera();

        void UpdateUniformsToShader(ShaderWeakPtr shader);

        Vector3 ScreenPointToNDC(const Vector2& position);
        Vector3 ScreenPointToWorld(const Vector2& position);

        Ray ScreenPointToRay(const Vector2& position);
        Ray ScreenPointToRay(const Vector3& position);

        Vector3 WorldToNDC(const Vector3& position);
        Vector3 NDCToWorld(const Vector3& position);

        Matrix4 GetView();
        Matrix4 GetProjection();
        Matrix4 GetViewProjection();
        inline Viewport GetViewport() { return mViewport; }
        inline TransformPtr GetTransform() { return mTransform; }

        inline float GetFieldOfView() const { return mFieldOfView; }
        inline float GetNearClip()    const { return mNear;        }
        inline float GetFarClip()     const { return mFar;         }

        void SetViewport(const Viewport& viewport);
        inline void SetProjection(const Matrix4& projection) { mProjection = projection; }
        inline void SetTransform(TransformPtr transform) { mTransform = transform; }

        static CameraPtr GetMainCamera() { return sMainCamera; }
        static void SetMainCamera(CameraPtr camera) { sMainCamera = camera; }

        void SetFieldOfView(float fov);
		void SetNearClip(float nearClip);
        void SetFarClip(float farClip);

        /** Const references getter/setter */
        inline const float& GetFieldOfViewRef() const { return mFieldOfView; }
        inline const float& GetNearClipRef()    const { return mNear;        }
        inline const float& GetFarClipRef()     const { return mFar;         }

        inline void SetFieldOfViewRef(const float& fov)  { SetFieldOfView(fov); }
		inline void SetNearClipRef(const float& nearClip)    { SetNearClip(nearClip); }
		inline void SetFarClipRef(const float& farClip)      { SetFarClip(farClip); }

    private:
        void ComputeProjectionMatrix();

        float        mFieldOfView;
        float        mNear;
        float        mFar;
        Viewport     mViewport;
        Matrix4      mProjection;
        TransformPtr mTransform;

        static CameraPtr sMainCamera;
    };

}