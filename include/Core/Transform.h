#ifndef TRANSFORM_H__
#define TRANSFORM_H__

#include <Core/Platform.h>
#include <Math/Matrix4.h>
#include <Math/Vector3.h>
#include <Math/Quaternion.h>
#include <Util/Parser/DataTranslator.h>
#include <Core/Types/Delegate.h>
#include <Core/Reflection/Type.h>
#include <Core/Reflection/TypeData.h>
#include <Core/Reflection/TypeInfo.h>
#include <Core/Reflection/TypeDatabase.h>

using namespace Math;

class btTransform;

namespace RGTek
{
    FWD_CLASS_EXTEND_PTR_TYPES(Transform);

    typedef enum
    {
        World,
        Self
    } Space;

    class Transform: public RGTek::Object
    {
    public:
		Transform();
        explicit Transform(const Vector3& pos, const Quaternion& rot = Quaternion(0,0,0,1), const Vector3& scale = Vector3(1.0f, 1.0f, 1.0f));
		explicit Transform(const btTransform& transform);

		RGTEK_REGISTER_TYPE(Transform,
			RGTEK_FIELD("Position", &Transform::GetPosition, &Transform::SetPosition)
			RGTEK_FIELD("Rotation", &Transform::GetRotation, &Transform::SetRotation)
			RGTEK_FIELD("Scale", &Transform::GetScale, &Transform::SetScale)
		)

		~Transform();

        Matrix4 GetTransformation() const;
        bool HasChanged();
        void Update();
        void Translate(const Vector3& translation);
        void Rotate(const Vector3& axis, float angle, const Space& relativeTo = Space::Self);
        void Rotate(const Quaternion& rotation, const Space& relativeTo = Space::Self);
        void LookAt(const Vector3& point, const Vector3& up);

        void MoveForward (float distance) { mPosition += distance * mRotation.GetForward(); }
        void MoveBackward(float distance) { mPosition += distance * mRotation.GetBack();    }
        void MoveUp      (float distance) { mPosition += distance * mRotation.GetUp();      }
        void MoveDown    (float distance) { mPosition += distance * mRotation.GetDown();    }
        void MoveLeft    (float distance) { mPosition += distance * mRotation.GetLeft();    }
        void MoveRight   (float distance) { mPosition += distance * mRotation.GetRight();   }

        Quaternion GetLookAtRotation(const Vector3& point, const Vector3& up)
        {
            return Quaternion(Matrix4().InitRotationFromDirection((point - mPosition).Normalized(), up));
        }

        Vector3    GetTransformedPosition() const;
        Quaternion GetTransformedRotation() const;

        const Vector3&    GetPosition() const { return mPosition; }
        const Quaternion& GetRotation() const { return mRotation; }
        const Vector3&    GetScale()    const { return mScale;    }

        void SetPosition(const Vector3& pos);
        void SetRotation(const Quaternion& rot);
        void SetScale(const Vector3& scale);
        void SetParent(Transform* parent);

        DataTranslator* GetDataTranslator();

        Transform& operator=(const btTransform& transform);
        operator btTransform() const;

		// Delegate called whenever the transformation changes.
		Delegate<void()> OnSetTransform;

	protected:
    private:

        const Matrix4& GetParentMatrix() const;

        Vector3            mPosition;
        Quaternion         mRotation;
        Vector3            mScale;

        Transform*         mParent;
        mutable Matrix4    mParentMatrix;

        mutable Vector3    mOldPosition;
        mutable Quaternion mOldRotation;
        mutable Vector3    mOldScale;
        mutable bool       mInitializedOldStuff;
    };

}

#endif
