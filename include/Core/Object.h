#ifndef OBJECT_H__
#define OBJECT_H__

//+--------------------------------------------------------------------------+
//|                                                                          |
//|                Copyright� 2015, Bruno Henrique Gouveia                   |
//|                All Rights Reserved.                                      |
//|                                                                          |
//+--------------------------------------------------------------------------+
//
//  OVERVIEW: Object.h
//  ========
//  Class definition for generic object.

namespace RGTek
{

    //////////////////////////////////////////////////////////
    //
    // Object: generic object class
    // ======
    class Object
    {
    public:
        virtual ~Object() {}

        int GetNumberOfUses() const { return counter; }

        template <typename T> friend T* makeUse(T*);

        void Release() {
            if (counter == 1)
            {
                delete this;
            }
            else
            {
                counter--;
            }
        }

    protected:
        Object():
            counter(0)
        {}

    public:
        int counter;
    };

    template <typename T>
    inline T*
        makeUse(T* object)
    {
        if (object != 0)
            ++object->counter;
        return object;
    }

    template <typename T>
    inline void
        release(T* object)
    {
        if (object != 0)
            object->Release();
    }


    //////////////////////////////////////////////////////////
    //
    // ObjectPtr: object pointer class
    // =========
    template <typename T>
    class ObjectPtr
    {
    public:
        ObjectPtr():
            object(0)
        {}
        ObjectPtr(const ObjectPtr<T>& ptr) { this->object = makeUse(ptr.object); }
        ObjectPtr(T* object) { this->object = makeUse(object); }

        ~ObjectPtr() { release(this->object); }

        ObjectPtr<T>& operator =(T* object)
        {
            release(this->object);
            this->object = makeUse(object);
            return *this;
        }

        ObjectPtr<T>& operator =(const ObjectPtr<T>& ptr)
        {
            release(this->object);
            this->object = makeUse(ptr.object);
            return *this;
        }

        bool operator ==(T* object) const { return this->object == object; }
        bool operator ==(const ObjectPtr<T>& ptr) const { return this->object == ptr.object; }
        bool operator !=(T* object) const { return !operator ==(object); }
        bool operator !=(const ObjectPtr<T>& ptr) const { return !operator ==(ptr); }

        template <typename P>
        operator ObjectPtr<P>() const { return ObjectPtr<P>(this->object); }

        operator T*() const {  return this->object; }
        T* operator ->() const { return object; }

    private:
        T* object;

    };

    //////////////////////////////////////////////////////////
    //
    // ObjectHandlePtr: object pointer class
    // =========
    template <typename T>
    class ObjectHandlePtr
    {
    public:
        ObjectHandlePtr()=delete;
        ObjectHandlePtr(const ObjectHandlePtr<T>& ptr) { this->mIndex = ptr.mIndex; }
        explicit ObjectHandlePtr(T* object) {
            this->mIndex = sCurrentIndex++;
            sObjects[mIndex] = object; }

        ~ObjectHandlePtr() {}

        ObjectHandlePtr<T>& operator =(T* object)
        {
            if (sObjects[mIndex]) {
                delete sObjects[mIndex];
            }
            sObjects[mIndex] = object;
            return *this;
        }

        ObjectHandlePtr<T>& operator =(const ObjectHandlePtr<T>& ptr)
        {
            this->mIndex = ptr.mIndex;
            return *this;
        }

        void Delete()
        {
            if (sObjects[mIndex])
            {
                delete sObjects[mIndex];
                sObjects[mIndex] = nullptr;
            }
        }

        bool operator ==(T* object) const { return sObjects[this->mIndex] == object; }
        bool operator ==(const ObjectHandlePtr<T>& ptr) const { return sObjects[this->mIndex] == sObjects[ptr.mIndex]; }
        bool operator !=(T* object) const { return !operator ==(object); }
        bool operator !=(const ObjectHandlePtr<T>& ptr) const { return !operator ==(ptr); }

        operator T*() const {  return sObjects[this->mIndex]; }
        T* operator ->() const { return sObjects[this->mIndex]; }

        static void ShutDown() {
            for (size_t i = 0; i < 4096; i++) {
                if (sObjects[i]) {
                    delete sObjects[i];
                }
            }
        }
//    private:
        unsigned int mIndex;
        static T*  sObjects[4096];
        static unsigned int sCurrentIndex;

    };


    template <typename T> T* ObjectHandlePtr<T>::sObjects[4096];
    template <typename T> unsigned int ObjectHandlePtr<T>::sCurrentIndex = 0;

}

#endif // OBJECT_H__
