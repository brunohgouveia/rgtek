#ifndef PLATFORM_H__
#define PLATFORM_H__

#include <stddef.h>
#include <Core/Object.h>
#include <Core/Handle.h>
#include <Math/Math.h>
#include <Math/Vector4.h>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include <memory>
#include <cstdint>

typedef unsigned int GLuint;
typedef int GLint;
typedef int GLsizei;
typedef ptrdiff_t GLsizeiptr;
typedef ptrdiff_t GLintptr;
typedef unsigned int GLenum;
typedef unsigned char GLboolean;

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long long int InstanceID;

// STL Types
using String = std::string;
template <typename T> using Vector = std::vector<T>;
template <typename T> using Stack  = std::stack<T>;
template <typename Key, typename T> using Map = std::map<Key, T>;
using AnyPtr = const void*;

//#if defined(_WIN32)
//# if defined(_WIN64)
//#  define FORCE_UNDEFINED_SYMBOL(x) __pragma(comment (linker, "/include:" #x))
//# else
//#  define FORCE_UNDEFINED_SYMBOL(Var) extern "C" { int lib_func_##Var() { return (int)&Var; } } \
//		__pragma( message("#pragma comment(linker,\"/include:_lib_func_"#Var"\")") )
//# endif
//#else
//# define FORCE_UNDEFINED_SYMBOL(x) extern "C" void x(void); void (*__ ## x ## _fp)(void)=&x;
//#endif

#define CLASS_EXTEND_PTR_TYPES(typeName) \
typedef RGTek::ObjectPtr<typeName> typeName##Ptr; \
    typedef typeName* typeName##WeakPtr; \
    typedef RGTek::Handle<typeName> typeName##Handle; \
    typedef std::vector<typeName##Ptr> typeName##PtrVector; \
    typedef std::vector<typeName##WeakPtr> typeName##WeakPtrVector; \
    typedef typeName##PtrVector::iterator typeName##PtrVectorItr; \
    typedef typeName##WeakPtrVector::iterator typeName##WeakPtrVectorItr;

#define FWD_CLASS_EXTEND_PTR_TYPES(typeName) \
    class typeName; \
    CLASS_EXTEND_PTR_TYPES(typeName)

#define CLASS_EXTEND_TYPES(typeName) \
    typedef std::vector<typeName> typeName##Vector; \
    typedef typeName##Vector::iterator typeName##VectorItr;


#ifndef __METHOD__
// #if defined __func__
// // Undeclared
// #define __METHOD__   __func__
// #elif defined __FUNCTION__
// // Undeclared
// #define __METHOD__   __FUNCTION__
// #elif defined __PRETTY_FUNCTION__
// // Undeclared
// #define __METHOD__   __PRETTY_FUNCTION__
#define __METHOD__ "N/A"
#else
// Declared
#define __METHOD__   "N/A"
// #endif // __func__

#endif // __FUNCTION_NAME__

#endif // PLATFORM_H__
