#pragma once

#include <Core/Platform.h>
#include <functional>

namespace RGTek
{

	template <typename T>
	class Delegate
	{
	public:
		template <typename ...Args>
		void operator()(Args... args)
		{
			for (auto callbackFunction : mCallBackFunctions)
			{
				callbackFunction(std::forward<Args>(args)...);
			}
		}

		Delegate<T> operator +=(const std::function<T>& callBackFunction)
		{
			mCallBackFunctions.push_back(callBackFunction);
			return *this;
		}

	private:
		Vector<std::function<T>> mCallBackFunctions;
	};

	/*template <typename R, typename... Args>
	class Delegate<R(Args...)>
	{
	public:
		typedef R(*FuncTypePointer)(Args...);
		typedef R(FuncType)(Args...);

		void operator ()(Args... args)
		{
			for (auto callbackFunction : mCallBackFunctions)
			{
				callbackFunction(std::forward<Args>(args)...);
			}
		}

		Delegate<FuncType>& operator +=(const FuncType& callBackFunction)
		{
			mCallBackFunctions.push_back(callBackFunction);
			return *this;
		}

	private:
		Vector<FuncTypePointer> mCallBackFunctions;
	};

	template <typename R, typename... Args>
	class Delegate<R(*)(Args...)>
	{
	public:
		typedef R(*FuncType)(Args...);

		void operator ()(Args... args)
		{
			for (auto callbackFunction : mCallBackFunctions)
			{
				callbackFunction(std::forward<Args>(args)...);
			}
		}

		Delegate<FuncType>& operator +=(const FuncType& callBackFunction)
		{
			mCallBackFunctions.push_back(callBackFunction);
			return *this;
		}

	private:
		Vector<FuncType> mCallBackFunctions;
	};*/

}