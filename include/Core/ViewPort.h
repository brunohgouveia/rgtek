#ifndef RGTEK_VIEWPORT_H__
#define RGTEK_VIEWPORT_H__

#include <Core/Platform.h>
#include <Math/Vector2.h>

namespace RGTek
{

    class Viewport
    {
    public:
        Viewport(Math::Vector2 size) : position(Math::Vector2(0.0f)), size(size) {}
        Viewport(Math::Vector2 position, Math::Vector2 size) : position(position), size(size) {}
        ~Viewport() {}

        float GetAspectRatio() const { return size.x / ((size.y != 0) ? size.y : 1); }

        Math::Vector2 position;
        Math::Vector2 size;
    };

}
#endif // RGTEK_VIEWPORT_H__
