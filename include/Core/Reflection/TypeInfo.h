#pragma once

#include <Core/Reflection/TypeDefinitions.h>
#include <Core/Reflection/Type.h>

namespace RGTek
{
	class TypeData;

	template<typename T>
	class TypeInfo
	{
	public:
		static TypeID ID;
		static bool Defined;

		static void Register(TypeID id, TypeData &data, bool beingDefined);
	};

#pragma warning(push)

	// unused template parameters
#pragma warning(disable : 4544)

#include <Core/Reflection/TypeData.h>

	template<typename T>
	TypeID TypeInfo<T>::ID = Type::Invalid;

	template<typename T>
	bool TypeInfo<T>::Defined = false;

	template<typename T>
	void TypeInfo<T>::Register(TypeID id, TypeData& data, bool beingDefined)
	{
		// already defined
		if (id == Type::Invalid)
			return;

		ID = id;

		typedef typename std::decay<T>::type DecayedType;
		
		data.isClass = std::is_class< DecayedType >::value;
		data.isEnum = std::is_enum< DecayedType >::value;
		data.isPointer = std::is_pointer< T >::value;
		data.isPrimitive = std::is_arithmetic< DecayedType >::value;

		/*if (beingDefined)
		{
			addDefaultConstructor(data);

			applyTrivialAttributes(data);
		}*/
	}

#pragma warning(pop)

}