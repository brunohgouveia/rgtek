#pragma once

#include <Core/Platform.h>
#include <Core/Reflection/TypeInfo.h>
#include <Core/Reflection/Property.h>

#include <unordered_map>

namespace RGTek
{
	
	///////////////////////////////////////////////////////////////////////
	// TypeData
	// ================================================================
	// This class is used to "collect" meta information of a certain type
	// and store such data in the TypeDatabase.
	// 
	// IMPORTANT: this type has a completely different purpose from 'Type'
	// because 'Type' is used to "read" meta information of a certain type
	// (e.g., which properties a type has).
	// All instances of this class are automatically instantiated (by macros)
	// or generated code) and YOU PROBABLY WANNA USE 'Type' INSTEAD.
	class TypeData
	{
	public:
		bool isEnum : 1;
		bool isPrimitive : 1;
		bool isPointer : 1;
		bool isClass : 1;

		String name;

		std::unordered_map<std::string, Property> properties;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		TypeData();
		TypeData(const std::string& name);

		~TypeData();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		template <typename ClassType, typename ValueType, typename Getter, typename Setter,
			typename C1 = typename std::enable_if<FunctionTraits<Getter>::ArgCount == 0>::type,
			typename C2 = typename std::enable_if<FunctionTraits<Setter>::ArgCount == 1>::type,
			typename C3 = typename std::enable_if<std::is_same<ValueType, typename FunctionTraits<Getter>::pure_return_type>::value>::type,
			typename C4 = typename std::enable_if<std::is_same<ValueType, typename FunctionTraits<Setter>::template arg<0>::pure_type>::value>::type
		>
		void AddProperty(const String& name, Getter getter, Setter setter)
		{
			properties.emplace(name, Property(name, typeof(ClassType), typeof(ValueType), new PropertyData<ClassType, ValueType>(getter, setter)));
		}

		template <typename ClassType, typename ValueType, typename Getter,
			typename C1 = typename std::enable_if<FunctionTraits<Getter>::ArgCount == 0>::type,
			typename C3 = typename std::enable_if<std::is_same<ValueType, typename FunctionTraits<Getter>::pure_return_type>::value>::type
		>
		void AddProperty(const String& name, Getter getter)
		{
			properties.emplace(name, Property(name, typeof(ClassType), typeof(ValueType), new PropertyData<ClassType, ValueType>(getter)));
		}
	};

}