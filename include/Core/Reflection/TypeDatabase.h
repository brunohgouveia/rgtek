#pragma once

#include <Core/Platform.h>
#include <Core/Reflection/Type.h>
#include <Core/Reflection/TypeData.h>

#include <unordered_map>

namespace RGTek
{
	
	class TypeDatabase
	{
	public:
		TypeDatabase();
		~TypeDatabase();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		static TypeDatabase& GetInstance();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		void Destroy();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		TypeID AllocateType(const String& name);

		std::vector<TypeData>& GetTypes();

		TypeData& GetTypeData(TypeID typeID);
		const TypeData& GetTypeData(TypeID typeID) const;

	private:
		TypeID mNextTypeID;

		std::vector<TypeData> mTypes;
		std::unordered_map<std::string, TypeID> mIDMap;
	};

}