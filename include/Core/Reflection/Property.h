#pragma once

#include <Core/Reflection/Type.h>
#include <Core/Reflection/Value.h>

#include <Core/Traits/FunctionTraits.h>
#include <functional>

namespace RGTek
{
	/////////////////////////////////////////////////
	// Interface that describes a property data
	// =====================================
	class IPropertyData
	{
	public:
		IPropertyData(){}
		virtual ~IPropertyData(){}

		virtual Meta::Value GetValue(Meta::PointerValue instance) const = 0;
		virtual void SetValue(Meta::PointerValue instance, Meta::Value value) = 0;

		virtual bool HasGetter() const = 0;
		virtual bool HasSetter() const = 0;
	};

	/////////////////////////////////////////////////
	// PropertyData (implementation of IPropertyData)
	// ========================================
	template <typename C, typename T>
	class PropertyData : public IPropertyData
	{
	public:
		using G = std::function < T(C*) >;
		using S = std::function < void(C*, T) >;

		template<typename Getter, typename Setter,
			typename C1 = typename std::enable_if<FunctionTraits<Getter>::ArgCount == 0>::type,
			typename C2 = typename std::enable_if<FunctionTraits<Setter>::ArgCount == 1>::type,
			typename C3 = typename std::enable_if<std::is_same<T, typename FunctionTraits<Getter>::pure_return_type>::value>::type,
			typename C4 = typename std::enable_if<std::is_same<T, typename FunctionTraits<Setter>::template arg<0>::pure_type>::value>::type
		>
		explicit PropertyData(Getter getter, Setter setter)
		{
			mGetter = G(std::bind(getter, std::placeholders::_1));
			mSetter = S(std::bind(setter, std::placeholders::_1, std::placeholders::_2));
		}

		template<typename Getter,
			typename C1 = typename std::enable_if<FunctionTraits<Getter>::ArgCount == 0>::type,
			typename C3 = typename std::enable_if<std::is_same<T, typename FunctionTraits<Getter>::pure_return_type>::value>::type
		>
		explicit PropertyData(Getter getter)
		{
			mGetter = G(std::bind(getter, std::placeholders::_1));
			mSetter = nullptr;
		}

		Meta::Value GetValue(Meta::PointerValue instance) const override
		{
			return Meta::Value(mGetter(instance.GetPtr<C>()));
		}

		void SetValue(Meta::PointerValue instance, Meta::Value value) override
		{
			mSetter(instance.GetPtr<C>(), value.GetValue<T>());
		}

		bool HasGetter() const override { return mGetter != nullptr; }
		bool HasSetter() const override { return mSetter != nullptr; }

	private:
		G mGetter;
		S mSetter;
	};

	/////////////////////////////////////////////////
	// Property - describes a property of a meta type
	// ========================================
	class Property
	{
	public:
		using Getter = std::function < Meta::Value(Meta::PointerValue) >;
		using Setter = std::function < void(Meta::PointerValue, Meta::Value&) >;

		Property();
		Property(Property&& prop);
		Property(const Property& prop);
		
		Property(const String& name, Type classType, Type valueType, IPropertyData* fieldData);

		bool IsReadOnly() const;

		Type GetClassType() const;
		Type GetValueType() const;

		const String& GetName() const;

		template <typename T>
		T GetValue(Meta::PointerValue instance) const
		{
			return mFieldData->GetValue(instance).GetValue<T>();
		}

		template <typename T>
		void SetValue(Meta::PointerValue instance, T value) const
		{
			Meta::Value newValue(value);
			if (!IsReadOnly())
			{
				mFieldData->SetValue(instance, newValue);
			}
		}

		template <typename T>
		std::function<T(void)> GetCallableGetter(void* instancePtr) const
		{
			return std::function<T(void)>(std::bind(&Property::GetValue<T>, this, Meta::PointerValue(GetClassType(), instancePtr)));
		}

		template <typename T>
		std::function<void(T)> GetCallableSetter(void* instancePtr) const
		{
			return std::function<void(T)>(std::bind(&Property::SetValue<T>, this, Meta::PointerValue(GetClassType(), instancePtr), std::placeholders::_1));
		}

		Property& operator=(const Property& prop);

	private:
		String mName;
		Type mClassType;
		Type mValueType;

		std::shared_ptr<IPropertyData> mFieldData;
	};

}