#pragma once

#include <type_traits>

// converts the type name into a meta::Type instance
#define typeof(type) RGTek::Type(RGTek::TypeInfo<RGTek::CleanedType<type>>::ID)

#define RGTEK_REGISTER_TYPE(typeName, ...)												\
	virtual Type& GetType() const														\
	{																					\
		static Type type;																\
		if (!TypeInfo<typeName>::Defined)												\
		{																				\
			TypeID typeID = TypeDatabase::GetInstance().AllocateType(#typeName);		\
			TypeData& typeData = TypeDatabase::GetInstance().GetTypeData(typeID);		\
			TypeInfo<typeName>::Register(typeID, typeData, true);						\
			type = typeof(typeName);													\
			__VA_ARGS__																	\
		}																				\
		return type;																	\
	}																					\

#define RGTEK_FIELD(name, getter, setter)												\
		typeData.AddProperty<															\
			FunctionTraits<decltype(getter)>::ClassType,								\
			FunctionTraits<decltype(getter)>::pure_return_type							\
		>(																				\
			name,																		\
			getter,																		\
			setter																		\
		);																				\

#define RGTEK_RONLY_FIELD(name, getter)													\
		typeData.AddProperty<															\
			FunctionTraits<decltype(getter)>::ClassType,								\
			FunctionTraits<decltype(getter)>::pure_return_type							\
		>(																				\
			name,																		\
			getter																		\
		);																				\

namespace RGTek
{

	template<typename T>
	using CleanedType =
		typename std::remove_cv <
		typename std::remove_reference< T >::type
		> ::type;

	typedef unsigned TypeID;

}