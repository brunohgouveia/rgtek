#pragma once

#include <memory>
#include <Core/Platform.h>
#include <Core/Types/Variant.h>
#include <Math/Vector2.h>
#include <Math/Vector3.h>
#include <Math/Vector4.h>
#include <Math/Quaternion.h>

namespace RGTek
{
	using namespace Math;
	namespace Meta
	{
		class Value
		{
		public:

			template <typename T>
			explicit Value(T value)
			{
				mType = typeof(T);
				data.set<T>(value);
			}

			~Value()
			{
				// Do nothing just yet
			}

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

			Type GetType() const { return mType; }

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

			void SetIntValue(int value) { data.set<int>(value); }
			void SetFloatValue(float value) { data.set<float>(value); }
			void SetDoubleValue(double value) { data.set<double>(value); }
			void SetVector2Value(Vector2 value) { data.set<Vector2>(value); }
			void SetVector3Value(Vector3 value) { data.set<Vector3>(value); }
			void SetVector4Value(Vector4 value) { data.set<Vector4>(value); }
			void SetQuaternionValue(Quaternion value) { data.set<Quaternion>(value); }
			void SetStringValue(String value) { data.set<String>(value); }

			template <typename T>
			void SetValue(T value)
			{
				// TODO: auto cast between castable types.
				Type tType = typeof(T);

				if (mType == tType)
				{
					data.set<T>(value);
				}
				else
				{
					String exceptionMessage = "Types [" + mType.GetName() + "] and [" + tType.GetName() + "] don't match";
					throw std::runtime_error(exceptionMessage.c_str());
				}
			}

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

			int GetIntValue() { return data.get<int>(); }
			float GetFloatValue() { return data.get<float>(); }
			double GetDoubleValue() { return data.get<double>(); }
			Vector2 GetVector2Value() { return data.get<Vector2>(); }
			Vector3 GetVector3Value() { return data.get<Vector3>(); }
			Vector4 GetVector4Value() { return data.get<Vector4>(); }
			Quaternion GetQuaternionValue() { return data.get<Quaternion>(); }
			String GetStringValue() { return data.get<String>(); }

			template <typename T>
			T GetValue()
			{
				// TODO: auto cast between castable types.
				Type tType = typeof(T);

				if (mType == tType)
				{
					return data.get<T>();
				}
				else
				{
					String exceptionMessage = "Types [" + mType.GetName() + "] and [" + tType.GetName() + "] don't match";
					throw std::runtime_error(exceptionMessage.c_str());
				}
			}

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

		private:
			using DataType = variant < int, float, double, Vector2, Vector3, Vector4, Quaternion, String > ;
			Type mType;
			DataType data;
		};


		class PointerValue
		{
		public:
			template <typename T>
			explicit PointerValue(T* ptr)
			{
				mType = typeof(T);
				mPointer = static_cast<void*>(ptr);
			}

			template <typename T>
			explicit PointerValue(T& ref)
			{
				mType = typeof(T);
				mPointer = static_cast<void*>(&ref);
			}

			explicit PointerValue(Type type, void* ptr)
			{
				mType = type;
				mPointer = ptr;
			}

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

			Type GetType() const { return mType; }

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

			template <typename T>
			T* GetPtr() const
			{
				if (GetType() == typeof(T))
				{
					return static_cast<T*>(mPointer);
				}
				else
				{
					String exceptionMessage = "Types [" + GetType().GetName() + "] and [" + typeof(T).GetName() + "] don't match";
					throw std::runtime_error(exceptionMessage.c_str());
				}
			}

			///////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////

		private:
			Type mType;
			void* mPointer;
		};
	}
}