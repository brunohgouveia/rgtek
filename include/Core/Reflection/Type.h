#pragma once

#include <Core/Platform.h>
#include <Core/Reflection/TypeDefinitions.h>

#include <vector>
#include <unordered_map>

namespace RGTek
{
	class Property;

	///////////////////////////////////////////////////////////////////////
	// Type 
	// ================================================================
	// This class contains meta information of a certain type (primitives/
	// classes/structures/enums) and it's highly used for reflection 
	// purposes.
	// 
	// IMPORTANT: this type has a completely different purpose from 'TypeData'
	// because 'TypeData' is used to "build" enough information of a type
	// and register this information in the 'TypeDatabase'. Then,
	// all this information can be accessed by an 'Type'  object.
	class Type
	{
	public:
		// Define a typeid for invalid types
		static const TypeID Invalid = 0;

		Type();
		Type(const Type &rhs);
		explicit Type(TypeID id);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Checks whether this type is a valid type.
		*  @return true if the type's id is not TypeID::Invalid
		*/
		bool IsValid() const;

		/** @brief Determines whether this type is primitive.
		*  @return true if the type is a built-in type (e.g. int, float, char).
		*/
		bool IsPrimitive() const;

		/** @brief Determines whether this type is an enum type.
		*  @return true if the type is either an enum or enum class.
		*/
		bool IsEnum() const;

		/** @brief Determines if this type is a pointer.
		*  @return true if the type has any level of indirection.
		*          ie - (int *), (int **), etc.
		*/
		bool IsPointer() const;

		/** @brief Determines if this type is a class.
		*  @return true if the type is a class or struct.
		*/
		bool IsClass() const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Gets the human readable name for this type.
		*  @return Qualified name of the type as it is declared.
		*          ie - "boost::regex"
		*/
		const std::string& GetName() const;

		/** @brief Gets the type's internal id.
		*/
		TypeID GetID() const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Gets a map of properties and their respective names.
		*  @return Map of pairs, containing a string and a property object.
		*/
		const std::unordered_map<std::string, Property>& GetProperties() const;

		/** @brief Gets the type's property by a name.
		*  @param name Name of the property.
		*  @return A constant reference to the property with name 'name' 
		*          If the property doesn't exist, an invalid property.
		*/
		const Property& GetProperty(const std::string &name) const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		operator bool() const;

		Type &operator=(const Type &rhs);

		bool operator<(const Type &rhs) const;
		bool operator>(const Type &rhs) const;
		bool operator<=(const Type &rhs) const;
		bool operator>=(const Type &rhs) const;
		bool operator==(const Type &rhs) const;
		bool operator!=(const Type &rhs) const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	private:
		TypeID mId;
	};

}
