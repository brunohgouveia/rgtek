//
// Created by Bruno Gouveia on 12/2/15.
//

#ifndef RGTEK_System_H
#define RGTEK_System_H

#include <Core/Platform.h>

namespace RGTek
{

    class Component;
    class Entity;

    class System: public Object
    {
    public:
        static const String SYSTEM_NAME;

        System();
        virtual ~System();

        virtual void Start();
        virtual void Stop();

        virtual void Pause();
        virtual void Resume();

        bool IsRunning();

        virtual const String& GetSystemName() const { return SYSTEM_NAME; }

        virtual Component* CreateComponentByName(const String& className) = 0;
        virtual void DeleteComponent(Component* component) = 0;
        virtual void DeleteComponentsOfEntity(Entity* entity) = 0;

    protected:
        bool mRunning;
	};

#define RSYSTEM(system) \
    static const String SYSTEM_NAME; \
    virtual const String& GetSystemName() const override { return SYSTEM_NAME; }

#define RSYSTEM_IMPLEMENTATION(system) \
	const String system::SYSTEM_NAME = #system;
}

#endif //RGTEK_System_H
