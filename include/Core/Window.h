//
// Created by Bruno Gouveia on 2/3/16.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Viewport.h>
#include <Math/Vector2.h>
#include <functional>

////////////////////////////////////////////////////////////
// This is bettern than including SDL2 header, because
// it makes possible for third party use this header without
// needing to include the SDL2 header as well.
// =================================================
class SDL_Window;
using SDL_GLContext = void*;

namespace RGTek
{
    using namespace std;
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(Window);

	class Window: public Object
    {
    public:
        typedef std::function<void(Vector2,Vector2)> DrawCallbackFunction;

        Window(const string& title, int width, int height);
        Window(const Window& other) = delete;
        virtual ~Window();

        virtual void Render();
        virtual void HandleEvents();
        virtual void SwapBuffers();

        virtual void ClearColor();
        virtual void ClearDepth();
        virtual void Clear();

        virtual void BindAsRenderTarget();
        virtual void BindCanvasAsRenderTarget(const Viewport& viewport);

        inline bool IsCloseRequested()       const { return mIsCloseRequested; }
        inline float GetAspectRatio()        const { return static_cast<float>(mWidth)/ static_cast<float>(mHeight); }
        inline const std::string& GetTitle() const { return mTitle; }

        inline void SetDrawCallback(DrawCallbackFunction drawCallback) { mCanvasCallback = drawCallback; }

        virtual Vector2 GetWindowPixelDensity() const;
        virtual Vector2 GetWindowSize() const;
        virtual Vector2 GetFramebufferSize() const;

        void operator=(const Window& other) = delete;

    protected:
        friend class Application;

        std::string    mTitle;
        int            mWidth;
        int            mHeight;

        SDL_Window*    mWindow;
        SDL_GLContext  mContext;

        bool           mIsCloseRequested;

        DrawCallbackFunction mCanvasCallback;
    };

}