//
// Created by Bruno Gouveia on 5/22/16.
//

#pragma once

#include <type_traits>
#include <tuple>
#include <functional>

template <typename T>
struct FunctionTraits: public FunctionTraits<decltype(&T::operator())>
{

};

template <typename R, typename... Args>
struct FunctionTraits<R(Args...)>
{
    enum { ArgCount = sizeof...(Args) };

    using ResultType = R;
    using pure_return_type = typename std::remove_cv<typename std::remove_reference<R>::type>::type;

    template <size_t i>
    struct arg
    {
        using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
        using pure_type = typename std::remove_cv<typename std::remove_reference<type>::type>::type;
    };
};

template <typename R, typename... Args>
struct FunctionTraits<R(*)(Args...)>
{
    enum { ArgCount = sizeof...(Args) };

    using ResultType = R;
    using pure_return_type = typename std::remove_cv<typename std::remove_reference<R>::type>::type;

    template <size_t i>
    struct arg
    {
        using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
        using pure_type = typename std::remove_cv<typename std::remove_reference<type>::type>::type;
    };
};

template <typename R, typename... Args>
struct FunctionTraits<std::function<R(Args...)>>
{
    enum { ArgCount = sizeof...(Args) };

    using ResultType = R;
    using pure_return_type = typename std::remove_cv<typename std::remove_reference<R>::type>::type;

    template <size_t i>
    struct arg
    {
        using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
        using pure_type = typename std::remove_cv<typename std::remove_reference<type>::type>::type;
    };
};

template <class T, typename R, typename... Args>
struct FunctionTraits<R(T::*)(Args...)>
{
    enum { ArgCount = sizeof...(Args) };

    using ResultType = R;
	using ClassType = T;
    using pure_return_type = typename std::remove_cv<typename std::remove_reference<R>::type>::type;

    template <size_t i>
    struct arg
    {
        using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
        using pure_type = typename std::remove_cv<typename std::remove_reference<type>::type>::type;
    };
};

template <class T, typename R, typename... Args>
struct FunctionTraits<R(T::*)(Args...)const>
{
    enum { ArgCount = sizeof...(Args) };

    using ResultType = R;
	using ClassType = T;
    using pure_return_type = typename std::remove_cv<typename std::remove_reference<R>::type>::type;

    template <size_t i>
    struct arg
    {
        using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
        using pure_type = typename std::remove_cv<typename std::remove_reference<type>::type>::type;
    };
};