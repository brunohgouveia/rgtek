#ifndef LIGHT_H__
#define LIGHT_H__

#include <Core/Platform.h>
#include <Core/Transform.h>
#include <Math/Vector4.h>
#include <Math/Matrix4.h>
#include <GL/Color.h>
#include <Resources/Shader.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(BaseLight);

    //TODO: Update transform
    class BaseLight: public Object
    {
    public:
        BaseLight(Transform* transform, const Color& color, float intensity, float lightBias = 0.05f)
            : mTransform(transform)
            , mColor(color)
            , mIntensity(intensity)
            , mLightBias(lightBias)
        {};
        virtual ~BaseLight() {};

        virtual void UpdateUniformsToShader(ShaderWeakPtr shader);

        virtual void SetTransform(TransformPtr transform) { mTransform = transform; }

        void SetColor(const Color& color)  { mColor = color;         }
        void SetIntensity(float intensity) { mIntensity = intensity; }
        void SetLightBias(float lightBias) { mLightBias = lightBias; }

        virtual TransformPtr GetTransform() { return mTransform; }
        virtual Matrix4 GetViewProjection() = 0;
        virtual const Color& GetColor() const { return mColor;     }
        virtual float GetIntensity()    const { return mIntensity; }
        virtual float GetLightBias()    const { return mLightBias; }

        /** Const references getter/setter */
        void SetColorRef(const Color& color)         { mColor = color;         }
        void SetIntensityRef(const float& intensity) { mIntensity = intensity; }
        void SetLightBiasRef(const float& lightBias) { mLightBias = lightBias; }

        virtual const Color& GetColorRef()     const { return mColor;     }
        virtual const float& GetIntensityRef() const { return mIntensity; }
        virtual const float& GetLightBiasRef() const { return mLightBias; }

    protected:
        TransformPtr mTransform;
        Color        mColor;
        float        mIntensity;
        float        mLightBias;
    };

    FWD_CLASS_EXTEND_PTR_TYPES(SpotLight)

    class SpotLight: public BaseLight
    {
    public:
        SpotLight(const Color& color, float intensity = 0.05f);
        SpotLight(TransformWeakPtr transform, const Color& color, float intensity = 0.05f);
        virtual ~SpotLight() {};

	    void UpdateUniformsToShader(ShaderWeakPtr shader) override;

	    Matrix4 GetViewProjection() override;

        inline float GetSpotAngle() const { return mSpotAngle; }
        inline float GetRange()     const { return mRange;     }

        inline void SetSpotAngle(float spotAngle) { mSpotAngle = spotAngle; }
        inline void SetRange(float range)         { mRange     = range;     }

        /** Const references getter/setter */
        inline const float& GetSpotAngleRef() const { return mSpotAngle; }
        inline const float& GetRangeRef()     const { return mRange;     }

        inline void SetSpotAngleRef(const float& spotAngle) { mSpotAngle = spotAngle; }
        inline void SetRangeRef(const float& range)         { mRange     = range;     }

    private:
        float mSpotAngle;
        float mRange;
    };

}

#endif // LIGHT_H__
