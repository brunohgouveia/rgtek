#ifndef RGTEK_RAY_H__
#define RGTEK_RAY_H__

#include <Math/Vector3.h>
#include <Math/Plane.h>

namespace RGTek
{
    using namespace Math;
	class Transform;

    class Ray
    {
    public:
        Ray() = delete;
        Ray(const Vector3& origin, const Vector3& direction);
        virtual ~Ray() {}
        
        inline Vector3 GetPoint(float distance) const { return origin + direction * distance; }

        bool IntersectPlane(const Plane& plane, Vector3& intersectionPoint) const;
        bool IntersectSphere(const Vector3& center, float radius, Vector3& intersectionPoint) const;
		bool IntersectOBB(const Transform& transform, const Vector3& min, const Vector3& max, Vector3& intersectionPoint) const;

        Vector3 origin;
        Vector3 direction;
        Vector3 inverseDirection;
        unsigned int negative[3];
    };

} /* RGTek */

#endif
