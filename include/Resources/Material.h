//
// Created by Bruno Gouveia on -/-/15.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Handle.h>
#include <Resources/Shader.h>
#include <GL/Color.h>
#include <GL/Texture.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);
    FWD_CLASS_EXTEND_PTR_TYPES(Material);

    class Material: public Object
    {
    public:
        Material(const String& materialName, ResourceManagerWeakPtr resourceManager);
        virtual ~Material();

        void UpdateUniformsToShader(ShaderWeakPtr shader, ResourceManagerWeakPtr resourceManager = nullptr);

        inline void SetProgram(ShaderHandle program)                      { mShader = program; }
        inline void SetColor  (String colorName, Color color)             { mColors[colorName] = color; }
        inline void SetTexture(String textureName, TextureHandle texture) { mTextures[textureName] = texture; }

        inline ShaderHandle  GetProgram()                   { return mShader; }
        inline Color         GetColor  (String colorName)   { return mColors[colorName]; }
        inline TextureHandle GetTexture(String textureName) { return mTextures[textureName]; }
        inline Map<String,Color>         GetColors()   { return mColors; }
        inline Map<String,TextureHandle> GetTextures() { return mTextures; }

    private:
        ShaderHandle              mShader;
        Map<String,Color>         mColors;
        Map<String,TextureHandle> mTextures;
    };

}
