#ifndef MESHDATA_H__
#define MESHDATA_H__

#include <Core/Platform.h>
#include <Math/Vector4.h>
#include <GL/Buffers/ArrayBuffer.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(MeshData);
    CLASS_EXTEND_TYPES(MeshData);

    class MeshData: public Object
    {
    public:
        MeshData();
        // Constructor with rvalues just to make sure that whoever is calling
        // those constructors know that the vector will be empty afterwards.
        explicit MeshData(std::vector<Vector4>&& vertices, std::vector<Vector4>&& normals);
		explicit MeshData(std::vector<Vector4>&& vertices, std::vector<Vector4>&& normals, std::vector<Vector4>&& textCoords);
        ~MeshData();

        void Draw() const;

	private:
        std::vector<Vector4>   mVertices;
        std::vector<Vector4>   mNormals;
        std::vector<Vector4>   mTextCoords;

        GLuint                 mVertexArray;
        GLuint                 mNumVertices;
        GL::ArrayBuffer<float> mVertexBuffer;
        GL::ArrayBuffer<float> mNormalBuffer;
        GL::ArrayBuffer<float> mTextCoordBuffer;
    };

}

#endif // MESHDATA_H__
