#ifndef MESH_H__
#define MESH_H__

#include <Core/Platform.h>
#include <Core/Transform.h>
#include <Resources/Material.h>
#include <Resources/MeshData.h>
#include <GL/Buffers/ArrayBuffer.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);
    FWD_CLASS_EXTEND_PTR_TYPES(Mesh);

    class Mesh: public Object
    {
    public:
        Mesh(const String& fileName, ResourceManagerWeakPtr resourceManager = nullptr);
        ~Mesh();

        const MeshDataPtrVector& GetSubMeshes() const { return mSubMeshes; }
        const Vector<MaterialHandle>& GetMaterials() const { return mMaterials; }

        void Draw(ResourceManagerWeakPtr resourceManager = nullptr) const;

    private:
        MeshDataPtrVector      mSubMeshes;
        Vector<MaterialHandle> mMaterials;
    };

}

#endif // MESH_H__
