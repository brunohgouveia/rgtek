#ifndef RGTEK_RESOURCEMANAGER_H__
#define RGTEK_RESOURCEMANAGER_H__

#include <Core/Platform.h>
#include <Core/Component.h>
#include <Resources/Shader.h>
#include <Resources/Mesh.h>
#include <Resources/Material.h>
#include <Util/Serializer.h>
#include <Util/ThirdPart/FileWatcher/FileWatcher.h>
#include <Tools/Debugger.h>

#include <set>

namespace RGTek
{
    class Script;
    FWD_CLASS_EXTEND_PTR_TYPES(GameComponent);
    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);

	class ResourceManager : public Object
    {
    public:
        ResourceManager(const String& resourcePath);
        ResourceManager(const String&& resourcePath);
        ResourceManager(const ResourceManager&) = delete;

        ~ResourceManager();

        void CheckResourcesChanges();

        template <typename T, typename ...Args> Handle<T> CreateResource(const String& fileName, Args... args);
        template <typename T> T* GetResource(Handle<T> resourceHandle);
        template <typename T> String GetResourceName(Handle<T> resourceHandle);
        template <typename T> void DestroyResource(const String& resourceName, Handle<T> resourceHandle);

        template <typename T> void DestroyResources();
        void  DestroyAllResources();

		const String& GetResourcePath() const { return mResourcePath; }
		void SetReourcePath(const String& resourcePath) { mResourcePath = resourcePath; }

        ResourceManager& operator=(const ResourceManager&) const = delete;

    protected:

        template <typename T> void ReloadResource(const String& fileName);

        template <typename T> std::map<String, Handle<T> >& GetResourceMap();
        template <typename T> HandleManager<T>& GetResourceHandleManager();


    private:
        String                           mResourcePath;

        std::map<String, ShaderHandle>   mShaderMap;
        HandleManager<Shader>            mShaderHandleManager;

        std::map<String, MeshHandle>     mMeshMap;
        HandleManager<Mesh>              mMeshHandleManager;

        std::map<String, MaterialHandle> mMaterialMap;
        HandleManager<Material>          mMaterialHandleManager;

        std::map<String, TextureHandle>  mTextureMap;
        HandleManager<Texture>           mTextureHandleManager;

        class FileWatchListener: public FW::FileWatchListener
        {
        public:
            FileWatchListener(ResourceManager* resourceManager) : mResourceManager(resourceManager) {}
            void handleFileAction(FW::WatchID watchid, const FW::String& dir, const FW::String& filename, FW::Action action);
        private:
            ResourceManager* mResourceManager;
        };

        FW::FileWatcher   mShaderFileWatcher;
        FW::FileWatcher   mTextureFileWatcher;
        FileWatchListener mResourceWatchListener;

        std::set<String>  mShadersOutdated;
        std::set<String>  mTexturesOutdated;
    };

    template <typename T, typename ...Args>
    Handle<T> ResourceManager::CreateResource(const String& fileName, Args... args)
    {
        std::map<String, Handle<T> >& resourceMap = GetResourceMap<T>();

        auto pair = resourceMap.find(mResourcePath + fileName);
        if (pair != resourceMap.end())
        {
            return pair->second;
        }
        else
        {
            Handle<T> handle = GetResourceHandleManager<T>().CreateHandle(mResourcePath + fileName, std::forward<Args>(args)..., this);
            resourceMap[mResourcePath + fileName] = handle;
            return handle;
        }
    }

    template<typename T>
    String ResourceManager::GetResourceName(Handle<T> resourceHandle)
    {
        // Typedef needed due to dependent name T
        typedef typename std::map<String, Handle<T> >::iterator iterator;

        std::map<String, Handle<T> >& resourceMap = GetResourceMap<T>();

        iterator pair = std::find_if(
                resourceMap.begin(),
                resourceMap.end(),
                // Alternative to lambda function would be 'std::bind2nd(customBinaryOperator<>(), value)'
                [&] (const std::pair<String, Handle<T> >& mapPair) -> bool
                {
                    return mapPair.second == resourceHandle;
                }
        );

        if (pair != resourceMap.end())
        {
            unsigned long resourcePathLength = mResourcePath.size();
            return pair->first.substr(resourcePathLength, pair->first.size() - resourcePathLength);
        }

        Debugger::Error("Resource's name not found.");
        // Suppress warning
        static String error = "";
        return error;
    }

    template <typename T>
    T* ResourceManager::GetResource(Handle<T> resourceHandle)
    {
        return GetResourceHandleManager<T>().GetObject(resourceHandle);
    }

    template <typename T>
    void ResourceManager::DestroyResource(const String& resourceName, Handle<T> resourceHandle)
    {
        std::map<String, Handle<T> >& resourceMap = GetResourceMap<T>();
        auto pair = resourceMap.find(resourceName);
        Debugger::AssertTrue(pair != resourceMap.end(), "Deleting nonexistent resource");
        Debugger::AssertTrue(pair->second == resourceHandle, "Handle does not match");

        resourceMap.erase(pair);
        GetResourceHandleManager<T>().DestroyHandle(resourceHandle);
    }

    template <typename T>
    void ResourceManager::ReloadResource(const String& fileName)
    {
        GetResourceHandleManager<T>().ReplaceHandle(GetResourceMap<T>()[fileName], fileName);
    }

    template <typename T> void ResourceManager::DestroyResources()
    {
        for (auto& pair : GetResourceMap<T>())
        {
            GetResourceHandleManager<T>().DestroyHandle(pair.second);
        }
        GetResourceMap<T>().clear();
    }

    /////////////////////////////////////////////////////////////////
    //
    // Generic - should not be instantiated, EVER!
    //
    template <typename T>
    std::map<String, Handle<T> >& ResourceManager::GetResourceMap()
    {
        Debugger::Fatal("Invalid resource type %s", typeid(T).name());
    }

    template <typename T>
    HandleManager<T>& ResourceManager::GetResourceHandleManager()
    {
        Debugger::Fatal("Invalid resource type %s", typeid(T).name());
    }


    /////////////////////////////////////////////////////////////////
    //
    // Specializations
    //
	template <> std::map<String, ShaderHandle>& ResourceManager::GetResourceMap<Shader>();
	template <> std::map<String, MeshHandle>& ResourceManager::GetResourceMap<Mesh>();
	template <> std::map<String, MaterialHandle>& ResourceManager::GetResourceMap<Material>();
	template <> std::map<String, TextureHandle>& ResourceManager::GetResourceMap<Texture>();

	template <> HandleManager<Shader>& ResourceManager::GetResourceHandleManager<Shader>();
	template <> HandleManager<Mesh>& ResourceManager::GetResourceHandleManager<Mesh>();
	template <> HandleManager<Material>& ResourceManager::GetResourceHandleManager<Material>();
	template <> HandleManager<Texture>& ResourceManager::GetResourceHandleManager<Texture>();

} /* RGTek */

#endif
