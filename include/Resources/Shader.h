#ifndef RGTEK_SHADER_H
#define RGTEK_SHADER_H

#include <Core/Platform.h>
#include <Math/Vector3.h>
#include <Math/Vector4.h>
#include <Math/Matrix4.h>
#include <GL/Color.h>

namespace RGTek
{
    using namespace Math;

	typedef int GLint;
	typedef unsigned int GLuint;
    typedef GLint Uniform;
    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);
	FWD_CLASS_EXTEND_PTR_TYPES(Shader);

	class Shader: public Object
    {
    public:
        Shader(const String& fileName, ResourceManagerWeakPtr resourceManager = nullptr);

        virtual ~Shader();

        operator GLuint() const { return mProgramName; };

		void UseShader() const;

		Uniform GetUniform(const std::string name) const;

        template <typename T>
        void SetUniform(const std::string name, const T value) const { SetUniform(GetUniform(name), value); }

        template <typename T>
        void SetUniform(const std::string name, const T* values, unsigned int count) const { SetUniform(GetUniform(name), values, count); }

		void SetUniform(const Uniform uniform, GLuint value)         const;
		void SetUniform(const Uniform uniform, int value)            const;
		void SetUniform(const Uniform uniform, float value)          const;
		void SetUniform(const Uniform uniform, const Matrix4& value) const;
		void SetUniform(const Uniform uniform, const Vector3& value) const;
		void SetUniform(const Uniform uniform, const Vector4& value) const;
		void SetUniform(const Uniform uniform, const Color& value)   const;

	private:
		bool CompileShader(const Vector<String> &shadersCode, const String &fileName);
        bool CheckGLShaderStatus(GLuint shaderName, std::string fileName);
        void LoadErrorShader();
		Vector<String> LoadShadersCode(const String& fileName);

		GLuint mProgramName;
	};

}

#endif //RGTEK_SHADER_H
