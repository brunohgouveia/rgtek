//
// Created by Bruno Gouveia on 12/11/15.
//

#ifndef RGTEK_LINE_H
#define RGTEK_LINE_H

#include <Math/Vector4.h>

namespace Math
{

    class Line
    {
    public:
        Line(const Vector3& startPoint, const Vector3& endPoint);
        Line(const Vector4& startPoint, const Vector4& endPoint);

        operator float*() { return static_cast<float*>(startPoint); }

        Vector4 startPoint;
        Vector4 endPoint;
    };

}

#endif //RGTEK_LINE_H
