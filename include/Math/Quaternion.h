#ifndef RGTEK_MATH_QUATERNION_H__
#define RGTEK_MATH_QUATERNION_H__

#include <Math/Vector3.h>
#include <Math/Vector4.h>
#include <Math/Matrix4.h>

class btQuaternion;

namespace Math
{

    class Quaternion : public Vector4
    {
    public:
		explicit Quaternion()
			: Vector4(0.0f, 0.0f, 0.0f, 1.0f)
		{
			// Do nothing
		}
        explicit Quaternion(float x, float y, float z, float w = 1.0f)
            : Vector4(x, y, z, w)
        {
        }

        explicit Quaternion(const Vector4& r)
        {
            (*this)[0] = r[0];
            (*this)[1] = r[1];
            (*this)[2] = r[2];
            (*this)[3] = r[3];
        }

        explicit Quaternion(const Vector3& axis, float angle)
        {
            float sinHalfAngle = sinf(angle/2);
            float cosHalfAngle = cosf(angle/2);

            (*this)[0] = axis.x * sinHalfAngle;
            (*this)[1] = axis.y * sinHalfAngle;
            (*this)[2] = axis.z * sinHalfAngle;
            (*this)[3] = cosHalfAngle;
        }

        explicit Quaternion(Vector3& angles)
        {
            (*this) = Quaternion(Vector3::forward, angles.z);
            (*this) = Quaternion((Quaternion(Vector3::right, angles.x) * (*this)).Normalized());
            (*this) = Quaternion((Quaternion(Vector3::up, angles.y) * (*this)).Normalized());
        }

        explicit Quaternion(const Matrix4& m)
        {
            float trace = m[0][0] + m[1][1] + m[2][2];

            if(trace > 0)
            {
                float s = 0.5f / sqrtf(trace + 1.0f);
                (*this)[3] = 0.25f / s;
                (*this)[0] = (m[1][2] - m[2][1]) * s;
                (*this)[1] = (m[2][0] - m[0][2]) * s;
                (*this)[2] = (m[0][1] - m[1][0]) * s;
            }
            else if(m[0][0] > m[1][1] && m[0][0] > m[2][2])
            {
                float s = 2.0f * sqrtf(1.0f + m[0][0] - m[1][1] - m[2][2]);
                (*this)[3] = (m[1][2] - m[2][1]) / s;
                (*this)[0] = 0.25f * s;
                (*this)[1] = (m[1][0] + m[0][1]) / s;
                (*this)[2] = (m[2][0] + m[0][2]) / s;
            }
            else if(m[1][1] > m[2][2])
            {
                float s = 2.0f * sqrtf(1.0f + m[1][1] - m[0][0] - m[2][2]);
                (*this)[3] = (m[2][0] - m[0][2]) / s;
                (*this)[0] = (m[1][0] + m[0][1]) / s;
                (*this)[1] = 0.25f * s;
                (*this)[2] = (m[2][1] + m[1][2]) / s;
            }
            else
            {
                float s = 2.0f * sqrtf(1.0f + m[2][2] - m[1][1] - m[0][0]);
                (*this)[3] = (m[0][1] - m[1][0]) / s;
                (*this)[0] = (m[2][0] + m[0][2]) / s;
                (*this)[1] = (m[1][2] + m[2][1]) / s;
                (*this)[2] = 0.25f * s;
            }

            float magnitude = Magnitude();
            (*this)[3] = (*this)[3] / magnitude;
            (*this)[0] = (*this)[0] / magnitude;
            (*this)[1] = (*this)[1] / magnitude;
            (*this)[2] = (*this)[2] / magnitude;
        }

        inline Quaternion NLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const
        {
            Quaternion correctedDest(0,0,0);

            if(shortestPath && this->Dot(r) < 0)
                correctedDest = r * -1;
            else
                correctedDest = r;

            return Quaternion(Lerp(correctedDest, lerpFactor).Normalized());
        }

        inline Quaternion SLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const
        {
            static const float EPSILON = 1e3;

            float cos = this->Dot(r);
            Quaternion correctedDest(0,0,0);

            if(shortestPath && cos < 0)
            {
                cos *= -1;
                correctedDest = r * -1;
            }
            else
                correctedDest = r;

            if(fabs(cos) > (1 - EPSILON))
                return NLerp(correctedDest, lerpFactor, false);

            float sin = (float)sqrtf(1.0f - cos * cos);
            float angle = atan2(sin, cos);
            float invSin = 1.0f/sin;

            float srcFactor = sinf((1.0f - lerpFactor) * angle) * invSin;
            float destFactor = sinf((lerpFactor) * angle) * invSin;

            return Quaternion((*this) * srcFactor + correctedDest * destFactor);
        }

        inline Matrix4 ToRotationMatrix() const
        {
            Vector3 forward = Vector3(2.0f * (this->x * this->z - this->w * this->y), 2.0f * (this->y * this->z + this->w * this->x), 1.0f - 2.0f * (this->x * this->x + this->y * this->y));
            Vector3 up = Vector3(2.0f * (this->x*this->y + this->w*this->z), 1.0f - 2.0f * (this->x*this->x + this->z*this->z), 2.0f * (this->y*this->z - this->w*this->x));
            Vector3 right = Vector3(1.0f - 2.0f * (this->y*this->y + this->z*this->z), 2.0f * (this->x*this->y - this->w*this->z), 2.0f * (this->x*this->z + this->w*this->y));

            return Matrix4().InitRotationFromVectors(forward,up,right);
        }

        inline Vector3 GetEulerAngles() const
        {
//            TODO: Make this clear
            float heading, attitude, bank;
			float test = (*this).x*(*this).y + (*this).z*(*this).w;
            if (test > 0.499) { // singularity at north pole
                heading = 2 * atan2((*this).x,(*this).w);
                attitude = Math::pi/2;
                bank = 0;
                return Vector3(ToDegrees(bank), ToDegrees(heading), ToDegrees(attitude));
            }
            if (test < -0.499) { // singularity at south pole
                heading = -2 * atan2((*this).x,(*this).w);
                attitude = - Math::pi/2;
                bank = 0;
                return Vector3(ToDegrees(bank), ToDegrees(heading), ToDegrees(attitude));
            }
            float sqx = (*this).x*(*this).x;
			float sqy = (*this).y*(*this).y;
			float sqz = (*this).z*(*this).z;
            heading = atan2(2*(*this).y*(*this).w-2*(*this).x*(*this).z , 1 - 2*sqy - 2*sqz);
            attitude = asin(2*test);
            bank = atan2(2*(*this).x*(*this).w-2*(*this).y*(*this).z , 1 - 2*sqx - 2*sqz);

            return Vector3(ToDegrees(bank), ToDegrees(heading), ToDegrees(attitude));
        }

        inline Vector3 GetForward() const
        {
            return Vector3(0,0,1).Rotate(*this);
        }

        inline Vector3 GetBack() const
        {
            return Vector3(0,0,-1).Rotate(*this);
        }

        inline Vector3 GetUp() const
        {
            return Vector3(0,1,0).Rotate(*this);
        }

        inline Vector3 GetDown() const
        {
            return Vector3(0,-1,0).Rotate(*this);
        }

        inline Vector3 GetRight() const
        {
            return Vector3(1,0,0).Rotate(*this);
        }

        inline Vector3 GetLeft() const
        {
            return Vector3(-1,0,0).Rotate(*this);
        }

        inline Quaternion Conjugate() const { return Quaternion(-this->x, -this->y, -this->z, this->w); }

        inline Quaternion operator*(const Quaternion& r) const
        {
            const float _w = (this->w * r.w) - (this->x * r.x) - (this->y * r.y) - (this->z * r.z);
            const float _x = (this->x * r.w) + (this->w * r.x) + (this->y * r.z) - (this->z * r.y);
            const float _y = (this->y * r.w) + (this->w * r.y) + (this->z * r.x) - (this->x * r.z);
            const float _z = (this->z * r.w) + (this->w * r.z) + (this->x * r.y) - (this->y * r.x);

            return Quaternion(_x, _y, _z, _w);
        }

        inline Quaternion operator*(const Vector3& v) const
        {
            const float _w = - (this->x * v.x) - (this->y * v.y) - (this->z * v.z);
            const float _x =   (this->w * v.x) + (this->y * v.z) - (this->z * v.y);
            const float _y =   (this->w * v.y) + (this->z * v.x) - (this->x * v.z);
            const float _z =   (this->w * v.z) + (this->x * v.y) - (this->y * v.x);

            return Quaternion(_x, _y, _z, _w);
        }
        
		Quaternion& operator=(const btQuaternion& quaternion);
        operator btQuaternion() const;
    };

} /* Math */

#endif
