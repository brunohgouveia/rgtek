//
// Created by Bruno Gouveia on 12/14/15.
//

#pragma once

#include <Math/Vector3.h>

namespace Math
{

    class Circle
    {
    public:
        Circle(const Vector3& center, const Vector3& normal, float radius);

        Vector3 center;
        Vector3 normal;
        float   radius;
    };

}
