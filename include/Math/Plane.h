//
// Created by Bruno Gouveia on 12/11/15.
//

#pragma once

#include <Math/Vector3.h>

namespace Math
{

    class Plane
    {
    public:
        Plane();
        Plane(const Vector3& point, const Vector3& normal);

        Vector3 point;
        Vector3 normal;
    };

}