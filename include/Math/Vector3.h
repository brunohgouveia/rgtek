#ifndef RGTEK_MATH_VECTOR3_H__
#define RGTEK_MATH_VECTOR3_H__

class btVector3;

namespace Math
{
    class Vector4;
    class Quaternion;
    class Plane;
    class Circle;

	class Vector3
    {
    public:
        Vector3(float value = 0.0f);
        Vector3(float x, float y, float z);
        Vector3(const float* data);
        Vector3(const Vector3& vec);
        Vector3(const Vector4& vec);
        
        float Magnitude() const;
        float MagnitudeSq() const;
        Vector3 Normalized() const;
        
        float Dot(const Vector3& vec) const;
        Vector3 Cross(const Vector3& vec) const;
        Vector3 Rotate(float angle, const Vector3& axis) const;
        Vector3 Rotate(const Quaternion& rotation) const;
        Vector3 Lerp(const Vector3& vec, float factor) const;
        Vector3 Reflect(const Vector3& normal) const;

        Vector3 ProjectOnto(const Vector3& vec) const;
        Vector3 ProjectOnto(const Plane& plane) const;
        Vector3 ProjectOnto(const Circle& circle) const;

        Vector3 operator-() const;

        Vector3 operator+(const Vector3& vec) const;
        Vector3 operator-(const Vector3& vec) const;
        Vector3 operator*(const Vector3& vec) const;
        Vector3 operator/(const Vector3& vec) const;
        
        Vector3 operator+(float value) const;
        Vector3 operator-(float value) const;
        Vector3 operator*(float value) const;
        Vector3 operator/(float value) const;
        
        Vector3& operator+=(const Vector3& vec);
        Vector3& operator-=(const Vector3& vec);
        Vector3& operator*=(const Vector3& vec);
        Vector3& operator/=(const Vector3& vec);
        
        Vector3& operator+=(float value);
        Vector3& operator-=(float value);
        Vector3& operator*=(float value);
        Vector3& operator/=(float value);
        
        bool operator==(const Vector3& vec) const;
        bool operator!=(const Vector3& vec) const;

        operator float*() { return reinterpret_cast<float*>(&x);  }
        float& operator [] (unsigned int i) { return reinterpret_cast<float*>(&x)[i]; }
        float operator [] (unsigned int i) const { return reinterpret_cast<const float*>(&x)[i]; }
        
        Vector3& operator=(const btVector3& vec);
        operator btVector3() const;
        
        float x, y, z;

        static Vector3 zero;
        static Vector3 one;
        static Vector3 up;
        static Vector3 down;
        static Vector3 left;
        static Vector3 right;
        static Vector3 forward;
        static Vector3 back;
    };

    Vector3 operator*(float value, const Vector3& vec);

} /* Math */

#endif
