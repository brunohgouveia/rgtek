#ifndef RGTEK_MATH_MATH_H__
#define RGTEK_MATH_MATH_H__

#include <math.h>
#include <limits>
#include <cmath>
#include <string>

namespace Math
{
    const float pi = static_cast<float>(3.1415926535897932384626433832795);

    inline float ToRadians(float euler) { return euler * Math::pi / 180.0f; }
    inline float ToDegrees(float radians) { return radians * 180.0f / Math::pi; }

    inline float Abs(float f)          { return std::abs(f); }
    inline float Max(float a, float b) { return (a >= b)    ? a :  b; }
    inline float Min(float a, float b) { return (a <= b)    ? a :  b; }

    inline float Sqrt(float f) { return sqrtf(f); }

    inline int Ceil(float f)  { return static_cast<int>(ceilf(f)); }
    inline int Floor(float f) { return static_cast<int>(floorf(f)); }

    inline float Sin(float radians) { return static_cast<float>(sin(radians)); }
    inline float Cos(float radians) { return static_cast<float>(cos(radians)); }

    inline bool IsZero(float f)             { return Abs(f) < std::numeric_limits<float>::epsilon(); }
    inline bool IsEqual(float f1, float f2) { return IsZero(f1 - f2);  }

    inline float ParseToFloat(const char* number)
    {
        if (number == nullptr) return 0.0f;

        const char* numberCstr = number;

        float result = 0.0f;

        float sign = (number[0] == '-' ? -1.0f : 1.0f);

        // Integer part
        while (*numberCstr != '\0' && *numberCstr != '.')
        {
            if (isdigit(*numberCstr))
            {
                result *= 10.0f;
                result += static_cast<float>(*numberCstr - '0');
            }
            numberCstr++;
        }
        if (*numberCstr == '.') numberCstr++;

        float decimalBase = 10.0f;
        while (*numberCstr != '\0')
        {
            if (isdigit(*numberCstr))
            {
                result += static_cast<float>(*numberCstr - '0') / decimalBase;
                decimalBase *= 10.0f;
            }
            numberCstr++;
        }

        return result * sign;
    }
    inline float ParseToFloat(const std::string& number) { return ParseToFloat(number.c_str()); }

    inline int ParseToInteger(const char* number)
    {
        if (number == nullptr) return 0;

        const char* numberCstr = number;

        int result = 0;

        int sign = (number[0] == '-' ? -1 : 1);

        // Integer part
        while (*numberCstr != '\0' && *numberCstr != '.')
        {
            if (isdigit(*numberCstr))
            {
                result *= 10;
                result += static_cast<int>(*numberCstr - '0');
            }
            numberCstr++;
        }

        return result * sign;
    }
    inline int ParseToInteger(const std::string& number) { return ParseToInteger(number.c_str()); }

} /* Math */

#endif
