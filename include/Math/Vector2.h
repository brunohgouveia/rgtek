#ifndef RGTEK_MATH_VECTOR2_H__
#define RGTEK_MATH_VECTOR2_H__

namespace Math
{

    class Vector2
    {
    public:
        Vector2(float value = 0.0f);
        Vector2(float x, float y);
        Vector2(const float* data);
        Vector2(const Vector2& vec);
        
        float Magnitude() const;
        float MagnitudeSq() const;
        Vector2 Normalized() const;
        
        float Dot(const Vector2& vec) const;
        Vector2 Lerp(const Vector2& vec, float factor) const;
        Vector2 Reflect(const Vector2& normal) const;
        
        Vector2 operator+(const Vector2& vec) const;
        Vector2 operator-(const Vector2& vec) const;
        Vector2 operator*(const Vector2& vec) const;
        Vector2 operator/(const Vector2& vec) const;
        
        Vector2 operator+(float value) const;
        Vector2 operator-(float value) const;
        Vector2 operator*(float value) const;
        Vector2 operator/(float value) const;
        
        Vector2& operator+=(const Vector2& vec);
        Vector2& operator-=(const Vector2& vec);
        Vector2& operator*=(const Vector2& vec);
        Vector2& operator/=(const Vector2& vec);
        
        Vector2& operator+=(float value);
        Vector2& operator-=(float value);
        Vector2& operator*=(float value);
        Vector2& operator/=(float value);
        
        bool operator==(const Vector2& vec) const;
        bool operator!=(const Vector2& vec) const;
        
        float& operator [] (unsigned int i) { return reinterpret_cast<float*>(&x)[i]; }
        float operator [] (unsigned int i) const { return reinterpret_cast<const float*>(&x)[i]; }
        
        float x, y;
    };

    Vector2 operator*(float value, const Vector2& vec);

} /* Math */

#endif
