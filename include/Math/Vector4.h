#ifndef RGTEK_MATH_VECTOR4_H__
#define RGTEK_MATH_VECTOR4_H__

namespace Math
{
    class Vector3;
    
    class Vector4
    {
    public:
        explicit Vector4(float value = 0.0f);
        explicit Vector4(float x, float y, float z, float w = 1.0f);
        explicit Vector4(const float* data);
        Vector4(const Vector4& vec);
        explicit Vector4(const Vector3& vec, float w = 1.0f);
        
        float Magnitude() const;
        float MagnitudeSq() const;
        Vector4 Normalized() const;
        
        float Dot(const Vector4& vec) const;
        Vector4 Lerp(const Vector4& vec, float factor) const;
        Vector4 Reflect(const Vector4& normal) const;
        
        Vector4 operator+(const Vector4& vec) const;
        Vector4 operator-(const Vector4& vec) const;
        Vector4 operator*(const Vector4& vec) const;
        Vector4 operator/(const Vector4& vec) const;
        
        Vector4 operator+(float value) const;
        Vector4 operator-(float value) const;
        Vector4 operator*(float value) const;
        Vector4 operator/(float value) const;
        
        Vector4& operator+=(const Vector4& vec);
        Vector4& operator-=(const Vector4& vec);
        Vector4& operator*=(const Vector4& vec);
        Vector4& operator/=(const Vector4& vec);
        
        Vector4& operator+=(float value);
        Vector4& operator-=(float value);
        Vector4& operator*=(float value);
        Vector4& operator/=(float value);
        
        bool operator==(const Vector4& vec) const;
        bool operator!=(const Vector4& vec) const;

        explicit operator float*() { return reinterpret_cast<float*>(&x); }
        float& operator [] (unsigned int i) { return reinterpret_cast<float*>(&x)[i]; }
        float operator [] (unsigned int i) const { return reinterpret_cast<const float*>(&x)[i]; }
        
        float x, y, z, w;
    };

} /* Math */

#endif
