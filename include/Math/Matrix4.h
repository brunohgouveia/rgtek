#ifndef RGTEK_MATH_MATRIX4_H__
#define RGTEK_MATH_MATRIX4_H__

#include <Math/Math.h>
#include <Math/Vector3.h>
#include <Math/Vector4.h>

namespace Math
{

    class Matrix4
    {
    public:
        Matrix4();
        Matrix4(const float* data);
        Matrix4(const Matrix4& mat);

        Matrix4 InitIdentity();
        Matrix4 InitScale(const Vector3& r);
        Matrix4 InitTranslation(const Vector3& r);
        
        Matrix4 InitRotationEuler(float rotateX, float rotateY, float rotateZ);
        Matrix4 InitRotationFromVectors(const Vector3& n, const Vector3& v, const Vector3& u);
        Matrix4 InitRotationFromDirection(const Vector3& forward, const Vector3& up);
        Matrix4 InitPerspective(float fov, float aspectRatio, float zNear, float zFar);
        Matrix4 InitOrthographic(float left, float right, float bottom, float top, float near, float far);

        Matrix4 Inverse() const;
        Matrix4 Transpose() const;
        
        Matrix4 operator*(const Matrix4& r) const;
        Vector4 operator*(const Vector4& r) const;
        
        Vector4 Transform(const Vector4& r) const;
        Vector3 Transform(const Vector3& r) const;
        
        inline void Set(unsigned int x, unsigned int y, float val) { m[x][y] = val; }
        
        inline operator const float*() const { return static_cast<const float*>(m[0]); }
        inline operator float*() { return static_cast<float*>(m[0]); }
        
        inline const float* operator[](int index) const { return m[index]; }
        inline float* operator[](int index) { return m[index]; }
        
    private:
        float m[4][4];
    };

} /* Math */

#endif
