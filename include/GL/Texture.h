#ifndef TEXUTRE_H__
#define TEXUTRE_H__

#include <Core/Platform.h>

namespace RGTek
{
	typedef int GLint;
	typedef unsigned int GLuint;
    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);
    FWD_CLASS_EXTEND_PTR_TYPES(Texture);

    class Texture: public Object
    {
    public:
        Texture(const String& textureName, GLuint textureUnit, int width, int height, bool canBeRenderTarget, ResourceManagerWeakPtr resourceManager = nullptr);
        Texture(const String& textureFile = "blankTexture.bmp", GLuint textureUnit = 0, ResourceManagerWeakPtr resourceManager = nullptr);
        ~Texture();

		void Bind();
		void Active();
        void BindAsRenderTarget();

        inline GLuint GetUnit() { return mTextureUnit; }
        operator GLuint() const { return mTextureName; }

    private:
        void LoadFile(const String& textureFile, bool requireMipMap = true);

        GLuint mFramebuffer;
        GLuint mTextureName;
        GLuint mTextureUnit;

        int    mWidth;
        int    mHeight;
    };

}

#endif // TEXUTRE_H__
