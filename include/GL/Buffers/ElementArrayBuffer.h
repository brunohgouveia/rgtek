#ifndef ELEMENTARRAYMBUFFER_H__
#define ELEMENTARRAYMBUFFER_H__

#include <Core/Platform.h>

namespace GL {

	template<typename T>
	class ElementArrayBuffer {
		private:
			GLuint bufferName;
			GLenum usage;
		public:
			// Constructor
			ElementArrayBuffer(GLenum usage);
			~ElementArrayBuffer();

			void init(T * data, GLsizeiptr size);
			void bind();
			void enableVertexAttribArray(GLuint index);
			void disableVertexAttribArray(GLuint index);
			void vertexAttribPointer(GLuint index, GLuint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid * pointer);
			void subData(T * data, GLintptr offset, GLsizeiptr size);

	};

	template<typename T>
	ElementArrayBuffer<T>::ElementArrayBuffer(GLenum usage) {
		this->bufferName = -1;
		this->usage = usage;
	}

	template<typename T>
	ElementArrayBuffer<T>::~ElementArrayBuffer() {
		glDeleteBuffers(1, &bufferName);
	}

	template<typename T>
	void ElementArrayBuffer<T>::bind() {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferName);
	}

	template<typename T>
	void ElementArrayBuffer<T>::init(T * data, GLsizeiptr size) {
		// Generate and bind buffer
		glGenBuffers(1, &bufferName);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferName);

		// Initialize buffer
		if (data) {
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(T), data, usage);
		} else {
			T blankData[size];
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, &blankData, usage);
		}

		// Unbind buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	template<typename T>
	void ElementArrayBuffer<T>::enableVertexAttribArray(GLuint index) {
		glEnableVertexAttribArray(index);
	}

	template<typename T>
	void ElementArrayBuffer<T>::disableVertexAttribArray(GLuint index) {
		glDisableVertexAttribArray(index);
	}

	template<typename T>
	void ElementArrayBuffer<T>::vertexAttribPointer(GLuint index, GLuint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid * pointer) {
		glVertexAttribPointer(index, size, type, normalized, stride, pointer);
	}

	template<typename T>
	void ElementArrayBuffer<T>::subData(T * data, GLintptr offset, GLsizeiptr size) {
		// Bind buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferName);

		// Copy data
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset * sizeof(T), size * sizeof(T), data);

		// Unbind buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

}

#endif
