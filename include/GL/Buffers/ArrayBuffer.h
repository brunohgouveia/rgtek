#ifndef ARRAYMBUFFER_H__
#define ARRAYMBUFFER_H__

#include <Core/Platform.h>
#include <vector>

namespace GL 
{
	typedef unsigned int GLuint;
	typedef int GLint;
	typedef int GLsizei;
	typedef ptrdiff_t GLsizeiptr;
	typedef ptrdiff_t GLintptr;
	typedef unsigned int GLenum;
	typedef unsigned char GLboolean;

	/////////////////////////////////////////////////////////////////
	// ArrayBufferBase - Implement all functionalities, but it works
	// with a opaque pointer object (void*). It's probably a better
	// idea to use the template ArrayBuffer<T>, which is type safe.
	// ============================================================
	class ArrayBufferBase
	{
	public:
		explicit ArrayBufferBase(size_t sizeT);

		void Bind() const;
		void Unbind() const;
		void EnableVertexAttribArray(GLuint index) const;
		void DisableVertexAttribArray(GLuint index) const;
		void VertexAttribPointer(GLuint index, GLuint size, GLenum type, GLboolean normalized, GLsizei stride, GLuint pointer) const;
		void Draw(GLint first, GLsizei count);

	protected:
		void InitPrivate(void* data, GLsizeiptr size, GLenum usage);
		void DataPrivate(void* data, GLsizeiptr size);
		void SubDataPrivate(void* data, GLintptr offset, GLsizeiptr size);

	private:
		GLuint mBufferName;
		GLenum mUsage;
		size_t mSizeT;
	};

	/////////////////////////////////////////////////////////////////
	// ArrayBuffer - a type safe wrapper to ArrayBufferBase class
	// ============================================================
	template<typename T>
	class ArrayBuffer: public ArrayBufferBase 
	{
		public:
			ArrayBuffer() : ArrayBufferBase(sizeof(T)) {}

			void Init(T * data, GLsizeiptr size, GLenum usage);
			void Data(T * data, GLsizeiptr size);
			void SubData(T * data, GLintptr offset, GLsizeiptr size);
	};

	template<typename T>
	void ArrayBuffer<T>::Init(T * data, GLsizeiptr size, GLenum usage) 
	{
		InitPrivate(data, size, usage);
	}


	template<typename T>
	void ArrayBuffer<T>::Data(T * data, GLsizeiptr size) 
	{
		DataPrivate(data, size);
	}

	template<typename T>
	void ArrayBuffer<T>::SubData(T * data, GLintptr offset, GLsizeiptr size) 
	{
		SubDataPrivate(data, offset, size);
	}
}

#endif
