#ifndef UNIFORMBUFFER_H__
#define UNIFORMBUFFER_H__

#include <Core/Platform.h>
#include <string>
#include <vector>

namespace GL {

	template<typename T>
	class UniformBuffer {
		private:
			// Static attributes
			static int currentBindingPoint;

			// Non-static attributes
			std::string blockName;
			GLuint bufferName;
			GLenum usage;
			GLuint bindingPoint;
		public:

			void init(std::string blockName, std::vector<T> data, GLenum usage);
			void init(std::string blockName, T * data, unsigned int size, GLenum usage);
			void bindProgram(GLuint program);
			void subData(T * data, GLintptr offset, GLsizeiptr size);
	};

// Static attributes
	template<typename T>
	int UniformBuffer<T>::currentBindingPoint = 1;

// Methods
	template<typename T>
	void UniformBuffer<T>::init(std::string blockName, std::vector<T> data, GLenum usage) {
		// Set attribute
		this->blockName = blockName;
		this->usage = usage;

		// Generate and bind buffer
		glGenBuffers(1, &bufferName);
		glBindBuffer(GL_UNIFORM_BUFFER, bufferName);

		// Initialize data
		glBufferData(GL_UNIFORM_BUFFER, data.size() * sizeof(T), &data[0], usage);

		// Set binding point
		bindingPoint = currentBindingPoint++;
		glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, bufferName);

		// Unbind data
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	template<typename T>
	void UniformBuffer<T>::init(std::string blockName, T * data, unsigned int size, GLenum usage) {
		// Set attribute
		this->blockName = blockName;
		this->usage = usage;

		// Generate and bind buffer
		glGenBuffers(1, &bufferName);
		glBindBuffer(GL_UNIFORM_BUFFER, bufferName);

		// Initialize data
		glBufferData(GL_UNIFORM_BUFFER, size * sizeof(T), data, usage);

		// Set binding point
		bindingPoint = currentBindingPoint++;
		glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, bufferName);

		// Unbind data
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	template<typename T>
	void UniformBuffer<T>::bindProgram(GLuint program) {
		// Find block index
		int id = glGetUniformBlockIndex(program, blockName.c_str());
		// Bind if block exists
		if (id >= 0) {
			glUniformBlockBinding(program, id, bindingPoint);
		}

	}

	template<typename T>
	void UniformBuffer<T>::subData(T * data, GLintptr offset, GLsizeiptr size) {
		// Bind buffer
		glBindBuffer(GL_UNIFORM_BUFFER, bufferName);

		// Copy data
		glBufferSubData(GL_UNIFORM_BUFFER, offset * sizeof(T), size * sizeof(T), data);

		// Unbind buffer
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

}

#endif
