#ifndef RGTEK_COLOR_H__
#define RGTEK_COLOR_H__

namespace RGTek
{
    class Color
    {
    public:
        Color(float gray = 0.0f);
        Color(float red, float green, float blue);
        Color(float red, float green, float blue, float alplha);
        Color(const Color& color);
        ~Color();

        bool operator==(const Color& other) const;
        bool operator!=(const Color& other) const;

        bool operator<(const Color& other) const;
        bool operator>(const Color& other) const;
        bool operator<=(const Color& other) const;
        bool operator>=(const Color& other) const;

        float r, g, b, a;
        
        static Color black;
        static Color white;
        static Color blue;
        static Color red;
        static Color green;
        static Color gray;
        static Color yellow;
    };
}


#endif
