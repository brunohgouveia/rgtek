//
// Created by Bruno Gouveia on 12/11/15.
//

#pragma once

#include <Editor/Handle3D.h>
#include <Math/Plane.h>

namespace RGTek
{

    class RotateHandle3D: public Handle3D
    {
    public:
        RotateHandle3D();
        virtual ~RotateHandle3D();

        virtual void OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection);

		/**
		 * Not supported on RotateHandle3D. This will always return Axis::None.
		 */
		virtual Handle3D::Axis GetAxisIntersectedByRay(const Ray& mouseClickRay) override;

        virtual float GetAxisLength(Axis axis) const;

    private:
        virtual void OnSetSize();
        virtual void OnSetTransform();

        Plane   mIntersectionPlane;
        Vector3 mIntersectionTangent;
        Vector3 mCurrentIntersectionDirectionProjected;
    };

}