//
// Created by Bruno Gouveia on 1/6/16.
//

#pragma once

#include <Core/Platform.h>
#include <Rendering/CEGUI.h>
#include <Math/Vector3.h>
#include <Math/Quaternion.h>
#include <GL/Color.h>
#include <Editor/EditorItem/PropertyDrawer.h>
#include <functional>
#include <unordered_map>

namespace RGTek
{
    using namespace Math;

    class Inspector;
    class Component;
	class Transform;

    class EditorItem
    {
    public:
		EditorItem(Transform* transform, Inspector* inspector);
        EditorItem(Component* component, Inspector* inspector);
        virtual ~EditorItem();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        virtual void OnInspectorGUI();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        template <typename T, typename Getter, typename Setter>
	    void FloatField(const String& label, T* object, Getter getter, Setter setter);

	    template <typename T, typename Getter, typename Setter>
	    void IntField(const String& label, T* object, Getter getter, Setter setter);

	    template <typename T, typename Getter, typename Setter>
	    void StringField(const String& label, T* object, Getter getter, Setter setter);

	    template <typename T, typename Getter, typename Setter>
	    void Vector3Field(const String& label, T* object, Getter getter, Setter setter);

	    template <typename T, typename Getter, typename Setter>
	    void QuaternionField(const String& label, T* object, Getter getter, Setter setter);

	    template <typename T, typename Getter, typename Setter>
	    void ColorField(const String& label, T* object, Getter getter, Setter setter);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		void FloatField(const String& label, FloatPropertyDrawer::Getter getter, FloatPropertyDrawer::Setter setter);
		void IntField(const String& label, IntPropertyDrawer::Getter getter, IntPropertyDrawer::Setter setter);
		void StringField(const String& label, StringFieldLayout::Getter getter, StringFieldLayout::Setter setter);
		void Vector3Field(const String& label, Vector3PropertyDrawer::Getter getter, Vector3PropertyDrawer::Setter setter);
		void QuaternionField(const String& label, QuaternionPropertyDrawer::Getter getter, QuaternionPropertyDrawer::Setter setter);
		void ColorField(const String& label, ColorPropertyDrawer::Getter getter, ColorPropertyDrawer::Setter setter);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    void BuildLayout();
        void Synchronize();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        Component* GetGameComponent() const { return reinterpret_cast<Component*>(mGameComponent); }

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        CEGUI::Window* GetEditorItemLabel()  const;
        CEGUI::Window* GetEditorItemLayout() const;
        CEGUI::Window* GetEditorItemTitle() const;
        CEGUI::Window* GetEditorItemDeleteButton() const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	protected:
        String mItemName;
        Inspector* mInspector;
		Transform* mTransform;
        Component* mGameComponent;

        std::vector<PropertyDrawer*>  mPropertyDrawers;
    };

	template <typename T, typename Getter, typename Setter>
	void EditorItem::FloatField(const String& label, T* object, Getter getter, Setter setter)
	{
		FloatPropertyDrawer::Getter callableGetter = std::bind(getter, object);
		FloatPropertyDrawer::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		FloatField(label, callableGetter, callableSetter);
	}

	template <typename T, typename Getter, typename Setter>
	void EditorItem::IntField(const String& label, T* object, Getter getter, Setter setter)
	{
		IntPropertyDrawer::Getter callableGetter = std::bind(getter, object);
		IntPropertyDrawer::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		IntField(label, callableGetter, callableSetter);
	}

	template <typename T, typename Getter, typename Setter>
	void EditorItem::StringField(const String& label, T* object, Getter getter, Setter setter)
	{
		StringFieldLayout::Getter callableGetter = std::bind(getter, object);
		StringFieldLayout::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		StringField(label, callableGetter, callableSetter);
	}

	template <typename T, typename Getter, typename Setter>
	void EditorItem::Vector3Field(const String& label, T* object, Getter getter, Setter setter)
	{
		Vector3PropertyDrawer::Getter callableGetter = std::bind(getter, object);
		Vector3PropertyDrawer::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		Vector3Field(label, callableGetter, callableSetter);
	}

	template <typename T, typename Getter, typename Setter>
	void EditorItem::QuaternionField(const String& label, T* object, Getter getter, Setter setter)
	{
		QuaternionPropertyDrawer::Getter callableGetter = std::bind(getter, object);
		QuaternionPropertyDrawer::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		QuaternionField(label, callableGetter, callableSetter);
	}

	template <typename T, typename Getter, typename Setter>
	void EditorItem::ColorField(const String& label, T* object, Getter getter, Setter setter)
	{
		ColorPropertyDrawer::Getter callableGetter = std::bind(getter, object);
		ColorPropertyDrawer::Setter callableSetter = std::bind(setter, object, std::placeholders::_1);
		ColorField(label, callableGetter, callableSetter);
	}
}
