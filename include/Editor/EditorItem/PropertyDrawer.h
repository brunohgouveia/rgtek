#pragma once

#include <Core/Platform.h>
#include <Math/Vector3.h>
#include <Math/Quaternion.h>
#include <GL/Color.h>
#include <functional>

namespace CEGUI
{
    class Window;
}

namespace RGTek
{
    using namespace Math;
    class EditorItem;

    class PropertyDrawer
    {
    public:
        PropertyDrawer(EditorItem* editorItem, const char* label);
        virtual ~PropertyDrawer();

		void BuildLayout();

        virtual const char* GetLayoutFileName() = 0;
        virtual void Synchronize() = 0;

    protected:
        EditorItem* mEditorItem;
        CEGUI::Window* mWindowLayout;
		String mLabel;
    };

    template <typename T>
    class CustomPropertyDrawer: public PropertyDrawer
    {
    public:
        using Getter = std::function<T(void)>;
        using Setter = std::function<void(T)>;

        CustomPropertyDrawer(EditorItem* editorItem, const char* label, Getter getter, Setter setter)
            : PropertyDrawer(editorItem, label)
            , mGetter(getter)
            , mSetter(setter)
        {

        }
        virtual ~CustomPropertyDrawer() {}

	    const char* GetLayoutFileName() override = 0;

		virtual void Synchronize() override = 0;

    protected:
        Getter mGetter;
        Setter mSetter;
    };

	class IntPropertyDrawer : public CustomPropertyDrawer<int>
	{
	public:
		IntPropertyDrawer(EditorItem* editorItem, const char* label, Getter getter, Setter setter)
			: CustomPropertyDrawer<int>(editorItem, label, getter, setter)
		{
			// Do nothing
		}
		virtual ~IntPropertyDrawer()
		{
			// Do nothing
		}

		virtual const char* GetLayoutFileName() override { return "properties/int.layout"; }
		virtual void Synchronize() override;
	};

    class FloatPropertyDrawer: public CustomPropertyDrawer<float>
    {
    public:
        FloatPropertyDrawer (EditorItem* editorItem, const char* label, Getter getter, Setter setter)
            : CustomPropertyDrawer<float>(editorItem, label, getter, setter)
        {
            // Do nothing
        }
        virtual ~FloatPropertyDrawer ()
        {
            // Do nothing
        }

        virtual const char* GetLayoutFileName() override { return "properties/float.layout"; }
		virtual void Synchronize() override;
    };

	class StringFieldLayout : public CustomPropertyDrawer<String>
	{
	public:
		StringFieldLayout(EditorItem* editorItem, const char* label, Getter getter, Setter setter)
			: CustomPropertyDrawer<String>(editorItem, label, getter, setter)
		{
			// Do nothing
		}
		virtual ~StringFieldLayout()
		{
			// Do nothing
		}

		virtual const char* GetLayoutFileName() override { return "properties/string.layout"; }
		virtual void Synchronize() override;
	};

    class Vector3PropertyDrawer: public CustomPropertyDrawer<Vector3>
    {
    public:
        Vector3PropertyDrawer (EditorItem* editorItem, const char* label, Getter getter, Setter setter)
            : CustomPropertyDrawer<Vector3>(editorItem, label, getter, setter)
        {
            // Do nothing
        }
        virtual ~Vector3PropertyDrawer ()
        {
            // Do nothing
        }

        virtual const char* GetLayoutFileName() override { return "properties/vector3.layout"; }
		virtual void Synchronize() override;
    };

	class QuaternionPropertyDrawer : public CustomPropertyDrawer<Quaternion>
	{
	public:
		QuaternionPropertyDrawer(EditorItem* editorItem, const char* label, Getter getter, Setter setter)
			: CustomPropertyDrawer<Quaternion>(editorItem, label, getter, setter)
		{
			// Do nothing
		}
		virtual ~QuaternionPropertyDrawer()
		{
			// Do nothing
		}

		virtual const char* GetLayoutFileName() override { return "properties/quaternion.layout"; }
		virtual void Synchronize() override;
	};


    class ColorPropertyDrawer: public CustomPropertyDrawer<Color>
    {
    public:
        ColorPropertyDrawer (EditorItem* editorItem, const char* label, Getter getter, Setter setter)
            : CustomPropertyDrawer<Color>(editorItem, label, getter, setter)
        {
            // Do nothing
        }
        virtual ~ColorPropertyDrawer ()
        {
            // Do nothing
        }

        virtual const char* GetLayoutFileName() override { return "properties/color.layout"; }
		virtual void Synchronize() override;
    };
}
