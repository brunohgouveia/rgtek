#pragma once

#include <Core/Platform.h>
#include <Editor/EditorItem.h>

namespace RGTek
{
    class EditorItemManager
    {
    public:
        using EditorItemInstantiator = EditorItem* (*)(Component*, Inspector*);
        
        void RegisterEditorItemInstantiator(String className, EditorItemInstantiator instantiator)
        {
            mEditorItemInstantiatorMap.insert(std::make_pair(className, instantiator));
        }
        		
        EditorItemInstantiator GetEditorItemInstantiator(String className)
        {
            return mEditorItemInstantiatorMap[className];
        }

    private:
        std::unordered_map<String, EditorItemInstantiator> mEditorItemInstantiatorMap;
    };   
}