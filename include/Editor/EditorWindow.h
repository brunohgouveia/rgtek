//
// Created by Bruno Gouveia on 11/22/15.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Viewport.h>
#include <Core/Window.h>
#include <Editor/SceneView.h>
#include <Editor/EditorSystem.h>
#include <Math/Vector2.h>

namespace CEGUI
{
    class Window;
}

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(EditorSystem);
    FWD_CLASS_EXTEND_PTR_TYPES(EditorWindow);

    class EditorWindow: public Window
    {
    public:
        EditorWindow(const String& title, int width, int height, int argc, char** argv);
        EditorWindow(const EditorWindow&) = delete;
        virtual ~EditorWindow();

        virtual void Render();
        virtual void HandleEvents();

        CEGUI::Window* GetInspectorWindow();

        inline void SetSceneViewCallback(std::function<void(Vector2,Vector2)> sceneViewCallback) { mSceneViewCallback = sceneViewCallback; }
        inline void SetEditorSystem(const EditorSystemWeakPtr editorSystem) { mEditorSystem = editorSystem; }

        void operator=(const EditorWindow& other) = delete;
    protected:
        void InitCEGUI();

        void FillGraphSceneWindow();

        friend class Application;

    private:
        class CEGUI::Window* mCanvasWindow;

        std::function<void(Vector2,Vector2)> mSceneViewCallback;

        EditorSystemWeakPtr mEditorSystem;

    };

}
