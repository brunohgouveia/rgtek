//
// Created by Bruno Gouveia on 12/8/15.
//

#ifndef RGTEK_SCALEHANDLE3D_H
#define RGTEK_SCALEHANDLE3D_H

#include <Editor/Handle3D.h>

namespace RGTek
{

    class ScaleHandle3D: public AxisBaseHandle3D
    {
    public:
        ScaleHandle3D();
        virtual ~ScaleHandle3D();

        virtual void OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection);

        virtual float GetAxisLength(Axis axis) const;

    private:
        Vector3 mFirstCenterIntersectionPoint;
        Vector3 mFirstIntersectionPoint;
        Vector3 mCurrentCenterIntersectionPoint;
        Vector3 mCurrentIntersectionPoint;
    };

}

#endif //RGTEK_SCALEHANDLE3D_H
