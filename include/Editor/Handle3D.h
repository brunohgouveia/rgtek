//
// Created by Bruno Gouveia on 12/4/15.
//

#ifndef RGTEK_HANDLE3D_H
#define RGTEK_HANDLE3D_H

#include <Core/Platform.h>
#include <Core/Transform.h>
#include <Core/Ray.h>

namespace RGTek
{
    FWD_CLASS_EXTEND_PTR_TYPES(Handle3D);

    class Handle3D: public Object
    {
    public:
        typedef int ControlID;

        enum Axis
        {
            None,
            X,
            Y,
            Z,
            All
        };

        Handle3D();
        ~Handle3D();

        virtual void OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection) = 0;
        virtual void OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection) = 0;
        virtual void OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection) = 0;

		virtual Handle3D::Axis GetAxisIntersectedByRay(const Ray& mouseClickRay) = 0;

        virtual float GetAxisLength(Axis axis) const;

        Vector3 GetRayIntersectionPoint(const Ray& ray, const Vector3& viewDirection);

        Axis GetSelectedAxis() const;
        float GetSize() const;
        Transform GetTransform() const;
        TransformWeakPtr GetCameraTransform() const;

        void SetSelectedAxis(Axis axis);
        void SetSize(float size);
        void SetTransform(const Transform& transform);
        void SetCameraTransform(TransformWeakPtr transform);

    protected:
		virtual void OnSetSize() {}
		virtual void OnSetTransform() {}

        Transform        mTransform;
        TransformWeakPtr mCameraTransform;
        Axis             mSelectedAxis;
        float            mSize;
    };

	class AxisBaseHandle3D : public Handle3D
	{
	public:
		virtual Handle3D::Axis GetAxisIntersectedByRay(const Ray& mouseClickRay) override;

		/**
		 * Does this kind of handle have a "All" axis?
		 */
		virtual bool HasAllAxis() const { return true; }

		virtual void OnSetTransform() override;
	protected:
		Transform xAxisTransform;
		Transform yAxisTransform;
		Transform zAxisTransform;
	};

}

#endif //RGTEK_HANDLE3D_H
