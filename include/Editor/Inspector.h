//
// Created by Bruno Gouveia on 1/14/16.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Entity.h>
#include <Editor/EditorItem.h>
#include <Editor/EditorItem/EditorItemManager.h>

namespace CEGUI
{
    class Window;
}

namespace RGTek
{
    class EditorSystem;
    class EditorItemManager;
    class Entity;

    class Inspector
    {
    public:
        Inspector(EditorSystem* editorSystem, CEGUI::Window* window);
        ~Inspector();

        void UpdateItems();

        void Populate(Entity* entity);
        void CleanUp();

    private:
        friend class EditorItem;
		void AddEditorItem(Component* component);
        void DeleteEditorItem(EditorItem* editorItem);

        CEGUI::Window* CreateAddComponentContextMenu(Entity* entity);

        CEGUI::Window* GetEditorItemsLayout();
        CEGUI::Window* GetMainLayout();

        EditorSystem* mEditorSystem;
        Entity *         mEntity;
        Vector<EditorItem*> mItems;
        CEGUI::Window* mInspectorWindow;
        CEGUI::Window* mInspectorWindowContent;
    };

}