//
// Created by Bruno Gouveia on 12/2/15.
//

#ifndef RGTEK_EDITORSystem_H
#define RGTEK_EDITORSystem_H

#include <Core/Platform.h>
#include <Core/System.h>
#include <Core/Entity.h>
#include <Rendering/RenderingSystem.h>
#include <Editor/SceneView.h>
#include <Editor/MoveHandle3D.h>
#include <Editor/RotateHandle3D.h>
#include <Editor/ScaleHandle3D.h>
#include <Core/ViewPort.h>
#include <Physics/PhysicsSystem.h>
#include <ScriptSystem/ScriptSystem.h>
#include <Editor/Inspector.h>
#include <Editor/EditorItem/EditorItemManager.h>
#include <Core/Scene.h>

namespace CEGUI
{
    class Window;
}

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(EditorSystem);
    FWD_CLASS_EXTEND_PTR_TYPES(EditorWindow);

    class EditorSystem: public Object
    {
    public:
        enum Mode
        {
            Editing,
            Playing
        };

        enum Tool
        {
            View,
            Move,
            Rotate,
            Scale
        };

        EditorSystem(EditorWindowWeakPtr window, RenderingSystemWeakPtr renderingSystem, PhysicsSystemWeakPtr physicsSystem, ScriptSystemWeakPtr scriptSystem);

        void AddSceneView(const SceneViewPtr& sceneView);
        void DrawSceneView(unsigned int sceneViewIndex, Viewport viewport);

        void DrawAxis(const Vector3& position, const Quaternion& rotation, float size);
        void DrawAxis(const Vector3& position, const Quaternion& rotation, const Vector3& size);

        void DrawWireArc(const Vector3& center, const Vector3& normal, const Vector3& direction, float angle, float radius);
        void DrawWireDisc(const Vector3& center, const Vector3& normal, float radius, bool culling = false);

        Vector3 PositionHandle(const Vector3& position, const Quaternion& rotation, float size);
        Vector3 ScaleHandle(const Vector3& scale, const Vector3& position, const Quaternion& rotation, float size);
        Quaternion RotationHandle(const Quaternion& rotation, const Vector3& position, float size);

        Inspector& GetInspector() { return mInspector; }
        Mode GetMode();
        Entity* GetSelectedEntity() const;
        SceneViewWeakPtr GetSceneView(unsigned int sceneViewIndex) { return mSceneViews[sceneViewIndex]; }

        EditorItemManager& GetEditorItemManager() { return mEditorItemManager; }

        void SetTool(Tool tool);
        void SetMode(Mode mode);
        void SetSelectedEntity(Entity* entity);

        SceneWeakPtr GetCurrentScene() { return mCurrentScene; }
        void SetCurrentScene(SceneWeakPtr scene) { mCurrentScene = scene;}
        void SaveCurrentScene() const;
        void LoadCurrentScene() const;

        void RecomputedSelectedEntityReference();

    private:
        void ProcessInput();
        void SetCurrentHandle3D(Handle3D* handle3D);

        void DrawMoveTool();
        void DrawScaleTool();
        void DrawRotateTool();

        RenderingSystemWeakPtr mRenderingSystem;
        PhysicsSystemWeakPtr   mPhysicsSystem;
        ScriptSystemWeakPtr    mScriptSystem;
        SceneWeakPtr           mCurrentScene;

        Inspector              mInspector;
        EditorItemManager      mEditorItemManager;

        Mode                   mMode;
        Tool                   mCurrentTool;

        MoveHandle3D           mMoveHandle3D;
        RotateHandle3D         mRotateHandle3D;
        ScaleHandle3D          mScaleHandle3D;
        Handle3D*              mCurrentHandle3D;

        ShaderHandle           mDrawPrimitive;
        MeshHandle             mConeMeshHandle;
        MeshHandle             mCubeMeshHandle;

        Vector<SceneViewPtr>   mSceneViews;
        Entity*                mSelectedEntity;
        String                 mSelectedEntityName;

        Color                  mHandleColor;
    };

}

#endif //RGTEK_EDITORSystem_H
