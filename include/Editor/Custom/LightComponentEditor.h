
#pragma once

#include <Editor/EditorItem.h>
#include <Rendering/Components/LightComponent.h>

namespace RGTek
{
    class LightComponentEditor: public EditorItem
    {
    public:
        static EditorItem* InstantiateEditorItem(Component* component, Inspector* inspector)
        {
            return new LightComponentEditor(component, inspector);
        }

        LightComponentEditor(Component* component, Inspector* inspector)
            : EditorItem(component, inspector)
        {

        }

	    void OnInspectorGUI() override
        {
            LightComponent* lightComponent = static_cast<LightComponent*>(mGameComponent);
			SpotLight* light = dynamic_cast<SpotLight*>(lightComponent->GetLight());

            ColorField("Color", lightComponent, &LightComponent::GetColor, &LightComponent::SetColor);
			FloatField("Range", light, &SpotLight::GetRange, &SpotLight::SetRange);
        }
    };
}
