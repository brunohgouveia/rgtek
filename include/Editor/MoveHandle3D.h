//
// Created by Bruno Gouveia on 12/8/15.
//

#ifndef RGTEK_MOVEHANDLE3D_H
#define RGTEK_MOVEHANDLE3D_H

#include <Editor/Handle3D.h>
#include <Core/Transform.h>

namespace RGTek
{
    class MoveHandle3D: public AxisBaseHandle3D
    {
    public:
        MoveHandle3D();
        virtual ~MoveHandle3D();

        virtual void OnMouseDown(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseDrag(const Ray& mouseClickDirection, const Vector3& viewDirection);
        virtual void OnMouseUp(const Ray& mouseClickDirection, const Vector3& viewDirection);

		virtual bool HasAllAxis() const override { return false; }

    private:
        Vector3 mLastIntersectionPoint;
    };

}

#endif //RGTEK_MOVEHANDLE3D_H
