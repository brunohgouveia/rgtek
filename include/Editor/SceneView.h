//
// Created by Bruno Gouveia on 12/3/15.
//

#ifndef RGTEK_SCENEVIEW_H
#define RGTEK_SCENEVIEW_H

#include <Core/Platform.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(Camera);
    FWD_CLASS_EXTEND_PTR_TYPES(SceneView);

    class SceneView: public Object
    {
    public:
        SceneView();
        ~SceneView();

        inline CameraPtr GetCamera() const { return mCamera; }

        void SetCamera(const CameraPtr& camera);

    private:
        CameraPtr           mCamera;
    };

}

#endif //RGTEK_SCENEVIEW_H
