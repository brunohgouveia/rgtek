//
// Created by Bruno Gouveia on 11/22/15.
//

#pragma once

#include <Core/Platform.h>
#include <Core/Viewport.h>
#include <Core/Window.h>
#include <string>
#include <mutex>
#include <Math/Vector2.h>

namespace RGTek
{
    using namespace Math;
    using namespace std;

    FWD_CLASS_EXTEND_PTR_TYPES(GameWindow);

    class GameWindow: public Window
    {
    public:
        typedef std::function<void(Vector2,Vector2)> DrawCallbackFunction;

        GameWindow(const string& title, int width, int height);
        GameWindow(const GameWindow& other) = delete;
        virtual ~GameWindow();

        virtual void Render();
        virtual void HandleEvents();

        void operator=(const GameWindow& other) = delete;

    protected:
        void InitCEGUI();

        friend class Application;
    };

}
