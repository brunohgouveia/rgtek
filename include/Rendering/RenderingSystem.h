#ifndef RENDERINGSystem_H__
#define RENDERINGSystem_H__

#include <Core/Platform.h>
#include <Core/System.h>
#include <Core/Entity.h>
#include <Core/Window.h>
#include <Core/Viewport.h>
#include <Resources/ResourceManager.h>
#include <Resources/Shader.h>
#include <Rendering/RenderingBackend.h>
#include <GL/Texture.h>
#include <Core/Camera.h>
#include <Core/Light.h>
#include <Rendering/Components/MeshRenderer.h>
#include <Rendering/Components/LightComponent.h>
#include <Rendering/Components/CameraComponent.h>

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(RenderingSystem);
	FWD_CLASS_EXTEND_PTR_TYPES(Window);
	class DataBin;

	class RenderingSystem: public System
	{
	public:
        RSYSTEM(RenderingSystem)

        RenderingSystem(Window* window);
		virtual ~RenderingSystem();

        void Render(const Viewport& viewport);
		void Render(const CameraPtr& camera, const Viewport& viewport);

        void UpdateUniformsToShader(ShaderWeakPtr shader);

		// TODO - delete this method
		inline void AddLight(BaseLightPtr light) {}

        void RegisterToWindow(WindowPtr window);

        inline TextureHandle GetShadowTexture() { return mShadowTexture; }

        MeshRendererWeakPtr CreateMeshRenderer(MeshHandle mesh);

        CameraComponent* CreateCameraComponent();
        LightComponent* CreateLightComponent(const Color& color, float intensity = 1.0);
		Camera* GetCurrentCamera() const;

        ResourceManager& GetResourceManager();
        RenderingBackend& GetBackend();
        Window* GetWindow() const { return mWindow; }

		Component* CreateComponentByName(const String& componentClass) override;
        void DeleteComponent(Component* component) override;
        void DeleteComponentsOfEntity(Entity* entity) override;

		inline void SetCurrentCamera(const CameraPtr& camera) { mCurrentCamera = camera; }
    private:
        friend class Application;

        friend class EditorSystem;

        void RenderComponents();
        void RenderComponents(ShaderHandle shaderHandle);

		Window*            mWindow;
		ResourceManager    mResourceManager;
		RenderingBackend   mRenderingBackend;

        ShaderHandle       mBasicShader;

        BaseLightPtr       mActiveLight;

        ShaderHandle       mShadowMapShader;
        TextureHandle      mShadowTexture;

		CameraPtr          mCurrentCamera;

		Vector<MeshRendererPtr>    mMeshRendererComponents;
        Vector<CameraComponentPtr> mCameraComponents;
        Vector<LightComponentPtr>  mLightCompeonts;
	};

}

#endif // RENDERINGSystem_H__
