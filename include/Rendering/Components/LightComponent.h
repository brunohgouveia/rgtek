#pragma once

#include <Core/Platform.h>
#include <Core/Component.h>
#include <Core/Light.h>
#include <Math/Vector4.h>
#include <Util/Parser/DataTranslator.h>

namespace RGTek
{
	using namespace Math;

	FWD_CLASS_EXTEND_PTR_TYPES(LightComponent);

	class LightComponent : public Component
	{
	public:
		RCOMPONENT(LightComponent);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		RGTEK_REGISTER_TYPE(LightComponent,
			RGTEK_FIELD("Color", &LightComponent::GetColor, &LightComponent::SetColor)
		)

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Constructor
		* 	@param color - Light's color
		*  @param intensity - Light's intensity
		*/
		explicit LightComponent(const Color& color, float intensity = 1.0f);

		/** @brief Destructor
		*/
		virtual ~LightComponent();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Render the light component Gizmo in the editor.
		*/
		void RenderGizmos() override;

		/** @brief Register the light component to the system
		*         (Rendergin System).
		*/
		void RegisterToSystem() override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Overrides Component's SetEntity method.
		*/
		void SetEntity(Entity* entity) override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Set the light's intensity.
		*/
		void SetIntensity(float intensity) { mLight->SetIntensity(intensity); }

		/** @brief Set the light's color.
		*/
		void SetColor(const Color& color) { mLight->SetColor(color); }

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Get the BaseLight object address.
		*  @return Returns a pointer to the BaseLight object associated
		*          with this component.
		*/
		BaseLight*   GetLight()     const { return mLight; }
		Vector4      GetPosition()  const { return Vector4(mLight->GetTransform()->GetPosition(), 1.0f); }

		/** @brief Get light's color
		*  @return Returns the light's color.
		*/
		const Color& GetColor() const { return mLight->GetColor(); }

		/** @brief Get light's intensity.
		*  @return Returns the light's intensity.
		*/
		float GetIntensity() const { return mLight->GetIntensity(); }

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

		/** @brief Overrides GetDataTranslator method.
		*/
		DataTranslator* GetDataTranslator() override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	private:
		BaseLightPtr mLight;
	};

}