//
// Created by Bruno Gouveia on 12/11/15.
//

#pragma once

#include <Core/Platform.h>
#include <Math/Line.h>
#include <Math/Circle.h>
#include <Core/Ray.h>
#include <Resources/Shader.h>
#include <GL/Buffers/ArrayBuffer.h>
#include <GL/Color.h>

namespace RGTek
{
    using namespace Math;
    using namespace GL;

    FWD_CLASS_EXTEND_PTR_TYPES(RenderingSystem);
    FWD_CLASS_EXTEND_PTR_TYPES(RenderingBackend);

    class RenderingBackend: public Object
    {
    public:
        RenderingBackend(RenderingSystemWeakPtr renderingSystem);

        void DrawLine(const Line& line, const Color& color = Color::blue);
        void DrawRay(const Ray& ray, const Color& color = Color::blue);
        void DrawWireArc(const Vector3& center, const Vector3& normal, const Vector3& direction, const Color& color, float angle, float radius);
        void DrawWireDisc(const Vector3& center, const Vector3& normal, const Color& color, float radius, bool culling = false);
        void DrawClampedWireDisc(const Circle& circle,const Vector3& center, const Vector3& normal, const Color& color, float radius, bool culling = false);

        void Dump();

    private:
        RenderingSystemWeakPtr mRenderingSystem;
        ShaderHandle           mWireFrameShader;
        GLuint                 mLineVertexArray;
        GL::ArrayBuffer<float> mLineVertexBuffer;

        Map<Color, Vector<Line> > mLinesMap;
    };

}