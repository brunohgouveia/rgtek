//
// Created by Bruno Gouveia on 1/6/16.
//

#pragma once

#ifdef _MSC_VER
    #pragma warning( push )
	#pragma warning( disable : 4061 )
	#pragma warning( disable : 4365 )
	#pragma warning( disable : 4512 )
	#pragma warning( disable : 4514 )
	#pragma warning( disable : 4820 )
	#pragma warning( disable : 4265 )
	#pragma warning( disable : 4625 )
	#pragma warning( disable : 4626 )
#elif __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wnon-virtual-dtor"
    #pragma clang diagnostic ignored "-Wcovered-switch-default"
#endif

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#ifdef _MSC_VER
    #pragma warning( pop )
#elif __clang__
    #pragma clang diagnostic pop
#endif



