#pragma once

#include <Core/Transform.h>

#include <btBulletDynamicsCommon.h>

namespace RGTek
{

    ATTRIBUTE_ALIGNED16(struct) MotionState: public btMotionState
    {
    public:
        BT_DECLARE_ALIGNED_ALLOCATOR();

        MotionState() : mTransform(nullptr) {}
        MotionState(TransformWeakPtr transform) : mTransform(transform) {}
        virtual ~MotionState() {}

        ///synchronizes world transform from user to physics
        virtual void getWorldTransform(btTransform& centerOfMassWorldTrans ) const
        {
            if (mTransform)
            {
                centerOfMassWorldTrans = static_cast<btTransform>(*mTransform) * mInverseCenterOfMassOffset.inverse();
            }
        }

        ///synchronizes world transform from physics to user
        ///Bullet only calls the update of worldtransform for active objects
        virtual void setWorldTransform(const btTransform& centerOfMassWorldTrans)
        {
            if (mTransform)
            {
                *mTransform = centerOfMassWorldTrans * mInverseCenterOfMassOffset;
            }
        }

        void SetTransform(TransformWeakPtr transform) { mTransform = transform; }
        void SetCenterOfMassTransformOffset(const Transform& transform)
        {
            mInverseCenterOfMassOffset = static_cast<btTransform>(transform).inverse();
        }

        TransformWeakPtr GetTransform() const { return mTransform; }
        Transform GetCenterOfMassOffset() const
        {
            return Transform(mInverseCenterOfMassOffset.inverse());
        }

    private:
        TransformWeakPtr mTransform;

        // For performance reasons we save the Inverse transform.
        // This is due to the fact that the method 'setWorldTransform' should
        // be called more often thatn 'getWorldTransform'.
        btTransform      mInverseCenterOfMassOffset;
    };

}
