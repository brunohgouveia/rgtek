#ifndef RGTEK_RACASTHIT_H__
#define RGTEK_RACASTHIT_H__

#include <Core/Platform.h>
#include <Math/Vector3.h>
#include <Editor/Handle3D.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(Entity);

    class RaycastHit: public Object
    {
    public:
        RaycastHit();
        virtual ~RaycastHit();

        float             mDistance;
        Vector3           mHitPoint;
        Entity*           mEntity;
        Handle3D::Axis    mSelectedAxis;
    };

} /* RGTek */

#endif
