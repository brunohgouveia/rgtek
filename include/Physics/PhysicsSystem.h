#ifndef RGTEK_PHYSICSSystem_H__
#define RGTEK_PHYSICSSystem_H__

#include <Core/Platform.h>
#include <Core/System.h>
#include <Core/Ray.h>
#include <Physics/BVH.h>
#include <Physics/RaycastHit.h>
#include <GL/Buffers/ArrayBuffer.h>
#include <Physics/Components/Rigidbody.h>
#include <Physics/Components/BoxCollider.h>
#include <Physics/Components/SphereCollider.h>

#include <Editor/Handle3D.h>

#include <btBulletDynamicsCommon.h>


namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(PhysicsSystem);

    class PhysicsSystem: public System
    {
    public:
        enum Layer
        {
            kRigidbody = -1,
            kGizmo = 0
        };

        RSYSTEM(PhysicsSystem)

        PhysicsSystem();
        virtual ~PhysicsSystem();

        virtual void Pause() override;
        virtual void Resume() override;

        void RenderDebugInfo();
        void StepSimulation();

        bool Raycast(const Ray& ray, RaycastHit& rayInfo, Layer layer = Layer::kRigidbody);

        void AddRigidbody(const RigidbodyWeakPtr& rigidbody);
        void AddCollider(const ColliderPtr& collider);

        void DeleteRigidbody(Rigidbody* rigidbody);

        RigidbodyWeakPtr CreateRigidbody(float mass = 1.0f);
        BoxColliderWeakPtr CreateBoxCollider(const Vector4& position, const Vector4& size);
        SphereColliderWeakPtr CreateSphereCollider(const Vector4& position, const float radius);

        virtual Component* CreateComponentByName(const String& className) override;
        virtual void DeleteComponent(Component* component) override;
        virtual void DeleteComponentsOfEntity(Entity* entity) override;

    private:
        friend class EditorSystem;


        class LayeredClosestRayResultCallback : public btCollisionWorld::ClosestRayResultCallback
        {
        public:
            LayeredClosestRayResultCallback(const btVector3& rayFromWorld, const btVector3& rayToWorld, Layer layer = Layer::kRigidbody)
                : btCollisionWorld::ClosestRayResultCallback(rayFromWorld, rayToWorld)
                , mLayer(layer)
            {
            }

            virtual	btScalar addSingleResult(btCollisionWorld::LocalRayResult& rayResult, bool normalInWorldSpace)
            {
                if (mLayer != rayResult.m_collisionObject->getUserIndex()) return m_closestHitFraction;

                //caller already does the filter on the m_closestHitFraction
                btAssert(rayResult.m_hitFraction <= m_closestHitFraction);

                m_closestHitFraction = rayResult.m_hitFraction;
                m_collisionObject = rayResult.m_collisionObject;
                if (normalInWorldSpace)
                {
                    m_hitNormalWorld = rayResult.m_hitNormalLocal;
                } else
                {
                    ///need to transform normal into worldspace
                    m_hitNormalWorld = m_collisionObject->getWorldTransform().getBasis()*rayResult.m_hitNormalLocal;
                }
                m_hitPointWorld.setInterpolate3(m_rayFromWorld,m_rayToWorld,rayResult.m_hitFraction);
                return rayResult.m_hitFraction;
            }

        private:
             int mLayer;
        };

		void AddCollisionObject(btCollisionObject* collisionObject) { mDynamicsWorld->addCollisionObject(collisionObject); }
        void RemoveCollisionObject(btCollisionObject* collisionObject) { mDynamicsWorld->removeCollisionObject(collisionObject); }


        void RenderBVH(BVHNode* node, int level, int currentLevel);

        BVH mBvh;
        Ray ray;


		// Bullet attributes
        btDefaultCollisionConfiguration*     mCollisionConfiguration;
        btCollisionDispatcher*               mDispatcher;
        btBroadphaseInterface*               mOverlappingPairCache;
        btSequentialImpulseConstraintSolver* mSolver;
        btDiscreteDynamicsWorld*             mDynamicsWorld;

//        Vector<RigidbodyWeakPtr>             mRigidbodyComponents;
        Vector3                              mGravity;

        btIDebugDraw*                        mDebugDraw;


        Vector<RigidbodyPtr>                 mRigidbodyComponents;
        Vector<BoxColliderPtr>               mBoxColliderComponents;
        Vector<SphereColliderPtr>            mSphereColliderComponents;

        // Used to keep track of pairs that are currently colliding
        using KeyType = std::pair<const btCollisionObject*, const btCollisionObject*>;
        std::map<KeyType, bool> pairsColliding;
    };

}

#endif // RGTEK_PHYSICSSystem_H__
