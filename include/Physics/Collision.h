#ifndef RGTEK_COLLISION_H
#define RGTEK_COLLISION_H

#include <Core/Platform.h>
#include <Core/Entity.h>
#include <Physics/Components/Rigidbody.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(Collision);

    class Collision: public Object
    {
    public:
        Collision(const Entity* entity, const Rigidbody* rigidbody, const Vector<Vector3>* contacts);
        Collision(const Collision&) = delete;
        ~Collision () {}

        const Entity* entity() const { return mEntity; }
        const Rigidbody*  rigidbody() const { return mRigidbody;  }
        const Vector<Vector3>*   contacts() const { return mContacts;   }

        Collision& operator=(const Collision&) = delete;

    private:
        const Entity*          mEntity;
        const Rigidbody*       mRigidbody;
        const Vector<Vector3>* mContacts;
    };

} /* RGTek */

#endif
