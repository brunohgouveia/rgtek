#ifndef RGTEK_BVHss_H__
#define RGTEK_BVHss_H__

#include <Core/Platform.h>
#include <Core/Entity.h>
#include <Physics/Components/Collider.h>
#include <Physics/AABB.h>

namespace RGTek
{
    class BVHNode
    {
    public:
        BVHNode ();
        BVHNode (BVHNode* parent);
        virtual ~BVHNode ();

        void InsertCollider(ColliderPtr collider);
        void Split();

        AABB      mBoundingBox;
        BVHNode*  mParent;
        BVHNode*  m_leftChild;
        BVHNode*  m_rightChild;

        bool      m_valid;
        bool      m_leaf;

        Vector<ColliderPtr> m_colliders;
    };

    FWD_CLASS_EXTEND_PTR_TYPES(BVH);

    class BVH: public Object
    {
    public:
        BVH();
        virtual ~BVH();

        void Insert(ColliderPtr);
        void Remove(ColliderPtr);
        void Update(ColliderPtr);

        inline BVHNode* GetRoot() { return mRoot; }

    protected:
        void Insert(ColliderPtr collider, BVHNode*& node);

        BVHNode* mRoot;
        int      m_maxCollidersPerBox;
    };

}

#endif // RGTEK_BVH_H__
