#ifndef RGTEK_AABB_H
#define RGTEK_AABB_H

#include <Math/Vector3.h>
#include <Core/Ray.h>
#include <Physics/RaycastHit.h>

namespace RGTek
{
    using namespace Math;

    class AABB
    {
    public:
        AABB ();
        AABB(const Vector3& min, const Vector3& max);
        virtual ~AABB () {}

        void Inflate(const AABB& box);
        void Inflate(const Vector3& min, const Vector3& max);

        void Fit(const AABB& box, float fatness = 0.05);
        void Fit(const Vector3& min, const Vector3& max, float fatness = 0.05);

        bool Intersect(const Ray& ray, RaycastHit& rayInfo) const;

        float GetVolume() const;

        Vector3 mMin;
        Vector3 mMax;

    };

} /* RGTek */

#endif
