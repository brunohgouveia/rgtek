#pragma once

#include <Core/Platform.h>
#include <Core/Component.h>
#include <Physics/AABB.h>

class btCollisionShape;

namespace RGTek
{

    FWD_CLASS_EXTEND_PTR_TYPES(Collider);

    class Collider: public Component
    {
    public:
        Collider();
        Collider(const Collider& collider) = delete;
        virtual ~Collider();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    void Update() override = 0;
	    void RegisterToSystem() override = 0;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    void SetEntity(Entity* entity) override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        virtual bool Intersect(const Ray& ray, RaycastHit& rayInfo) const = 0;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        const Vector3&    GetRelativePosition()  const;
        const Quaternion& GetRelativeRotation()  const;
        const Transform&  GetRelativeTransform() const;
        btCollisionShape* GetBtCollisionShape()  const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        void SetRelativePosition(const Vector3& position);
        void SetRelativeRotation(const Quaternion& rotation);
        void SetRelativeTransform(const Transform& transform);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        virtual AABB GetAABB() const = 0;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        const Collider& operator=(const Collider& collider) = delete;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

    protected:
        btCollisionShape* mBtCollistionShape;
        Transform         mRelativeTransform;
    };

}
