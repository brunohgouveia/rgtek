#pragma once

#include <Core/Platform.h>
#include <Core/Transform.h>
#include <Core/Ray.h>
#include <Physics/RaycastHit.h>
#include <Physics/Components/Collider.h>

namespace RGTek
{
    using namespace Math;

    FWD_CLASS_EXTEND_PTR_TYPES(BoxCollider);

    class BoxCollider: public Collider
    {
    public:
		RCOMPONENT(BoxCollider);

		RGTEK_REGISTER_TYPE(BoxCollider,
			RGTEK_FIELD("Position", &BoxCollider::GetRelativePosition, &BoxCollider::SetRelativePosition)
			RGTEK_FIELD("Rotation", &BoxCollider::GetRelativeRotation, &BoxCollider::SetRelativeRotation)
			RGTEK_FIELD("Size", &BoxCollider::GetSize, &BoxCollider::SetSize)
		)

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        BoxCollider();
	    explicit BoxCollider(const Vector4& size);
        explicit BoxCollider(const Vector4& position, const Vector4& size);
        virtual ~BoxCollider();

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    void Update() override;
	    void RegisterToSystem() override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    bool Intersect(const Ray& ray, RaycastHit& rayInfo) const override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    AABB GetAABB() const override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        const Vector3& GetSize() const;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

        void SetSize(const Vector3& size);

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

	    DataTranslator* GetDataTranslator() override;

		///////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////

    protected:
        // This is not relative to the game object
        AABB    mBoundingBox;
    };

}
