#pragma once

#include <Core/Platform.h>
#include <Core/Scene.h>
#include <Physics/PhysicsSystem.h>
#include <Rendering/GameWindow.h>
#include <ScriptSystem/ScriptSystem.h>
#include <Editor/EditorWindow.h>
#include <Editor/EditorSystem.h>

namespace RGTek
{
    FWD_CLASS_EXTEND_PTR_TYPES(ResourceManager);
    FWD_CLASS_EXTEND_PTR_TYPES(Application);

    class Application : public Object
    {
    public:
        Application(WindowPtr window, ScenePtr scene);
        virtual ~Application();

        void Start();
        void Stop();
        void Run() { Start(); }

        inline bool IsRunning() { return mRunning; }

        static ApplicationWeakPtr GetSharedApplication(){ return sSharedApplication; }

        void AddCustomSystem(SystemPtr system, int frequency);

        inline RenderingSystemPtr GetRenderingSystem() { return mRenderingSystem;  }
        inline PhysicsSystemPtr   GetPhysicsSystem()   { return mPhysicsSystem;    }
        inline ScriptSystemPtr    GetScriptSystem()    { return mScriptSystem;     }
        inline EditorSystemPtr    GetEditorSystem()    { return mEditorSystem;     }
        inline ScenePtr           GetScene()           { return mScene;            }


        inline Window*    GetWindow()          { return mWindow;     }

        inline void SetRenderingSystem(RenderingSystemPtr renderingSystem) { mRenderingSystem = renderingSystem; if (mScene) mScene->AddSystem(renderingSystem); }
        inline void SetPhysicsSystem(PhysicsSystemPtr physicsSystem)       { mPhysicsSystem = physicsSystem;     if (mScene) mScene->AddSystem(physicsSystem);   }
        inline void SetScriptSystem(ScriptSystemPtr scriptSystem)          { mScriptSystem = scriptSystem;       if (mScene) mScene->AddSystem(scriptSystem);    }
        inline void SetEditorSystem(EditorSystemPtr editorSystem)          { mEditorSystem = editorSystem;       }

    private:
        WindowPtr          mWindow;
        RenderingSystemPtr mRenderingSystem;
        PhysicsSystemPtr   mPhysicsSystem;
        ScriptSystemPtr    mScriptSystem;
        EditorSystemPtr    mEditorSystem;
        ScenePtr           mScene;
        bool               mRunning;

        static ApplicationWeakPtr sSharedApplication;
    };

}